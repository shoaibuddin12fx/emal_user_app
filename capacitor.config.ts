import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.bidrider.customer',
  appName: 'RideBidder',
  webDir: 'www',
  bundledWebRuntime: false,
  plugins: {
    DatePickerPlugin: {
      "mode": "date",
      "locale": "en_US",      
    }
  }
};

export default config;
