(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_receipt_receipt_module_ts"],{

/***/ 72669:
/*!*********************************************************!*\
  !*** ./src/app/pages/receipt/receipt-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReceiptPageRoutingModule": () => (/* binding */ ReceiptPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _receipt_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./receipt.page */ 59418);




const routes = [
    {
        path: '',
        component: _receipt_page__WEBPACK_IMPORTED_MODULE_0__.ReceiptPage
    }
];
let ReceiptPageRoutingModule = class ReceiptPageRoutingModule {
};
ReceiptPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ReceiptPageRoutingModule);



/***/ }),

/***/ 27583:
/*!*************************************************!*\
  !*** ./src/app/pages/receipt/receipt.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReceiptPageModule": () => (/* binding */ ReceiptPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _receipt_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./receipt-routing.module */ 72669);
/* harmony import */ var _receipt_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./receipt.page */ 59418);







let ReceiptPageModule = class ReceiptPageModule {
};
ReceiptPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _receipt_routing_module__WEBPACK_IMPORTED_MODULE_0__.ReceiptPageRoutingModule
        ],
        declarations: [_receipt_page__WEBPACK_IMPORTED_MODULE_1__.ReceiptPage]
    })
], ReceiptPageModule);



/***/ }),

/***/ 59418:
/*!***********************************************!*\
  !*** ./src/app/pages/receipt/receipt.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReceiptPage": () => (/* binding */ ReceiptPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_receipt_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./receipt.page.html */ 27535);
/* harmony import */ var _receipt_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./receipt.page.scss */ 11724);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);





let ReceiptPage = class ReceiptPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad ReceiptPage');
    }
    postReview() {
        this.nav.setRoot('pages/reviewdriver');
    }
    ngOnInit() { }
};
ReceiptPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
ReceiptPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-receipt',
        template: _raw_loader_receipt_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_receipt_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ReceiptPage);



/***/ }),

/***/ 11724:
/*!*************************************************!*\
  !*** ./src/app/pages/receipt/receipt.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".receiptpagellogo {\n  display: block;\n  margin: 0 auto;\n  margin-top: 15px;\n}\n\n.booking {\n  text-align: center;\n  font-family: \"PT Sans\";\n  color: #2b2b2b;\n  font-weight: bold;\n  font-size: 18px;\n  margin: 0;\n  padding-top: 10px;\n}\n\n.booking span {\n  color: #222;\n}\n\n.receiptdiv {\n  width: 95%;\n  position: relative;\n  margin: 38px auto;\n  height: 0.2%;\n  background: #222;\n}\n\n.receiptdiv h2 {\n  width: 48%;\n  display: block;\n  background: #fff;\n  color: #2b2b2b;\n  font-family: \"PT Sans\";\n  font-size: 47px;\n  position: absolute;\n  top: -28px;\n  left: 0;\n  right: 0;\n  margin: auto;\n  font-weight: bold;\n  text-align: center;\n}\n\n.ashrectangle-receipt, .ashrectangle-fare {\n  width: 90%;\n  margin: auto;\n  margin-top: 10px;\n  background: #f4f4f4;\n  border-radius: 10px;\n  border: 1px solid #d4d4d4;\n  padding: 12px;\n}\n\n.pick {\n  text-align: left;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #a0a0a0;\n  padding-bottom: 18px;\n  margin: 0;\n}\n\n.address {\n  font-family: \"PT Sans\";\n  font-size: 20px;\n  color: #2e2e2e;\n  text-align: center;\n  margin: 0;\n  padding-bottom: 30px;\n  font-weight: bold;\n}\n\n.duration {\n  width: 90%;\n  margin: auto;\n  margin-bottom: 10px;\n  color: #9d9d9d;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n}\n\n.duration span {\n  color: #222;\n  font-weight: bold;\n}\n\n.righttext {\n  text-align: right;\n}\n\n.ashrectangle-fare {\n  text-align: center;\n  font-family: \"PT Sans\";\n  color: #2e2e2e;\n  font-weight: bold;\n  font-size: 47px;\n  margin-bottom: 20px;\n}\n\n@media (min-width: 568px) {\n  .receiptdiv h2 {\n    width: 30%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlY2VpcHQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBQUNGOztBQUNBO0VBQ0Usa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxTQUFBO0VBQ0EsaUJBQUE7QUFFRjs7QUFBQTtFQUNFLFdBQUE7QUFHRjs7QUFEQTtFQUNFLFVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0FBSUY7O0FBRkE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsWUFBQTtFQUNBLGlCQUFBO0VBQ0Esa0JBQUE7QUFLRjs7QUFIQTtFQUNFLFVBQUE7RUFDQSxZQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FBTUY7O0FBSkE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtFQUNBLFNBQUE7QUFPRjs7QUFMQTtFQUNFLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxvQkFBQTtFQUNBLGlCQUFBO0FBUUY7O0FBTkE7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0FBU0Y7O0FBUEE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7QUFVRjs7QUFSQTtFQUNFLGlCQUFBO0FBV0Y7O0FBVEE7RUFFRSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsY0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0FBV0Y7O0FBUkE7RUFDRTtJQUNFLFVBQUE7RUFXRjtBQUNGIiwiZmlsZSI6InJlY2VpcHQucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJlY2VpcHRwYWdlbGxvZ28ge1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuLmJvb2tpbmcge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgY29sb3I6ICMyYjJiMmI7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nLXRvcDogMTBweDtcclxufVxyXG4uYm9va2luZyBzcGFuIHtcclxuICBjb2xvcjogIzIyMjtcclxufVxyXG4ucmVjZWlwdGRpdiB7XHJcbiAgd2lkdGg6IDk1JTtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luOiAzOHB4IGF1dG87XHJcbiAgaGVpZ2h0OiAwLjIlO1xyXG4gIGJhY2tncm91bmQ6ICMyMjI7XHJcbn1cclxuLnJlY2VpcHRkaXYgaDIge1xyXG4gIHdpZHRoOiA0OCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBjb2xvcjogIzJiMmIyYjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiA0N3B4O1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IC0yOHB4O1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uYXNocmVjdGFuZ2xlLXJlY2VpcHQge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgYmFja2dyb3VuZDogI2Y0ZjRmNDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbiAgcGFkZGluZzogMTJweDtcclxufVxyXG4ucGljayB7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGNvbG9yOiAjYTBhMGEwO1xyXG4gIHBhZGRpbmctYm90dG9tOiAxOHB4O1xyXG4gIG1hcmdpbjogMDtcclxufVxyXG4uYWRkcmVzcyB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmctYm90dG9tOiAzMHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5kdXJhdGlvbiB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxuICBjb2xvcjogIzlkOWQ5ZDtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbn1cclxuLmR1cmF0aW9uIHNwYW4ge1xyXG4gIGNvbG9yOiAjMjIyO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5yaWdodHRleHQge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcbi5hc2hyZWN0YW5nbGUtZmFyZSB7XHJcbiAgQGV4dGVuZCAuYXNocmVjdGFuZ2xlLXJlY2VpcHQ7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBmb250LXNpemU6IDQ3cHg7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDU2OHB4KSB7XHJcbiAgLnJlY2VpcHRkaXYgaDIge1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 27535:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/receipt/receipt.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Receipt page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!--<ion-header>\r\n  <ion-toolbarbar color=\"headerblue\">\r\n    <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">RECEIPT</span></ion-title>\r\n  </ion-toolbarbar>\r\n</ion-header>-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n    <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-button>\r\n  </ion-buttons>\r\n    <ion-title><span class=\"heavy toptitle\">RECEIPT</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <img class=\"receiptpagellogo\" src=\"assets/images/supportlogo.png\">\r\n  <p class=\"booking\">BOOKING ID : <span>7002598</span></p>\r\n  <section class=\"receiptdiv\">\r\n    <h2>$80.00</h2>\r\n  </section>\r\n  <section class=\"ashrectangle-receipt\">\r\n    <p class=\"pick\">Pickup 05:02 PM</p>\r\n    <p class=\"address\">25, Haberfield New South Wales 2045</p>\r\n  </section>\r\n  <section class=\"ashrectangle-receipt\">\r\n    <p class=\"pick\">Drop Off 05:21 PM</p>\r\n    <p class=\"address\">10, Lorem Ipsum New South Wales 2045</p>\r\n  </section>\r\n  <section class=\"duration\">\r\n    <ion-grid>\r\n      <ion-row>\r\n        <ion-col class=\"no-padding\">Duration <span>00H:18M:44S</span></ion-col>\r\n        <ion-col class=\"righttext\">Distance <span>6.48 Km</span></ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </section>\r\n  <section class=\"ashrectangle-fare\">\r\n    $80.00\r\n  </section>\r\n  <button class=\"darkbutton\" (click)='postReview()' ion-button color=\"dark\" round>Review Driver</button>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_receipt_receipt_module_ts.js.map