(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_bookings_bookings_module_ts"],{

/***/ 96134:
/*!***********************************************************!*\
  !*** ./src/app/pages/bookings/bookings-routing.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingsPageRoutingModule": () => (/* binding */ BookingsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _bookings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookings.page */ 50867);




const routes = [
    {
        path: '',
        component: _bookings_page__WEBPACK_IMPORTED_MODULE_0__.BookingsPage
    }
];
let BookingsPageRoutingModule = class BookingsPageRoutingModule {
};
BookingsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], BookingsPageRoutingModule);



/***/ }),

/***/ 13582:
/*!***************************************************!*\
  !*** ./src/app/pages/bookings/bookings.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingsPageModule": () => (/* binding */ BookingsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _bookings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bookings-routing.module */ 96134);
/* harmony import */ var _bookings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookings.page */ 50867);







let BookingsPageModule = class BookingsPageModule {
};
BookingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _bookings_routing_module__WEBPACK_IMPORTED_MODULE_0__.BookingsPageRoutingModule
        ],
        declarations: [_bookings_page__WEBPACK_IMPORTED_MODULE_1__.BookingsPage]
    })
], BookingsPageModule);



/***/ }),

/***/ 50867:
/*!*************************************************!*\
  !*** ./src/app/pages/bookings/bookings.page.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BookingsPage": () => (/* binding */ BookingsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_bookings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./bookings.page.html */ 7006);
/* harmony import */ var _bookings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bookings.page.scss */ 94220);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);





let BookingsPage = class BookingsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.injector = injector;
        this.bookingList = [];
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad BookingsPage');
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            let ref = this.getDb()
                .ref('/users/' + this.curUser.uid)
                .orderByChild('tripdate:');
            ref.once('value', (snapshot) => {
                if (snapshot.val() != null) {
                    this.bookingList = [];
                    this.zone.run(() => {
                        console.log(snapshot.val());
                        this.userProfile = snapshot.val();
                        for (let key in this.userProfile.bookings) {
                            let bs = this.userProfile.bookings[key].status;
                            this.userProfile.bookings[key].uid = key;
                            if (bs == 'ENDED' || bs == 'CANCELLED') {
                                if (this.userProfile.bookings[key].driver == null) {
                                    this.userProfile.bookings[key].image_url =
                                        'assets/images/notfound.png';
                                }
                                else {
                                    this.userProfile.bookings[key].image_url =
                                        this.userProfile.bookings[key].driver_image;
                                }
                                this.bookingList.push(this.userProfile.bookings[key]);
                            }
                        }
                        this.bookingList.sort(function (a, b) {
                            let aa = Number(new Date(a.tripdate));
                            let bb = Number(new Date(b.tripdate));
                            console.log(bb - aa);
                            return bb - aa;
                        });
                    });
                }
            });
        });
    }
    getDb() {
        return this.firebaseService.getDatabase();
    }
};
BookingsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
BookingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-bookings',
        template: _raw_loader_bookings_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_bookings_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], BookingsPage);



/***/ }),

/***/ 94220:
/*!***************************************************!*\
  !*** ./src/app/pages/bookings/bookings.page.scss ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".bookings {\n  width: 95%;\n  margin: 0 auto;\n}\n\n.bookings img {\n  width: 100%;\n  display: block;\n  margin: 0 auto;\n  border-radius: 50%;\n}\n\n.list-ios .item-block .item-inner {\n  padding-right: 0;\n}\n\n.item-md.item-block .item-inner {\n  padding-right: 0;\n}\n\n.bookings h4 {\n  font-family: \"PT Sans\";\n  font-size: 18px;\n  text-align: left;\n  color: #222;\n  padding-bottom: 3px;\n  margin: 0;\n}\n\n.bookings p {\n  margin: 0;\n  font-family: \"PT Sans\";\n  font-size: 13px;\n  text-align: left;\n  color: #222;\n  padding-bottom: 2px;\n}\n\n.bookings p span {\n  font-weight: bold;\n}\n\n.bookings h3 {\n  text-align: right;\n  font-family: \"PT Sans\";\n  font-size: 24px;\n  color: #2e2e2e;\n  margin: 0;\n  font-weight: bold;\n}\n\n.no-padding {\n  padding: 0 !important;\n}\n\n.topTitle {\n  width: 85%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJvb2tpbmdzLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxXQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtBQUVGOztBQUFBO0VBQ0UsZ0JBQUE7QUFHRjs7QUFEQTtFQUNFLGdCQUFBO0FBSUY7O0FBRkE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFNBQUE7QUFLRjs7QUFIQTtFQUNFLFNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtBQU1GOztBQUpBO0VBQ0UsaUJBQUE7QUFPRjs7QUFKQTtFQUNFLGlCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQU9GOztBQUxBO0VBQ0UscUJBQUE7QUFRRjs7QUFOQTtFQUNFLFVBQUE7QUFTRiIsImZpbGUiOiJib29raW5ncy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9va2luZ3Mge1xyXG4gIHdpZHRoOiA5NSU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbn1cclxuLmJvb2tpbmdzIGltZyB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgYm9yZGVyLXJhZGl1czogNTAlO1xyXG59XHJcbi5saXN0LWlvcyAuaXRlbS1ibG9jayAuaXRlbS1pbm5lciB7XHJcbiAgcGFkZGluZy1yaWdodDogMDtcclxufVxyXG4uaXRlbS1tZC5pdGVtLWJsb2NrIC5pdGVtLWlubmVyIHtcclxuICBwYWRkaW5nLXJpZ2h0OiAwO1xyXG59XHJcbi5ib29raW5ncyBoNCB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGNvbG9yOiAjMjIyO1xyXG4gIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcbi5ib29raW5ncyBwIHtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGNvbG9yOiAjMjIyO1xyXG4gIHBhZGRpbmctYm90dG9tOiAycHg7XHJcbn1cclxuLmJvb2tpbmdzIHAgc3BhbiB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuXHJcbi5ib29raW5ncyBoMyB7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMjRweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBtYXJnaW46IDA7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLm5vLXBhZGRpbmcge1xyXG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcclxufVxyXG4udG9wVGl0bGUge1xyXG4gIHdpZHRoOiA4NSU7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 7006:
/*!*****************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bookings/bookings.page.html ***!
  \*****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Bookings page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!--<ion-header>\r\n  <ion-toolbarbar color=\"headerblue\">\r\n      <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">PREVIOUS BOOKINGS</span></ion-title>\r\n  </ion-toolbarbar>\r\n</ion-header>-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n        <ion-icon name=\"menu\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <!--<ion-title><span class=\"heavy toptitle\">PREVIOUS BOOKINGS</span></ion-title>-->\r\n    <ion-title class=\"topTitle\"\r\n      ><span class=\"heavy\">PREVIOUS BOOKINGS</span></ion-title\r\n    >\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section class=\"bookings\">\r\n    <h2 *ngIf=\"bookingList.length==0\" class=\"review-title\">\r\n      No booking history.\r\n    </h2>\r\n    <ion-list>\r\n      <ion-item *ngFor=\"let booking of bookingList; let i = index;\">\r\n        <ion-avatar>\r\n          <img\r\n            src=\"{{booking.image_url}}\"\r\n            onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n            alt=\"Profile Image\"\r\n          />\r\n        </ion-avatar>\r\n\r\n        <ion-label class=\"ion-margin-start ion-text-wrap\">\r\n          <!--<h4><strong>{{booking.tripdate | date:'dd-MM-yyyy HH:mm' : 'UTC'}}</strong></h4>-->\r\n\r\n          <h4><strong>{{booking.serviceType}}</strong></h4>\r\n          <p><span>Start</span> : {{booking.pickupAddress}}</p>\r\n          <p *ngIf=\"booking.dropAddress\">\r\n            <span>End</span> : {{booking.dropAddress}}\r\n          </p>\r\n          <p>\r\n            <ion-icon\r\n              name=\"calendar-outline\"\r\n              style=\"margin-right: 5px\"\r\n            ></ion-icon>\r\n            <strong\r\n              >{{booking.tripdate.substring(8,10)}}{{booking.tripdate.substring(4,8)}}{{booking.tripdate.substring(0,4)}}\r\n              {{booking.tripdate.substring(11,16)}}</strong\r\n            >\r\n\r\n            <ion-text>\r\n              <h3>${{booking.trip_cost ? booking.trip_cost : \"0\"}}</h3>\r\n            </ion-text>\r\n          </p>\r\n        </ion-label>\r\n      </ion-item>\r\n    </ion-list>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_bookings_bookings_module_ts.js.map