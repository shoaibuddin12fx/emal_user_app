(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_reviewdriver_reviewdriver_module_ts"],{

/***/ 80001:
/*!*************************************************************************!*\
  !*** ./node_modules/ionic-rating/__ivy_ngcc__/fesm2015/ionic-rating.js ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "IonRatingComponent": () => (/* binding */ IonRatingComponent),
/* harmony export */   "IonicRatingModule": () => (/* binding */ IonicRatingModule)
/* harmony export */ });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 80476);





/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */




function IonRatingComponent_ion_button_0_Template(rf, ctx) { if (rf & 1) {
    const _r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "ion-button", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseenter", function IonRatingComponent_ion_button_0_Template_ion_button_mouseenter_0_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const i_r1 = restoredCtx.$implicit; const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r2.onMouseEnter(i_r1); })("click", function IonRatingComponent_ion_button_0_Template_ion_button_click_0_listener() { const restoredCtx = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r3); const i_r1 = restoredCtx.$implicit; const ctx_r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r4.onClick(i_r1); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "ion-icon", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const i_r1 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵclassProp"]("filled", i_r1 <= ctx_r0.hoverRate || !ctx_r0.hoverRate && i_r1 <= ctx_r0.rate)("readonly", ctx_r0.readonly);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("disabled", ctx_r0.disabled || ctx_r0.readonly);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("size", ctx_r0.size);
} }
const _c0 = function () { return [1, 2, 3, 4, 5]; };
class IonRatingComponent {
    /**
     * @param {?} cd
     */
    constructor(cd) {
        this.cd = cd;
        this.hover = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.leave = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
        this.rateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__.EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.rate) {
            this.update(this.rate);
        }
    }
    /**
     * @private
     * @param {?} value
     * @param {?=} internalChange
     * @return {?}
     */
    update(value, internalChange = true) {
        if (!(this.readonly || this.disabled || this.rate === value)) {
            this.rate = value;
            this.rateChange.emit(this.rate);
        }
        if (internalChange) {
            if (this.onChange) {
                this.onChange(this.rate);
            }
            if (this.onTouched) {
                this.onTouched();
            }
        }
    }
    /**
     * @param {?} rate
     * @return {?}
     */
    onClick(rate) {
        this.update(this.resettable && this.rate === rate ? 0 : rate);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onMouseEnter(value) {
        if (!(this.disabled || this.readonly)) {
            this.hoverRate = value;
        }
        this.hover.emit(value);
    }
    /**
     * @return {?}
     */
    onMouseLeave() {
        this.leave.emit(this.hoverRate);
        this.hoverRate = 0;
    }
    /**
     * @return {?}
     */
    onBlur() {
        if (this.onTouched) {
            this.onTouched();
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.update(value, false);
        this.cd.markForCheck();
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this.onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this.onTouched = fn;
    }
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    setDisabledState(isDisabled) {
        this.disabled = isDisabled;
    }
}
IonRatingComponent.ɵfac = function IonRatingComponent_Factory(t) { return new (t || IonRatingComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef)); };
IonRatingComponent.ɵcmp = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: IonRatingComponent, selectors: [["ion-rating"]], hostBindings: function IonRatingComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("mouseleave", function IonRatingComponent_mouseleave_HostBindingHandler() { return ctx.onMouseLeave(); })("blur", function IonRatingComponent_blur_HostBindingHandler() { return ctx.onBlur(); });
    } }, inputs: { rate: "rate", readonly: "readonly", resettable: "resettable", size: "size" }, outputs: { hover: "hover", leave: "leave", rateChange: "rateChange" }, features: [_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵProvidersFeature"]([
            {
                provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NG_VALUE_ACCESSOR,
                useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(( /**
                 * @return {?}
                 */() => IonRatingComponent)),
                multi: true
            }
        ]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵNgOnChangesFeature"]], decls: 1, vars: 2, consts: [["type", "button", "fill", "clear", 3, "disabled", "filled", "readonly", "mouseenter", "click", 4, "ngFor", "ngForOf"], ["type", "button", "fill", "clear", 3, "disabled", "mouseenter", "click"], ["slot", "icon-only", "name", "star", 3, "size"]], template: function IonRatingComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, IonRatingComponent_ion_button_0_Template, 2, 6, "ion-button", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpureFunction0"](1, _c0));
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.NgForOf, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonButton, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonIcon], styles: ["[_nghost-%COMP%]{--star-color:gray;--star-color-filled:orange;display:inline-block}ion-button[_ngcontent-%COMP%]{--padding-start:0;--padding-end:0;--color:var(--star-color);--color-focused:var(--star-color);--color-activated:var(--star-color)}ion-button.filled[_ngcontent-%COMP%]{--color:var(--star-color-filled);--color-focused:var(--star-color-filled);--color-activated:var(--star-color-filled)}ion-button.readonly[_ngcontent-%COMP%]{--opacity:1}"], changeDetection: 0 });
/** @nocollapse */
IonRatingComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef }
];
IonRatingComponent.propDecorators = {
    rate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    readonly: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    resettable: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    size: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input }],
    hover: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    leave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    rateChange: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output }],
    onMouseLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['mouseleave', [],] }],
    onBlur: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener, args: ['blur', [],] }]
};
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IonRatingComponent, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Component,
        args: [{
                selector: 'ion-rating',
                template: "<ion-button *ngFor=\"let i of [1, 2, 3, 4, 5]\" type=\"button\" fill=\"clear\" [disabled]=\"disabled || readonly\"\n  (mouseenter)=\"onMouseEnter(i)\" (click)=\"onClick(i)\" [class.filled]=\"i <= hoverRate || (!hoverRate && i <= rate)\"\n  [class.readonly]=\"readonly\">\n  <ion-icon slot=\"icon-only\" name=\"star\" [size]=\"size\">\n  </ion-icon>\n</ion-button>",
                changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectionStrategy.OnPush,
                providers: [
                    {
                        provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__.NG_VALUE_ACCESSOR,
                        useExisting: (0,_angular_core__WEBPACK_IMPORTED_MODULE_0__.forwardRef)(( /**
                         * @return {?}
                         */() => IonRatingComponent)),
                        multi: true
                    }
                ],
                styles: [":host{--star-color:gray;--star-color-filled:orange;display:inline-block}ion-button{--padding-start:0;--padding-end:0;--color:var(--star-color);--color-focused:var(--star-color);--color-activated:var(--star-color)}ion-button.filled{--color:var(--star-color-filled);--color-focused:var(--star-color-filled);--color-activated:var(--star-color-filled)}ion-button.readonly{--opacity:1}"]
            }]
    }], function () { return [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.ChangeDetectorRef }]; }, { hover: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], leave: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], rateChange: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Output
        }], rate: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], 
    /**
     * @return {?}
     */
    onMouseLeave: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener,
            args: ['mouseleave', []]
        }], 
    /**
     * @return {?}
     */
    onBlur: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.HostListener,
            args: ['blur', []]
        }], readonly: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], resettable: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }], size: [{
            type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.Input
        }] }); })();

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class IonicRatingModule {
}
IonicRatingModule.ɵfac = function IonicRatingModule_Factory(t) { return new (t || IonicRatingModule)(); };
IonicRatingModule.ɵmod = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineNgModule"]({ type: IonicRatingModule });
IonicRatingModule.ɵinj = /*@__PURE__*/ _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjector"]({ imports: [[_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonicModule]] });
(function () { (typeof ngDevMode === "undefined" || ngDevMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵsetClassMetadata"](IonicRatingModule, [{
        type: _angular_core__WEBPACK_IMPORTED_MODULE_0__.NgModule,
        args: [{
                imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonicModule],
                exports: [IonRatingComponent],
                declarations: [IonRatingComponent]
            }]
    }], null, null); })();
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsetNgModuleScope"](IonicRatingModule, { declarations: function () { return [IonRatingComponent]; }, imports: function () { return [_angular_common__WEBPACK_IMPORTED_MODULE_2__.CommonModule, _angular_forms__WEBPACK_IMPORTED_MODULE_1__.FormsModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.IonicModule]; }, exports: function () { return [IonRatingComponent]; } }); })();



//# sourceMappingURL=ionic-rating.js.map

/***/ }),

/***/ 22276:
/*!*******************************************************************!*\
  !*** ./src/app/pages/reviewdriver/reviewdriver-routing.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReviewdriverPageRoutingModule": () => (/* binding */ ReviewdriverPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _reviewdriver_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reviewdriver.page */ 59267);




const routes = [
    {
        path: '',
        component: _reviewdriver_page__WEBPACK_IMPORTED_MODULE_0__.ReviewdriverPage
    }
];
let ReviewdriverPageRoutingModule = class ReviewdriverPageRoutingModule {
};
ReviewdriverPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ReviewdriverPageRoutingModule);



/***/ }),

/***/ 5462:
/*!***********************************************************!*\
  !*** ./src/app/pages/reviewdriver/reviewdriver.module.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReviewdriverPageModule": () => (/* binding */ ReviewdriverPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var ionic_rating__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ionic-rating */ 80001);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _reviewdriver_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reviewdriver-routing.module */ 22276);
/* harmony import */ var _reviewdriver_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reviewdriver.page */ 59267);








let ReviewdriverPageModule = class ReviewdriverPageModule {
};
ReviewdriverPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _reviewdriver_routing_module__WEBPACK_IMPORTED_MODULE_0__.ReviewdriverPageRoutingModule,
            ionic_rating__WEBPACK_IMPORTED_MODULE_7__.IonicRatingModule
        ],
        declarations: [_reviewdriver_page__WEBPACK_IMPORTED_MODULE_1__.ReviewdriverPage]
    })
], ReviewdriverPageModule);



/***/ }),

/***/ 59267:
/*!*********************************************************!*\
  !*** ./src/app/pages/reviewdriver/reviewdriver.page.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ReviewdriverPage": () => (/* binding */ ReviewdriverPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_reviewdriver_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./reviewdriver.page.html */ 20341);
/* harmony import */ var _reviewdriver_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reviewdriver.page.scss */ 41557);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);





let ReviewdriverPage = class ReviewdriverPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        var _a;
        super(injector);
        this.favourite = false;
        this.notes = '';
        this.bookingKey = (_a = this.getQueryParams()) === null || _a === void 0 ? void 0 : _a.bookingKey;
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            let user = yield this.firebaseService.getCurrentUser();
            let bookingRef = this.getDbRef('/bookings/' + this.bookingKey);
            bookingRef.once('value', (snapshot) => {
                this.booking = snapshot.val();
                this.customerID = snapshot.val().customer;
                this.driverID = snapshot.val().driver;
                console.log(this.booking);
                this.booking.image_url = snapshot.val().driver_image;
            });
            this.refcard = this.getDbRef('/users/' + user.uid);
            this.refcard.on('value', (_snapshot) => {
                if (_snapshot.val()) {
                    this.stripeID = _snapshot.val().stripeID;
                }
            });
        });
    }
    reviewSubmit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            if (!this.rate) {
                var alert = yield this.alertCtrl.create({
                    subHeader: 'Please give rating',
                    buttons: ['Ok'],
                });
                alert.present();
                return;
            }
            if (!this.notes) {
                var alert = yield this.alertCtrl.create({
                    subHeader: 'Please enter comment or notes',
                    buttons: ['Ok'],
                });
                alert.present();
                return;
            }
            let user = yield this.firebaseService.getCurrentUser();
            this.userId = user.uid;
            let reviewData = {
                rating: this.rate,
                // favourite:this.favourite//,
                notes: this.notes,
            };
            let userRef = this.getDbRef('/users/' + this.driverID);
            let bookingRef = this.getDbRef('/bookings/' + this.bookingKey);
            let customerRef = this.getDbRef('/users/' + this.customerID + '/bookings/' + this.bookingKey);
            let driverRef = this.getDbRef('/users/' + this.driverID + '/bookings/' + this.bookingKey);
            userRef.child('ratings').push(this.rate);
            bookingRef.child('ratings').set(reviewData);
            customerRef.child('ratings').set(reviewData);
            driverRef.child('ratings').set(reviewData);
            bookingRef.child('status').set('ENDED');
            customerRef.child('status').set('ENDED');
            driverRef.child('status').set('ENDED');
            this.nav.setRoot('pages/currentbookings');
        });
    }
    paymentSubmit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.userId = (yield this.firebaseService.getCurrentUser())['uid'];
            /* ---added by hr  -----------*/
            var paymentbill = this.getDbRef('/bookings/' + this.bookingKey + '/trip_cost');
            paymentbill.once('value', (snapbill) => {
                let totalBill = snapbill.val();
                console.log('MY paypal bill.........', totalBill);
                var payRef = this.getDbRef('/users/' + this.customerID + '/payment_methods');
                payRef.once('value', (snapshot) => {
                    if (snapshot.val()) {
                        let cards = snapshot.val();
                        let cardID = '';
                        console.log('cards.........', cards);
                        let GotIt = false;
                        for (let i = 0; i < cards.length; i++) {
                            if (cards[i].primary) {
                                GotIt = true;
                                cardID = cards[i].stripeCardID;
                            }
                        }
                        if (GotIt) {
                            this.usersService
                                .stripePayment(cardID, this.stripeID, totalBill)
                                .subscribe((response) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
                                console.log('paypal payment response', response);
                                let temp;
                                temp = response;
                                if (temp.status == 'succeeded') {
                                    let alert = yield this.alertCtrl.create({
                                        header: 'Payment',
                                        subHeader: 'Payment succeeded',
                                        buttons: ['OK'],
                                    });
                                    alert.present();
                                }
                                else {
                                    let alert = yield this.alertCtrl.create({
                                        header: 'Payment',
                                        subHeader: temp.message,
                                        buttons: ['OK'],
                                    });
                                    alert.present();
                                    // this.usersService.sendPushDriver('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                                }
                            }), (err) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
                                let alert = yield this.alertCtrl.create({
                                    header: 'Payment',
                                    subHeader: 'Server Error ! Try again',
                                    buttons: ['OK'],
                                });
                                alert.present();
                            }));
                        }
                        else {
                            alert('Please select Primary card');
                            //  this.usersService.sendPushDriver('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                        }
                    }
                    else {
                        alert('Card Not Available');
                        // this.usersService.sendPush('Payment Failed !!',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                    }
                });
            });
            /*-------------------------------*/
        });
    }
    onRateChange(event) {
        console.log('onRateChange', event);
    }
    getDbRef(ref) {
        return this.firebaseService.getDatabase().ref(ref);
    }
};
ReviewdriverPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
ReviewdriverPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-reviewdriver',
        template: _raw_loader_reviewdriver_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_reviewdriver_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ReviewdriverPage);



/***/ }),

/***/ 41557:
/*!***********************************************************!*\
  !*** ./src/app/pages/reviewdriver/reviewdriver.page.scss ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".heavy {\n  font-weight: bold;\n}\n\n.bannerterm {\n  width: 100%;\n  padding-top: 180px;\n  background: url(\"/assets/images/term.png\") no-repeat;\n  position: relative;\n  background-size: cover;\n}\n\n.aboutlogo {\n  position: absolute;\n  display: block;\n  width: auto;\n  margin: auto;\n  top: 72%;\n  left: 0;\n  right: 0;\n}\n\n.headerabout {\n  color: #2e2e2e;\n  font-size: 18px;\n  text-align: center;\n  font-family: \"PT Sans\";\n  padding-top: 1%;\n  font-weight: bold;\n}\n\n.blueline {\n  display: block !important;\n  margin: 0 auto !important;\n  width: auto !important;\n  border-radius: 0 !important;\n}\n\n.aboutpara {\n  font-family: \"PT Sans\";\n  color: #919191;\n  font-size: 16px;\n  text-align: left;\n  line-height: 18px;\n  padding: 12px 20px 0 20px;\n  margin: 0px;\n}\n\n.aboutpara span {\n  color: #222;\n}\n\n.ashrectangle, .ashrectangle-review {\n  width: 90%;\n  margin: 12px auto;\n  background: #f4f4f4;\n  border-radius: 10px;\n  border: 1px solid #d4d4d4;\n  padding: 12px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 19px;\n  font-weight: bold;\n}\n\n.liststyle {\n  width: 90%;\n  margin: 0 auto;\n  margin-top: 15px;\n}\n\n.liststyle ul {\n  margin: 0px;\n  padding: 0px;\n}\n\n.liststyle ul li {\n  list-style: none;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 20px;\n  color: #919191;\n  padding-left: 9%;\n  background: url(\"/assets/images/carbullet.png\") no-repeat;\n  margin-bottom: 20px;\n}\n\n.font {\n  font-weight: bold;\n  color: #737373;\n}\n\n.acceptBtn {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n\n.btnaccept {\n  background: #09738d !important;\n  color: white;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n.btnaccept:hover {\n  background: white !important;\n  color: black;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n@media (min-width: 568px) {\n  .headerabout {\n    padding-top: 7%;\n    font-weight: bold;\n  }\n}\n\n.review-title {\n  font-family: \"PT Sans\";\n  font-size: 23px;\n  color: #2e2e2e;\n  text-align: center;\n  margin: 0;\n  padding-top: 3%;\n}\n\nion-rating {\n  --color: gray;\n  --color-filled: green;\n}\n\n.ashrectangle-review {\n  width: 90%;\n  padding-bottom: 0px;\n  margin-bottom: 5px;\n}\n\n.no-border {\n  border: none !important;\n}\n\n.checkboxwrapper {\n  width: 53%;\n  margin: 0 auto;\n  display: block;\n}\n\n.checkboxtext {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #2e2e2e;\n}\n\n.darkbutton {\n  border-radius: 10px;\n  padding: 10px 100px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.padding-back {\n  padding-right: 5px;\n}\n\n.reviewimage img {\n  padding-bottom: 10px !important;\n}\n\n.rate {\n  color: #ffae00;\n  font-size: 25px;\n  padding-bottom: 20px;\n  display: inline;\n  text-align: center;\n}\n\n.rate ul {\n  margin: 0 auto !important;\n  padding: 0;\n  display: block;\n  width: 100%;\n}\n\n@media screen and (orientation: landscape) {\n  .rate ul {\n    margin: 0 auto !important;\n    padding: 0;\n    display: block;\n    width: 100%;\n  }\n}\n\n@media (max-width: 320px) {\n  .checkboxwrapper {\n    width: 60%;\n  }\n}\n\n@media (min-width: 640px) {\n  .checkboxwrapper {\n    width: 30%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFx0ZXJtc1xcdGVybXMucGFnZS5zY3NzIiwicmV2aWV3ZHJpdmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxvREFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNFRjs7QURBQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FDR0Y7O0FEREE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNJRjs7QURGQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0FDS0Y7O0FESEE7RUFDRSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtBQ01GOztBREpBO0VBQ0UsV0FBQTtBQ09GOztBRExBO0VBQ0UsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNRRjs7QUROQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNTRjs7QURQQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDVUY7O0FEUkE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5REFBQTtFQUNBLG1CQUFBO0FDV0Y7O0FEVEE7RUFDRSxpQkFBQTtFQUNBLGNBQUE7QUNZRjs7QURWQTtFQUNFLGVBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7QUNhRjs7QURYQTtFQUNFLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ2NGOztBRFpBO0VBQ0UsNEJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDZUY7O0FEWkE7RUFDRTtJQUNFLGVBQUE7SUFDQSxpQkFBQTtFQ2VGO0FBQ0Y7O0FBN0hBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUErSEY7O0FBNUhBO0VBQ0UsYUFBQTtFQUNBLHFCQUFBO0FBK0hGOztBQTVIQTtFQUVFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBOEhGOztBQTNIQTtFQUNFLHVCQUFBO0FBOEhGOztBQTNIQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBQThIRjs7QUEzSEE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FBOEhGOztBQTNIQTtFQUNFLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0FBOEhGOztBQTNIQTtFQUNFLGtCQUFBO0FBOEhGOztBQTVIQTtFQUNFLCtCQUFBO0FBK0hGOztBQTdIQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUFnSUY7O0FBOUhBO0VBQ0UseUJBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUFpSUY7O0FBOUhBO0VBQ0U7SUFDRSx5QkFBQTtJQUNBLFVBQUE7SUFDQSxjQUFBO0lBQ0EsV0FBQTtFQWlJRjtBQUNGOztBQTlIQTtFQUNFO0lBQ0UsVUFBQTtFQWdJRjtBQUNGOztBQTdIQTtFQUNFO0lBQ0UsVUFBQTtFQStIRjtBQUNGIiwiZmlsZSI6InJldmlld2RyaXZlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaGVhdnkge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5iYW5uZXJ0ZXJtIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwYWRkaW5nLXRvcDogMTgwcHg7XHJcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdGVybS5wbmdcIikgbm8tcmVwZWF0O1xyXG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG59XHJcbi5hYm91dGxvZ28ge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogYXV0bztcclxuICBtYXJnaW46IGF1dG87XHJcbiAgdG9wOiA3MiU7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxufVxyXG4uaGVhZGVyYWJvdXQge1xyXG4gIGNvbG9yOiAjMmUyZTJlO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIHBhZGRpbmctdG9wOiAxJTtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4uYmx1ZWxpbmUge1xyXG4gIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcclxuICB3aWR0aDogYXV0byAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1yYWRpdXM6IDAgIWltcG9ydGFudDtcclxufVxyXG4uYWJvdXRwYXJhIHtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgY29sb3I6ICM5MTkxOTE7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XHJcbiAgcGFkZGluZzogMTJweCAyMHB4IDAgMjBweDtcclxuICBtYXJnaW46IDBweDtcclxufVxyXG4uYWJvdXRwYXJhIHNwYW4ge1xyXG4gIGNvbG9yOiAjMjIyO1xyXG59XHJcbi5hc2hyZWN0YW5nbGUge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAxMnB4IGF1dG87XHJcbiAgYmFja2dyb3VuZDogI2Y0ZjRmNDtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbiAgcGFkZGluZzogMTJweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLmxpc3RzdHlsZSB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBtYXJnaW4tdG9wOiAxNXB4O1xyXG59XHJcbi5saXN0c3R5bGUgdWwge1xyXG4gIG1hcmdpbjogMHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxufVxyXG4ubGlzdHN0eWxlIHVsIGxpIHtcclxuICBsaXN0LXN0eWxlOiBub25lO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBsaW5lLWhlaWdodDogMjBweDtcclxuICBjb2xvcjogIzkxOTE5MTtcclxuICBwYWRkaW5nLWxlZnQ6IDklO1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2NhcmJ1bGxldC5wbmdcIikgbm8tcmVwZWF0O1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuLmZvbnQge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGNvbG9yOiAjNzM3MzczO1xyXG59XHJcbi5hY2NlcHRCdG4ge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBib3R0b206IDA7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxufVxyXG4uYnRuYWNjZXB0e1xyXG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcclxuICBjb2xvcjogd2hpdGU7XHJcbiAgbWFyZ2luLWxlZnQ6IDE0MHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgcGFkZGluZzogMTBweCAxNXB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcbi5idG5hY2NlcHQ6aG92ZXJ7XHJcbiAgYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcclxuICBjb2xvcjogYmxhY2s7XHJcbiAgbWFyZ2luLWxlZnQ6IDE0MHB4O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgcGFkZGluZzogMTBweCAxNXB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNTY4cHgpIHtcclxuICAuaGVhZGVyYWJvdXQge1xyXG4gICAgcGFkZGluZy10b3A6IDclO1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgfVxyXG59XHJcbiIsIi5oZWF2eSB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYmFubmVydGVybSB7XG4gIHdpZHRoOiAxMDAlO1xuICBwYWRkaW5nLXRvcDogMTgwcHg7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3Rlcm0ucG5nXCIpIG5vLXJlcGVhdDtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xufVxuXG4uYWJvdXRsb2dvIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IGF1dG87XG4gIG1hcmdpbjogYXV0bztcbiAgdG9wOiA3MiU7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xufVxuXG4uaGVhZGVyYWJvdXQge1xuICBjb2xvcjogIzJlMmUyZTtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgcGFkZGluZy10b3A6IDElO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmJsdWVsaW5lIHtcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcbiAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xufVxuXG4uYWJvdXRwYXJhIHtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBjb2xvcjogIzkxOTE5MTtcbiAgZm9udC1zaXplOiAxNnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBsaW5lLWhlaWdodDogMThweDtcbiAgcGFkZGluZzogMTJweCAyMHB4IDAgMjBweDtcbiAgbWFyZ2luOiAwcHg7XG59XG5cbi5hYm91dHBhcmEgc3BhbiB7XG4gIGNvbG9yOiAjMjIyO1xufVxuXG4uYXNocmVjdGFuZ2xlLCAuYXNocmVjdGFuZ2xlLXJldmlldyB7XG4gIHdpZHRoOiA5MCU7XG4gIG1hcmdpbjogMTJweCBhdXRvO1xuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xuICBwYWRkaW5nOiAxMnB4O1xuICBjb2xvcjogIzJlMmUyZTtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmxpc3RzdHlsZSB7XG4gIHdpZHRoOiA5MCU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBtYXJnaW4tdG9wOiAxNXB4O1xufVxuXG4ubGlzdHN0eWxlIHVsIHtcbiAgbWFyZ2luOiAwcHg7XG4gIHBhZGRpbmc6IDBweDtcbn1cblxuLmxpc3RzdHlsZSB1bCBsaSB7XG4gIGxpc3Qtc3R5bGU6IG5vbmU7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBsaW5lLWhlaWdodDogMjBweDtcbiAgY29sb3I6ICM5MTkxOTE7XG4gIHBhZGRpbmctbGVmdDogOSU7XG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL2NhcmJ1bGxldC5wbmdcIikgbm8tcmVwZWF0O1xuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xufVxuXG4uZm9udCB7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBjb2xvcjogIzczNzM3Mztcbn1cblxuLmFjY2VwdEJ0biB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbn1cblxuLmJ0bmFjY2VwdCB7XG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcbiAgY29sb3I6IHdoaXRlO1xuICBtYXJnaW4tbGVmdDogMTQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG4uYnRuYWNjZXB0OmhvdmVyIHtcbiAgYmFja2dyb3VuZDogd2hpdGUgIWltcG9ydGFudDtcbiAgY29sb3I6IGJsYWNrO1xuICBtYXJnaW4tbGVmdDogMTQwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHBhZGRpbmc6IDEwcHggMTVweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgZm9udC1zaXplOiAxNXB4O1xufVxuXG5AbWVkaWEgKG1pbi13aWR0aDogNTY4cHgpIHtcbiAgLmhlYWRlcmFib3V0IHtcbiAgICBwYWRkaW5nLXRvcDogNyU7XG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIH1cbn1cbi5yZXZpZXctdGl0bGUge1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMjNweDtcbiAgY29sb3I6ICMyZTJlMmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nLXRvcDogMyU7XG59XG5cbmlvbi1yYXRpbmcge1xuICAtLWNvbG9yOiBncmF5O1xuICAtLWNvbG9yLWZpbGxlZDogZ3JlZW47XG59XG5cbi5hc2hyZWN0YW5nbGUtcmV2aWV3IHtcbiAgd2lkdGg6IDkwJTtcbiAgcGFkZGluZy1ib3R0b206IDBweDtcbiAgbWFyZ2luLWJvdHRvbTogNXB4O1xufVxuXG4ubm8tYm9yZGVyIHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5jaGVja2JveHdyYXBwZXIge1xuICB3aWR0aDogNTMlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5jaGVja2JveHRleHQge1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgY29sb3I6ICMyZTJlMmU7XG59XG5cbi5kYXJrYnV0dG9uIHtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgcGFkZGluZzogMTBweCAxMDBweDtcbiAgbWFyZ2luOiAxMHB4IGF1dG87XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBiYWNrZ3JvdW5kOiAjMmUyZTJlO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAxOXB4O1xufVxuXG4ucGFkZGluZy1iYWNrIHtcbiAgcGFkZGluZy1yaWdodDogNXB4O1xufVxuXG4ucmV2aWV3aW1hZ2UgaW1nIHtcbiAgcGFkZGluZy1ib3R0b206IDEwcHggIWltcG9ydGFudDtcbn1cblxuLnJhdGUge1xuICBjb2xvcjogI2ZmYWUwMDtcbiAgZm9udC1zaXplOiAyNXB4O1xuICBwYWRkaW5nLWJvdHRvbTogMjBweDtcbiAgZGlzcGxheTogaW5saW5lO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5yYXRlIHVsIHtcbiAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcbiAgcGFkZGluZzogMDtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xuICAucmF0ZSB1bCB7XG4gICAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcbiAgICBwYWRkaW5nOiAwO1xuICAgIGRpc3BsYXk6IGJsb2NrO1xuICAgIHdpZHRoOiAxMDAlO1xuICB9XG59XG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcbiAgLmNoZWNrYm94d3JhcHBlciB7XG4gICAgd2lkdGg6IDYwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDY0MHB4KSB7XG4gIC5jaGVja2JveHdyYXBwZXIge1xuICAgIHdpZHRoOiAzMCU7XG4gIH1cbn0iXX0= */");

/***/ }),

/***/ 20341:
/*!*************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/reviewdriver/reviewdriver.page.html ***!
  \*************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Reviewdriver page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!--<ion-header>\r\n  <ion-toolbarbar color=\"headerblue\">\r\n    <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">REVIEW DRIVER</span></ion-title>\r\n  </ion-toolbarbar>\r\n</ion-header>-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <!-- <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-button> -->\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title><span class=\"heavy toptitle\">REVIEW DRIVER</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section class=\"support-section reviewimage\">\r\n    <!--<img src=\"assets/images/supportlogo.png\">-->\r\n    <!--<img src=\"assets/images/stars.jpg\">-->\r\n    <div class=\"rate\">\r\n      <ion-rating [(ngModel)]=\"rate\" size=\"default\" style=\"margin-left: 30%\">\r\n      </ion-rating>\r\n      <!-- <rating [(ngModel)]=\"rate\" name=\"rate\" ngDefaultControl max=\"5\"></rating> -->\r\n    </div>\r\n    <h2\r\n      class=\"review-title\"\r\n      *ngIf=\"booking!= undefined && booking.hasOwnProperty('trip_cost')\"\r\n    >\r\n      Your Trip Cost is ${{booking.trip_cost.toFixed(2)}}\r\n    </h2>\r\n    <section class=\"ashrectangle-review\">\r\n      <ion-textarea\r\n        [(ngModel)]=\"notes\"\r\n        placeholder=\"Enter your comments or notes\"\r\n      >\r\n      </ion-textarea>\r\n    </section>\r\n\r\n    <!--<section class=\"checkboxwrapper\">\r\n\t\t\t<ion-grid>\r\n\t\t\t\t<ion-row>\r\n\t\t\t\t\t<ion-col width-20>\r\n\t\t\t\t\t\t<ion-checkbox color=\"headerblue\" checked=\"false\" [(ngModel)]=\"favourite\"></ion-checkbox>\r\n\t\t\t\t\t</ion-col>\r\n\t\t\t\t\t<ion-col width-80 class=\"checkboxtext\">Mark as favourite</ion-col>\r\n\t\t\t\t</ion-row>\r\n\t\t\t</ion-grid>\r\n\t\t</section>-->\r\n\r\n    <button\r\n      class=\"darkbutton\"\r\n      ion-button\r\n      color=\"dark\"\r\n      round\r\n      (click)=\"reviewSubmit()\"\r\n    >\r\n      Submit\r\n    </button>\r\n    <!-- <button class=\"darkbutton\" ion-button color=\"dark\" round (click)=\"paymentSubmit()\">payment</button> -->\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_reviewdriver_reviewdriver_module_ts.js.map