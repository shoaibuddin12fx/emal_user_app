(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_activebooking_activebooking_module_ts"],{

/***/ 90744:
/*!*********************************************************************!*\
  !*** ./src/app/pages/activebooking/activebooking-routing.module.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivebookingPageRoutingModule": () => (/* binding */ ActivebookingPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _activebooking_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activebooking.page */ 15182);




const routes = [
    {
        path: '',
        component: _activebooking_page__WEBPACK_IMPORTED_MODULE_0__.ActivebookingPage
    }
];
let ActivebookingPageRoutingModule = class ActivebookingPageRoutingModule {
};
ActivebookingPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ActivebookingPageRoutingModule);



/***/ }),

/***/ 84162:
/*!*************************************************************!*\
  !*** ./src/app/pages/activebooking/activebooking.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivebookingPageModule": () => (/* binding */ ActivebookingPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _activebooking_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activebooking-routing.module */ 90744);
/* harmony import */ var _activebooking_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activebooking.page */ 15182);







let ActivebookingPageModule = class ActivebookingPageModule {
};
ActivebookingPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _activebooking_routing_module__WEBPACK_IMPORTED_MODULE_0__.ActivebookingPageRoutingModule
        ],
        declarations: [_activebooking_page__WEBPACK_IMPORTED_MODULE_1__.ActivebookingPage]
    })
], ActivebookingPageModule);



/***/ }),

/***/ 15182:
/*!***********************************************************!*\
  !*** ./src/app/pages/activebooking/activebooking.page.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ActivebookingPage": () => (/* binding */ ActivebookingPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_activebooking_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./activebooking.page.html */ 18579);
/* harmony import */ var _activebooking_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activebooking.page.scss */ 58367);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ 64687);
/* harmony import */ var src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/data.service */ 42498);
/* harmony import */ var _home_fare_now_fare_now_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../home/fare-now/fare-now.component */ 69753);








let ActivebookingPage = class ActivebookingPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector, callNumber, dataService) {
        var _a;
        super(injector);
        this.callNumber = callNumber;
        this.dataService = dataService;
        this.clickable = false;
        this.mapReady = false;
        this.mapService.showmap = true;
        this.bookingKey = (_a = this.getQueryParams()) === null || _a === void 0 ? void 0 : _a.bookingKey;
        this.latestStatus = 'None';
        this.booking = {
            pickup: {
                add: '',
            },
            image_url: 'assets/images/notfound.png',
            status: '',
        };
        console.log(this.bookingKey);
        let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
        //bookingRef.once('value', (snapshot:any) => {
        bookingRef.on('value', (snapshot) => {
            if (!snapshot.val())
                return;
            this.zone.run(() => {
                var _a, _b;
                console.log('BOOKING_DETAIL', snapshot.val());
                this.booking = snapshot.val();
                console.log('Booking', this.booking);
                if (this.booking != undefined && this.booking != null) {
                    //  this.driverUserId= snapshot.val().driver;
                    this.customerID = snapshot.val().customer;
                    //alert("hiii");
                    this.platform.ready().then(() => {
                        console.log('Loading map');
                        //  this.loadMap();
                    });
                    if (snapshot.val().driver == undefined) {
                        this.showImage = true;
                    }
                    else {
                        this.driverID = snapshot.val().driver;
                    }
                    if (((_a = snapshot.val()) === null || _a === void 0 ? void 0 : _a.hasOwnProperty('driver_image')) &&
                        ((_b = snapshot.val()) === null || _b === void 0 ? void 0 : _b.driver_image) != undefined &&
                        snapshot.val().driver_image != '') {
                        this.booking.image_url = snapshot.val().driver_image;
                    }
                    else {
                        this.booking.image_url = 'assets/images/notfound.png';
                    }
                    this.bookingStatus = snapshot.val().status;
                    if (this.booking.status == 'REVIEW') {
                        let geoRef = this.getDb().ref('geofire');
                        console.log(this.bookingKey);
                        //  let geoRef = firebase.database().ref('/geofire/' + this.bookingKey);
                        geoRef.child(this.bookingKey).remove();
                        this.nav.setRoot('pages/reviewdriver', {
                            bookingKey: this.bookingKey,
                        });
                    }
                }
                else {
                    this.nav.setRoot('pages/currentbookings');
                }
            });
        });
    }
    ngAfterViewInit() {
        if (this.bookingKey)
            this.loadMap();
    }
    loadMap() {
        var _a, _b, _c;
        const location = {
            lat: parseFloat((_a = this.booking.pickup) === null || _a === void 0 ? void 0 : _a.lat),
            lng: parseFloat((_b = this.booking.pickup) === null || _b === void 0 ? void 0 : _b.lng),
        };
        console.log('Map before', this.map);
        this.map = new google.maps.Map((_c = this.mapElement) === null || _c === void 0 ? void 0 : _c.nativeElement, {
            zoom: 15,
            center: location,
        });
        console.log('Map After', this.map);
        // this.map = new GoogleMap('map2', {
        //   backgroundColor: 'white',
        //   controls: {
        //     compass: false,
        //     myLocationButton: false,
        //     indoorPicker: false,
        //     zoom: true,
        //   },
        //   gestures: {
        //     scroll: true,
        //     tilt: true,
        //     rotate: true,
        //     zoom: true,
        //   },
        //   camera: {
        //     target: location,
        //     tilt: 30,
        //     zoom: 15, //,
        //     // 'bearing': 50
        //   },
        // });
        // console.log(this.map);
        let self = this;
        google.maps.event.addListenerOnce(this.map, 'idle', function () {
            // do something only the first time the map is loaded
            self.onMapReady();
        });
    }
    onMapReady() {
        this.mapReady = true;
        let self = this;
        let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
        bookingRef.on('value', (snapshot) => {
            var _a, _b;
            self.booking = snapshot.val();
            console.log('booking', self.booking);
            if (snapshot.val().hasOwnProperty('driver_image') &&
                snapshot.val().driver_image != undefined &&
                snapshot.val().driver_image != '') {
                self.booking.image_url = snapshot.val().driver_image;
            }
            else {
                self.booking.image_url = 'assets/images/notfound.png';
            }
            self.bookingStatus = snapshot.val().status;
            console.log(self.latestStatus, self.bookingStatus);
            if (self.latestStatus != self.bookingStatus ||
                self.latestStatus == 'None') {
                console.log('Latest status', self.latestStatus);
                // self.map.clear();
                self.latestStatus = self.bookingStatus;
                console.log('map clear');
                let location = {
                    lat: parseFloat((_a = self.booking.pickup) === null || _a === void 0 ? void 0 : _a.lat),
                    lng: parseFloat((_b = self.booking.pickup) === null || _b === void 0 ? void 0 : _b.lng),
                };
                console.log('location', location);
                if (self.bookingStatus == 'ARRIVING' ||
                    self.bookingStatus == 'STARTED') {
                    let marker = new google.maps.Marker({
                        position: location,
                        title: 'Driver Current Location',
                        icon: 'www/assets/images/driver.png',
                    });
                    marker.setMap(self.map);
                    self.addAndShowMarkerWindow(marker, 'Driver Current Location');
                    let geoRef = self
                        .getDb()
                        .ref('users/' + self.booking.driver + '/location');
                    geoRef.on('value', (snapshot) => {
                        var _a, _b, _c, _d;
                        let pos = {
                            lat: parseFloat((_a = snapshot.val()) === null || _a === void 0 ? void 0 : _a.lat),
                            lng: parseFloat((_b = snapshot.val()) === null || _b === void 0 ? void 0 : _b.lng),
                        };
                        console.log('latitude: ' +
                            ((_c = snapshot.val()) === null || _c === void 0 ? void 0 : _c.lat) +
                            ' , ' +
                            'longitude: ' +
                            ((_d = snapshot.val()) === null || _d === void 0 ? void 0 : _d.lng));
                        self.map.setCenter(pos);
                        marker.setPosition(pos);
                    });
                }
                else if (self.bookingStatus == 'ARRIVED') {
                    let marker = new google.maps.Marker({
                        position: location,
                        title: 'Driver Arrived',
                        icon: 'www/assets/images/driver.png',
                    });
                    self.addAndShowMarkerWindow(marker, 'Driver Arrived');
                    self.map.setCenter(location);
                }
                else {
                    console.log('Else -> Pickup Location');
                    let trackMarker = new google.maps.Marker({
                        position: location,
                        title: 'Pickup Location',
                    });
                    trackMarker.setMap(self.map);
                    self.addAndShowMarkerWindow(trackMarker, 'Pickup Location');
                    self.map.setCenter(location);
                }
            }
        });
    }
    addAndShowMarkerWindow(marker, content) {
        var infoWindow = new google.maps.InfoWindow({
            content: content,
        });
        marker.setMap(this.map);
        infoWindow.open(this.map, marker);
    }
    getHelp() {
        this.nav.push('pages/support', {
            bookingKey: this.bookingKey,
        });
    }
    cancelBooking() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            let setStatus = 'CANCELLED';
            if (this.booking.status == setStatus)
                return;
            if (this.booking.status == 'Finding your driver') {
                let alertCustom = yield this.alertCtrl.create({
                    header: 'Do you want to Cancel your booking',
                    buttons: [
                        {
                            text: 'Cancel',
                            handler: (data) => {
                                this.setClickable(true);
                                this.mapService.showmap = true;
                                alertCustom.dismiss();
                            },
                        },
                        {
                            text: 'OK',
                            handler: (data) => {
                                this.setClickable(true);
                                this.mapService.showmap = true;
                                let geocancleRef = this.getDb().ref('geofire');
                                let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
                                let customerRef = this.getDb().ref('/users/' + this.customerID + '/bookings/' + this.bookingKey);
                                // let driverRef = firebase.database().ref('/users/'+ this.driverID + '/bookings/'+ this.bookingKey);
                                bookingRef.child('status').set(setStatus);
                                customerRef.child('status').set(setStatus);
                                //  driverRef.child('status').set(setStatus);
                                geocancleRef.child(this.bookingKey).remove();
                                //*TODO
                                // this.nav.setRoot('pages/bookings');
                                this.nav.pop();
                            },
                        },
                    ],
                });
                this.setClickable(false);
                this.mapService.showmap = false;
                alertCustom.present();
                // this.navCtrl.setRoot(BookingsPage);
            }
            else {
                let alertCustom = yield this.alertCtrl.create({
                    header: 'Driver  ' + this.booking.status,
                    subHeader: 'Cancellation Charge $7',
                    buttons: [
                        {
                            text: 'Cancel',
                            handler: (data) => {
                                this.setClickable(true);
                                this.mapService.showmap = true;
                                alertCustom.dismiss();
                            },
                        },
                        {
                            text: 'OK',
                            handler: (data) => {
                                this.setClickable(true);
                                this.mapService.showmap = true;
                                let geocancleRef = this.getDb().ref('geofire');
                                let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
                                let customerRef = this.getDb().ref('/users/' + this.customerID + '/bookings/' + this.bookingKey);
                                let driverRef = this.getDb().ref('/users/' + this.driverID + '/bookings/' + this.bookingKey);
                                bookingRef.child('status').set(setStatus);
                                customerRef.child('status').set(setStatus);
                                driverRef.child('status').set(setStatus);
                                geocancleRef.child(this.bookingKey).remove();
                                /* Cancelllation charge */
                                var totalBill = 7;
                                let customerRef1 = this.getDb().ref('/users/' + this.customerID);
                                var paystripe = this.getDb().ref('/users/' + this.customerID);
                                paystripe.on('value', (_snapshot) => {
                                    if (_snapshot.val()) {
                                        this.stripeID = _snapshot.val().stripeID;
                                        console.log('this.stripeID.................', this.stripeID);
                                    }
                                });
                                var payRef = this.getDb().ref('/users/' + this.customerID + '/payment_methods');
                                payRef.once('value', (snapshot) => {
                                    if (snapshot.val()) {
                                        let cards = snapshot.val();
                                        let cardID = '';
                                        console.log('cards.........', cards);
                                        let GotIt = false;
                                        for (let i = 0; i < cards.length; i++) {
                                            if (cards[i].primary) {
                                                GotIt = true;
                                                cardID = cards[i].stripeCardID;
                                            }
                                        }
                                        if (GotIt) {
                                            console.log(cardID, totalBill);
                                            this.usersService
                                                .stripePayment(cardID, this.stripeID, totalBill)
                                                .subscribe((response) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                                                console.log('stripe payment response', response);
                                                let temp;
                                                temp = response;
                                                if (temp.status == 'succeeded') {
                                                    this.getDb()
                                                        .ref('/bookings/' + this.bookingKey + '/trip_cost')
                                                        .set(totalBill);
                                                    this.getDb()
                                                        .ref('/users/' +
                                                        this.booking.customer +
                                                        '/bookings/' +
                                                        this.bookingKey +
                                                        '/trip_cost')
                                                        .set(totalBill);
                                                    this.getDb()
                                                        .ref('/users/' +
                                                        this.booking.driver +
                                                        '/bookings/' +
                                                        this.bookingKey +
                                                        '/trip_cost')
                                                        .set(totalBill);
                                                    // alert("succeeded payment");
                                                    let alert = yield this.alertCtrl.create({
                                                        header: 'Payment',
                                                        subHeader: 'Payment succeeded',
                                                        buttons: ['OK'],
                                                    });
                                                    alert.present();
                                                    //alert('Payment succeeded');
                                                    this.nav.setRoot('pages/bookings');
                                                    //   this.usersService.sendPush('Trip Ended - Payment Success !!',"Your cost for the trip is $" + totalBill ,this.booking.customer_push_token).subscribe(response=>console.log(response));
                                                    //   this.paymentclick=true;
                                                    // firebase.database().ref('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                                                    // firebase.database().ref('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                                                    //   this.navCtrl.setRoot(JobcompletePage,{
                                                    //     bookingKey:this.bookingKey
                                                    //   });
                                                }
                                                else {
                                                    // let alert = this.alertCtrl.create({
                                                    //   title: 'Payment',
                                                    //   subTitle: temp.message,
                                                    //   buttons: ['OK']
                                                    // });
                                                    // let alert = this.alertCtrl.create();
                                                    // alert.present();
                                                    //   alert(temp.message);
                                                    // firebase.database().ref('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('REVIEW');
                                                    // firebase.database().ref('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('REVIEW');
                                                    //   this.usersService.sendPush('Payment Not Approve !!',"No Payment Method Found. Your cost for the trip is $" + totalBill ,this.booking.customer_push_token).subscribe(response=>console.log(response));
                                                }
                                            }), (err) => {
                                                alert('Server Error ! Try again');
                                                this.firebaseService
                                                    .getDatabase()
                                                    .ref('users/' +
                                                    this.booking.customer +
                                                    '/bookings/' +
                                                    this.bookingKey +
                                                    '/status')
                                                    .set('PAYMENT');
                                                this.firebaseService
                                                    .getDatabase()
                                                    .ref('users/' +
                                                    this.booking.driver +
                                                    '/bookings/' +
                                                    this.bookingKey +
                                                    '/status')
                                                    .set('PAYMENT');
                                            });
                                        }
                                        else {
                                            //   alert("Customer Not Selected Primary card");
                                            // firebase.database().ref('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                                            // firebase.database().ref('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                                            //   this.usersService.sendPush('Payment Failed !!',"Not Selected Primary card. Your cost for the trip is $" + totalBill ,this.booking.customer_push_token).subscribe(response=>console.log(response));
                                        }
                                    }
                                    else {
                                        //   alert("Customer Card Not Available");
                                        //   this.usersService.sendPush('Payment Failed !!'," Card Not Available. Your cost for the trip is $" + totalBill,this.booking.customer_push_token).subscribe(response=>console.log(response));
                                    }
                                });
                                /* Cancelllation charge */
                            },
                        },
                    ],
                });
                this.setClickable(false);
                this.mapService.showmap = false;
                // alertCustom.setSubTitle('Do you want to Cancel your Booking');
                alertCustom.present();
            }
        });
    }
    giveCall() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            if (this.driverID != undefined) {
                let alertCustom = yield this.alertCtrl.create({
                    header: 'Do you want to call Driver',
                    buttons: [
                        {
                            text: 'Cancel',
                            handler: (data) => {
                                this.setClickable(true);
                                this.mapService.showmap = true;
                                alertCustom.dismiss();
                            },
                        },
                        {
                            text: 'OK',
                            handler: (data) => {
                                this.setClickable(true);
                                this.mapService.showmap = true;
                                let driverRef = this.getDb().ref('/users/' + this.driverID);
                                driverRef.once('value', (snapshot) => {
                                    this.callDriver = snapshot.val().phone;
                                    this.callNumber
                                        .callNumber(this.callDriver, true)
                                        .then(() => console.log('Launched dialer!'))
                                        .catch((reason) => console.log('Error launching dialer', reason));
                                });
                            },
                        },
                    ],
                });
                this.setClickable(false);
                this.mapService.showmap = false;
                alertCustom.present();
            }
            else {
                console.log('Driver not Select');
            }
        });
    }
    ngDoCheck() {
        if (this.mapReady) {
            if (this.mapService.showmap) {
                this.setClickable(true);
            }
            else {
                this.setClickable(false);
            }
        }
    }
    editLocation() {
        console.log('edit location');
        console.log(this.booking);
        let details = this.booking;
        delete details.image_url;
        this.dataService.farePageData = {
            bookingId: this.bookingKey,
            bookingDetails: this.booking,
        };
        this.modals.present(_home_fare_now_fare_now_component__WEBPACK_IMPORTED_MODULE_5__.FareNowComponent, { details });
        // this.nav.push('pages/fare');
    }
    ngOnDestroy() {
        console.log('page destroy successfully');
        if (this.map != undefined) {
            this.map = null;
        }
    }
    ngOnInit() { }
    getDb() {
        return this.firebaseService.getDatabase();
    }
    setClickable(clickable) {
        this.clickable = clickable;
    }
};
ActivebookingPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Injector },
    { type: _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_3__.CallNumber },
    { type: src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_4__.DataService }
];
ActivebookingPage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: ['map2', { static: false },] }]
};
ActivebookingPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-activebooking',
        template: _raw_loader_activebooking_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_activebooking_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ActivebookingPage);



/***/ }),

/***/ 58367:
/*!*************************************************************!*\
  !*** ./src/app/pages/activebooking/activebooking.page.scss ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("@charset \"UTF-8\";\npage-activebooking #map2 {\n  height: 100%;\n}\n.rightmenu {\n  color: #fff;\n  background: none !important;\n  padding: 0 !important;\n}\n.sebm-google-map-container {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  transition: opacity 150ms ease-in;\n  position: relative;\n}\n.gmap {\n  width: 100%;\n  position: relative;\n}\n.gmap-button-wrapper {\n  position: absolute;\n  width: 100%;\n  display: block;\n  height: auto;\n}\n.cusb {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 20%;\n  height: 40px;\n}\n.darkbutton1 {\n  width: 100%;\n  padding: 22px 0;\n  font-family: \"PT Sans\";\n  font-size: 18px;\n  text-transform: capitalize;\n  font-weight: normal;\n}\n.button-md-dark.activated,\n.button-md.activated {\n  background: #222 !important;\n}\n.button-md:hover:not(.disable-hover) {\n  background: #222 !important;\n}\n.mapcontainer {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0;\n}\n.topnotification-wrapper {\n  width: 100%;\n  position: absolute;\n  top: 0;\n  overflow: hidden;\n}\n.mapbutton-wrapper {\n  width: 80%;\n  margin: auto;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.topnotification {\n  width: 100%;\n  background: #fff;\n  margin-bottom: 2px;\n  padding: 0px 2px;\n}\n.subbluetext-map {\n  font-family: \"PT Sans\";\n  font-size: 14px;\n  text-align: left;\n  color: #2b2b2b;\n  margin: 0;\n  padding-bottom: 8px;\n}\n.bluetext-map {\n  color: #222;\n  margin-left: 0;\n  margin-right: 0;\n  margin-top: 0;\n  margin-bottom: 2px;\n  font-size: 14px;\n  line-height: normal;\n  text-overflow: inherit;\n  overflow: inherit;\n}\n.marker img {\n  display: block;\n  margin: 0 auto;\n  padding-top: 5px;\n}\n.driver-snippet {\n  position: fixed;\n  bottom: 0px;\n  width: 100%;\n  background-color: #fff;\n}\n.nopadding-avataar .item-md ion-avatar[item-left] {\n  margin: 8px 16px 8px 6px;\n}\nion-col {\n  flex: 1 0 100px !important;\n}\n.driver-snippet .item-md {\n  padding: 0 !important;\n}\n.driver-snippet .item-md ion-avatar[item-left] {\n  margin: 8px 16px 8px 12px;\n}\n.wrapper-driversnippet .label-md {\n  margin: 0 !important;\n}\n.no-border {\n  border: none !important;\n}\n.padding-status {\n  padding-top: 6.5%;\n}\n.no-margin {\n  margin: 0 !important;\n}\n​.customer-icons {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: center;\n  color: #2b2b2b;\n}\n.customer-icons img {\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 2px;\n}\n​ .wrapper-driversnippet {\n  width: 95%;\n  margin: 0 auto;\n}\n.onlinetoppadding {\n  padding-top: 8px;\n  padding-left: 80px;\n}\n.sidepadding-status {\n  padding-left: 3px;\n}\n.item-md {\n  padding: 0 0 0 22px !important;\n}\n.no-padding {\n  padding: 0;\n}\n.no-padding-bottom {\n  padding-bottom: 0;\n}\n.no-padding-top {\n  padding-top: 0;\n}\n.centertext {\n  text-align: center;\n}\n.paddingright {\n  padding-right: 10px;\n}\n@media screen and (orientation: landscape) {\n  .onlinetoppadding {\n    padding-left: 69%;\n  }\n}\n@media (min-width: 1024px) {\n  .darkbutton1 {\n    padding: 30px 0;\n  }\n}\nion-app._gmaps_cdv_ .nav-decor {\n  background-color: transparent !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGl2ZWJvb2tpbmcucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBLGdCQUFnQjtBQUNkO0VBQ0UsWUFBQTtBQUNKO0FBR0E7RUFDRSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSxxQkFBQTtBQUFGO0FBR0E7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFVBQUE7RUFDQSxpQ0FBQTtFQUNBLGtCQUFBO0FBQUY7QUFHQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQUFGO0FBRUE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0EsWUFBQTtBQUNGO0FBQ0E7RUFDRSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsVUFBQTtFQUNBLFlBQUE7QUFFRjtBQUFBO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSwwQkFBQTtFQUNBLG1CQUFBO0FBR0Y7QUFEQTs7RUFFRSwyQkFBQTtBQUlGO0FBRkE7RUFDRSwyQkFBQTtBQUtGO0FBRkE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBRUEsTUFBQTtBQUlGO0FBRkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxNQUFBO0VBQ0EsZ0JBQUE7QUFLRjtBQUhBO0VBQ0UsVUFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQU1GO0FBSkE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxrQkFBQTtFQUNBLGdCQUFBO0FBT0Y7QUFMQTtFQUNFLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLFNBQUE7RUFDQSxtQkFBQTtBQVFGO0FBTkE7RUFHRSxXQUFBO0VBR0EsY0FBQTtFQUNFLGVBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0FBS0o7QUFIQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUFNRjtBQUhBO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7QUFNRjtBQUhBO0VBQ0Usd0JBQUE7QUFNRjtBQUpBO0VBQ0UsMEJBQUE7QUFPRjtBQUxBO0VBQ0UscUJBQUE7QUFRRjtBQU5BO0VBQ0UseUJBQUE7QUFTRjtBQVBBO0VBQ0Usb0JBQUE7QUFVRjtBQVBBO0VBQ0UsdUJBQUE7QUFVRjtBQVJBO0VBQ0UsaUJBQUE7QUFXRjtBQVRBO0VBQ0Usb0JBQUE7QUFZRjtBQVRBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0FBWUY7QUFWQTtFQUNFLGNBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7QUFhRjtBQVhBO0VBQ0UsVUFBQTtFQUNBLGNBQUE7QUFjRjtBQVpBO0VBQ0UsZ0JBQUE7RUFDQSxrQkFBQTtBQWVGO0FBYkE7RUFDRSxpQkFBQTtBQWdCRjtBQWRBO0VBQ0UsOEJBQUE7QUFpQkY7QUFmQTtFQUNFLFVBQUE7QUFrQkY7QUFoQkE7RUFDRSxpQkFBQTtBQW1CRjtBQWpCQTtFQUNFLGNBQUE7QUFvQkY7QUFsQkE7RUFDRSxrQkFBQTtBQXFCRjtBQW5CQTtFQUNFLG1CQUFBO0FBc0JGO0FBbkJBO0VBQ0U7SUFDRSxpQkFBQTtFQXNCRjtBQUNGO0FBbkJBO0VBQ0U7SUFDRSxlQUFBO0VBcUJGO0FBQ0Y7QUFsQkE7RUFDRSx3Q0FBQTtBQW9CRiIsImZpbGUiOiJhY3RpdmVib29raW5nLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIkBjaGFyc2V0IFwiVVRGLThcIjtcbnBhZ2UtYWN0aXZlYm9va2luZyAjbWFwMiB7XG4gIGhlaWdodDogMTAwJTtcbn1cblxuLnJpZ2h0bWVudSB7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbn1cblxuLnNlYm0tZ29vZ2xlLW1hcC1jb250YWluZXIge1xuICB3aWR0aDogMTAwJTtcbiAgaGVpZ2h0OiAxMDAlO1xuICBvcGFjaXR5OiAwO1xuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDE1MG1zIGVhc2UtaW47XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cblxuLmdtYXAge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZ21hcC1idXR0b24td3JhcHBlciB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBoZWlnaHQ6IGF1dG87XG59XG5cbi5jdXNiIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHdpZHRoOiAyMCU7XG4gIGhlaWdodDogNDBweDtcbn1cblxuLmRhcmtidXR0b24xIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmc6IDIycHggMDtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtdHJhbnNmb3JtOiBjYXBpdGFsaXplO1xuICBmb250LXdlaWdodDogbm9ybWFsO1xufVxuXG4uYnV0dG9uLW1kLWRhcmsuYWN0aXZhdGVkLFxuLmJ1dHRvbi1tZC5hY3RpdmF0ZWQge1xuICBiYWNrZ3JvdW5kOiAjMjIyICFpbXBvcnRhbnQ7XG59XG5cbi5idXR0b24tbWQ6aG92ZXI6bm90KC5kaXNhYmxlLWhvdmVyKSB7XG4gIGJhY2tncm91bmQ6ICMyMjIgIWltcG9ydGFudDtcbn1cblxuLm1hcGNvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgdG9wOiAwO1xufVxuXG4udG9wbm90aWZpY2F0aW9uLXdyYXBwZXIge1xuICB3aWR0aDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIG92ZXJmbG93OiBoaWRkZW47XG59XG5cbi5tYXBidXR0b24td3JhcHBlciB7XG4gIHdpZHRoOiA4MCU7XG4gIG1hcmdpbjogYXV0bztcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xufVxuXG4udG9wbm90aWZpY2F0aW9uIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgcGFkZGluZzogMHB4IDJweDtcbn1cblxuLnN1YmJsdWV0ZXh0LW1hcCB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAxNHB4O1xuICB0ZXh0LWFsaWduOiBsZWZ0O1xuICBjb2xvcjogIzJiMmIyYjtcbiAgbWFyZ2luOiAwO1xuICBwYWRkaW5nLWJvdHRvbTogOHB4O1xufVxuXG4uYmx1ZXRleHQtbWFwIHtcbiAgY29sb3I6ICMyMjI7XG4gIG1hcmdpbi1sZWZ0OiAwO1xuICBtYXJnaW4tcmlnaHQ6IDA7XG4gIG1hcmdpbi10b3A6IDA7XG4gIG1hcmdpbi1ib3R0b206IDJweDtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBsaW5lLWhlaWdodDogbm9ybWFsO1xuICB0ZXh0LW92ZXJmbG93OiBpbmhlcml0O1xuICBvdmVyZmxvdzogaW5oZXJpdDtcbn1cblxuLm1hcmtlciBpbWcge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmctdG9wOiA1cHg7XG59XG5cbi5kcml2ZXItc25pcHBldCB7XG4gIHBvc2l0aW9uOiBmaXhlZDtcbiAgYm90dG9tOiAwcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xufVxuXG4ubm9wYWRkaW5nLWF2YXRhYXIgLml0ZW0tbWQgaW9uLWF2YXRhcltpdGVtLWxlZnRdIHtcbiAgbWFyZ2luOiA4cHggMTZweCA4cHggNnB4O1xufVxuXG5pb24tY29sIHtcbiAgZmxleDogMSAwIDEwMHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5kcml2ZXItc25pcHBldCAuaXRlbS1tZCB7XG4gIHBhZGRpbmc6IDAgIWltcG9ydGFudDtcbn1cblxuLmRyaXZlci1zbmlwcGV0IC5pdGVtLW1kIGlvbi1hdmF0YXJbaXRlbS1sZWZ0XSB7XG4gIG1hcmdpbjogOHB4IDE2cHggOHB4IDEycHg7XG59XG5cbi53cmFwcGVyLWRyaXZlcnNuaXBwZXQgLmxhYmVsLW1kIHtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5uby1ib3JkZXIge1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLnBhZGRpbmctc3RhdHVzIHtcbiAgcGFkZGluZy10b3A6IDYuNSU7XG59XG5cbi5uby1tYXJnaW4ge1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbn1cblxu4oCLLmN1c3RvbWVyLWljb25zIHtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMyYjJiMmI7XG59XG5cbi5jdXN0b21lci1pY29ucyBpbWcge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmctYm90dG9tOiAycHg7XG59XG5cbuKAiyAud3JhcHBlci1kcml2ZXJzbmlwcGV0IHtcbiAgd2lkdGg6IDk1JTtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbi5vbmxpbmV0b3BwYWRkaW5nIHtcbiAgcGFkZGluZy10b3A6IDhweDtcbiAgcGFkZGluZy1sZWZ0OiA4MHB4O1xufVxuXG4uc2lkZXBhZGRpbmctc3RhdHVzIHtcbiAgcGFkZGluZy1sZWZ0OiAzcHg7XG59XG5cbi5pdGVtLW1kIHtcbiAgcGFkZGluZzogMCAwIDAgMjJweCAhaW1wb3J0YW50O1xufVxuXG4ubm8tcGFkZGluZyB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5uby1wYWRkaW5nLWJvdHRvbSB7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xufVxuXG4ubm8tcGFkZGluZy10b3Age1xuICBwYWRkaW5nLXRvcDogMDtcbn1cblxuLmNlbnRlcnRleHQge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5wYWRkaW5ncmlnaHQge1xuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xufVxuXG5AbWVkaWEgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xuICAub25saW5ldG9wcGFkZGluZyB7XG4gICAgcGFkZGluZy1sZWZ0OiA2OSU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiAxMDI0cHgpIHtcbiAgLmRhcmtidXR0b24xIHtcbiAgICBwYWRkaW5nOiAzMHB4IDA7XG4gIH1cbn1cbmlvbi1hcHAuX2dtYXBzX2Nkdl8gLm5hdi1kZWNvciB7XG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XG59Il19 */");

/***/ }),

/***/ 18579:
/*!***************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/activebooking/activebooking.page.html ***!
  \***************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>Requesting...</ion-title>\r\n    <!-- <ion-buttons slot=\"end\">\r\n      <ion-button (click)=\"editLocation()\">\r\n        <ion-icon name=\"create-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons> -->\r\n  </ion-toolbar>\r\n\r\n  <!-- <ion-toolbar color=\"dark\">\r\n    <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\" color=\"light\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-button>\r\n\r\n    <ion-title><span class=\"heavy\">BOOKING</span></ion-title>\r\n\r\n    <ion-button slot=\"end\" fill=\"clear\" (click)=\"editLocation()\" color=\"light\">\r\n      <ion-icon name=\"create-outline\"></ion-icon>\r\n    </ion-button>\r\n  </ion-toolbar> -->\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div #map2 id=\"map2\" style=\"height: 100%; margin-top: 15%\"></div>\r\n  <section class=\"mapcontainer\">\r\n    <div>\r\n      <ion-list class=\"ion-margin-top\">\r\n        <!-- <ion-item *ngIf=\"PickupService === true\">\r\n          <ion-icon slot=\"start\" name=\"navigate-outline\"></ion-icon>\r\n\r\n          <ion-label class=\"ion-text-wrap\">\r\n            <h2>Destination Address</h2>\r\n            <p>{{booking?.pickup.add}}</p>\r\n          </ion-label>\r\n        </ion-item> -->\r\n        <ion-item>\r\n          <ion-icon slot=\"start\" name=\"navigate-outline\"></ion-icon>\r\n\r\n\r\n          <ion-label class=\"ion-text-wrap\">\r\n            <h2>Pickup Address</h2>\r\n            <p>{{booking?.pickup?.add}}</p>\r\n          </ion-label>\r\n        </ion-item>\r\n\r\n        <ion-item *ngIf=\"booking?.drop?.add\">\r\n          <ion-icon slot=\"start\" name=\"navigate-outline\"></ion-icon>\r\n\r\n\r\n          <ion-label class=\"ion-text-wrap\">\r\n            <h2>Drop Address</h2>\r\n            <p>{{booking?.drop?.add}}</p>\r\n          </ion-label>\r\n        </ion-item>\r\n      </ion-list>\r\n      <!-- <section class=\"topnotification-wrapper\">\r\n        <section class=\"topnotification\">\r\n          <ion-grid class=\"no-padding\">\r\n            <ion-row>\r\n              <ion-col size=\"2\" class=\"marker\">\r\n                <img src=\"assets/images/marker.png\" />\r\n              </ion-col>\r\n              <ion-col size=\"10\" class=\"no-padding\">\r\n                <ion-item>\r\n          \r\n\r\n                  <ion-label class=\"ion-text-wrap\">\r\n                    <h2>Pickup Address</h2>\r\n                    <p>{{booking?.pickup.add}}</p>\r\n                  </ion-label>\r\n                </ion-item>\r\n              \r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </section>\r\n      </section> -->\r\n\r\n      <section class=\"driver-snippet\">\r\n        <ion-grid>\r\n          <ion-row wrap *ngIf=\"booking?.status !== 'Finding your driver'\">\r\n\r\n            <ion-col size=\"12\">\r\n              <!-- <ion-list *ngIf!=\"showImage\" no-lines class=\"no-padding no-margin\">-->\r\n              <ion-list no-lines class=\"no-padding no-margin\">\r\n                <ion-item lines=\"none\">\r\n                  <ion-avatar slot=\"start\">\r\n                    <img [src]=\"booking.image_url !== '' ? booking.image_url : 'assets/images/notfound.png'\"\r\n                      onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\" />\r\n                  </ion-avatar>\r\n                  <ion-label>\r\n                    <p>\r\n                      <small>{{booking?.tripdate?.substring(8,10)}}{{booking?.tripdate?.substring(4,8)}}{{booking?.tripdate?.substring(0,4)}}\r\n                        {{booking?.tripdate?.substring(11,16)}}</small>\r\n                    </p>\r\n                    <p *ngIf=\"booking.status === 'REVIEW'\">PLEASE REVIEW YOUR DRIVER</p>\r\n                  </ion-label>\r\n                  <ion-badge slot=\"end\" *ngIf=\"booking.status !== 'REVIEW'\">{{booking?.status}}</ion-badge>\r\n                </ion-item>\r\n\r\n                <!-- <ion-item class=\"no-border\">\r\n                  <ion-avatar item-left>\r\n                    <img [src]=\"booking?.image_url\" onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n                      alt=\"avatar\" />\r\n                  </ion-avatar>\r\n                  <ion-label class=\"ion-text-wrap\">\r\n                    <h2>{{booking?.driver_name}}</h2>\r\n                  </ion-label>\r\n                </ion-item> -->\r\n              </ion-list>\r\n            </ion-col>\r\n            <!-- <ion-col width-33 class=\"padding-status\">\r\n              <ion-icon name=\"ios-checkmark-circle\" item-left></ion-icon>\r\n              {{booking?.status}}\r\n            </ion-col> -->\r\n          </ion-row>\r\n\r\n          <ion-row *ngIf=\"booking?.status === 'Finding your driver'\">\r\n            <ion-col>\r\n              <p style=\"text-align: center\">\r\n                <strong>Finding your driver, please wait.....</strong>\r\n              </p>\r\n            </ion-col>\r\n          </ion-row>\r\n        </ion-grid>\r\n\r\n        <div class=\"wrapper-driversnippet\">\r\n          <ion-grid class=\"no-padding-top\">\r\n            <ion-row class=\"customer-icons\" *ngIf=\"booking?.status !== 'Finding your driver'\">\r\n              <ion-col class=\"centertext no-padding-top\">\r\n                <!--<ion-col *ngIf!=\"showImage\"  class=\"centertext no-padding-top\" >-->\r\n                <img src=\"assets/images/phone.png\" (click)=\"giveCall()\" alt=\"phone\" />\r\n                Contact\r\n              </ion-col>\r\n              <ion-col (click)=\"getHelp()\" class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/info.png\" alt=\"help\" />\r\n                Help\r\n              </ion-col>\r\n              <ion-col (click)=\"cancelBooking()\" class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/cancel.png\" alt=\"cancel\" />\r\n                Cancel\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-row class=\"customer-icons\" *ngIf=\"booking?.status === 'Finding your driver'\">\r\n              <ion-col (click)=\"getHelp()\" class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/info.png\" alt=\"help\" />\r\n                Help\r\n              </ion-col>\r\n              <ion-col class=\"centertext no-padding-top\" style=\"width: 50%\">\r\n                <ion-spinner></ion-spinner>\r\n                <!-- <img src=\"assets/images/loader-spinner.gif\" alt=\"loader\" /> -->\r\n              </ion-col>\r\n              <ion-col (click)=\"cancelBooking()\" [disabled]=\"true\" class=\"centertext no-padding-top\">\r\n                <img src=\"assets/images/cancel.png\" alt=\"cancel\" />\r\n                Cancel\r\n              </ion-col>\r\n            </ion-row>\r\n          </ion-grid>\r\n        </div>\r\n      </section>\r\n    </div>\r\n  </section>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_activebooking_activebooking_module_ts.js.map