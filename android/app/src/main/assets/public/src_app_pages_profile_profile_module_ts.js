(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_profile_profile_module_ts"],{

/***/ 64021:
/*!****************************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/definitions.js ***!
  \****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraSource": () => (/* binding */ CameraSource),
/* harmony export */   "CameraDirection": () => (/* binding */ CameraDirection),
/* harmony export */   "CameraResultType": () => (/* binding */ CameraResultType)
/* harmony export */ });
var CameraSource;
(function (CameraSource) {
    /**
     * Prompts the user to select either the photo album or take a photo.
     */
    CameraSource["Prompt"] = "PROMPT";
    /**
     * Take a new photo using the camera.
     */
    CameraSource["Camera"] = "CAMERA";
    /**
     * Pick an existing photo fron the gallery or photo album.
     */
    CameraSource["Photos"] = "PHOTOS";
})(CameraSource || (CameraSource = {}));
var CameraDirection;
(function (CameraDirection) {
    CameraDirection["Rear"] = "REAR";
    CameraDirection["Front"] = "FRONT";
})(CameraDirection || (CameraDirection = {}));
var CameraResultType;
(function (CameraResultType) {
    CameraResultType["Uri"] = "uri";
    CameraResultType["Base64"] = "base64";
    CameraResultType["DataUrl"] = "dataUrl";
})(CameraResultType || (CameraResultType = {}));
//# sourceMappingURL=definitions.js.map

/***/ }),

/***/ 37673:
/*!**********************************************************!*\
  !*** ./node_modules/@capacitor/camera/dist/esm/index.js ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CameraDirection": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraDirection),
/* harmony export */   "CameraResultType": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraResultType),
/* harmony export */   "CameraSource": () => (/* reexport safe */ _definitions__WEBPACK_IMPORTED_MODULE_1__.CameraSource),
/* harmony export */   "Camera": () => (/* binding */ Camera)
/* harmony export */ });
/* harmony import */ var _capacitor_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @capacitor/core */ 68384);
/* harmony import */ var _definitions__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./definitions */ 64021);

const Camera = (0,_capacitor_core__WEBPACK_IMPORTED_MODULE_0__.registerPlugin)('Camera', {
    web: () => __webpack_require__.e(/*! import() */ "node_modules_capacitor_camera_dist_esm_web_js").then(__webpack_require__.bind(__webpack_require__, /*! ./web */ 14028)).then(m => new m.CameraWeb()),
});


//# sourceMappingURL=index.js.map

/***/ }),

/***/ 41474:
/*!*********************************************************!*\
  !*** ./src/app/pages/profile/profile-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePageRoutingModule": () => (/* binding */ ProfilePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile.page */ 64629);




const routes = [
    {
        path: '',
        component: _profile_page__WEBPACK_IMPORTED_MODULE_0__.ProfilePage
    }
];
let ProfilePageRoutingModule = class ProfilePageRoutingModule {
};
ProfilePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], ProfilePageRoutingModule);



/***/ }),

/***/ 88558:
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePageModule": () => (/* binding */ ProfilePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _profile_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./profile-routing.module */ 41474);
/* harmony import */ var _profile_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.page */ 64629);







let ProfilePageModule = class ProfilePageModule {
};
ProfilePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _profile_routing_module__WEBPACK_IMPORTED_MODULE_0__.ProfilePageRoutingModule
        ],
        declarations: [_profile_page__WEBPACK_IMPORTED_MODULE_1__.ProfilePage]
    })
], ProfilePageModule);



/***/ }),

/***/ 64629:
/*!***********************************************!*\
  !*** ./src/app/pages/profile/profile.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProfilePage": () => (/* binding */ ProfilePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./profile.page.html */ 79291);
/* harmony import */ var _profile_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./profile.page.scss */ 2777);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @capacitor/camera */ 37673);






let ProfilePage = class ProfilePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.driver = 'driver';
        this.customer = 'customer';
        //upload database
        this.users = [];
        this.data = {
            fullname: '',
            email: '',
            phone: '',
            registration: '',
            cityname: '',
        };
    }
    //constructor end
    doAdd() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            // console.log(this.data,userId);
            // this.usersService.updateData(this.data,this.curUser.uid)
            let profileRef = this.getDbRef('/users/' + this.curUser.uid);
            profileRef.child('fullname').set(this.data.fullname);
            profileRef.child('email').set(this.data.email);
            profileRef.child('phone').set(this.data.phone);
            profileRef.child('cityname').set(this.data.cityname);
            profileRef.child('profile_image').set(this.profileImage);
            let alert = yield this.alertCtrl.create({
                header: 'Success', subHeader: 'Profile updated successfully',
                buttons: [{
                        text: 'Ok',
                        handler: () => {
                            this.events.publish("login_event");
                            this.nav.pop();
                        }
                    }]
            });
            alert.present();
        });
    }
    //open camera picture
    openCamera() {
        const cameraOptions = {
            height: 150,
            width: 150,
            allowEditing: true,
            quality: 50,
            resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.CameraResultType.DataUrl,
            source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.CameraSource.Camera,
        };
        _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.Camera.getPhoto(cameraOptions).then((imageData) => {
            console.log("imageData", imageData);
            this.captureDataUrl = imageData.dataUrl;
            //  console.log('CaptureDataUrl:' + this.captureDataUrl);
            this.upload();
        }, (err) => { });
    }
    //open galary picture
    openGalary() {
        const cameraOptions = {
            height: 150,
            width: 150,
            allowEditing: true,
            quality: 50,
            resultType: _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.CameraResultType.DataUrl,
            source: _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.CameraSource.Photos,
        };
        _capacitor_camera__WEBPACK_IMPORTED_MODULE_3__.Camera.getPhoto(cameraOptions).then((imageData) => {
            console.log("imageData", imageData);
            this.captureDataUrl = imageData.dataUrl;
            // console.log('CaptureDataUrl:' + this.captureDataUrl);
            this.upload();
        }, (err) => { });
    }
    //upload firebase
    upload() {
        this.profileImage = this.captureDataUrl;
        return;
        let storageRef = this.firebaseService.fireStorage.ref('/');
        const filename = Math.floor(Date.now() / 1000);
        const imageRef = storageRef.child(`images/${filename}.jpg`);
        console.log('Upload Const ImageRef:', imageRef);
        console.log("upload", this.captureDataUrl);
        //*TODO*//
        // firebase.storage.StringFormat.DATA_URL
        imageRef
            .put(this.captureDataUrl)
            .then((snapshot) => {
            console.log('SnapShot:' + JSON.stringify(snapshot.ref.getDownloadURL()));
            this.saveDatabase(snapshot.ref);
        });
    }
    saveDatabase(snapshot) {
        var _a;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let imgUrl = yield snapshot.getDownloadURL();
            var ref = this.getDbRef('users/' + this.curUser.uid);
            ref.child('profile_image').set(snapshot.downloadURL);
            var ref = this.getDbRef('/users/' + ((_a = this.curUser) === null || _a === void 0 ? void 0 : _a.uid) + '/bookings');
            // var bookingref = firebase.database().ref('bookings');
            ref.on('value', (snap1) => {
                var _a;
                let userdata = snap1.val();
                for (var key in userdata) {
                    userdata[key].uid = key;
                    if (key != ((_a = this.curUser) === null || _a === void 0 ? void 0 : _a.uid)) {
                        this.users.push(userdata[key]);
                    }
                }
                this.bookingsUpdateData(this.users, imgUrl);
            });
        });
    }
    bookingsUpdateData(userIds, imgUrl) {
        var ref = this.getDbRef('/users/' + this.curUser.uid);
        ref.once('value', (snap2) => {
            let userType = snap2.val().type;
            if (userType == 'customer') {
                for (let i = 0; i < userIds.length; i++) {
                    console.log(userIds[i].uid);
                    var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
                    let userBookings = this.getDbRef('/users/' + userIds[i].driver + '/bookings/' + userIds[i].uid);
                    bookingref.child('customer_image').set(imgUrl);
                    userBookings.child('customer_image').set(imgUrl);
                }
            }
            else {
                for (let i = 0; i < userIds.length; i++) {
                    let id = userIds[i].uid;
                    var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
                    let userBookings = this.getDbRef('/users/' + userIds[i].customer + '/bookings/' + userIds[i].uid);
                    bookingref.child('driver_image').set(imgUrl);
                    userBookings.child('driver_image').set(imgUrl);
                }
            }
        });
    }
    //option Select Camera or Album
    doGetPicture() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let actionSheet = yield this.actionSheetController.create({
                buttons: [
                    {
                        text: 'Camera',
                        role: 'camera',
                        handler: () => {
                            this.openCamera();
                        },
                    },
                    {
                        text: 'Album',
                        handler: () => {
                            this.openGalary();
                        },
                    },
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        },
                    },
                ],
            });
            actionSheet.present();
        });
    }
    //End Camera and Album related work
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            var ref = this.getDbRef('/users/' + this.curUser.uid);
            ref.on('value', (_snapshot) => {
                if (_snapshot.val()) {
                    this.data = _snapshot.val();
                    this.profileImage = _snapshot.val().profile_image;
                    // this.usertype = _snapshot.val().type;
                    // console.log(this.usertype);
                    /*   if(this.usertype==this.customer){
                                        this.showCustomer= true;
                                    }else if((this.usertype==this.driver)) {
                                        this.showCustomer= false;
                                    }else{
                                      alert("no type found");
                                    }*/
                }
                else {
                    console.log('no type added');
                }
            });
        });
    }
    getDbRef(ref) {
        return this.firebaseService.getDatabase().ref(ref);
    }
};
ProfilePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
ProfilePage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-profile',
        template: _raw_loader_profile_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_profile_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], ProfilePage);



/***/ }),

/***/ 2777:
/*!*************************************************!*\
  !*** ./src/app/pages/profile/profile.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".heavy {\n  font-weight: bold;\n}\n\n.bannerterm {\n  width: 100%;\n  padding-top: 180px;\n  background: url(\"/assets/images/term.png\") no-repeat;\n  position: relative;\n  background-size: cover;\n}\n\n.aboutlogo {\n  position: absolute;\n  display: block;\n  width: auto;\n  margin: auto;\n  top: 72%;\n  left: 0;\n  right: 0;\n}\n\n.headerabout {\n  color: #2e2e2e;\n  font-size: 18px;\n  text-align: center;\n  font-family: \"PT Sans\";\n  padding-top: 1%;\n  font-weight: bold;\n}\n\n.blueline {\n  display: block !important;\n  margin: 0 auto !important;\n  width: auto !important;\n  border-radius: 0 !important;\n}\n\n.aboutpara {\n  font-family: \"PT Sans\";\n  color: #919191;\n  font-size: 16px;\n  text-align: left;\n  line-height: 18px;\n  padding: 12px 20px 0 20px;\n  margin: 0px;\n}\n\n.aboutpara span {\n  color: #222;\n}\n\n.ashrectangle, .ashrectangle-review {\n  width: 90%;\n  margin: 12px auto;\n  background: #f4f4f4;\n  border-radius: 10px;\n  border: 1px solid #d4d4d4;\n  padding: 12px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 19px;\n  font-weight: bold;\n}\n\n.liststyle {\n  width: 90%;\n  margin: 0 auto;\n  margin-top: 15px;\n}\n\n.liststyle ul {\n  margin: 0px;\n  padding: 0px;\n}\n\n.liststyle ul li {\n  list-style: none;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 20px;\n  color: #919191;\n  padding-left: 9%;\n  background: url(\"/assets/images/carbullet.png\") no-repeat;\n  margin-bottom: 20px;\n}\n\n.font {\n  font-weight: bold;\n  color: #737373;\n}\n\n.acceptBtn {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n\n.btnaccept {\n  background: #09738d !important;\n  color: white;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n.btnaccept:hover {\n  background: white !important;\n  color: black;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n@media (min-width: 568px) {\n  .headerabout {\n    padding-top: 7%;\n    font-weight: bold;\n  }\n}\n\n.review-title {\n  font-family: \"PT Sans\";\n  font-size: 23px;\n  color: #2e2e2e;\n  text-align: center;\n  margin: 0;\n  padding-top: 3%;\n}\n\nion-rating {\n  --color: gray;\n  --color-filled: green;\n}\n\n.ashrectangle-review {\n  width: 90%;\n  padding-bottom: 0px;\n  margin-bottom: 5px;\n}\n\n.no-border {\n  border: none !important;\n}\n\n.checkboxwrapper {\n  width: 53%;\n  margin: 0 auto;\n  display: block;\n}\n\n.checkboxtext {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #2e2e2e;\n}\n\n.darkbutton, .facebookbtn:hover, .facebookbtn, .googlebtn:hover, .googlebtn, .lightgreenbutton:hover, .lightgreenbutton, .bluebutton, .redbutton, .blackbutton {\n  border-radius: 10px;\n  padding: 10px 100px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.padding-back {\n  padding-right: 5px;\n}\n\n.reviewimage img {\n  padding-bottom: 10px !important;\n}\n\n.rate {\n  color: #ffae00;\n  font-size: 25px;\n  padding-bottom: 20px;\n  display: inline;\n  text-align: center;\n}\n\n.rate ul {\n  margin: 0 auto !important;\n  padding: 0;\n  display: block;\n  width: 100%;\n}\n\n@media screen and (orientation: landscape) {\n  .rate ul {\n    margin: 0 auto !important;\n    padding: 0;\n    display: block;\n    width: 100%;\n  }\n}\n\n@media (max-width: 320px) {\n  .checkboxwrapper {\n    width: 60%;\n  }\n}\n\n@media (min-width: 640px) {\n  .checkboxwrapper {\n    width: 30%;\n  }\n}\n\n.back {\n  background: url(\"/assets/images/background.png\") repeat;\n}\n\n.loginimage {\n  width: 30% !important;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 10px;\n}\n\n.formwrapper, .profileformwrapper {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n  margin-top: 15%;\n}\n\n.field {\n  padding: 20px;\n  background: #fff;\n  border: 1px solid #f9f9f9;\n  /* border-radius: 25px; */\n  color: #333131;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  width: 100%;\n  display: block;\n  margin: 0px !important;\n  border-radius: 4px 4px 0px 0px;\n  border-radius: 50px;\n}\n\n.field ::placeholder {\n  color: black !important;\n}\n\n.blackbutton {\n  width: 100%;\n}\n\n.clearbutton {\n  display: block;\n  margin-top: 10px;\n  font-size: 15px;\n  text-align: center;\n  margin-left: 100px;\n  text-transform: none !important;\n  height: 1.5em;\n}\n\n.redbutton {\n  background: #ea4335 !important;\n  padding: 25px 95px !important;\n  width: 100%;\n}\n\n.bluebutton {\n  width: 100%;\n  background: #3b5998 !important;\n  margin-bottom: 25px !important;\n}\n\n.lightgreenbutton {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 10px;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.lightgreenbutton:hover {\n  background: white !important;\n  margin-bottom: 25px !important;\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 10px;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.googlebtn {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  overflow-x: hidden;\n  padding: 15px 32px;\n}\n\n.googlebtn:hover {\n  background: #ffffff !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  overflow-x: hidden;\n  padding: 15px 32px;\n  color: #000;\n}\n\n.facebookbtn {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.facebookbtn:hover {\n  background: #fdfdfd !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n  color: #000;\n}\n\n.lightgreenbutton .login-btn {\n  text-align: center;\n  display: block;\n}\n\n.lightgreenbutton:hover {\n  background: #d3d3d3 !important;\n  color: #000;\n  font-weight: bold;\n  font-size: 15px;\n}\n\n.alert-md .alert-head {\n  padding: 10px;\n  text-align: center;\n  color: #222;\n  background: none;\n  font-family: \"PT Sans\";\n}\n\n.alert-md .alert-title {\n  font-size: 18px;\n  text-transform: uppercase;\n}\n\n.alert-md .alert-button {\n  padding: 0;\n  margin: 0 auto;\n  display: block;\n}\n\n.scroll-content {\n  margin-top: 13%;\n}\n\n.fixed-content {\n  margin-top: 13%;\n}\n\n.redlogo {\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  background: #ea4335 !important;\n  border-radius: 50px;\n}\n\n.bluelogo {\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  background: #3b5998 !important;\n  border-radius: 50px;\n}\n\n.left-float {\n  float: left;\n  margin-right: 10px;\n}\n\n.social-login {\n  margin-left: 31%;\n  margin-top: 3%;\n}\n\n.logo-text {\n  color: #fff;\n  font-weight: 700;\n  font-size: 50px;\n  text-align: center;\n  text-shadow: #000;\n}\n\n.logo-text span {\n  padding: 4px;\n  font-weight: 300 !important;\n  color: #d3d3d3;\n}\n\n.signup-container {\n  text-align: center;\n  margin-top: 22%;\n}\n\n.formwrapper input:email, .profileformwrapper input:email {\n  border-radius: 6px 6px 0px 0px;\n}\n\n.formwrapper Password, .profileformwrapper Password {\n  border-radius: 0px 0px 6px 6px;\n}\n\n.logo-font {\n  font-size: 3.4rem !important;\n}\n\n.signup-container p {\n  color: #fff;\n  margin-top: -50px;\n}\n\n.signup-container span {\n  color: #969494;\n}\n\n.login-container {\n  background-color: #000;\n}\n\n@media (min-width: 568px) {\n  .loginimage {\n    width: 15% !important;\n  }\n}\n\n.bannerprofile {\n  width: 100%;\n  background: url(\"/assets/images/profileback.png\") no-repeat;\n  background-size: cover;\n  padding-top: 20px;\n  margin-bottom: 20px;\n}\n\n.bannerprofile img {\n  display: block;\n  margin: 0 auto;\n  border-radius: 50%;\n  width: 100px;\n  height: 100px;\n  border: 2px solid gray;\n}\n\n.bannerprofile p {\n  color: #fff;\n  margin: 0;\n  padding: 5px 0 18px 0;\n  font-family: \"PT Sans\";\n  font-size: 23px;\n  text-align: center;\n}\n\n.bannerprofile p span {\n  font-size: 16px;\n}\n\n.profileformwrapper {\n  width: 85%;\n  padding: 15px;\n}\n\n.ion-button.activated {\n  background-color: #000 !important;\n}\n\n.blackbutton {\n  border-radius: 20px;\n}\n\nion-button:hover {\n  background-color: #000 !important;\n}\n\n.field {\n  border: 1px solid gray;\n  padding: 4%;\n  border-radius: 10px;\n  margin-bottom: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFx0ZXJtc1xcdGVybXMucGFnZS5zY3NzIiwicHJvZmlsZS5wYWdlLnNjc3MiLCIuLlxccmV2aWV3ZHJpdmVyXFxyZXZpZXdkcml2ZXIucGFnZS5zY3NzIiwiLi5cXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBQTtBQ0NGOztBRENBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0RBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0FDRUY7O0FEQUE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBQ0dGOztBRERBO0VBQ0UsY0FBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0FDSUY7O0FERkE7RUFDRSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtBQ0tGOztBREhBO0VBQ0Usc0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7QUNNRjs7QURKQTtFQUNFLFdBQUE7QUNPRjs7QURMQTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FDUUY7O0FETkE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FDU0Y7O0FEUEE7RUFDRSxXQUFBO0VBQ0EsWUFBQTtBQ1VGOztBRFJBO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EseURBQUE7RUFDQSxtQkFBQTtBQ1dGOztBRFRBO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0FDWUY7O0FEVkE7RUFDRSxlQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FDYUY7O0FEWEE7RUFDRSw4QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUNjRjs7QURaQTtFQUNFLDRCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ2VGOztBRFpBO0VBQ0U7SUFDRSxlQUFBO0lBQ0EsaUJBQUE7RUNlRjtBQUNGOztBQzdIQTtFQUNFLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxlQUFBO0FEK0hGOztBQzVIQTtFQUNFLGFBQUE7RUFDQSxxQkFBQTtBRCtIRjs7QUM1SEE7RUFFRSxVQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBRDhIRjs7QUMzSEE7RUFDRSx1QkFBQTtBRDhIRjs7QUMzSEE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7QUQ4SEY7O0FDM0hBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtBRDhIRjs7QUMzSEE7RUFDRSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtBRDhIRjs7QUMzSEE7RUFDRSxrQkFBQTtBRDhIRjs7QUM1SEE7RUFDRSwrQkFBQTtBRCtIRjs7QUM3SEE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLG9CQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FEZ0lGOztBQzlIQTtFQUNFLHlCQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0FEaUlGOztBQzlIQTtFQUNFO0lBQ0UseUJBQUE7SUFDQSxVQUFBO0lBQ0EsY0FBQTtJQUNBLFdBQUE7RURpSUY7QUFDRjs7QUM5SEE7RUFDRTtJQUNFLFVBQUE7RURnSUY7QUFDRjs7QUM3SEE7RUFDRTtJQUNFLFVBQUE7RUQrSEY7QUFDRjs7QUV2TkE7RUFDRSx1REFBQTtBRnlORjs7QUV2TkE7RUFDRSxxQkFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esb0JBQUE7QUYwTkY7O0FFdk5BO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtFQUNBLFlBQUE7RUFDQSxlQUFBO0FGME5GOztBRXROQTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtFQUNBLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLDhCQUFBO0VBQWdDLG1CQUFBO0FGME5sQzs7QUV4TkE7RUFDRSx1QkFBQTtBRjJORjs7QUV6TkE7RUFFRSxXQUFBO0FGMk5GOztBRXpOQTtFQUNFLGNBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7RUFDQSxhQUFBO0FGNE5GOztBRTFOQTtFQUVFLDhCQUFBO0VBQ0EsNkJBQUE7RUFDQSxXQUFBO0FGNE5GOztBRTFOQTtFQUVFLFdBQUE7RUFDQSw4QkFBQTtFQUNBLDhCQUFBO0FGNE5GOztBRTFOQTtFQUVFLDhCQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBRjRORjs7QUUxTkE7RUFFRSw0QkFBQTtFQUNBLDhCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUY0TkY7O0FFMU5BO0VBRUUsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7QUY0TkY7O0FFMU5BO0VBRUUsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FGNE5GOztBRTFOQTtFQUVFLDhCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBRjRORjs7QUUxTkE7RUFFRSw4QkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0FGNE5GOztBRTFOQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtBRjZORjs7QUUxTkE7RUFFRSw4QkFBQTtFQUNBLFdBQUE7RUFDQSxpQkFBQTtFQUNBLGVBQUE7QUY0TkY7O0FFek5BO0VBQ0UsYUFBQTtFQUNBLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7QUY0TkY7O0FFMU5BO0VBQ0UsZUFBQTtFQUNBLHlCQUFBO0FGNk5GOztBRTFOQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBRjZORjs7QUUxTkE7RUFDRSxlQUFBO0FGNk5GOztBRTNOQTtFQUNFLGVBQUE7QUY4TkY7O0FFNU5BO0VBQ0UsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FGK05GOztBRTdOQTtFQUNFLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0EsOEJBQUE7RUFDQSxtQkFBQTtBRmdPRjs7QUU3TkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7QUZnT0Y7O0FFN05BO0VBQ0UsZ0JBQUE7RUFDQSxjQUFBO0FGZ09GOztBRTdOQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FGZ09GOztBRTlOQTtFQUNFLFlBQUE7RUFDQSwyQkFBQTtFQUNBLGNBQUE7QUZpT0Y7O0FFL05BO0VBQ0Usa0JBQUE7RUFDQSxlQUFBO0FGa09GOztBRWhPQTtFQUNFLDhCQUFBO0FGbU9GOztBRWpPQTtFQUNFLDhCQUFBO0FGb09GOztBRWxPQTtFQUNFLDRCQUFBO0FGcU9GOztBRW5PQTtFQUNFLFdBQUE7RUFDQSxpQkFBQTtBRnNPRjs7QUVwT0E7RUFDRSxjQUFBO0FGdU9GOztBRXJPQTtFQUNFLHNCQUFBO0FGd09GOztBRXJPQTtFQUNFO0lBQ0UscUJBQUE7RUZ3T0Y7QUFDRjs7QUFoZEE7RUFDRSxXQUFBO0VBQ0EsMkRBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7QUFrZEY7O0FBaGRBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLFlBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7QUFtZEY7O0FBamRBO0VBQ0UsV0FBQTtFQUNBLFNBQUE7RUFDQSxxQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0FBb2RGOztBQWxkQTtFQUNFLGVBQUE7QUFxZEY7O0FBbmRBO0VBRUUsVUFBQTtFQUNBLGFBQUE7QUFxZEY7O0FBbGRBO0VBQ0UsaUNBQUE7QUFxZEY7O0FBbGRBO0VBQ0UsbUJBQUE7QUFxZEY7O0FBbGRBO0VBQ0UsaUNBQUE7QUFxZEY7O0FBbGRBO0VBQ0Usc0JBQUE7RUFDQSxXQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtBQXFkRiIsImZpbGUiOiJwcm9maWxlLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWF2eSB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLmJhbm5lcnRlcm0ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBhZGRpbmctdG9wOiAxODBweDtcclxuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy90ZXJtLnBuZ1wiKSBuby1yZXBlYXQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuLmFib3V0bG9nbyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB0b3A6IDcyJTtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcbi5oZWFkZXJhYm91dCB7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgcGFkZGluZy10b3A6IDElO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5ibHVlbGluZSB7XHJcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcclxuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xyXG59XHJcbi5hYm91dHBhcmEge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBjb2xvcjogIzkxOTE5MTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBsaW5lLWhlaWdodDogMThweDtcclxuICBwYWRkaW5nOiAxMnB4IDIwcHggMCAyMHB4O1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcbi5hYm91dHBhcmEgc3BhbiB7XHJcbiAgY29sb3I6ICMyMjI7XHJcbn1cclxuLmFzaHJlY3RhbmdsZSB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IDEycHggYXV0bztcclxuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcclxuICBwYWRkaW5nOiAxMnB4O1xyXG4gIGNvbG9yOiAjMmUyZTJlO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBsaW5lLWhlaWdodDogMTlweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4ubGlzdHN0eWxlIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuLmxpc3RzdHlsZSB1bCB7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgcGFkZGluZzogMHB4O1xyXG59XHJcbi5saXN0c3R5bGUgdWwgbGkge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gIGNvbG9yOiAjOTE5MTkxO1xyXG4gIHBhZGRpbmctbGVmdDogOSU7XHJcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2FyYnVsbGV0LnBuZ1wiKSBuby1yZXBlYXQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4uZm9udCB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgY29sb3I6ICM3MzczNzM7XHJcbn1cclxuLmFjY2VwdEJ0biB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcbi5idG5hY2NlcHR7XHJcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tbGVmdDogMTQwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuLmJ0bmFjY2VwdDpob3ZlcntcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBtYXJnaW4tbGVmdDogMTQwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xyXG4gIC5oZWFkZXJhYm91dCB7XHJcbiAgICBwYWRkaW5nLXRvcDogNyU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbn1cclxuIiwiLmhlYXZ5IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5iYW5uZXJ0ZXJtIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAxODBweDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdGVybS5wbmdcIikgbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5hYm91dGxvZ28ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogYXV0bztcbiAgbWFyZ2luOiBhdXRvO1xuICB0b3A6IDcyJTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG59XG5cbi5oZWFkZXJhYm91dCB7XG4gIGNvbG9yOiAjMmUyZTJlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBwYWRkaW5nLXRvcDogMSU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYmx1ZWxpbmUge1xuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICB3aWR0aDogYXV0byAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5hYm91dHBhcmEge1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGNvbG9yOiAjOTE5MTkxO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICBwYWRkaW5nOiAxMnB4IDIwcHggMCAyMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLmFib3V0cGFyYSBzcGFuIHtcbiAgY29sb3I6ICMyMjI7XG59XG5cbi5hc2hyZWN0YW5nbGUsIC5hc2hyZWN0YW5nbGUtcmV2aWV3IHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAxMnB4IGF1dG87XG4gIGJhY2tncm91bmQ6ICNmNGY0ZjQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGNvbG9yOiAjMmUyZTJlO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubGlzdHN0eWxlIHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5saXN0c3R5bGUgdWwge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4ubGlzdHN0eWxlIHVsIGxpIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBjb2xvcjogIzkxOTE5MTtcbiAgcGFkZGluZy1sZWZ0OiA5JTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2FyYnVsbGV0LnBuZ1wiKSBuby1yZXBlYXQ7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5mb250IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNzM3MzczO1xufVxuXG4uYWNjZXB0QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xufVxuXG4uYnRuYWNjZXB0IHtcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5idG5hY2NlcHQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xuICAuaGVhZGVyYWJvdXQge1xuICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxufVxuLnJldmlldy10aXRsZSB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBjb2xvcjogIzJlMmUyZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmctdG9wOiAzJTtcbn1cblxuaW9uLXJhdGluZyB7XG4gIC0tY29sb3I6IGdyYXk7XG4gIC0tY29sb3ItZmlsbGVkOiBncmVlbjtcbn1cblxuLmFzaHJlY3RhbmdsZS1yZXZpZXcge1xuICB3aWR0aDogOTAlO1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5uby1ib3JkZXIge1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLmNoZWNrYm94d3JhcHBlciB7XG4gIHdpZHRoOiA1MyU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmNoZWNrYm94dGV4dCB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzJlMmUyZTtcbn1cblxuLmRhcmtidXR0b24sIC5mYWNlYm9va2J0bjpob3ZlciwgLmZhY2Vib29rYnRuLCAuZ29vZ2xlYnRuOmhvdmVyLCAuZ29vZ2xlYnRuLCAubGlnaHRncmVlbmJ1dHRvbjpob3ZlciwgLmxpZ2h0Z3JlZW5idXR0b24sIC5ibHVlYnV0dG9uLCAucmVkYnV0dG9uLCAuYmxhY2tidXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiAxMHB4IDEwMHB4O1xuICBtYXJnaW46IDEwcHggYXV0bztcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6ICMyZTJlMmU7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE5cHg7XG59XG5cbi5wYWRkaW5nLWJhY2sge1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG5cbi5yZXZpZXdpbWFnZSBpbWcge1xuICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xufVxuXG4ucmF0ZSB7XG4gIGNvbG9yOiAjZmZhZTAwO1xuICBmb250LXNpemU6IDI1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJhdGUgdWwge1xuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKSB7XG4gIC5yYXRlIHVsIHtcbiAgICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xuICAuY2hlY2tib3h3cmFwcGVyIHtcbiAgICB3aWR0aDogNjAlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNjQwcHgpIHtcbiAgLmNoZWNrYm94d3JhcHBlciB7XG4gICAgd2lkdGg6IDMwJTtcbiAgfVxufVxuLmJhY2sge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kLnBuZ1wiKSByZXBlYXQ7XG59XG5cbi5sb2dpbmltYWdlIHtcbiAgd2lkdGg6IDMwJSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uZm9ybXdyYXBwZXIsIC5wcm9maWxlZm9ybXdyYXBwZXIge1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiAxNSU7XG59XG5cbi5maWVsZCB7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOWY5Zjk7XG4gIC8qIGJvcmRlci1yYWRpdXM6IDI1cHg7ICovXG4gIGNvbG9yOiAjMzMzMTMxO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA0cHggNHB4IDBweCAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbi5maWVsZCA6OnBsYWNlaG9sZGVyIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG59XG5cbi5ibGFja2J1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY2xlYXJidXR0b24ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxLjVlbTtcbn1cblxuLnJlZGJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6ICNlYTQzMzUgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMjVweCA5NXB4ICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYmx1ZWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjM2I1OTk4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbn1cblxuLmxpZ2h0Z3JlZW5idXR0b24ge1xuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xufVxuXG4ubGlnaHRncmVlbmJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xufVxuXG4uZ29vZ2xlYnRuIHtcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBwYWRkaW5nOiAxNXB4IDMycHg7XG59XG5cbi5nb29nbGVidG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIHBhZGRpbmc6IDE1cHggMzJweDtcbiAgY29sb3I6ICMwMDA7XG59XG5cbi5mYWNlYm9va2J0biB7XG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHBhZGRpbmc6IDE1cHggMzJweDtcbn1cblxuLmZhY2Vib29rYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2ZkZmRmZCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xuICBjb2xvcjogIzAwMDtcbn1cblxuLmxpZ2h0Z3JlZW5idXR0b24gLmxvZ2luLWJ0biB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5saWdodGdyZWVuYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2QzZDNkMyAhaW1wb3J0YW50O1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmFsZXJ0LW1kIC5hbGVydC1oZWFkIHtcbiAgcGFkZGluZzogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzIyMjtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xufVxuXG4uYWxlcnQtbWQgLmFsZXJ0LXRpdGxlIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uYWxlcnQtbWQgLmFsZXJ0LWJ1dHRvbiB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLnNjcm9sbC1jb250ZW50IHtcbiAgbWFyZ2luLXRvcDogMTMlO1xufVxuXG4uZml4ZWQtY29udGVudCB7XG4gIG1hcmdpbi10b3A6IDEzJTtcbn1cblxuLnJlZGxvZ28ge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6ICNlYTQzMzUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cblxuLmJsdWVsb2dvIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiAjM2I1OTk4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbi5sZWZ0LWZsb2F0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLnNvY2lhbC1sb2dpbiB7XG4gIG1hcmdpbi1sZWZ0OiAzMSU7XG4gIG1hcmdpbi10b3A6IDMlO1xufVxuXG4ubG9nby10ZXh0IHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXNoYWRvdzogIzAwMDtcbn1cblxuLmxvZ28tdGV4dCBzcGFuIHtcbiAgcGFkZGluZzogNHB4O1xuICBmb250LXdlaWdodDogMzAwICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZDNkM2QzO1xufVxuXG4uc2lnbnVwLWNvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjIlO1xufVxuXG4uZm9ybXdyYXBwZXIgaW5wdXQ6ZW1haWwsIC5wcm9maWxlZm9ybXdyYXBwZXIgaW5wdXQ6ZW1haWwge1xuICBib3JkZXItcmFkaXVzOiA2cHggNnB4IDBweCAwcHg7XG59XG5cbi5mb3Jtd3JhcHBlciBQYXNzd29yZCwgLnByb2ZpbGVmb3Jtd3JhcHBlciBQYXNzd29yZCB7XG4gIGJvcmRlci1yYWRpdXM6IDBweCAwcHggNnB4IDZweDtcbn1cblxuLmxvZ28tZm9udCB7XG4gIGZvbnQtc2l6ZTogMy40cmVtICFpbXBvcnRhbnQ7XG59XG5cbi5zaWdudXAtY29udGFpbmVyIHAge1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luLXRvcDogLTUwcHg7XG59XG5cbi5zaWdudXAtY29udGFpbmVyIHNwYW4ge1xuICBjb2xvcjogIzk2OTQ5NDtcbn1cblxuLmxvZ2luLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xuICAubG9naW5pbWFnZSB7XG4gICAgd2lkdGg6IDE1JSAhaW1wb3J0YW50O1xuICB9XG59XG4uYmFubmVycHJvZmlsZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9wcm9maWxlYmFjay5wbmdcIikgbm8tcmVwZWF0O1xuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xuICBwYWRkaW5nLXRvcDogMjBweDtcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcbn1cblxuLmJhbm5lcnByb2ZpbGUgaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBib3JkZXItcmFkaXVzOiA1MCU7XG4gIHdpZHRoOiAxMDBweDtcbiAgaGVpZ2h0OiAxMDBweDtcbiAgYm9yZGVyOiAycHggc29saWQgZ3JheTtcbn1cblxuLmJhbm5lcnByb2ZpbGUgcCB7XG4gIGNvbG9yOiAjZmZmO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmc6IDVweCAwIDE4cHggMDtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDIzcHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmJhbm5lcnByb2ZpbGUgcCBzcGFuIHtcbiAgZm9udC1zaXplOiAxNnB4O1xufVxuXG4ucHJvZmlsZWZvcm13cmFwcGVyIHtcbiAgd2lkdGg6IDg1JTtcbiAgcGFkZGluZzogMTVweDtcbn1cblxuLmlvbi1idXR0b24uYWN0aXZhdGVkIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMCAhaW1wb3J0YW50O1xufVxuXG4uYmxhY2tidXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAyMHB4O1xufVxuXG5pb24tYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMCAhaW1wb3J0YW50O1xufVxuXG4uZmllbGQge1xuICBib3JkZXI6IDFweCBzb2xpZCBncmF5O1xuICBwYWRkaW5nOiA0JTtcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcbn0iLCJAaW1wb3J0IFwiLi4vdGVybXMvL3Rlcm1zLnBhZ2Uuc2Nzc1wiO1xyXG5cclxuLnJldmlldy10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMjNweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmctdG9wOiAzJTtcclxufVxyXG5cclxuaW9uLXJhdGluZyB7XHJcbiAgLS1jb2xvcjogZ3JheTtcclxuICAtLWNvbG9yLWZpbGxlZDogZ3JlZW47XHJcbn1cclxuXHJcbi5hc2hyZWN0YW5nbGUtcmV2aWV3IHtcclxuICBAZXh0ZW5kIC5hc2hyZWN0YW5nbGU7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG5cclxuLm5vLWJvcmRlciB7XHJcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jaGVja2JveHdyYXBwZXIge1xyXG4gIHdpZHRoOiA1MyU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5jaGVja2JveHRleHQge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbn1cclxuXHJcbi5kYXJrYnV0dG9uIHtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMTAwcHg7XHJcbiAgbWFyZ2luOiAxMHB4IGF1dG87XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgYmFja2dyb3VuZDogIzJlMmUyZTtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE5cHg7XHJcbn1cclxuXHJcbi5wYWRkaW5nLWJhY2sge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcclxufVxyXG4ucmV2aWV3aW1hZ2UgaW1nIHtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5yYXRlIHtcclxuICBjb2xvcjogI2ZmYWUwMDtcclxuICBmb250LXNpemU6IDI1cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgZGlzcGxheTogaW5saW5lO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ucmF0ZSB1bCB7XHJcbiAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xyXG4gIC5yYXRlIHVsIHtcclxuICAgIG1hcmdpbjogMCBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xyXG4gIC5jaGVja2JveHdyYXBwZXIge1xyXG4gICAgd2lkdGg6IDYwJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA2NDBweCkge1xyXG4gIC5jaGVja2JveHdyYXBwZXIge1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbn1cclxuIiwiQGltcG9ydCBcIi4uL3Jldmlld2RyaXZlci9yZXZpZXdkcml2ZXIucGFnZS5zY3NzXCI7XHJcblxyXG4uYmFjayB7XHJcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvYmFja2dyb3VuZC5wbmdcIikgcmVwZWF0O1xyXG59XHJcbi5sb2dpbmltYWdlIHtcclxuICB3aWR0aDogMzAlICFpbXBvcnRhbnQ7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHg7XHJcbn1cclxuXHJcbi5mb3Jtd3JhcHBlciB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIGhlaWdodDogYXV0bztcclxuICBtYXJnaW4tdG9wOiAxNSU7XHJcbn1cclxuXHJcbi8vIC5maWVsZHtwYWRkaW5nOiAxNXB4OyBiYWNrZ3JvdW5kOiAjZjRmNGY0OyBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0OyBib3JkZXItcmFkaXVzOiAyNXB4OyBjb2xvcjogIzhkOGQ4ZDsgZm9udC1mYW1pbHk6J1BUIFNhbnMnOyBmb250LXNpemU6IDE2cHg7IHdpZHRoOiAxMDAlOyBkaXNwbGF5OiBibG9jazt9XHJcbi5maWVsZCB7XHJcbiAgcGFkZGluZzogMjBweDtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOWY5Zjk7XHJcbiAgLyogYm9yZGVyLXJhZGl1czogMjVweDsgKi9cclxuICBjb2xvcjogIzMzMzEzMTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMHB4ICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4IDRweCAwcHggMHB4OyBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG59XHJcbi5maWVsZCA6OnBsYWNlaG9sZGVyIHtcclxuICBjb2xvcjogYmxhY2sgIWltcG9ydGFudDtcclxufVxyXG4uYmxhY2tidXR0b24ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuLmNsZWFyYnV0dG9uIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW4tdG9wOiAxMHB4O1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luLWxlZnQ6IDEwMHB4O1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lICFpbXBvcnRhbnQ7XHJcbiAgaGVpZ2h0OiAxLjVlbTtcclxufVxyXG4ucmVkYnV0dG9uIHtcclxuICBAZXh0ZW5kIC5kYXJrYnV0dG9uO1xyXG4gIGJhY2tncm91bmQ6ICNlYTQzMzUgIWltcG9ydGFudDtcclxuICBwYWRkaW5nOiAyNXB4IDk1cHggIWltcG9ydGFudDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG4uYmx1ZWJ1dHRvbiB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kOiAjM2I1OTk4ICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xyXG59XHJcbi5saWdodGdyZWVuYnV0dG9uIHtcclxuICBAZXh0ZW5kIC5kYXJrYnV0dG9uO1xyXG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcclxuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIG1hcmdpbi10b3A6IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHBhZGRpbmc6IDE1cHggMzJweDtcclxufVxyXG4ubGlnaHRncmVlbmJ1dHRvbjpob3ZlciB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbWFyZ2luLXRvcDogMTBweDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgcGFkZGluZzogMTVweCAzMnB4O1xyXG59XHJcbi5nb29nbGVidG4ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgcGFkZGluZzogMTVweCAzMnB4O1xyXG59XHJcbi5nb29nbGVidG46aG92ZXIge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgYmFja2dyb3VuZDogI2ZmZmZmZiAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBvdmVyZmxvdy14OiBoaWRkZW47XHJcbiAgcGFkZGluZzogMTVweCAzMnB4O1xyXG4gIGNvbG9yOiAjMDAwO1xyXG59XHJcbi5mYWNlYm9va2J0biB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHBhZGRpbmc6IDE1cHggMzJweDtcclxufVxyXG4uZmFjZWJvb2tidG46aG92ZXIge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgYmFja2dyb3VuZDogI2ZkZmRmZCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBwYWRkaW5nOiAxNXB4IDMycHg7XHJcbiAgY29sb3I6ICMwMDA7XHJcbn1cclxuLmxpZ2h0Z3JlZW5idXR0b24gLmxvZ2luLWJ0biB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4ubGlnaHRncmVlbmJ1dHRvbjpob3ZlciB7XHJcbiAgQGV4dGVuZCAuZGFya2J1dHRvbjtcclxuICBiYWNrZ3JvdW5kOiAjZDNkM2QzICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6ICMwMDA7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG59XHJcblxyXG4uYWxlcnQtbWQgLmFsZXJ0LWhlYWQge1xyXG4gIHBhZGRpbmc6IDEwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGNvbG9yOiAjMjIyO1xyXG4gIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG59XHJcbi5hbGVydC1tZCAuYWxlcnQtdGl0bGUge1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xyXG59XHJcblxyXG4uYWxlcnQtbWQgLmFsZXJ0LWJ1dHRvbiB7XHJcbiAgcGFkZGluZzogMDtcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBkaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLnNjcm9sbC1jb250ZW50IHtcclxuICBtYXJnaW4tdG9wOiAxMyU7XHJcbn1cclxuLmZpeGVkLWNvbnRlbnQge1xyXG4gIG1hcmdpbi10b3A6IDEzJTtcclxufVxyXG4ucmVkbG9nbyB7XHJcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBjb2xvcjogI2ZmZjtcclxuICBiYWNrZ3JvdW5kOiAjZWE0MzM1ICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxufVxyXG4uYmx1ZWxvZ28ge1xyXG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XHJcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgYmFja2dyb3VuZDogIzNiNTk5OCAhaW1wb3J0YW50O1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XHJcbn1cclxuXHJcbi5sZWZ0LWZsb2F0IHtcclxuICBmbG9hdDogbGVmdDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuXHJcbi5zb2NpYWwtbG9naW4ge1xyXG4gIG1hcmdpbi1sZWZ0OiAzMSU7XHJcbiAgbWFyZ2luLXRvcDogMyU7XHJcbn1cclxuXHJcbi5sb2dvLXRleHQge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtd2VpZ2h0OiA3MDA7XHJcbiAgZm9udC1zaXplOiA1MHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB0ZXh0LXNoYWRvdzogIzAwMDtcclxufVxyXG4ubG9nby10ZXh0IHNwYW4ge1xyXG4gIHBhZGRpbmc6IDRweDtcclxuICBmb250LXdlaWdodDogMzAwICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6ICNkM2QzZDM7XHJcbn1cclxuLnNpZ251cC1jb250YWluZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXJnaW4tdG9wOiAyMiU7XHJcbn1cclxuLmZvcm13cmFwcGVyIGlucHV0OmVtYWlsIHtcclxuICBib3JkZXItcmFkaXVzOiA2cHggNnB4IDBweCAwcHg7XHJcbn1cclxuLmZvcm13cmFwcGVyIFBhc3N3b3JkIHtcclxuICBib3JkZXItcmFkaXVzOiAwcHggMHB4IDZweCA2cHg7XHJcbn1cclxuLmxvZ28tZm9udCB7XHJcbiAgZm9udC1zaXplOiAzLjRyZW0gIWltcG9ydGFudDtcclxufVxyXG4uc2lnbnVwLWNvbnRhaW5lciBwIHtcclxuICBjb2xvcjogI2ZmZjtcclxuICBtYXJnaW4tdG9wOiAtNTBweDtcclxufVxyXG4uc2lnbnVwLWNvbnRhaW5lciBzcGFuIHtcclxuICBjb2xvcjogIzk2OTQ5NDtcclxufVxyXG4ubG9naW4tY29udGFpbmVyIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAwO1xyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNTY4cHgpIHtcclxuICAubG9naW5pbWFnZSB7XHJcbiAgICB3aWR0aDogMTUlICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 79291:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/profile/profile.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--<ion-header>\r\n  <ion-toolbarbar color=\"headerblue\">\r\n    <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">PROFILE</span></ion-title>\r\n  </ion-toolbarbar>\r\n</ion-header>-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n        <ion-icon name=\"menu\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title><span class=\"heavy toptitle\">PROFILE</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <section class=\"bannerprofile\">\r\n    <!--<img src=\"assets/images/profileavataar.png\" (click)=\"doGetPicture()\">-->\r\n    <img\r\n      src=\"{{profileImage ?? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///9UWV1PVVmBhIZKUFSztbdCSE1FS09OU1dGTFBARkv8/Pzh4uJKT1RESU5NUlfKy8z39/fx8fFaX2NobG+JjI7q6+umqKqQk5VgZGjExcbV1tducnWanJ6Dhoh0eHu6vL2ho6Xc3d17foGur7GvHrXYAAAGTklEQVR4nO2d65KqOhBGJRPDHREEL4yCyvs/45HxOO4ZRQmk6WbvXlVW+TNfpdOXkHRmM4ZhGIZhGIb5ZnmK5+tNdvg4ZJv1PD4tsQdkEr+oP1LbDuXCcRxx+S1kaEfWuS587KGZIKnOF3HCekRINzrPc+wBDsOvPqOn6r5VhtFnNd2ZzPehfCXvJtLdT3Mi84NavJV3ZaEOAfZwtUky5XTU1+CoLMEesh5rLX3XeVxjD1qDUyo19TXI9IQ98K7svR76Grw99tA7kWz7TOCVcDWB1Vi47wNEO8ItsAW8Y97XQm94c2wJr9mrgQItKyK9GDfuYIGW5W6wZbSTmRB4kZhhC2nDyAx+SSRqqHVkSOBlLdbYYp6xG+5k7ng7bDmPBCYFWpYiV2z4RvU1UKuLD7q1xDucA7aknxhdhFcUqaW47J9styMpbTgat9EGSnZ6GppuP8ejUxGvhhRM7YgVtrAbOxtEoGXZVJxNCiTQslJsaVdiUwn3I3aMLe6LT5hV2CA+scU1nMwH+zuKgjvdQMTCGw6Bet+HcqRXXPwEfBeCKgzxAwZIwnaHQOoGFyquuNgCC3CF2JvgR1gjvZjpEVkhYLi/gh30fWgjRY8Xgbk90jYi3F034GjYgBwR112PW/Rngft9P4N2pRdnivudBtyVojtTuPL+Dm6hDx8ssPM2mG3En3iYApeQ9f0Nhbn3zQpZIX2Ff7+nmUF8VfvNAlXhGBF/i6qwHCFrK1EVbuBrC+RN4Rp+IUrc00PxCBUw7venfIRdDOTLGPAVMG6wmM3O4LuJZ2SFNfRCDLGPKQawH9fQNxMvgFsptkDYT8Do8b6hgDVTG/vT0wzaTPGNdDZbQyZuksKFthz0tAmJ26WAX/IJfMVvADwyROLA0AywDEYufu+ATSKVKbysRKATtDRWYUMCdAqa0IXSNUSFEVKIhd9szdupwN1F/E1g3k499LLpJ7Xpb6UuduX7QGk2s3GohMI7vmV2KeKfnH0gN1ko2iQy7t8U5ryNR6DufcbOlESKd2SvVGYkehW2kHaMzCLdGWyIh5cZisZNoFaCDr2vXiFCYqnMI8lqSBY+iQY1Q/qbKPz9307ETr8MznGIL8E7fubpr0bhZQQztVaKrW6t4W6J5jGtVJbOXrgUFfaAe1CldjdbFW5aYQ+2J3Gp3k+kVJ+TcTBPyNdb9aK9pwjVdk2yUNIhqEvbls5vmWIhbbusyWcw3fBP881KKtuVMgxDKV1bhavN/DSl6NCFJCh2VTWvql0RTCE3YxiGYRiG+Rfxl0meB1fyPFn+HTlpEsRVvc/KVSpcpaILdkPzRylXpKsy26+reIpZalJUx4+tGzXVxMIRbQWiEM6iqTQiNz0fq2IiQvPd8WwpN3woCF8jnNBVVnncka6H/aI+29FjsaulM7Kpvs5yKeVDe+BHi/9lStstazIHvr7w443z/C2Z3irDaLGJqUxlnIUuSHdPV2YEduFOGznoqYB3IsMNqrn61TYC76IUbedY1prv3TGuAVuWxHlMKDhoPyXTH4THhILziPoaHHUeU2N+8MbV1yDUYTRb3UfwN7ifaoz2o/icnRzHvzxDjtAWKynhb/6+IiqBK5AKyUDvCAX51M6yhL4x2gW7BGt2Ugyoi0wiHKBDDfUYzWi6oUAOgWcULPSGbb5NnT/ouJp55MpwaExSGkvwjpMaDRs5ER/zJ8IxmMTlrfuBmIiFMYkJSYHNLBoy1CW5NXhDpGZi/2r8SqkrjpFXTA54pcR7pIFrpjWlQP+IPTi7Keikas8Zehvap+pk7ohhyU0G349tKMM6KRu4AgPPoEs29Gewwekv8EirnmhD9n4+IcHddOpO1Dd7G6Hhuhn6OhvQ1jpm8fpVGcBdvEzSryPYkna69pOoT5ExQmtSc/RqcjpGi2Bz9Hhe4DQlI73UGPoJOPgzR2bp8WjStIy0h5lOKBhe0e5dV03JkzbISlPhCI26zaId9LfYI9ZGs62UP4266U+k3m7GCH26TaPZ9xv8WUPzaD6UuJuaK71Yqd52TTW9dRhWWgrnE5xDvVMorJAgrJAV0ocVskL6sEJWSB9WyArpwwpZIX1YISukDytkhfRhhayQPqyQFdKHFbJC+ugqtMXUsPUU7s4fU+NM+vEWhmEYhmEY5jX/ASVYkKOp66h3AAAAAElFTkSuQmCC'}}\"\r\n      (click)=\"doGetPicture()\"\r\n      onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n    />\r\n    <p>{{data.fullname}}, <span>{{data.cityname}}</span></p>\r\n  </section>\r\n  <section class=\"profileformwrapper\">\r\n    <form>\r\n      <input\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"fullname\"\r\n        placeholder=\"Fullname\"\r\n        [(ngModel)]=\"data.fullname\"\r\n      />\r\n      <br />\r\n      <!--\t<input class=\"field\" type=\"text\" name=\"Lastname\" placeholder=\"Lastname\" [(ngModel)]=\"data.lastname\">-->\r\n      <input\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"Phone\"\r\n        placeholder=\"Phone Number\"\r\n        [(ngModel)]=\"data.phone\"\r\n      />\r\n      <br />\r\n      <input\r\n        [disabled]=\"true\"\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"Email\"\r\n        placeholder=\"Email\"\r\n        [(ngModel)]=\"data.email\"\r\n      />\r\n      <br />\r\n      <input\r\n        *ngIf=\"showCustomer\"\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"Car Registration\"\r\n        placeholder=\"Car Registration\"\r\n        [(ngModel)]=\"data.registration\"\r\n      />\r\n      <input\r\n        class=\"field\"\r\n        type=\"text\"\r\n        name=\"City\"\r\n        placeholder=\"City\"\r\n        [(ngModel)]=\"data.cityname\"\r\n      />\r\n      <br />\r\n      <ion-button\r\n        color=\"dark\"\r\n        style=\"width: 100%; color: white\"\r\n        (click)=\"doAdd()\"\r\n      >\r\n        Save Profile\r\n      </ion-button>\r\n    </form>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_profile_profile_module_ts.js.map