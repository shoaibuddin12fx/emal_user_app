(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_bid-driver_bid-driver_module_ts"],{

/***/ 42498:
/*!******************************************!*\
  !*** ./src/app/Services/data.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DataService": () => (/* binding */ DataService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);


let DataService = class DataService {
    constructor() { }
};
DataService.ctorParameters = () => [];
DataService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root'
    })
], DataService);



/***/ }),

/***/ 83706:
/*!***************************************************************!*\
  !*** ./src/app/pages/bid-driver/bid-driver-routing.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BidDriverPageRoutingModule": () => (/* binding */ BidDriverPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _bid_driver_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bid-driver.page */ 50632);




const routes = [
    {
        path: '',
        component: _bid_driver_page__WEBPACK_IMPORTED_MODULE_0__.BidDriverPage
    }
];
let BidDriverPageRoutingModule = class BidDriverPageRoutingModule {
};
BidDriverPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], BidDriverPageRoutingModule);



/***/ }),

/***/ 57145:
/*!*******************************************************!*\
  !*** ./src/app/pages/bid-driver/bid-driver.module.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BidDriverPageModule": () => (/* binding */ BidDriverPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _bid_driver_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bid-driver-routing.module */ 83706);
/* harmony import */ var _bid_driver_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bid-driver.page */ 50632);







let BidDriverPageModule = class BidDriverPageModule {
};
BidDriverPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _bid_driver_routing_module__WEBPACK_IMPORTED_MODULE_0__.BidDriverPageRoutingModule
        ],
        declarations: [_bid_driver_page__WEBPACK_IMPORTED_MODULE_1__.BidDriverPage]
    })
], BidDriverPageModule);



/***/ }),

/***/ 50632:
/*!*****************************************************!*\
  !*** ./src/app/pages/bid-driver/bid-driver.page.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BidDriverPage": () => (/* binding */ BidDriverPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_bid_driver_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./bid-driver.page.html */ 27340);
/* harmony import */ var _bid_driver_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bid-driver.page.scss */ 69945);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/Services/data.service */ 42498);






let BidDriverPage = class BidDriverPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector, dataService) {
        super(injector);
        this.dataService = dataService;
        this.job = [];
        let params = this.dataService.data;
        this.job = params === null || params === void 0 ? void 0 : params.booking;
        this.bookingKey = params === null || params === void 0 ? void 0 : params.bookingKey;
        let ref = this.getDb().ref('bookings/' + this.bookingKey);
        ref.on('value', (snapshot) => {
            this.bookingData = snapshot.val();
            this.pickup = this.bookingData.pickup;
            if (this.bookingData.hasOwnProperty('drop')) {
                this.drop = this.bookingData.drop;
            }
            if (snapshot.val() != undefined && snapshot.val() != '') {
                this.job.bidding = snapshot.val().bidding;
                this.job.bidding.sort(function (a, b) {
                    return a.rate - b.rate;
                });
            }
        });
    }
    ionViewDidLoad() {
        this.job.bidding.sort(function (a, b) {
            return a.rate - b.rate;
        });
        setTimeout(() => {
            this.loadMap();
        }, 1000);
    }
    setMapInfo(lat, lng, add) {
        this.zone.run(() => {
            if (this.markerSetTo == 'pickup') {
                this.pickup.lat = lat;
                this.pickup.lng = lng;
                this.pickup.add = add;
            }
            else {
                this.drop.lat = lat;
                this.drop.lng = lng;
                this.drop.add = add;
            }
        });
    }
    calculateAndDisplayRoute(lat, lng) {
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var map = new google.maps.Map(document.getElementById('map5'), {
            zoom: 7,
            center: { lat: lat, lng: lng },
        });
        directionsDisplay.setMap(map);
        directionsService.route({
            origin: this.pickup.add,
            destination: this.drop.add,
            travelMode: 'DRIVING',
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            }
            else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
    animateMarker(lat, lng) {
        var map = new google.maps.Map(document.getElementById('map5'), {
            zoom: 13,
            center: { lat: lat, lng: lng },
        });
        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: { lat: lat, lng: lng },
        });
    }
    loadMap() {
        let location = { lat: this.pickup.lat, lng: this.pickup.lng };
        var map = new google.maps.Map(document.getElementById('map3'), {
            zoom: 15,
            center: location,
        });
        // this.map = new GoogleMap('map3', {
        //   backgroundColor: 'white',
        //   controls: {
        //     compass: false,
        //     myLocationButton: false,
        //     indoorPicker: false,
        //     zoom: true,
        //   },
        //   gestures: {
        //     scroll: true,
        //     tilt: true,
        //     rotate: true,
        //     zoom: true,
        //   },
        //   camera: {
        //     target: location,
        //     tilt: 30,
        //     zoom: 15, //,
        //     // 'bearing': 50
        //   },
        // });
        let info = this.setMapInfo;
        if (this.job.serviceType == 'Pickup') {
            this.custHeight = '210px !important';
            this.calculateAndDisplayRoute(this.pickup.lat, this.pickup.lng);
        }
        else {
            this.custHeight = '280px !important';
            this.animateMarker(this.pickup.lat, this.pickup.lng);
        }
    }
    acceptRide(data) {
        var customerData = {
            customer: this.job.customer,
            customer_name: this.job.customer_name,
            customer_image: this.job.customer_image,
            driver: data.driver,
            status: 'ASSIGNED',
            pickupAddress: this.job.pickupAddress,
            // dropAddress: this.job.dropAddress,
            tripdate: this.job.tripdate,
            serviceType: this.job.serviceType,
            //    customer_email: this.customer_email,
            corporate_booking_name: null,
            booking_type: this.bookingData.tripType,
            driver_name: data.driver_name,
            driver_image: data.driver_image,
            trip_cost: data.rate,
        };
        var driverData = {
            customer: this.job.customer,
            customer_name: this.job.customer_name,
            customer_image: this.job.customer_image,
            driver: data.driver,
            status: 'ASSIGNED',
            pickupAddress: this.job.pickupAddress,
            //  dropAddress: this.job.dropAddress,
            tripdate: this.job.tripdate,
            serviceType: this.job.serviceType,
            trip_cost: data.rate,
        };
        if (this.job.hasOwnProperty('dropAddress')) {
            driverData['dropAddress'] = this.job.dropAddress;
            customerData['dropAddress'] = this.job.dropAddress;
        }
        let ref = this.getDb().ref('bookings/' + this.bookingKey);
        this.bookingData.status = 'ASSIGNED';
        this.bookingData.trip_cost = data.rate;
        this.bookingData.estimate = data.rate;
        this.bookingData.driver = data.driver;
        this.bookingData.driver_name = data.driver_name;
        this.bookingData.driver_image = data.driver_image;
        ref.update(this.bookingData);
        var updates = {};
        updates['/users/' + data.driver + '/bookings/' + this.bookingKey] =
            driverData;
        updates['/users/' + this.bookingData.customer + '/bookings/' + this.bookingKey] = customerData;
        this.getDb().ref().update(updates);
        console.log(data);
        this.usersService
            .sendPushDriver('Your bid accepted by ' + this.job.customer_name, this.job.pickupAddress, data.driver_push_token)
            .subscribe((response) => {
            console.log('response........', response);
            this.nav.setRoot('pages/currentbookings');
        });
        this.getDb().ref('geofire').child(this.bookingKey).remove();
        //  this.navCtrl.setRoot(CurrentbookingsPage);
        //  this.usersService.sendPush('Driver is bid your trip', "Driver "+this.userProfile.fullname+" bid your trip.", this.bookingData.customer_push_token).subscribe(response => console.log(response));
    }
    ngOnDestroy() {
        console.log('page destroy successfully');
        if (this.map != undefined) {
            this.map.remove();
        }
    }
    ngOnInit() { }
    getDb() {
        return this.firebaseService.getDatabase();
    }
};
BidDriverPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector },
    { type: src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_3__.DataService }
];
BidDriverPage = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-bid-driver',
        template: _raw_loader_bid_driver_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_bid_driver_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], BidDriverPage);



/***/ }),

/***/ 69945:
/*!*******************************************************!*\
  !*** ./src/app/pages/bid-driver/bid-driver.page.scss ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("page-bid-driver .ul.rating[_ngcontent-hdo-40] li[_ngcontent-hdo-40] {\n  padding: 7px 0px !important;\n}\npage-bid-driver .driver-card {\n  border-top: 1px solid #ccc;\n}\npage-bid-driver .topnotification-fare {\n  margin: 0px;\n}\npage-bid-driver .driver-section {\n  font-size: 2.2rem;\n}\npage-bid-driver .marker {\n  margin-right: 10px;\n}\npage-bid-driver .marker img {\n  margin-top: 10%;\n}\npage-bid-driver .marker-p {\n  margin: 7px;\n  font-size: 14px;\n  margin-top: 5px;\n}\npage-bid-driver .location {\n  font-size: 15px;\n  margin: 8px;\n}\npage-bid-driver .acceptBtns {\n  background: #191b1a;\n  width: 100%;\n  padding: 8px 0;\n  font-family: \"PT Sans\";\n  font-size: 15px;\n  text-transform: capitalize;\n  font-weight: normal;\n  color: #fff;\n}\npage-bid-driver .bid-price {\n  font-weight: 700;\n  font-size: 18px;\n  margin: 2px;\n  margin-top: 7px;\n  margin-left: 8px;\n}\npage-bid-driver .rate {\n  color: #ffae00;\n  font-size: 12px;\n  display: inline;\n  text-align: left;\n}\npage-bid-driver .map3 {\n  height: 250px;\n}\npage-bid-driver .map4 {\n  height: 387px;\n}\npage-bid-driver ul.rating li {\n  display: inline;\n  border: 0px;\n  background: none;\n  padding: 5px !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImJpZC1kcml2ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNFO0VBQ0UsMkJBQUE7QUFBSjtBQUVFO0VBU0UsMEJBQUE7QUFSSjtBQVVFO0VBQ0UsV0FBQTtBQVJKO0FBVUU7RUFFRSxpQkFBQTtBQVRKO0FBV0U7RUFDRSxrQkFBQTtBQVRKO0FBV0U7RUFDRSxlQUFBO0FBVEo7QUFXRTtFQUNFLFdBQUE7RUFDQSxlQUFBO0VBQ0EsZUFBQTtBQVRKO0FBV0U7RUFDRSxlQUFBO0VBQ0EsV0FBQTtBQVRKO0FBV0U7RUFDRSxtQkFBQTtFQUNBLFdBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsMEJBQUE7RUFDQSxtQkFBQTtFQUNBLFdBQUE7QUFUSjtBQVdFO0VBQ0UsZ0JBQUE7RUFDQSxlQUFBO0VBQ0EsV0FBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQVRKO0FBWUU7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQVZKO0FBWUU7RUFDRSxhQUFBO0FBVko7QUFZRTtFQUNFLGFBQUE7QUFWSjtBQWFFO0VBQ0UsZUFBQTtFQUNBLFdBQUE7RUFDQSxnQkFBQTtFQUNBLHVCQUFBO0FBWEoiLCJmaWxlIjoiYmlkLWRyaXZlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwYWdlLWJpZC1kcml2ZXIge1xyXG4gIC51bC5yYXRpbmdbX25nY29udGVudC1oZG8tNDBdIGxpW19uZ2NvbnRlbnQtaGRvLTQwXSB7XHJcbiAgICBwYWRkaW5nOiA3cHggMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC5kcml2ZXItY2FyZCB7XHJcbiAgICAvLyAgICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgLy8gdG9wOiAxMDtcclxuXHJcbiAgICAvLyBsZWZ0OiAxNSU7XHJcbiAgICAvLyBtYXJnaW4tbGVmdDoxNSU7XHJcbiAgICAvLyAgbWFyZ2luLXRvcDoyJTtcclxuICAgIC8vICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG4gICAgLy8gd2lkdGg6IDcwJTtcclxuICAgIGJvcmRlci10b3A6IDFweCBzb2xpZCAjY2NjO1xyXG4gIH1cclxuICAudG9wbm90aWZpY2F0aW9uLWZhcmUge1xyXG4gICAgbWFyZ2luOiAwcHg7XHJcbiAgfVxyXG4gIC5kcml2ZXItc2VjdGlvbiB7XHJcbiAgICAvLyBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBmb250LXNpemU6IDIuMnJlbTtcclxuICB9XHJcbiAgLm1hcmtlciB7XHJcbiAgICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbiAgfVxyXG4gIC5tYXJrZXIgaW1nIHtcclxuICAgIG1hcmdpbi10b3A6IDEwJTtcclxuICB9XHJcbiAgLm1hcmtlci1wIHtcclxuICAgIG1hcmdpbjogN3B4O1xyXG4gICAgZm9udC1zaXplOiAxNHB4O1xyXG4gICAgbWFyZ2luLXRvcDogNXB4O1xyXG4gIH1cclxuICAubG9jYXRpb24ge1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgbWFyZ2luOiA4cHg7XHJcbiAgfVxyXG4gIC5hY2NlcHRCdG5zIHtcclxuICAgIGJhY2tncm91bmQ6ICMxOTFiMWE7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIHBhZGRpbmc6IDhweCAwO1xyXG4gICAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gICAgZm9udC1zaXplOiAxNXB4O1xyXG4gICAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XHJcbiAgICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gICAgY29sb3I6ICNmZmY7XHJcbiAgfVxyXG4gIC5iaWQtcHJpY2Uge1xyXG4gICAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICAgIGZvbnQtc2l6ZTogMThweDtcclxuICAgIG1hcmdpbjogMnB4O1xyXG4gICAgbWFyZ2luLXRvcDogN3B4O1xyXG4gICAgbWFyZ2luLWxlZnQ6IDhweDtcclxuICB9XHJcblxyXG4gIC5yYXRlIHtcclxuICAgIGNvbG9yOiAjZmZhZTAwO1xyXG4gICAgZm9udC1zaXplOiAxMnB4O1xyXG4gICAgZGlzcGxheTogaW5saW5lO1xyXG4gICAgdGV4dC1hbGlnbjogbGVmdDtcclxuICB9XHJcbiAgLm1hcDMge1xyXG4gICAgaGVpZ2h0OiAyNTBweDtcclxuICB9XHJcbiAgLm1hcDQge1xyXG4gICAgaGVpZ2h0OiAzODdweDtcclxuICB9XHJcblxyXG4gIHVsLnJhdGluZyBsaSB7XHJcbiAgICBkaXNwbGF5OiBpbmxpbmU7XHJcbiAgICBib3JkZXI6IDBweDtcclxuICAgIGJhY2tncm91bmQ6IG5vbmU7XHJcbiAgICBwYWRkaW5nOiA1cHggIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 27340:
/*!*********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/bid-driver/bid-driver.page.html ***!
  \*********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the BidDriverPage page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!--\r\n  Generated template for the Fare page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header>\r\n\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n        <ion-icon name=\"menu\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title><span class=\"heavy toptitle textCenter\">Driver`s Price List</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <!--<section class=\"topnotification-fare\">-->\r\n  <!--<ion-grid class=\"no-padding\">-->\r\n  <!--<ion-row>-->\r\n  <!--<ion-col width-20 class=\"marker\">-->\r\n  <!--<img src=\"assets/images/car.png\">-->\r\n  <!--</ion-col>-->\r\n\r\n  <!--<ion-col width-80 class=\"no-padding\">-->\r\n  <!--<p class=\"loaction\">{{job.pickupAddress}}</p>-->\r\n\r\n  <!--</ion-col>-->\r\n  <!--</ion-row>-->\r\n  <!--<ion-row *ngIf=\"job != undefined && job.hasOwnProperty('dropAddress')\">-->\r\n  <!--<ion-col width-20 class=\"marker\">-->\r\n  <!--</ion-col>-->\r\n\r\n  <!--<ion-col width-80 class=\"no-padding\">-->\r\n  <!--<p class=\"loaction\">{{job.dropAddress}}</p>-->\r\n  <!--</ion-col>-->\r\n  <!--</ion-row>-->\r\n  <!--</ion-grid>-->\r\n  <!--</section>-->\r\n\r\n  <section>\r\n    <div id=\"map5\" data-tap-disabled=\"true\" class=\"map3\" *ngIf=\"this.PickupService == true\">\r\n    </div>\r\n    <div id=\"map5\" data-tap-disabled=\"true\" class=\"map4\" *ngIf=\"this.PickupService != true\">\r\n    </div>\r\n  </section>\r\n\r\n  <section class=\"driver-section\">\r\n    <p style=\"text-align: center;\"><strong>Drivers biding for your ride</strong></p>\r\n    <ion-grid class=\"no-padding driver-card\" *ngFor=\"let bid of job.bidding; let i = index;\">\r\n\r\n      <ion-card>\r\n        <ion-item lines=\"none\">\r\n          <ion-avatar style=\"width: 60px; height: 60px;\">\r\n            <img src=\"{{bid.driver_image != '' ? bid.driver_image : 'assets/images/notfound.png'}}\"\r\n              onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\">\r\n          </ion-avatar>\r\n          <ion-label class=\"ion-margin-start\">\r\n            <h2 class=\"marker-p\"> {{bid.driver_name}} </h2>\r\n            <p class=\"bid-price\"> ${{bid.rate.toFixed(2)}}</p>\r\n\r\n            <div>\r\n              <ion-icon *ngFor=\"let n of [1,2,3,4,5]\" [name]=\"bid.driver_rating.rating <= n ? 'star-outline' : 'star'\"\r\n                [color]=\"bid.driver_rating.rating <= n ? 'tertiary' : ''   \"></ion-icon>\r\n\r\n            </div>\r\n\r\n            <!-- <rating [(ngModel)]=\"bid.driver_rating\" name=\"driver_rating\" ngDefaultControl max=\"5\" readOnly=\"true\"\r\n              emptyStarIconName=\"star-outline\">\r\n            </rating> -->\r\n          </ion-label>\r\n          <ion-button (click)=\"acceptRide(bid)\">\r\n            Accept\r\n          </ion-button>\r\n        </ion-item>\r\n      </ion-card>\r\n      <!-- <ion-row *ngIf=\"i < 4\">\r\n\r\n\r\n        <ion-col width-25 class=\"\" style=\"height: 80px;\">\r\n          <img src=\"{{bid.driver_image != '' ? bid.driver_image : 'assets/images/notfound.png'}}\" alt=\"Drive Image\">\r\n\r\n        </ion-col>\r\n\r\n        <ion-col width-75 class=\"\">\r\n          <ion-row>\r\n            <ion-col width-40 class=\"marker\">\r\n              <p class=\"marker-p\"> {{bid.driver_name}} </p>\r\n            </ion-col>\r\n\r\n            <ion-col width-60 style=\"padding-top: 8px;\">\r\n              <div class=\"rate\">\r\n                <rating [(ngModel)]=\"bid.driver_rating\" name=\"driver_rating\" ngDefaultControl max=\"5\" readOnly=\"true\"\r\n                  emptyStarIconName=\"star-outline\">\r\n                </rating>\r\n              </div>\r\n            </ion-col>\r\n          </ion-row>\r\n          <ion-row>\r\n            <ion-col width-50>\r\n              <p class=\"bid-price\"> ${{bid.rate.toFixed(2)}}</p>\r\n\r\n            </ion-col>\r\n\r\n            <ion-col width-50 style=\"text-align:right;\">\r\n              <button class=\"acceptBtns\" (click)=\"acceptRide(bid)\">Accept</button>\r\n            </ion-col>\r\n\r\n          </ion-row>\r\n        </ion-col>\r\n\r\n      </ion-row> -->\r\n\r\n    </ion-grid>\r\n  </section>\r\n\r\n\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_bid-driver_bid-driver_module_ts.js.map