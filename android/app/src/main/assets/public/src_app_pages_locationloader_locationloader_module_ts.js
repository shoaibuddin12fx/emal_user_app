(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_locationloader_locationloader_module_ts"],{

/***/ 86727:
/*!***********************************************************************!*\
  !*** ./src/app/pages/locationloader/locationloader-routing.module.ts ***!
  \***********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LocationloaderPageRoutingModule": () => (/* binding */ LocationloaderPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _locationloader_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./locationloader.page */ 95508);




const routes = [
    {
        path: '',
        component: _locationloader_page__WEBPACK_IMPORTED_MODULE_0__.LocationloaderPage
    }
];
let LocationloaderPageRoutingModule = class LocationloaderPageRoutingModule {
};
LocationloaderPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LocationloaderPageRoutingModule);



/***/ }),

/***/ 76147:
/*!***************************************************************!*\
  !*** ./src/app/pages/locationloader/locationloader.module.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LocationloaderPageModule": () => (/* binding */ LocationloaderPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _locationloader_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./locationloader-routing.module */ 86727);
/* harmony import */ var _locationloader_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./locationloader.page */ 95508);







let LocationloaderPageModule = class LocationloaderPageModule {
};
LocationloaderPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _locationloader_routing_module__WEBPACK_IMPORTED_MODULE_0__.LocationloaderPageRoutingModule
        ],
        declarations: [_locationloader_page__WEBPACK_IMPORTED_MODULE_1__.LocationloaderPage]
    })
], LocationloaderPageModule);



/***/ }),

/***/ 95508:
/*!*************************************************************!*\
  !*** ./src/app/pages/locationloader/locationloader.page.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LocationloaderPage": () => (/* binding */ LocationloaderPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_locationloader_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./locationloader.page.html */ 57608);
/* harmony import */ var _locationloader_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./locationloader.page.scss */ 71624);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);





let LocationloaderPage = class LocationloaderPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.injector = injector;
        this.image = 'assets/images/splash.png';
        this.msg = 'Getting Location...';
        this.platform.ready().then(() => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.initialize();
        }));
    }
    ngAfterViewInit() {
        console.log('size of content', this.platform.width(), this.platform.height());
    }
    ionViewDidEnter() {
        this.platform.ready().then(() => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            this.initialize();
        }));
    }
    ngOnInit() { }
    initialize() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            var self = this;
            this.geolocation.getCurrentPosition().then((loc) => {
                console.log(loc);
                if (loc) {
                    clearInterval(this.timer);
                    self.msg = 'Setting Contrycode by reverse geocoding...';
                    setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
                        const res = yield self.setCountryCodeViaLocation(loc);
                        self.reverseGeoCode(res);
                    }), 1000);
                }
                else {
                    // alert("Hello world")
                    self.msg =
                        'Getting Location permission, please allow it in your phone settings';
                    this.timer = setInterval(() => {
                        this.initialize();
                    }, 1000);
                }
            }),
                (reason) => {
                    self.msg = reason === null || reason === void 0 ? void 0 : reason.toString();
                    //alert("Rejected : "+ reason);
                };
            // this.utility.presentFailureToast('Location permission required, please allow ');
            // const user = await this.firebaseService.getCurrentUser();
            // if (user) {
            //   this.usersService.curUser = user;
            //   const loc = await this.geolocation.getCurrentPosition();
            //   if (loc) {
            //     const res = await this.setCountryCodeViaLocation(loc);
            //     this.reverseGeoCode(res);
            //   } else {
            //     this.utility.presentFailureToast('Error getting location');
            //     this.reverseGeoCode({ lat: -33.865143, lng: 151.2099, add: null });
            //   }
            // } else {
            //   this.nav.setRoot('pages/login');
            // }
        });
    }
    setCountryCodeViaLocation(position) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => {
                console.log('position', position);
                const res = this.geolocation.getAddressFromCoords(position.coords.latitude, position.coords.longitude);
                if (res) {
                    if (res[0] != undefined && res[0].hasOwnProperty('countryCode')) {
                        this.mapService.currentCountrycode = res[0].countryCode;
                    }
                }
                resolve(res);
            });
        });
    }
    isUserLoggedIn() {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const user = yield this.firebaseService.getCurrentUser();
            if (user) {
                this.usersService.curUser = user;
                resolve(user);
                // const loc = await this.geolocation.getCurrentPosition();
                // if (loc) {
                //   const res = await this.setCountryCodeViaLocation(loc);
                //   this.reverseGeoCode(res);
                // } else {
                //   this.utility.presentFailureToast('Error getting location');
                //   this.reverseGeoCode({ lat: -33.865143, lng: 151.2099, add: null });
                // }
            }
            else {
                // this.nav.setRoot('pages/login');
                resolve(null);
            }
        }));
    }
    reverseGeoCode(obj) {
        var _a;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            console.log('reverseGeoCode reached');
            console.log(obj);
            var self = this;
            self.msg = 'Getting User Information ...';
            const user = yield this.firebaseService.getCurrentUser(); // await this.isUserLoggedIn();
            if (user) {
                this.usersService.curUser = user;
            }
            else {
                self.nav.setRoot('pages/login');
                return;
            }
            const ref = self.firebaseService.getDatabase().ref('users/' + user.uid);
            if (obj)
                ref.child('location').set({
                    lat: obj.lat,
                    lng: obj.lng,
                    add: (_a = obj.add) !== null && _a !== void 0 ? _a : '',
                });
            ref.once('value', (snapshot) => {
                self.msg = 'User Found, Checking profile ...';
                if (snapshot.val().phone) {
                    if (!user['emailVerified'] && !snapshot.val().authToken) {
                        user.sendEmailVerification().then(function () {
                            self.utility.presentFailureToast('Email has been sent. Please verify your account first.');
                            self.firebaseService.fireAuth.signOut();
                            self.nav.setRoot('pages/login');
                        }, function (error) {
                            self.utility.presentFailureToast('Email has been sent. Please verify your account first.');
                            console.log('COuld not send Verification email', error.toString());
                            self.nav.setRoot('pages/login');
                        });
                    }
                    else {
                        self.msg = 'Set Home Page ...';
                        this.nav.setRoot('pages/home', {
                            lat: obj.lat,
                            lng: obj.lng,
                            add: obj.add,
                        });
                    }
                }
                else {
                    self.msg = 'Register Customer ...';
                    this.nav.setRoot('pages/registercustomer');
                }
            });
        });
    }
};
LocationloaderPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
LocationloaderPage = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-locationloader',
        template: _raw_loader_locationloader_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_locationloader_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LocationloaderPage);



/***/ }),

/***/ 71624:
/*!***************************************************************!*\
  !*** ./src/app/pages/locationloader/locationloader.page.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("page-locationloader .locationLoaderPage {\n  background: #000;\n}\npage-locationloader .locationLoaderPage img {\n  width: 100%;\n  margin: 0px auto;\n  padding-top: 30%;\n  display: block;\n}\npage-locationloader .locationLoaderPage .animated {\n  width: 100%;\n  margin: 0px auto;\n  margin-bottom: 35px;\n}\npage-locationloader .locationLoaderPage .getloc {\n  font-family: \"PT Sans\";\n  font-size: 5vw;\n  color: #fff;\n  text-align: center;\n  font-style: italic;\n}\n/* landscape */\n@media screen and (orientation: landscape) {\n  .locationLoaderPage img {\n    padding-top: 0% !important;\n    width: 50% !important;\n  }\n}\n@media screen and (min-width: 768px) and (orientation: landscape) {\n  .locationLoaderPage img {\n    padding-top: 5% !important;\n    width: 70% !important;\n  }\n}\n.map-bg {\n  background: url(/assets/images/mapbg.jpg) 100%/auto;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImxvY2F0aW9ubG9hZGVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFDRTtFQUNFLGdCQUFBO0FBQUo7QUFDSTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQUNOO0FBQ0k7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQUNOO0FBQ0k7RUFDRSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQUNOO0FBSUEsY0FBQTtBQUNBO0VBRUk7SUFDRSwwQkFBQTtJQUNBLHFCQUFBO0VBRko7QUFDRjtBQU1BO0VBRUk7SUFDRSwwQkFBQTtJQUNBLHFCQUFBO0VBTEo7QUFDRjtBQVNBO0VBQ0UsbURBQUE7QUFQRiIsImZpbGUiOiJsb2NhdGlvbmxvYWRlci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJwYWdlLWxvY2F0aW9ubG9hZGVyIHtcclxuICAubG9jYXRpb25Mb2FkZXJQYWdlIHtcclxuICAgIGJhY2tncm91bmQ6ICMwMDA7XHJcbiAgICBpbWcge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgbWFyZ2luOiAwcHggYXV0bztcclxuICAgICAgcGFkZGluZy10b3A6IDMwJTtcclxuICAgICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB9XHJcbiAgICAuYW5pbWF0ZWQge1xyXG4gICAgICB3aWR0aDogMTAwJTtcclxuICAgICAgbWFyZ2luOiAwcHggYXV0bztcclxuICAgICAgbWFyZ2luLWJvdHRvbTogMzVweDtcclxuICAgIH1cclxuICAgIC5nZXRsb2Mge1xyXG4gICAgICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgICAgIGZvbnQtc2l6ZTogNXZ3O1xyXG4gICAgICBjb2xvcjogI2ZmZjtcclxuICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgICBmb250LXN0eWxlOiBpdGFsaWM7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG4vKiBsYW5kc2NhcGUgKi9cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpIHtcclxuICAubG9jYXRpb25Mb2FkZXJQYWdlIHtcclxuICAgIGltZyB7XHJcbiAgICAgIHBhZGRpbmctdG9wOiAwJSAhaW1wb3J0YW50O1xyXG4gICAgICB3aWR0aDogNTAlICFpbXBvcnRhbnQ7XHJcbiAgICB9XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAobWluLXdpZHRoOiA3NjhweCkgYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKSB7XHJcbiAgLmxvY2F0aW9uTG9hZGVyUGFnZSB7XHJcbiAgICBpbWcge1xyXG4gICAgICBwYWRkaW5nLXRvcDogNSUgIWltcG9ydGFudDtcclxuICAgICAgd2lkdGg6IDcwJSAhaW1wb3J0YW50O1xyXG4gICAgfVxyXG4gIH1cclxufVxyXG5cclxuLm1hcC1iZyB7XHJcbiAgYmFja2dyb3VuZDogdXJsKC9hc3NldHMvaW1hZ2VzL21hcGJnLmpwZykgMTAwJSAvIGF1dG87XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 57608:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/locationloader/locationloader.page.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Locationloader page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n\r\n<!-- <ion-content class=\"locationLoaderPage\">\r\n\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-title>\r\n        Ionic 5 Geolocation Demo\r\n      </ion-title>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n\r\n  <ion-content>\r\n    <ion-button (click)=\"geoInformation()\" color=\"danger\">\r\n      Get Device Location\r\n    </ion-button>\r\n\r\n    <ion-item>\r\n      <strong>Location Information</strong>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label>Latitue</ion-label>\r\n      <ion-badge color=\"secondary\">{{lat}}</ion-badge>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label>Longitude</ion-label>\r\n      <ion-badge color=\"tertiary\">{{long}}</ion-badge>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <strong>Address</strong>\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      {{ address }}\r\n    </ion-item>\r\n\r\n    <ion-item>\r\n      <ion-label><strong>Accuracy</strong></ion-label>\r\n      <ion-badge>{{ accuracy }}</ion-badge>\r\n    </ion-item>\r\n  </ion-content>\r\n\r\n</ion-content> -->\r\n\r\n<!--<img src=\"assets/images/supportlogo.png\" alt=\"Support Logo\"/>-->\r\n<!-- <img src=\"assets/images/splash.png\" alt=\"Front Loader\" /> -->\r\n<!--<img src=\"assets/images/loading.png\"/>-->\r\n<!-- <p class=\"getloc\">Getting Your Location...</p> -->\r\n<!--\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-title></ion-title>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n<ion-content #content class=\"map-bg\">\r\n  <img src=\"assets/images/mapbg.jpg\" alt=\"...\" />\r\n  <p>{{msg}}</p>\r\n  <!-- <img class=\"box-image\" src=\"assets/images/mapbg.jpg\" /> -->\r\n  <!-- <ion-card>\r\n    <ion-card-header>\r\n      <ion-card-subtitle>Getting Your Location...</ion-card-subtitle>\r\n    </ion-card-header>\r\n  </ion-card> -->\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_locationloader_locationloader_module_ts.js.map