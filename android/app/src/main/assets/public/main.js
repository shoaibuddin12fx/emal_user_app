(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["main"],{

/***/ 98255:
/*!*******************************************************!*\
  !*** ./$_lazy_route_resources/ lazy namespace object ***!
  \*******************************************************/
/***/ ((module) => {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(() => {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = () => ([]);
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 98255;
module.exports = webpackEmptyAsyncContext;

/***/ }),

/***/ 13126:
/*!**********************************************!*\
  !*** ./src/app/Services/firebase.service.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FirebaseService": () => (/* binding */ FirebaseService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/fire/compat/auth */ 1325);
/* harmony import */ var _angular_fire_compat_database__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/fire/compat/database */ 79978);
/* harmony import */ var _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/fire/compat/storage */ 43893);





let FirebaseService = class FirebaseService {
    constructor(fireAuth, firebase, fireStorage) {
        this.fireAuth = fireAuth;
        this.firebase = firebase;
        this.fireStorage = fireStorage;
    }
    getCurrentUser() {
        return new Promise((res) => (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            var user = yield this.fireAuth.currentUser;
            if (user && user.uid)
                res(user);
            else
                res(null);
        }));
    }
    getDatabase() {
        return this.firebase.database;
    }
    signOut() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            yield this.fireAuth.signOut();
        });
    }
};
FirebaseService.ctorParameters = () => [
    { type: _angular_fire_compat_auth__WEBPACK_IMPORTED_MODULE_1__.AngularFireAuth },
    { type: _angular_fire_compat_database__WEBPACK_IMPORTED_MODULE_2__.AngularFireDatabase },
    { type: _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_3__.AngularFireStorage }
];
FirebaseService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root',
    })
], FirebaseService);



/***/ }),

/***/ 40929:
/*!*************************************************!*\
  !*** ./src/app/Services/geolocation.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeolocationService": () => (/* binding */ GeolocationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ 83046);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _capacitor_geolocation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @capacitor/geolocation */ 61091);


// import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';



let GeolocationService = class GeolocationService {
    constructor(platform, 
    // private geolocation: Geolocation,
    nativeGeocoder) {
        this.platform = platform;
        this.nativeGeocoder = nativeGeocoder;
    }
    getCurrentPosition() {
        return new Promise((resolve, reject) => {
            _capacitor_geolocation__WEBPACK_IMPORTED_MODULE_1__.Geolocation.getCurrentPosition().then((res) => {
                resolve(res);
            }, (err) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
                reject(err);
                // console.log(err);
                // const res = await this.getCoordsForGeoAddress('706 Timber Branch Drive, Alexandria, VA, USA')
                // resolve(res);
            }));
        });
    }
    getCoordsForGeoAddress(address, _default = true) {
        const self = this;
        return new Promise((resolve) => {
            const self = this;
            const geocoder = new google.maps.Geocoder();
            geocoder.geocode({ address }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        const loc = results[0].geometry.location;
                        const lat = loc.lat();
                        const lng = loc.lng();
                        resolve({ lat, lng, coords: { latitude: lat, longitude: lng } });
                    }
                    else {
                        resolve(null);
                    }
                }
                else {
                    console.log({ results, status });
                    resolve(null);
                }
            });
        });
    }
    getCoordsViaHTML5Navigator() {
        return new Promise((resolve) => {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    resolve(pos);
                }, () => {
                    resolve({ lat: 51.5074, lng: 0.1278 });
                });
            }
            else {
                // Browser doesn't support Geolocation
                resolve({ lat: 51.5074, lng: 0.1278 });
            }
        });
    }
    getCurrentLocationCoordinates() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            let user = yield this.getCurrentPosition();
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
                _capacitor_geolocation__WEBPACK_IMPORTED_MODULE_1__.Geolocation.getCurrentPosition().then(position => {
                    const lt = position.coords.latitude;
                    const lg = position.coords.longitude;
                    resolve({ lat: lt, lng: lg });
                }).catch((reason) => {
                    console.log("getCurrentPosition", reason);
                });
            }));
        });
    }
    getAddressFromCoords(lat, lng) {
        return new Promise((resolve) => {
            // if (this.platform.is('cordova')) {
            //   this.nativeGeocoder
            //     .reverseGeocode(lat, lng)
            //     .then((result) => {
            //       console.log(result);
            //       if (result[0] != undefined) {
            //         const obj = {
            //           lat,
            //           lng,
            //           add: result[0]['formatted_address']
            //         }
            //         resolve(obj);
            //       } else {
            //         resolve(null);
            //       }
            //     })
            //     .catch((error: any) => {
            //       console.log('reverseGeocode Error', error);
            //       resolve(null);
            //     });
            // } else {
            var geocoder = new google.maps.Geocoder();
            geocoder
                .geocode({ location: { lat, lng } })
                .then((response) => {
                if (response.results[0]) {
                    const obj = {
                        lat,
                        lng,
                        add: response.results[0].formatted_address
                    };
                    resolve(obj);
                }
                else {
                    resolve(null);
                }
            })
                .catch((e) => {
                console.log('Geocoder failed due to: ' + e);
                resolve(null);
            });
            //   }
        });
    }
};
GeolocationService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform },
    { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_0__.NativeGeocoder }
];
GeolocationService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root',
    })
], GeolocationService);



/***/ }),

/***/ 57775:
/*!*****************************************!*\
  !*** ./src/app/Services/map-service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MapService": () => (/* binding */ MapService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/add/operator/map */ 16137);



let MapService = class MapService {
    constructor() {
        this.currentCountrycode = 'au';
        this.showmap = true;
    }
};
MapService.ctorParameters = () => [];
MapService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)()
], MapService);



/***/ }),

/***/ 21521:
/*!******************************************!*\
  !*** ./src/app/Services/user-service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserService": () => (/* binding */ UserService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var rxjs_add_operator_map__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/add/operator/map */ 16137);
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ 19342);
/* harmony import */ var _firebase_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./firebase.service */ 13126);






let UserService = class UserService {
    constructor(http, googlePlus, 
    // public fireAuth: AngularFireAuth,
    // public firebase: AngularFireDatabase
    firebase) {
        this.http = http;
        this.googlePlus = googlePlus;
        this.firebase = firebase;
        this.loginType = '';
        this.userProfile = this.firebase.getDatabase().ref('users');
        this.reviewApp = firebase.getDatabase().ref('review');
        this.onInit();
    }
    onInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            let user = yield this.firebase.getCurrentUser();
            if (user)
                this.ref = this.firebase
                    .getDatabase()
                    .ref('/users/' + user['uid'] + '/email');
        });
    }
    //SignUpUsers
    signUpUser(fullname, email, password, phone, cityname, vehicle) {
        return this.firebase.fireAuth
            .createUserWithEmailAndPassword(email, password)
            .then((newUser) => {
            //sign in the user
            this.firebase.fireAuth
                .signInWithEmailAndPassword(email, password)
                .then((authenticatedUser) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
                //successful login, create user profile
                //new added by me
                authenticatedUser.user.sendEmailVerification().then(function () {
                    console.log('please check email');
                }, function (error) {
                    // An error happened.
                });
                this.userProfile.child(authenticatedUser.user.uid).set({
                    fullname: fullname,
                    email: email,
                    phone: phone,
                    cityname: cityname,
                    // vehicle: vehicle,
                    profile_image: 'https://firebasestorage.googleapis.com/v0/b/pickmyride-9f86d.appspot.com/o/images%2Fdefault.png?alt=media&token=f9177f5d-4f9b-4d11-a668-db0674a44e32',
                    customer: true,
                    islogin: 'customer',
                });
            }));
        });
    }
    insertUser(fullname, email, phone, cityname, vehicle, AuthUser) {
        return this.userProfile.child(AuthUser.uid).update({
            profile_image: AuthUser.photoURL,
            fullname: fullname,
            email: email,
            phone: phone,
            cityname: cityname,
            vehicle: vehicle,
            customer: true,
        });
    }
    //LoginUser
    loginUser(email, password) {
        //return this.fireAuth.signInWithEmailAndPassword(email, password);
        return this.firebase.fireAuth
            .signInWithEmailAndPassword(email, password)
            .then((authenticatedUser) => {
            console.log(this.userProfile);
            this.userProfile.child(authenticatedUser.user.uid).update({
                customer: true,
                islogin: 'customer',
                customer_push_token: localStorage.getItem('FCM_TOKEN'),
                pushToken: localStorage.getItem('FCM_TOKEN'),
            });
        });
    }
    //facebook insert data on database
    signFacebook(success) {
        this.userProfile.child(success.uid).set({
            // displayname:success.displayName,
            //  provider:success.providerData[0].providerId,
            email: success.email,
        });
    }
    //Google insert Data on database
    signGoogle(success, provider) {
        this.userProfile.child(success.uid).set({
            //displayname:success.displayName
            //  provider:provider.provider,
            email: success.email,
        });
    }
    getToken() {
        return localStorage.getItem('FCM_TOKEN');
    }
    setToken(token) {
        localStorage.setItem('FCM_TOKEN', token);
    }
    //logout
    logoutUser() {
        if (this.loginType == 'Google') {
            this.googlePlus.logout().then((response) => {
                console.log(response);
            });
        }
        else if (this.loginType == 'Facebook') {
            // Facebook.logout().then((response) => {
            //   console.log(response);
            // });
        }
        return this.firebase.fireAuth.signOut();
    }
    //update data from profile
    updateData(data, userId) {
        return this.userProfile.child(userId).update({
            fullname: data.fullname,
            // lastname:data.lastname,
            email: data.email,
            phone: data.phone,
            cityname: data.cityname,
            registration: data.registration,
        });
    }
    /*
    //review app and mark favourite
    review(rate,favourite,userId){
       return this.reviewApp.child(userId).set({
            rating:rate,
            favourite:favourite
       })
    }*/
    //password reset
    passReset(email) {
        return new Promise((res) => {
            this.firebase.fireAuth
                .sendPasswordResetEmail(email)
                .then((success) => {
                console.log('Sucess', success);
                res('Success');
            })
                .catch((error) => {
                res('Invalid or incomplete email');
            });
        });
    }
    addStripeCard(card, stripeId, email) {
        let body = {
            number: card.cardNumber,
            exp_month: card.expiryMonth,
            exp_year: card.expiryYear,
            cvc: card.cvv,
            email: email,
            stripeId: stripeId,
        };
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        return this.http.post('https://us-central1-bidrider-app.cloudfunctions.net/addStripeCard', body, options);
        //.subscribe(res => JSON.stringify(res))
        // .map((res: Response) => res.json())
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    stripePayment(card, cust_id, amount) {
        console.log('card..................', card);
        let body = {
            customer_id: cust_id,
            card_id: card,
            amount: amount,
        };
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        return this.http.post('https://us-central1-bidrider-app.cloudfunctions.net/stripPayment', body, options);
        // .map((res: Response) => res.json())
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    deleteStripeCard(card, cust_id) {
        console.log('card..................', card);
        let body = {
            customer_id: cust_id,
            card_id: card,
        };
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({ 'Content-Type': 'application/json' });
        let options = { headers: headers };
        return this.http.post('https://us-central1-bidrider-app.cloudfunctions.net/deleteStripeCard', body, options);
        // .map((res: Response) => res.json())
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
    sendPushDriver(title, msg, token) {
        console.log(token);
        let body = {
            to: token,
            notification: {
                body: msg,
                title: title,
            },
        };
        let headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpHeaders({
            'Content-Type': 'application/json',
            Authorization: 'key=AAAA3AtDWWI:APA91bG4NezdXvmdKnPBW-r97NKT59DsDQymxlJv68P4lbUaj709Pi-DhTtykKGtDDUUeG2jf5PK_0qjQN-1mUq2oMSTQI1FFUOmluNvDexzBzezAUiD8_0PTd3uUyf8fuu_TipVH0H3',
        });
        let options = { headers: headers };
        return this.http.post('https://fcm.googleapis.com/fcm/send', body, options);
        // .map((res: Response) => res.json())
        // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
    }
};
UserService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_4__.HttpClient },
    { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_1__.GooglePlus },
    { type: _firebase_service__WEBPACK_IMPORTED_MODULE_2__.FirebaseService }
];
UserService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)()
], UserService);



/***/ }),

/***/ 90158:
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppRoutingModule": () => (/* binding */ AppRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 39895);



const routes = [
    {
        path: '',
        redirectTo: 'pages',
        pathMatch: 'full',
    },
    {
        path: 'pages',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_pages_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./pages/pages.module */ 18950)).then((m) => m.PagesModule),
    },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__.PreloadAllModules }),
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], AppRoutingModule);



/***/ }),

/***/ 55041:
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppComponent": () => (/* binding */ AppComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./app.component.html */ 91106);
/* harmony import */ var _app_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component.scss */ 43069);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _services_basic_modal_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./services/basic/modal.service */ 24307);
/* harmony import */ var _services_events_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./services/events.service */ 80106);
/* harmony import */ var _Services_firebase_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./Services/firebase.service */ 13126);
/* harmony import */ var _Services_user_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Services/user-service */ 21521);
/* harmony import */ var _services_utility_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/utility.service */ 67278);
/* harmony import */ var _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @capacitor/push-notifications */ 98748);
/* harmony import */ var _services_nav_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/nav.service */ 53501);













let AppComponent = class AppComponent {
    constructor(events, firebaseService, utility, modals, menuCtrl, usersService, router, nav, platform, zone) {
        this.events = events;
        this.firebaseService = firebaseService;
        this.utility = utility;
        this.modals = modals;
        this.menuCtrl = menuCtrl;
        this.usersService = usersService;
        this.router = router;
        this.nav = nav;
        this.platform = platform;
        this.zone = zone;
        this.appPages = [
            { title: 'Home', url: '/pages/home', icon: 'home' },
            {
                title: 'Current Bookings',
                url: '/pages/currentbookings',
                icon: 'bookmarks',
            },
            { title: 'Booking History', url: '/pages/bookings', icon: 'bookmarks' },
            { title: 'Support', url: '/pages/support', icon: 'cog' },
            { title: 'Profile', url: '/pages/profile', icon: 'person' },
            { title: 'Payments', url: '/pages/payments', icon: 'cash' },
            { title: 'About Us', url: '/pages/about', icon: 'information' },
            { title: 'Logout', url: '', icon: 'exit' },
        ];
        this.events.subscribe('login_event', () => (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
                console.log('login_event RECEIVED');
                let user = yield this.firebaseService.getCurrentUser();
                var ref = this.firebaseService
                    .getDatabase()
                    .ref('/users/' + user['uid']);
                ref.on('value', (_snapshot) => {
                    if (_snapshot.val()) {
                        var data = _snapshot.val();
                        console.log(data);
                        this.user = _snapshot.val();
                    }
                });
            }), 1000);
            // this.user = this.users.getLocalUser();
        }));
        document.addEventListener('backbutton', (event) => {
            event.preventDefault();
            event.stopPropagation();
            const url = this.router.url;
            console.log(url);
            this.createBackRoutingLogics(url);
        }, false);
    }
    createBackRoutingLogics(url) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            if (url.includes('login') ||
                url.includes('signup') ||
                url.includes('home') ||
                url.includes('splash')) {
                this.utility.hideLoader();
                const isModalOpen = yield this.modals.modal.getTop();
                console.log(isModalOpen);
                if (isModalOpen) {
                    this.modals.dismiss({ data: 'A' });
                }
                else {
                    this.exitApp();
                }
            }
            else {
                // if(this.isModalOpen){
                // }
            }
        });
    }
    exitApp() {
        navigator['app'].exitApp();
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__awaiter)(this, void 0, void 0, function* () {
            this.user = yield this.firebaseService.getCurrentUser();
            console.log(this.user);
            console.log('setting menu ctrl disable');
            this.menuCtrl.enable(false, 'drawer');
            this.firebaseInit();
            //this.routerOutlet.swipeGesture = false;
        });
    }
    firebaseInit() {
        console.log('Initializing firebase');
        this.firebaseService.fireAuth.onAuthStateChanged((user) => {
            console.log('OnFireAuthChanged', user);
            if (user) {
                // this.firebase.grantPermission();
                console.log(user);
                this.zone.run(() => {
                    console.log('Page change requested');
                    this.nav.push('pages/locationloader');
                });
            }
        });
        // Request permission to use push notifications
        // iOS will prompt user and return if they granted permission or not
        // Android will just grant without prompting
        _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__.PushNotifications.requestPermissions().then((result) => {
            if (result.receive === 'granted') {
                // Register with Apple / Google to receive push via APNS/FCM
                _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__.PushNotifications.register();
            }
            else {
                // Show some error
            }
        });
        // On success, we should be able to receive notifications
        _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__.PushNotifications.addListener('registration', (token) => {
            console.log('FCM_TOKEN', token.value);
            localStorage.setItem('FCM_TOKEN', token.value);
            // alert('Push registration success, token: ' + token.value);
        });
        // Some issue with our setup and push will not work
        _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__.PushNotifications.addListener('registrationError', (error) => {
            alert('Error on registration: ' + JSON.stringify(error));
        });
        // Show us the notification payload if the app is open on our device
        _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__.PushNotifications.addListener('pushNotificationReceived', (notification) => {
            this.utility.alerts.showAlert(notification.body, notification.title);
            //alert('Push received: ' + JSON.stringify(notification));
        });
        // Method called when tapping on a notification
        _capacitor_push_notifications__WEBPACK_IMPORTED_MODULE_7__.PushNotifications.addListener('pushNotificationActionPerformed', (notification) => {
            alert('Push action performed: ' + JSON.stringify(notification));
        });
    }
    navigate(path) {
        this.nav.push(path);
    }
    handleClicked(title) {
        if (title === 'Logout') {
            console.log('onLogout');
            this.usersService.logoutUser();
        }
    }
};
AppComponent.ctorParameters = () => [
    { type: _services_events_service__WEBPACK_IMPORTED_MODULE_3__.EventsService },
    { type: _Services_firebase_service__WEBPACK_IMPORTED_MODULE_4__.FirebaseService },
    { type: _services_utility_service__WEBPACK_IMPORTED_MODULE_6__.UtilityService },
    { type: _services_basic_modal_service__WEBPACK_IMPORTED_MODULE_2__.ModalService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.MenuController },
    { type: _Services_user_service__WEBPACK_IMPORTED_MODULE_5__.UserService },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_11__.Router },
    { type: _services_nav_service__WEBPACK_IMPORTED_MODULE_8__.NavService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_10__.Platform },
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_12__.NgZone }
];
AppComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.Component)({
        selector: 'app-root',
        template: _raw_loader_app_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_app_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], AppComponent);



/***/ }),

/***/ 36747:
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AppModule": () => (/* binding */ AppModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/platform-browser */ 39075);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_26__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ 87152);
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component */ 55041);
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ 90158);
/* harmony import */ var _angular_fire_auth__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/fire/auth */ 2552);
/* harmony import */ var _angular_fire_database__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/fire/database */ 78380);
/* harmony import */ var _angular_fire_firestore__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/fire/firestore */ 20630);
/* harmony import */ var _Services_user_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./Services/user-service */ 21521);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_23__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ 19342);
/* harmony import */ var _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_28__ = __webpack_require__(/*! @angular/fire/compat/storage */ 43893);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/environments/environment */ 92340);
/* harmony import */ var _services_utility_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/utility.service */ 67278);
/* harmony import */ var _services_network_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/network.service */ 62982);
/* harmony import */ var _services_times_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/times.service */ 85390);
/* harmony import */ var _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic-native/date-picker/ngx */ 61194);
/* harmony import */ var _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_27__ = __webpack_require__(/*! @pscoped/ngx-pub-sub */ 88171);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_25__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _Services_map_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./Services/map-service */ 57775);
/* harmony import */ var _Services_geolocation_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./Services/geolocation.service */ 40929);
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ 83046);
/* harmony import */ var _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/call-number/ngx */ 64687);
/* harmony import */ var _ionic_native_card_io_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/card-io/ngx */ 86248);
/* harmony import */ var _components_gmap_gmap_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/gmap/gmap.component */ 25460);
/* harmony import */ var _angular_fire_compat__WEBPACK_IMPORTED_MODULE_24__ = __webpack_require__(/*! @angular/fire/compat */ 88939);

























// import { Facebook } from '@ionic-native/facebook/ngx';





// import { AngularFireModule } from '@angular/fire';
let AppModule = class AppModule {
};
AppModule = (0,tslib__WEBPACK_IMPORTED_MODULE_16__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_17__.NgModule)({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent, _components_gmap_gmap_component__WEBPACK_IMPORTED_MODULE_15__.GmapComponent],
        entryComponents: [],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_18__.BrowserModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_19__.IonicModule.forRoot({
                mode: 'ios'
            }),
            _app_routing_module__WEBPACK_IMPORTED_MODULE_2__.AppRoutingModule,
            (0,_angular_fire_auth__WEBPACK_IMPORTED_MODULE_20__.provideAuth)(() => (0,_angular_fire_auth__WEBPACK_IMPORTED_MODULE_20__.getAuth)()),
            (0,_angular_fire_database__WEBPACK_IMPORTED_MODULE_21__.provideDatabase)(() => (0,_angular_fire_database__WEBPACK_IMPORTED_MODULE_21__.getDatabase)()),
            //  provideFirebaseApp(() => initializeApp(environment.firebase)),
            (0,_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_22__.provideFirestore)(() => (0,_angular_fire_firestore__WEBPACK_IMPORTED_MODULE_22__.getFirestore)()),
            _angular_common_http__WEBPACK_IMPORTED_MODULE_23__.HttpClientModule,
            _angular_fire_compat__WEBPACK_IMPORTED_MODULE_24__.AngularFireModule.initializeApp(src_environments_environment__WEBPACK_IMPORTED_MODULE_5__.environment.firebase),
            _angular_fire_database__WEBPACK_IMPORTED_MODULE_21__.DatabaseModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_25__.ReactiveFormsModule,
        ],
        // AngularFireAuthModule,
        // AngularFireStorageModule,
        // AngularFireDatabaseModule
        providers: [
            {
                provide: _angular_router__WEBPACK_IMPORTED_MODULE_26__.RouteReuseStrategy,
                useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_19__.IonicRouteStrategy,
            },
            _Services_user_service__WEBPACK_IMPORTED_MODULE_3__.UserService,
            _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_4__.GooglePlus,
            _services_utility_service__WEBPACK_IMPORTED_MODULE_6__.UtilityService,
            _services_network_service__WEBPACK_IMPORTED_MODULE_7__.NetworkService,
            _services_times_service__WEBPACK_IMPORTED_MODULE_8__.TimesService,
            _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_9__.DatePicker,
            _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_27__.NgxPubSubService,
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_0__.Geolocation,
            _Services_geolocation_service__WEBPACK_IMPORTED_MODULE_11__.GeolocationService,
            _Services_map_service__WEBPACK_IMPORTED_MODULE_10__.MapService,
            //  Facebook,
            _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_12__.NativeGeocoder,
            _ionic_native_call_number_ngx__WEBPACK_IMPORTED_MODULE_13__.CallNumber,
            _ionic_native_card_io_ngx__WEBPACK_IMPORTED_MODULE_14__.CardIO,
            { provide: _angular_fire_compat_storage__WEBPACK_IMPORTED_MODULE_28__.BUCKET, useValue: 'my-bucket-name' }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_1__.AppComponent],
    })
], AppModule);



/***/ }),

/***/ 25460:
/*!***************************************************!*\
  !*** ./src/app/components/gmap/gmap.component.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GmapComponent": () => (/* binding */ GmapComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_gmap_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./gmap.component.html */ 1921);
/* harmony import */ var _gmap_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./gmap.component.scss */ 22985);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/pages/base-page/base-page */ 24282);





let GmapComponent = class GmapComponent extends src_app_pages_base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.newAddress = false;
        this.isDirections = false;
        this.start = 'chicago, il';
        this.end = 'chicago, il';
        this.directionsService = new google.maps.DirectionsService();
        this.directionsDisplay = new google.maps.DirectionsRenderer();
        this.searchQuery = '';
        this.isBothDirectionsAvailable = false;
        console.log('in GmapComponent');
    }
    ngOnInit() { }
    ngAfterViewInit() {
        this.platform.ready().then(() => {
            this.initializeMapBeforeSetCoordinates().then((v) => {
                this.initMap(v);
                setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
                    const active = localStorage.getItem('google_map');
                    if (!active) {
                        const flag = yield this.utility.presentConfirm('Thanks', 'Remind Later', 'How To Use', 'In the search field, type in the new address and press enter. You can also hold your finger and drag to new location, then press the + symbol in the top right corner');
                        localStorage.setItem('google_map', `${flag}`);
                    }
                }), 1000);
            });
        });
    }
    initializeMapBeforeSetCoordinates() {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__awaiter)(this, void 0, void 0, function* () {
            const mylocation = yield this.utility.getCurrentLocationCoordinates();
            this.map = new google.maps.Map(this.mapElement.nativeElement, {
                zoom: 13,
                center: mylocation,
            });
            console.log(this.myAddress);
            this.utility.getCoordsForGeoAddress(this.myAddress).then((coords) => {
                resolve({
                    mylocation,
                    destinatioLocation: coords,
                });
            }, (err) => {
                console.log(err);
                this.utility.presentFailureToast('Destination Not Found for given address');
                resolve({
                    mylocation,
                    destinatioLocation: null,
                });
            });
        }));
    }
    initMap(val) {
        // this.map = new google.maps.Map(this.mapElement.nativeElement, {
        //   zoom: 13,
        //   center: localatlong
        // });
        var self = this;
        if (this.isDirections === false) {
            this.mmarker = new google.maps.Marker({
                position: val.mylocation,
                map: this.map,
                draggable: true,
                title: 'Destination',
            });
            google.maps.event.addListener(this.mmarker, 'dragend', (event) => {
                const lt = event.latLng.lat();
                const lg = event.latLng.lng();
                const coords = { lat: lt, lng: lg };
                self.getGeoAddress(coords);
            });
            google.maps.event.addListener(this.map, 'click', (event) => {
                const lt = event.latLng.lat();
                const lg = event.latLng.lng();
                const coords = { lat: lt, lng: lg };
                this.mmarker.setMap(null);
                this.mmarker = new google.maps.Marker({
                    position: coords,
                    map: this.map,
                    draggable: true,
                    title: 'Destination',
                });
                self.getGeoAddress(coords);
            });
            this.setSearchBox();
        }
        this.directionsDisplay.setMap(this.map);
        // console.log(this.isDirections);
        if (this.isDirections == true) {
            const destination = new google.maps.LatLng(val.destinatioLocation);
            const mylocation = new google.maps.LatLng(val.mylocation);
            console.log('d-', destination, mylocation);
            const request = {
                origin: val.mylocation,
                destination: val.destinatioLocation,
                travelMode: 'DRIVING',
                drivingOptions: {
                    departureTime: new Date(Date.now() + 2000),
                    trafficModel: 'bestguess',
                },
                // drivingOptions: DrivingOptions,
                // unitSystem: UnitSystem,
                // waypoints[]: DirectionsWaypoint,
                // optimizeWaypoints: Boolean,
                // provideRouteAlternatives: Boolean,
                // avoidFerries: Boolean,
                // avoidHighways: Boolean,
                // avoidTolls: Boolean,
                // region: String
            };
            self.directionsService.route(request, (result, status) => {
                // console.log(status);
                if (status == 'OK') {
                    self.directionsDisplay.setDirections(result);
                    self.isBothDirectionsAvailable = true;
                    self.iOrigin = mylocation;
                    self.iDestination = destination;
                }
                else {
                    console.log(result, status);
                    self.utility.presentFailureToast('Error Finding Driving Directions on Map');
                    self.addDestinationOriginMarkers(val.mylocation, val.destinatioLocation);
                }
            });
        }
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
    }
    addDestinationOriginMarkers(origin, destination) {
        const marker_o = new google.maps.Marker({
            position: new google.maps.LatLng(origin.lat, origin.lng),
            title: 'Origin',
        });
        const marker_d = new google.maps.Marker({
            position: new google.maps.LatLng(destination.lat, destination.lng),
            title: 'Destination',
        });
        marker_o.setMap(this.map);
        marker_d.setMap(this.map);
        const bounds = new google.maps.LatLngBounds();
        bounds.extend(origin);
        bounds.extend(destination);
        this.map.fitBounds(bounds);
    }
    setSearchBox() {
        const self = this;
        const searchInput = this.searchbar.nativeElement.querySelector('.searchbar-input');
        // console.log("Search input", searchInput);
        const searchBox = new google.maps.places.SearchBox(searchInput);
        let markers = [];
        searchBox.addListener('places_changed', () => {
            console.log('places_changed');
            const places = searchBox.getPlaces();
            if (places.length === 0) {
                return;
            }
            self.mmarker.setMap(null);
            markers = [];
            // For each place, get the icon, name and location.
            const bounds = new google.maps.LatLngBounds();
            places.forEach((place) => {
                if (!place.geometry) {
                    // console.log("Returned place contains no geometry");
                    return;
                }
                // Create a marker for each place.
                self.mmarker = new google.maps.Marker({
                    position: place.geometry.location,
                    map: self.map,
                    draggable: true,
                    title: 'Destination',
                });
                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                }
                else {
                    bounds.extend(place.geometry.location);
                }
            });
            self.map.fitBounds(bounds);
        });
    }
    getGeoAddress(coords) {
        const self = this;
        const geocoder = new google.maps.Geocoder();
        const latlng = coords;
        geocoder.geocode({ location: latlng }, function (results, status) {
            if (status === 'OK') {
                if (results[0]) {
                    self.paddress = results[0].formatted_address;
                    self.utility.presentToast(self.paddress);
                }
                else {
                    self.utility.presentToast('No results found');
                }
            }
            else {
                self.utility.presentToast('Geocoder failed due to: ' + status);
            }
        });
    }
    getMarkerLocation() {
        const lt = this.mmarker.position.lat();
        const lg = this.mmarker.position.lng();
        const coords = { lat: lt, lng: lg };
        const self = this;
        const geocoder = new google.maps.Geocoder();
        const latlng = coords;
        geocoder.geocode({ location: latlng }, (results, status) => {
            if (status === 'OK') {
                // console.log(results);
                if (results[0]) {
                    self.paddress = results[0].formatted_address;
                    self.utility.presentToast(self.paddress);
                    if (self.newAddress == true) {
                        const addressComponets = results[0].address_components;
                        // console.log(addressComponets)
                        const countryObject = addressComponets.filter((x) => x.types.includes('country'))[0];
                        const country = countryObject ? countryObject.long_name : '';
                        const stateObject = addressComponets.filter((x) => x.types.includes('administrative_area_level_1'))[0];
                        const state = stateObject ? stateObject.long_name : '';
                        const cityObject = addressComponets.filter((x) => x.types.includes('administrative_area_level_2'))[0];
                        const city = cityObject ? cityObject.long_name : '';
                        const streetObject = addressComponets.filter((x) => x.types.includes('route'))[0];
                        const street = streetObject ? streetObject.long_name : '';
                        // console.log(country);
                        const threepartAddress = {
                            country,
                            state,
                            city,
                            street,
                        };
                        const coords2 = {
                            lat: lt,
                            lng: lg,
                            address: self.paddress,
                            parts: threepartAddress,
                        };
                        self.closeModal(coords2);
                    }
                    else {
                        const coords3 = { lat: lt, lng: lg, address: self.paddress };
                        self.closeModal(coords3);
                    }
                }
                else {
                    self.utility.presentToast('No results found');
                }
            }
            else {
                self.utility.presentToast('Geocoder failed due to: ' + status);
            }
        });
    }
    closeModal(res) {
        this.modals.dismiss(res);
    }
    calculateAndDisplayRoute() {
        this.directionsService.route({
            origin: this.start,
            destination: this.end,
            travelMode: 'DRIVING',
        }, (response, status) => {
            if (status === 'OK') {
                this.directionsDisplay.setDirections(response);
            }
            else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
};
GmapComponent.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Injector }
];
GmapComponent.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.ViewChild, args: ['map', { static: false },] }],
    searchbar: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.ViewChild, args: ['searchbox', { read: _angular_core__WEBPACK_IMPORTED_MODULE_4__.ElementRef },] }],
    myAddress: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    newAddress: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    isDirections: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }]
};
GmapComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-gmap',
        template: _raw_loader_gmap_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_gmap_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], GmapComponent);



/***/ }),

/***/ 96015:
/*!***************************************!*\
  !*** ./src/app/config/main.config.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Config": () => (/* binding */ Config)
/* harmony export */ });
const Config = {
    // api: 'https://api.digital-wallet-su-node-server.com/api/v1',
    api: 'http://89.233.107.200/api/v1',
    // api: 'http://localhost:8080/api/v1', // TEST API
    API_KEY: '4314791685',
    API_SECRET: 'girnqlhg4738qtgbfu438',
    PUSHER_APP_ID: '1165500',
    PUSHER_APP_KEY: '13e287990c19d3603075',
    PUSHER_APP_SECRET: 'ab5a4e1224cdf363ddc5',
    PUSHER_APP_CLUSTER: 'ap2',
};
// export const Config = {
//   api: 'http://ec2-18-217-195-71.us-east-2.compute.amazonaws.com/public/index.php/api',
//   imageURL: 'http://ec2-18-217-195-71.us-east-2.compute.amazonaws.com/public/index.php/images'
// };


/***/ }),

/***/ 24282:
/*!**********************************************!*\
  !*** ./src/app/pages/base-page/base-page.ts ***!
  \**********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BasePage": () => (/* binding */ BasePage)
/* harmony export */ });
/* harmony import */ var src_app_services_nav_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/app/services/nav.service */ 53501);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var src_app_services_network_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! src/app/services/network.service */ 62982);
/* harmony import */ var src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/utility.service */ 67278);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var src_app_services_events_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/events.service */ 80106);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var src_app_services_basic_popovers_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/basic/popovers.service */ 56384);
/* harmony import */ var src_app_services_basic_modal_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/basic/modal.service */ 24307);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var src_app_Services_user_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/Services/user-service */ 21521);
/* harmony import */ var src_app_services_basic_storage_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/basic/storage.service */ 85911);
/* harmony import */ var src_app_Services_map_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! src/app/Services/map-service */ 57775);
/* harmony import */ var src_app_Services_geolocation_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! src/app/Services/geolocation.service */ 40929);
/* harmony import */ var src_app_Services_firebase_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! src/app/Services/firebase.service */ 13126);

















class BasePage {
    constructor(injector) {
        this.platform = injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_11__.Platform);
        this.usersService = injector.get(src_app_Services_user_service__WEBPACK_IMPORTED_MODULE_6__.UserService);
        this.network = injector.get(src_app_services_network_service__WEBPACK_IMPORTED_MODULE_1__.NetworkService);
        this.utility = injector.get(src_app_services_utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService);
        this.location = injector.get(_angular_common__WEBPACK_IMPORTED_MODULE_12__.Location);
        this.common = injector.get(_angular_common__WEBPACK_IMPORTED_MODULE_12__.CommonModule);
        this.events = injector.get(src_app_services_events_service__WEBPACK_IMPORTED_MODULE_3__.EventsService);
        this.nav = injector.get(src_app_services_nav_service__WEBPACK_IMPORTED_MODULE_0__.NavService);
        this.formBuilder = injector.get(_angular_forms__WEBPACK_IMPORTED_MODULE_13__.FormBuilder);
        this.popover = injector.get(src_app_services_basic_popovers_service__WEBPACK_IMPORTED_MODULE_4__.PopoversService);
        this.modals = injector.get(src_app_services_basic_modal_service__WEBPACK_IMPORTED_MODULE_5__.ModalService);
        this.menuCtrl = injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_11__.MenuController);
        this.router = injector.get(_angular_router__WEBPACK_IMPORTED_MODULE_14__.Router);
        this.activatedRoute = injector.get(_angular_router__WEBPACK_IMPORTED_MODULE_14__.ActivatedRoute);
        this.storage = injector.get(src_app_services_basic_storage_service__WEBPACK_IMPORTED_MODULE_7__.StorageService);
        this.zone = injector.get(_angular_core__WEBPACK_IMPORTED_MODULE_15__.NgZone);
        this.alertCtrl = injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_11__.AlertController);
        this.mapService = injector.get(src_app_Services_map_service__WEBPACK_IMPORTED_MODULE_8__.MapService);
        this.actionSheetController = injector.get(_ionic_angular__WEBPACK_IMPORTED_MODULE_11__.ActionSheetController);
        this.geolocation = injector.get(src_app_Services_geolocation_service__WEBPACK_IMPORTED_MODULE_9__.GeolocationService);
        this.firebaseService = injector.get(src_app_Services_firebase_service__WEBPACK_IMPORTED_MODULE_10__.FirebaseService);
    }
    getParams() {
        return this.activatedRoute.snapshot.params;
    }
    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }
    popToRoot() {
        this.nav.navigateTo('pages/home');
    }
    ToggleMenuBar() {
        this.modals.dismiss();
        this.menuCtrl.open();
    }
}


/***/ }),

/***/ 5830:
/*!*****************************************!*\
  !*** ./src/app/services/api.service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ApiService": () => (/* binding */ ApiService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ 91841);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _config_main_config__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../config/main.config */ 96015);




let ApiService = class ApiService {
    constructor(http) {
        this.http = http;
        this.url = _config_main_config__WEBPACK_IMPORTED_MODULE_0__.Config.api;
    }
    get(endpoint, params, reqOpts) {
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    }
    post(endpoint, body, reqOpts) {
        return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    }
    put(endpoint, body, reqOpts) {
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    }
    delete(endpoint, reqOpts) {
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    }
    patch(endpoint, body, reqOpts) {
        return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_1__.HttpClient }
];
ApiService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], ApiService);



/***/ }),

/***/ 87661:
/*!**************************************************!*\
  !*** ./src/app/services/basic/alerts.service.ts ***!
  \**************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AlertsService": () => (/* binding */ AlertsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _strings_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./strings.service */ 45030);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ 80476);





let AlertsService = class AlertsService {
    constructor(alertController, toastCtrl, strings) {
        this.alertController = alertController;
        this.toastCtrl = toastCtrl;
        this.strings = strings;
    }
    showAlert(msg, title = 'Alerts') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                cssClass: 'my-custom-class',
                header: title,
                message: msg,
                buttons: [
                    {
                        text: 'OK',
                        cssClass: 'secondary',
                        handler: (blah) => {
                            resolve(true);
                        },
                    },
                ],
            });
            yield alert.present();
        }));
    }
    presentSuccessToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: this.strings.capitalizeEachFirst(msg),
                duration: 5000,
                position: 'top',
                cssClass: 'successToast',
            });
            toast.present();
        });
    }
    presentFailureToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: this.strings.capitalizeEachFirst(msg ? msg : 'ERROR'),
                duration: 5000,
                position: 'top',
                cssClass: 'failureToast',
            });
            toast.present();
        });
    }
    presentToast(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const toast = yield this.toastCtrl.create({
                message: msg,
                duration: 5000,
                position: 'bottom',
            });
            toast.present();
        });
    }
    presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message: message,
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        },
                    },
                    {
                        text: okText,
                        handler: () => {
                            resolve(true);
                        },
                    },
                ],
            });
            alert.present();
        }));
    }
    presentRadioSelections(title, message, inputs, okText = 'OK', cancelText = 'Cancel') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__awaiter)(this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: title,
                message: message,
                inputs: inputs,
                buttons: [
                    {
                        text: cancelText,
                        role: 'cancel',
                        handler: () => {
                            resolve(false);
                        },
                    },
                    {
                        text: okText,
                        handler: (data) => {
                            resolve(data);
                        },
                    },
                ],
            });
            alert.present();
        }));
    }
};
AlertsService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.AlertController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__.ToastController },
    { type: _strings_service__WEBPACK_IMPORTED_MODULE_0__.StringsService }
];
AlertsService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], AlertsService);



/***/ }),

/***/ 62537:
/*!***************************************************!*\
  !*** ./src/app/services/basic/loading.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoadingService": () => (/* binding */ LoadingService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 80476);



let LoadingService = class LoadingService {
    constructor(loadingController) {
        this.loadingController = loadingController;
    }
    showLoader(message = 'Please wait...') {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            this.loading = yield this.loadingController.create({
                cssClass: 'my-custom-class',
                message: message,
            });
            // await this.loading.present();
        });
    }
    hideLoader() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            if (this.loading) {
                this.loading.dismiss();
            }
        });
    }
};
LoadingService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.LoadingController }
];
LoadingService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], LoadingService);



/***/ }),

/***/ 24307:
/*!*************************************************!*\
  !*** ./src/app/services/basic/modal.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ModalService": () => (/* binding */ ModalService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 80476);



let ModalService = class ModalService {
    constructor(modal) {
        this.modal = modal;
    }
    present(component, data = {}, cssClass = '') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const modal = yield this.modal.create({
                component,
                cssClass,
                componentProps: data,
            });
            modal.onDidDismiss().then((data) => {
                resolve(data);
            });
            yield modal.present();
        }));
    }
    dismiss(data = {}) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            var top = yield this.modal.getTop();
            if (!top || top.hidden)
                return;
            if (this.modal.getTop())
                return new Promise((resolve) => {
                    data['dismiss'] = true;
                    this.modal.dismiss(data).then((v) => resolve());
                });
        });
    }
};
ModalService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.ModalController }
];
ModalService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], ModalService);



/***/ }),

/***/ 56384:
/*!****************************************************!*\
  !*** ./src/app/services/basic/popovers.service.ts ***!
  \****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PopoversService": () => (/* binding */ PopoversService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ 80476);



let PopoversService = class PopoversService {
    constructor(popoverController) {
        this.popoverController = popoverController;
    }
    present(component, ev, data = {}, cssClass = 'my-custom-class') {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const popover = yield this.popoverController.create({
                component,
                cssClass,
                event: ev,
                translucent: true,
                componentProps: data,
            });
            popover.onDidDismiss().then((v) => {
                resolve(v);
            });
            yield popover.present();
        }));
    }
};
PopoversService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_1__.PopoverController }
];
PopoversService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], PopoversService);



/***/ }),

/***/ 85911:
/*!***************************************************!*\
  !*** ./src/app/services/basic/storage.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageService": () => (/* binding */ StorageService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var localforage__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! localforage */ 92430);
/* harmony import */ var localforage__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(localforage__WEBPACK_IMPORTED_MODULE_0__);



let StorageService = class StorageService {
    // nativeStorage = localStorage;
    constructor() {
        localforage__WEBPACK_IMPORTED_MODULE_0___default().config({
            name: 'MyApp',
        });
    }
    saveData(data) {
        return new Promise(resolve => {
            localforage__WEBPACK_IMPORTED_MODULE_0___default().getItem('address', (err, value) => {
                let arr = [];
                if (value) {
                    let hasEntry = false;
                    for (var key in value) {
                        if (key == 'entries') {
                            hasEntry = true;
                        }
                    }
                    if (hasEntry) {
                        arr = value['entries'];
                        let hasNot = true;
                        arr.forEach((element) => {
                            if (element.add == data.add) {
                                hasNot = false;
                            }
                        });
                        if (hasNot) {
                            arr.push(data);
                            let temp = {};
                            temp['entries'] = arr;
                            localforage__WEBPACK_IMPORTED_MODULE_0___default().setItem('address', temp, (err) => {
                                console.log('Entry added', temp);
                                resolve(true);
                            });
                        }
                    }
                    else {
                        arr.push(data);
                        let temp = {};
                        temp['entries'] = arr;
                        localforage__WEBPACK_IMPORTED_MODULE_0___default().setItem('address', temp, (err) => {
                            console.log('Entry added', temp);
                            resolve(true);
                        });
                    }
                }
                else {
                    arr.push(data);
                    let temp = {};
                    temp['entries'] = arr;
                    localforage__WEBPACK_IMPORTED_MODULE_0___default().setItem('address', temp, (err) => {
                        console.log('Entry added', temp);
                        resolve(true);
                    });
                }
            });
        });
    }
    getData() {
        return localforage__WEBPACK_IMPORTED_MODULE_0___default().getItem('address');
    }
    set(key, data) {
        return new Promise((resolve) => {
            localStorage.setItem(key, JSON.stringify(data));
            resolve(true);
        });
    }
    get(key) {
        return new Promise((resolve) => {
            let data = localStorage.getItem(key);
            resolve(data);
        });
    }
};
StorageService.ctorParameters = () => [];
StorageService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], StorageService);



/***/ }),

/***/ 45030:
/*!***************************************************!*\
  !*** ./src/app/services/basic/strings.service.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StringsService": () => (/* binding */ StringsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);


let StringsService = class StringsService {
    constructor() { }
    getOnlyDigits(phoneNumber) {
        var numberString = phoneNumber;
        var numberInDigits = numberString.replace(/[^\d]/g, '');
        var numberVal = parseInt(numberInDigits);
        console.log(numberVal);
        return numberVal.toString();
    }
    isPhoneNumberValid(number) {
        var _validPhoneNumber = this.getOnlyDigits(number);
        // remove trailing zeros
        let s = _validPhoneNumber.toString();
        return _validPhoneNumber.toString().length < 10 ? false : true;
    }
    capitalizeEachFirst(str) {
        if (!str)
            return '';
        var splitStr = str.toLowerCase().split(' ');
        for (var i = 0; i < splitStr.length; i++) {
            // You do not need to check if i is larger than splitStr length, as your for does that for you
            // Assign it back to the array
            splitStr[i] =
                splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
        }
        // Directly return the joined string
        return splitStr.join(' ');
    }
    checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
        return (group) => {
            let passwordInput = group.controls[passwordKey], passwordConfirmationInput = group.controls[passwordConfirmationKey];
            if (passwordInput.value !== passwordConfirmationInput.value) {
                return passwordConfirmationInput.setErrors({ notEquivalent: true });
            }
            else {
                return passwordConfirmationInput.setErrors(null);
            }
        };
    }
    parseAddressFromProfile(__profile) {
        return `${__profile['apartment'] || ''} ${__profile['street_address'] || ''} ${__profile['city'] || ''} ${__profile['state'] || ''} ${__profile['zip_code'] || ''}`;
    }
    parseName(input) {
        const capitalize = (s) => {
            if (typeof s !== 'string')
                return '';
            s = s.toLowerCase();
            return s.charAt(0).toUpperCase() + s.slice(1);
        };
        var fullName = input || '';
        var result = {};
        if (fullName.length > 0) {
            var nameTokens = fullName.match(/\w*/g) || [];
            nameTokens = nameTokens.filter((n) => n);
            if (nameTokens.length > 3) {
                result['firstName'] = nameTokens.slice(0, 2).join(' ');
                result['firstName'] = capitalize(result['firstName']);
            }
            else {
                result['firstName'] = nameTokens.slice(0, 1).join(' ');
                result['firstName'] = capitalize(result['firstName']);
            }
            if (nameTokens.length > 2) {
                result['middleName'] = nameTokens.slice(-2, -1).join(' ');
                result['lastName'] = nameTokens.slice(-1).join(' ');
                result['middleName'] = capitalize(result['middleName']);
                result['lastName'] = capitalize(result['lastName']);
            }
            else {
                if (nameTokens.length == 1) {
                    result['lastName'] = '';
                    result['middleName'] = '';
                }
                else {
                    result['lastName'] = nameTokens.slice(-1).join(' ');
                    result['lastName'] = capitalize(result['lastName']);
                    result['middleName'] = '';
                }
            }
        }
        var display_name = result['lastName'] +
            (result['lastName'] ? ' ' : '') +
            result['firstName'];
        return display_name;
    }
    isLastNameExist(input) {
        var fullname = this.parseName(input);
        return !(fullname['lastName'] == '');
    }
    formatPhoneNumberRuntime(phoneNumber) {
        if (phoneNumber == null || phoneNumber == '') {
            return phoneNumber;
        }
        var cleaned = ('' + phoneNumber).replace(/\D/g, '');
        function numDigits(x) {
            return (Math.log(x) * Math.LOG10E + 1) | 0;
        }
        // only keep number and +
        var p1 = cleaned.match(/\d+/g);
        if (p1 == null) {
            return cleaned;
        }
        var p2 = phoneNumber.match(/\d+/g).map(Number);
        var len = numDigits(p2);
        // document.write(len + " " );
        switch (len) {
            case 1:
            case 2:
                return '(' + phoneNumber.toString();
            case 3:
                return '(' + phoneNumber.toString() + ')';
            case 4:
            case 5:
            case 6:
                var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                var f2 = phoneNumber.toString().substring(len, 3);
                return f1 + ' ' + f2;
            case 7:
            case 8:
            case 9:
            case 10:
                f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                f2 = phoneNumber.toString().substring(6, 3);
                var f3 = phoneNumber.toString().substring(len + 1, 6);
                return f1 + ' ' + f2 + '-' + f3;
            default:
                phoneNumber = phoneNumber
                    .replace(/\D/g, '')
                    .substr(phoneNumber.length - 10);
                f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                f2 = phoneNumber.toString().substring(6, 3);
                var f3 = phoneNumber.toString().substring(len, 4);
                return f1 + ' ' + f2 + '-' + f3;
        }
        // return "len";
    }
    onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
        if (phoneNumber == null || phoneNumber == '') {
            return phoneNumber;
        }
        phoneNumber = this.getOnlyDigits(phoneNumber);
        // phoneNumber = phoneNumber.substring(phoneNumber.length - 1,-11);//keep only 10 digit Number
        // phoneNumber = phoneNumber.substring(phoneNumber.length - 10, -11);//keep only 10 digit Number
        phoneNumber = last
            ? phoneNumber.substring(phoneNumber.length - 10, phoneNumber.length)
            : phoneNumber.substring(0, 10);
        const cleaned = ('' + phoneNumber).replace(/\D/g, '');
        function numDigits(x) {
            return (Math.log(x) * Math.LOG10E + 1) | 0;
        }
        // only keep number and +
        const p1 = cleaned.match(/\d+/g);
        if (p1 == null) {
            return cleaned;
        }
        const p2 = phoneNumber.match(/\d+/g).map(Number);
        const len = numDigits(p2);
        // document.write(len + " " );
        return phoneNumber;
        switch (len) {
            case 1:
            case 2:
                return '(' + phoneNumber;
            case 3:
                return '(' + phoneNumber + ')';
            case 4:
            case 5:
            case 6:
                var f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                var f2 = phoneNumber.toString().substring(len, 3);
                return f1 + ' ' + f2;
            default:
                f1 = '(' + phoneNumber.toString().substring(0, 3) + ')';
                f2 = phoneNumber.toString().substring(3, 6);
                var f3 = phoneNumber.toString().substring(6, 10);
                console.log(phoneNumber, f3);
                return f1 + ' ' + f2 + '-' + f3;
        }
    }
    onkeyupFormatCardNumberRuntime(cardNumber, last = true) {
        if (cardNumber == null || cardNumber == '') {
            return cardNumber;
        }
        cardNumber = this.getOnlyDigits(cardNumber);
        // cardNumber = cardNumber.substring(cardNumber.length - 1,-11);//keep only 10 digit Number
        // cardNumber = cardNumber.substring(cardNumber.length - 10, -11);//keep only 10 digit Number
        cardNumber = last
            ? cardNumber.substring(cardNumber.length - 16, cardNumber.length)
            : cardNumber.substring(0, 16);
        const cleaned = ('' + cardNumber).replace(/\D/g, '');
        function numDigits(x) {
            return (Math.log(x) * Math.LOG10E + 1) | 0;
        }
        // only keep number and +
        const p1 = cleaned.match(/\d+/g);
        if (p1 == null) {
            return cleaned;
        }
        const p2 = cardNumber.match(/\d+/g).map(Number);
        // const len = numDigits(p2);
        console.log(cardNumber);
        var temp = '';
        var value = cardNumber;
        var v = value.replace(/\s+/g, '').replace(/[^0-9]/gi, '');
        var matches = v.match(/\d{4,16}/g);
        var match = (matches && matches[0]) || '';
        var parts = [];
        for (var i = 0, len = match.length; i < len; i += 4) {
            parts.push(match.substring(i, i + 4));
        }
        if (parts.length) {
            return parts.join(' ');
        }
        else {
            return value;
        }
        // if(cardNumber.length === 5){
        //   cardNumber = cardNumber.toString().substring(0, 4) + '-' + cardNumber.toString().substring(4, 5);
        //   console.log(cardNumber)
        // }
        // else if(cardNumber.length === 10) {
        //    let _previous = cardNumber.toString().substring(0,9)
        //    let result = _previous + '-' + cardNumber.toString().substring(9, 10)
        //    console.log(result)
        // } else if(cardNumber.length === 15) {
        //    let _previous = cardNumber.toString().substring(0, 14)
        //    let result = _previous + '-' + cardNumber.toString().substring(14, 15)
        //    console.log(result)
        // }
        // console.log(cardNumber.length)
        // switch (len) {
        //   case 1:
        //     return cardNumber;
        //   case 2:
        //     return cardNumber;
        //   case 3:
        //     return cardNumber;
        //   case 4:
        //     return cardNumber;
        //   case 5:
        //     return cardNumber.toString().substring(0, 4) + '-' + cardNumber.toString().substring(5, );
        //   case 6:
        //   case 7:
        //   case 8:
        //   case 9:
        //     return cardNumber.toString().substring(0, 4) + '-' + cardNumber.toString().substring(5, 9) ;
        // case 4:
        // case 5:
        // case 6:
        //   var f1 = '(' + cardNumber.toString().substring(0, 3) + ')';
        //   var f2 = cardNumber.toString().substring(len, 3);
        //   return f1 + ' ' + f2;
        // default:
        //   f1 = '(' + cardNumber.toString().substring(0, 3) + ')';
        //   f2 = cardNumber.toString().substring(3, 6);
        //   var f3 = cardNumber.toString().substring(6, 10);
        //   console.log(cardNumber, f3);
        //   return f1 + ' ' + f2 + '-' + f3;
        // }
        return cardNumber;
        // document.write(len + " " );
    }
};
StringsService.ctorParameters = () => [];
StringsService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root',
    })
], StringsService);



/***/ }),

/***/ 80106:
/*!********************************************!*\
  !*** ./src/app/services/events.service.ts ***!
  \********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EventsService": () => (/* binding */ EventsService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @pscoped/ngx-pub-sub */ 88171);



let EventsService = class EventsService {
    constructor(pubsubSvc) {
        this.pubsubSvc = pubsubSvc;
        this.latestEvent = 'randomLast';
        this.historicalEvent = 'randomHistory';
        this.subscriptions = [];
        pubsubSvc.registerEventWithHistory(this.historicalEvent, 6);
        pubsubSvc.registerEventWithLastValue(this.latestEvent, undefined);
    }
    publish(key, data = {}) {
        this.pubsubSvc.publishEvent(key, data);
    }
    subscribe(key, handler) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__awaiter)(this, void 0, void 0, function* () {
            const item = this.subscriptions.find((x) => x.key === key);
            if (item) {
                this.unsubscribe(key);
            }
            const subs = this.pubsubSvc.subscribe(key, (data) => handler(data));
            this.subscriptions.push({ key, subs });
            //this.subscribe[key] = subs;
        });
    }
    unsubscribe(key) {
        console.log('unsubscribe', key);
        console.log('unsubscribe-array', this.subscriptions);
        const item = this.subscriptions.find((x) => x.key === key);
        console.log(item);
        if (item) {
            const subs = item['subs'];
            subs.unsubscribe();
            const index = this.subscriptions.findIndex((x) => x.key === key);
            if (index > -1) {
                this.subscriptions.splice(index, 1);
            }
        }
    }
};
EventsService.ctorParameters = () => [
    { type: _pscoped_ngx_pub_sub__WEBPACK_IMPORTED_MODULE_1__.NgxPubSubService }
];
EventsService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], EventsService);



/***/ }),

/***/ 74375:
/*!*************************************************!*\
  !*** ./src/app/services/geolocation.service.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "GeolocationService": () => (/* binding */ GeolocationService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/native-geocoder/ngx */ 83046);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _capacitor_geolocation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @capacitor/geolocation */ 61091);


// import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';



let GeolocationService = class GeolocationService {
    constructor(platform, 
    // private geolocation: Geolocation,
    nativeGeocoder) {
        this.platform = platform;
        this.nativeGeocoder = nativeGeocoder;
    }
    getCurrentPosition() {
        return new Promise((resolve, reject) => {
            _capacitor_geolocation__WEBPACK_IMPORTED_MODULE_1__.Geolocation.getCurrentPosition().then((res) => {
                resolve(res);
            }, (err) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
                reject(err);
                // console.log(err);
                // const res = await this.getCoordsForGeoAddress('706 Timber Branch Drive, Alexandria, VA, USA')
                // resolve(res);
            }));
        });
    }
    getCoordsForGeoAddress(address, _default = true) {
        const self = this;
        return new Promise((resolve) => {
            const self = this;
            const geocoder = new google.maps.Geocoder();
            geocoder.geocode({ address }, (results, status) => {
                if (status === 'OK') {
                    if (results[0]) {
                        const loc = results[0].geometry.location;
                        const lat = loc.lat();
                        const lng = loc.lng();
                        resolve({ lat, lng, coords: { latitude: lat, longitude: lng } });
                    }
                    else {
                        resolve(null);
                    }
                }
                else {
                    console.log({ results, status });
                    resolve(null);
                }
            });
        });
    }
    getCoordsViaHTML5Navigator() {
        return new Promise((resolve) => {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition((position) => {
                    const pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    resolve(pos);
                }, () => {
                    resolve({ lat: 51.5074, lng: 0.1278 });
                });
            }
            else {
                // Browser doesn't support Geolocation
                resolve({ lat: 51.5074, lng: 0.1278 });
            }
        });
    }
    getCurrentLocationCoordinates() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            let user = yield this.getCurrentPosition();
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
                _capacitor_geolocation__WEBPACK_IMPORTED_MODULE_1__.Geolocation.getCurrentPosition().then(position => {
                    const lt = position.coords.latitude;
                    const lg = position.coords.longitude;
                    resolve({ lat: lt, lng: lg });
                }).catch((reason) => {
                    console.log("getCurrentPosition", reason);
                });
            }));
        });
    }
    getAddressFromCoords(lat, lng) {
        return new Promise((resolve) => {
            // if (this.platform.is('cordova')) {
            //   this.nativeGeocoder
            //     .reverseGeocode(lat, lng)
            //     .then((result) => {
            //       console.log(result);
            //       if (result[0] != undefined) {
            //         const obj = {
            //           lat,
            //           lng,
            //           add: result[0]['formatted_address']
            //         }
            //         resolve(obj);
            //       } else {
            //         resolve(null);
            //       }
            //     })
            //     .catch((error: any) => {
            //       console.log('reverseGeocode Error', error);
            //       resolve(null);
            //     });
            // } else {
            var geocoder = new google.maps.Geocoder();
            geocoder
                .geocode({ location: { lat, lng } })
                .then((response) => {
                if (response.results[0]) {
                    const obj = {
                        lat,
                        lng,
                        add: response.results[0].formatted_address
                    };
                    resolve(obj);
                }
                else {
                    resolve(null);
                }
            })
                .catch((e) => {
                console.log('Geocoder failed due to: ' + e);
                resolve(null);
            });
            //   }
        });
    }
};
GeolocationService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__.Platform },
    { type: _ionic_native_native_geocoder_ngx__WEBPACK_IMPORTED_MODULE_0__.NativeGeocoder }
];
GeolocationService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root',
    })
], GeolocationService);



/***/ }),

/***/ 54249:
/*!*******************************************!*\
  !*** ./src/app/services/image.service.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ImageService": () => (/* binding */ ImageService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _basic_alerts_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic/alerts.service */ 87661);
/* harmony import */ var _basic_modal_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/modal.service */ 24307);




let ImageService = class ImageService {
    constructor(alerts, modals) {
        this.alerts = alerts;
        this.modals = modals;
        this.isValidUrl = (string) => {
            try {
                new URL(string);
                return true;
            }
            catch (_) {
                return false;
            }
        };
    }
    snapImage(type) {
        return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () {
            const radioOptions = [
                {
                    type: 'radio',
                    label: 'Camera',
                    value: '1',
                    checked: false,
                },
                {
                    type: 'radio',
                    label: 'Gallery',
                    value: '0',
                    checked: false,
                },
            ];
            const option = type == 'Gallery' ? '0' : '1'; // await this.alerts.presentRadioSelections('Select From', '', radioOptions)
            if (option == null) {
                resolve(null);
                return;
            }
            // var options: CameraOptions = {
            //   quality: 100,
            //   targetWidth: 1024,
            //   saveToPhotoAlbum: false,
            //   destinationType: this.camera.DestinationType.DATA_URL,
            //   encodingType: this.camera.EncodingType.JPEG,
            //   mediaType: this.camera.MediaType.PICTURE,
            //   sourceType: parseInt(option),
            // };
            // this.camera.getPicture(options).then((imageData) => {
            //   if (imageData) {
            //     // this.cropWithController('data:image/jpeg;base64,' + imageData, type)
            //     //   .then(image =>
            //     //     resolve(image),
            //     //   err => console.error(err))
            //     resolve('data:image/jpeg;base64,' + imageData);
            //   } else {
            //     resolve(null);
            //   }
            //});
        }));
    }
    cropWithController(imageData, type) {
        function getCropOptions() {
            if (type == 'licence') {
                const c = {
                    dragMode: 'crop',
                    aspectRatio: 16 / 9,
                    autoCrop: true,
                    movable: true,
                    zoomable: true,
                    scalable: true,
                    autoCropArea: 0.8,
                };
                return c;
            }
            else {
                const c = {
                    dragMode: 'crop',
                    aspectRatio: 1,
                    autoCrop: true,
                    movable: true,
                    zoomable: true,
                    scalable: true,
                    autoCropArea: 0.8,
                };
                return c;
            }
        }
        return new Promise((resolve, reject) => (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__awaiter)(this, void 0, void 0, function* () { }));
    }
    convertImageUrltoBase64(url) {
        console.log('here-url-', url);
        return new Promise((resolve) => {
            if (!url) {
                resolve(null);
            }
            else {
                if (!this.isValidUrl(url) || /^http/.test(url)) {
                    var index = url.lastIndexOf('/') + 1;
                    var filename = url.substr(index);
                    resolve(filename);
                }
                else {
                    this.convertToDataURLviaCanvas(url).then((base64) => {
                        resolve(this.getRB64fromB64(base64));
                    });
                }
            }
        });
    }
    convertToDataURLviaCanvas(url, outputFormat = 'image/jpeg') {
        return new Promise((resolve, reject) => {
            var img = new Image();
            img.crossOrigin = 'Anonymous';
            img.onload = () => {
                let canvas = document.createElement('CANVAS'), ctx = canvas.getContext('2d'), dataURL;
                canvas.height = img.height;
                canvas.width = img.width;
                ctx.drawImage(img, 0, 0);
                dataURL = canvas.toDataURL(outputFormat);
                resolve(dataURL);
                canvas = null;
            };
            img.src = url;
        });
    }
    getRB64fromB64(str) {
        return str.substring(str.indexOf(',') + 1);
    }
};
ImageService.ctorParameters = () => [
    { type: _basic_alerts_service__WEBPACK_IMPORTED_MODULE_0__.AlertsService },
    { type: _basic_modal_service__WEBPACK_IMPORTED_MODULE_1__.ModalService }
];
ImageService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], ImageService);



/***/ }),

/***/ 53501:
/*!*****************************************!*\
  !*** ./src/app/services/nav.service.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NavService": () => (/* binding */ NavService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ 39895);




let NavService = class NavService {
    constructor(location, router, activatedRoute) {
        this.location = location;
        this.router = router;
        this.activatedRoute = activatedRoute;
    }
    setRoot(page, param = {}) {
        this.push(page, param);
    }
    push(page, param = {}) {
        let extras = {
            queryParams: param,
        };
        this.navigateTo(page, extras);
    }
    pop() {
        return new Promise((resolve) => {
            this.location.back();
            resolve(true);
        });
    }
    navigateTo(link, data) {
        console.log(link);
        this.router.navigate([link], data);
    }
    navigateToChild(link, data) {
        data.relativeTo = this.activatedRoute;
        this.router.navigate([link], data);
    }
    getParams() {
        return this.activatedRoute.snapshot.params;
    }
    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }
};
NavService.ctorParameters = () => [
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_0__.Location },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__.Router },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_1__.ActivatedRoute }
];
NavService = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Injectable)({
        providedIn: 'root',
    })
], NavService);



/***/ }),

/***/ 62982:
/*!*********************************************!*\
  !*** ./src/app/services/network.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "NetworkService": () => (/* binding */ NetworkService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _events_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./events.service */ 80106);
/* harmony import */ var _api_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./api.service */ 5830);
/* harmony import */ var _utility_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./utility.service */ 67278);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ 91841);






let NetworkService = class NetworkService {
    constructor(utility, api, events) {
        this.utility = utility;
        this.api = api;
        this.events = events;
        // console.log('Hello NetworkProvider Provider');
    }
    // post requests -- start
    login(data) {
        return this.httpPostResponse('user/loginCustomer', data, null, true);
    }
    register(data) {
        return this.httpPostResponse('user/signUpCircleCash', data, null, true);
    }
    rechargeElectricityMeter(data) {
        return this.httpPostResponse('bill/prepaidElectricityBillCircleCash', data, null, true);
    }
    topupMobile(data) {
        return this.httpPostResponse('bill/topupMobileCircleCash', data, null, true);
    }
    addEducationPayment(data) {
        return this.httpPostResponse('bill/addEducationPaymentCircleCash', data, null, true);
    }
    addMobileAccount(data) {
        return this.httpPostResponse('bill/addMobileAccount', data, null, true);
    }
    getMobileAccounts(data) {
        return this.httpPostResponse('bill/getMobileAccounts', data, null, true);
    }
    getMeterAccounts(data) {
        return this.httpPostResponse('bill/getMeterAccounts', data, null, true);
    }
    addMeterAccount(data) {
        return this.httpPostResponse('bill/addMeterAccount', data, null, true);
    }
    addEducationPayment2(data) {
        return this.httpPostResponse('bill/addEducationPaymentCircleCash2', data, null, true);
    }
    addCard(data) {
        return this.httpPostResponse('user/addCard', data, null, true);
    }
    getCards(data) {
        return this.httpPostResponse('user/getCards', data, null, true);
    }
    addBankAccount(data) {
        return this.httpPostResponse('user/addBankAccount', data, null, true);
    }
    getBankAccounts(data) {
        return this.httpPostResponse('user/getBankAccounts', data, null, true);
    }
    addCardTransfer(data) {
        return this.httpPostResponse('user/addCardTransfer', data, null, true);
    }
    generateVoucher(data) {
        return this.httpPostResponse('user/generateVoucher', data, null, true);
    }
    getUser() {
        return this.httpPostResponse('user/getProfile', null, null, true);
    }
    payE15Service(data) {
        return this.httpPostResponse('user/payE15Services', data, null, true);
    }
    InquiryE15Services(data) {
        return this.httpPostResponse('user/InquiryE15Services', data, null, true);
    }
    payCustomsServices(data) {
        return this.httpPostResponse('user/payCustomsServices', data, null, true);
    }
    inquiryCustomsServices(data) {
        return this.httpPostResponse('user/inquiryCustomsServices', data, null, true);
    }
    verifyMobileNumber(data) {
        return this.httpPostResponse('user/verifyMobileNumber', data, null, true);
    }
    getCircleCashUser(data) {
        return this.httpPostResponse('user/getCircleCashUser', data, null, true);
    }
    addLocalCashCircleAccount(data) {
        return this.httpPostResponse('user/addLocalCashCircleAccount', data, null, true);
    }
    getCircleCashContactList(data) {
        return this.httpPostResponse('user/getCircleCashContactList', data, null, true);
    }
    addLocalBankAccount(data) {
        return this.httpPostResponse('user/addLocalBankAccount', data, null, true);
    }
    transactionHistoryOfCustomer(data) {
        return this.httpGetResponse('user/transactionHistoryOfCustomer', data, true, true);
    }
    getTransactionById(data) {
        return this.httpPostResponse('user/getTransactionById', data, null, true);
    }
    forgotPasswordCircleCash(data) {
        return this.httpPostResponse('user/forgotPasswordCircleCash', data, null, true);
    }
    changePasswordVerified(data) {
        return this.httpPostResponse('user/changePasswordVerified', data, null, true);
    }
    changePassword(data) {
        return this.httpPostResponse('user/changePassword', data, null, true);
    }
    getProfile(data) {
        return this.httpGetResponse('user/getProfile', data, true, true);
    }
    editsettingInformation(data) {
        return this.httpPostResponse('user/editsettingInformation', data, null, true);
    }
    verifyOtp(data) {
        return this.httpPostResponse('user/verifyOtp', data, null, true);
    }
    accountTransfer(data) {
        return this.httpPostResponse('user/accountTransfer', data, null, true);
    }
    getNotificationsCC(data) {
        return this.httpPostResponse('user/getNotificationsCC', data, null, true);
    }
    addFCMToken(data) {
        return this.httpPostResponse('user/addFCMToken', data, null, true);
    }
    encryptionTest(data) {
        return this.httpPostResponse('maintest/encryptionTest', data, null, true);
    }
    // get requests -- end
    httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
    }
    httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
        return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
    }
    // default 'Content-Type': 'application/json',
    httpResponse(type = 'get', key, data, query = null, showloader = false, showError = true, contenttype = 'application/json') {
        return new Promise((resolve, reject) => {
            if (showloader == true) {
                this.utility.showLoader();
            }
            //const _id = (id) ? '/' + id : '';
            var url = key;
            if (query)
                url = key + query;
            let reqOpts = {
                headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_3__.HttpHeaders({
                    'Content-Type': contenttype,
                    language: 'en',
                }),
            };
            console.log(url, data, reqOpts);
            const seq = type == 'get'
                ? this.api.get(url, null, reqOpts)
                : this.api.post(url, data, reqOpts);
            seq.subscribe((res) => {
                console.log('showloader ?', showloader);
                if (showloader == true) {
                    this.utility.hideLoader();
                }
                console.log('response_code', res.response_code);
                if (res.response_code != 200) {
                    this.utility.presentFailureToast(res.response_message);
                    resolve({ success: false, result: res });
                }
                else {
                    // let apiResult = res.result;
                    // if (apiResult) {
                    //   var bytes = CryptoJS.AES.decrypt(apiResult, 'mySecretKey');
                    //   var originalText = bytes.toString(CryptoJS.enc.Utf8);
                    //   apiResult = JSON.parse(originalText);
                    //   console.log('apiResult', apiResult);
                    // }
                    // resolve({ success: true, result: apiResult });
                }
                // this.utility.presentSuccessToast(res['message']);
            }, (err) => {
                console.log('Error in API', err);
                let error = err['error'];
                if (showloader == true) {
                    this.utility.hideLoader();
                }
                if (showError) {
                    this.utility.presentFailureToast(error['message']);
                }
                console.log('Error in API', err);
                reject(null);
            });
        });
    }
    showFailure(err) {
        console.error('ERROR', err);
        var _error = err ? err['response_message'] : 'check logs';
        this.utility.presentFailureToast(_error);
    }
    getActivityLogs() {
        return this.httpGetResponse('get_activity_logs', null, true);
    }
    getPermissions() {
        return this.httpGetResponse('get-all-permissions', null, false);
    }
    getChatList(loader) {
        return this.httpGetResponse('online-offline?update_for_me=1', null, loader);
    }
    getChatHistory(id) {
        return this.httpGetResponse('chat/history', id, true);
    }
    sendMessage(param) {
        return this.httpPostResponse('chat/message-post', param, null, false, false);
    }
    markAsRead(id) {
        return this.httpPostResponse('chat/read', {}, id, false, false);
    }
    updateNotesProject(data, id) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            return this.httpPostResponse('update-project-notes', data, id, false, false);
        });
    }
    createInvoice(data, id) {
        return this.httpPostResponse('create-invoice', data, id, true, false);
    }
    getInvoice(projectId, invoiceId) {
        return this.httpGetResponse(`show-invoice/${projectId}/${invoiceId}`, null, true, false);
    }
    updateInvoice(data, invoiceId) {
        return this.httpPostResponse('update-invoice', data, invoiceId, true, false);
    }
    deleteInvoiceProject(param, id) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            return this.httpPostResponse('delete-invoice', param, id, false, false);
        });
    }
    uploadContract(data) {
        return this.httpPostResponse('contract-upload-mobile', data, null, true, false);
    }
    generateInvoiceLink(id) {
        return this.httpGetResponse('generate-invoice-link', id, true, false);
    }
    generateContractLink(id) {
        return this.httpGetResponse('generate-contract-link', id, true, false);
    }
    updateBio(data) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            return this.httpPostResponse('update_profile', data, null, true, false);
        });
    }
    getTaskSkills(id) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            return this.httpGetResponse('get-task-skills', id, false, false);
        });
    }
    saveFeedback(data) {
        this.httpPostResponse('add-supervisor-task-notes', data, null, true);
    }
    createContract(projectId, data) {
        this.httpPostResponse('create-contract', data, projectId, true);
    }
    getSkills() {
        return this.httpGetResponse('get-skills-multiselect', null, false, false);
    }
    removePhaseMember(payload) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            yield this.httpPostResponse('delete-phase-member', payload, null, true);
        });
    }
    getPhaseline(id) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            return this.httpGetResponse('get-phase', id, true, false);
        });
    }
};
NetworkService.ctorParameters = () => [
    { type: _utility_service__WEBPACK_IMPORTED_MODULE_2__.UtilityService },
    { type: _api_service__WEBPACK_IMPORTED_MODULE_1__.ApiService },
    { type: _events_service__WEBPACK_IMPORTED_MODULE_0__.EventsService }
];
NetworkService = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Injectable)({
        providedIn: 'root',
    })
], NetworkService);



/***/ }),

/***/ 85390:
/*!*******************************************!*\
  !*** ./src/app/services/times.service.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TimesService": () => (/* binding */ TimesService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @ionic-native/date-picker/ngx */ 61194);



let TimesService = class TimesService {
    constructor(datePicker) {
        this.datePicker = datePicker;
    }
    diffInHours(start_date, end_date) {
        let startDate = new Date(start_date);
        let EndDate = new Date(end_date);
        console.log(Date.parse(startDate), Date.parse(EndDate));
        if (EndDate < startDate) {
            return -1;
        }
        let diffMs = Math.abs(Date.parse(EndDate) - Date.parse(startDate)); // milliseconds
        let diffHrs = (diffMs / (1000 * 60 * 60)).toFixed(0); // hours
        return diffHrs;
    }
    formatDateTime(date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear(), minutes = d.getMinutes(), hour = d.getHours();
        var _hour = hour.toString();
        var _minutes = minutes.toString();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        if (_hour.length < 2)
            _hour = '0' + _hour;
        if (_minutes.length < 2)
            _minutes = '0' + _minutes;
        return [year, month, day].join('-') + ' ' + [_hour, _minutes].join(':');
    }
    formatDateMDYHM(date) {
        if (date == null) {
            return date;
        }
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear(), minutes = d.getMinutes(), hour = d.getHours();
        var ampm = 'AM';
        var _hour = hour.toString();
        var _minutes = minutes.toString();
        if (hour > 12) {
            _hour = (hour - 12).toString();
            ampm = 'PM';
        }
        if (month.toString().length < 2)
            month = '0' + month;
        if (day.toString().length < 2)
            day = '0' + day;
        if (hour.toString().length < 2)
            _hour = '0' + hour;
        if (minutes.toString().length < 2)
            _minutes = '0' + minutes;
        return ([month, day, year].join('-') +
            ' ' +
            [_hour, _minutes].join(':') +
            ' ' +
            ampm);
    }
    formatAMPM(_dt) {
        let dt = new Date(_dt);
        var month = dt.getMonth() > 8 ? dt.getMonth() + 1 : '0' + (dt.getMonth() + 1);
        var date = dt.getDate() > 9 ? dt.getDate() : '0' + dt.getDate();
        var year = dt.getFullYear();
        var hours = dt.getHours();
        var minutes = dt.getMinutes();
        var ampm = hours >= 12 ? 'PM' : 'AM';
        hours = hours % 12;
        hours = hours ? hours : 12; // the hour '0' should be '12'
        var _hours = hours < 10 ? '0' + hours : hours;
        var _minutes = minutes < 10 ? '0' + minutes : minutes;
        var strTime = month +
            '-' +
            date +
            '-' +
            year +
            ' ' +
            _hours +
            ':' +
            _minutes +
            ' ' +
            ampm;
        return strTime;
    }
    customMDYHMformatDateMDYHM(_date) {
        if (_date == null) {
            return _date;
        }
        // format must be 04-17-2019 08:13 // 24 hour format
        var date = _date.split(' ')[0];
        var time = _date.split(' ')[1];
        var hour = parseInt(time.split(':')[0]);
        var minutes = parseInt(time.split(':')[1]);
        var ampm = 'AM';
        if (hour > 12) {
            hour = hour - 12;
            ampm = 'PM';
        }
        var _hour = hour.toString();
        var _minutes = minutes.toString();
        if (_hour.length < 2)
            _hour = '0' + _hour;
        if (_minutes.length < 2)
            _minutes = '0' + _minutes;
        return date + ' ' + [_hour, _minutes].join(':') + ' ' + ampm;
    }
    formatHoursToText(hour) {
        var lbl = '';
        switch (hour.toString()) {
            case '3':
                lbl = '3 Hours';
                break;
            case '6':
                lbl = '6 Hours';
                break;
            case '12':
                lbl = '12 Hours';
                break;
            case '24':
                lbl = '24 Hours';
                break;
            case '48':
                lbl = '48 Hours';
                break;
            case '168':
                lbl = '1 Week';
                break;
            case '336':
                lbl = '2 Weeks';
                break;
            case '720':
                lbl = '1 Month';
                break;
            case '876000':
                lbl = 'No Limit';
                break;
            default:
                lbl = hour + ' Hours';
                break;
        }
        return lbl;
    }
    formatDateMDY(date) {
        if (date == null) {
            return date;
        }
        let y = date.substring(0, 4);
        var temp = date.slice(5);
        temp = temp + '/' + y;
        temp = temp.replace('-', '/');
        return temp;
    }
    formatDate(date) {
        var d = new Date(date), month = '' + (d.getMonth() + 1), day = '' + d.getDate(), year = d.getFullYear();
        if (month.length < 2)
            month = '0' + month;
        if (day.length < 2)
            day = '0' + day;
        return [year, month, day].join('-');
    }
    showDatePicker(_date = Date(), mode = 'date') {
        return new Promise((resolve) => {
            this.datePicker
                .show({
                date: _date,
                mode: mode,
                androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK,
            })
                .then((date) => {
                resolve(date);
            })
                .catch((err) => {
                resolve(null);
            });
        });
    }
    isOverThirteen(dob) {
        if (dob.getFullYear() != undefined &&
            new Date().getFullYear() - dob.getFullYear() < 13) {
            return false;
        }
        else {
            return true;
        }
    }
};
TimesService.ctorParameters = () => [
    { type: _ionic_native_date_picker_ngx__WEBPACK_IMPORTED_MODULE_0__.DatePicker }
];
TimesService = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Injectable)({
        providedIn: 'root',
    })
], TimesService);



/***/ }),

/***/ 67278:
/*!*********************************************!*\
  !*** ./src/app/services/utility.service.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UtilityService": () => (/* binding */ UtilityService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _basic_strings_service__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./basic/strings.service */ 45030);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _basic_alerts_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./basic/alerts.service */ 87661);
/* harmony import */ var _basic_loading_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./basic/loading.service */ 62537);
/* harmony import */ var _times_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./times.service */ 85390);
/* harmony import */ var _image_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./image.service */ 54249);
/* harmony import */ var _basic_storage_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./basic/storage.service */ 85911);
/* harmony import */ var _geolocation_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./geolocation.service */ 74375);










let UtilityService = class UtilityService {
    constructor(loading, plt, alerts, images, times, strings, storage, geolocations) {
        this.loading = loading;
        this.plt = plt;
        this.alerts = alerts;
        this.images = images;
        this.times = times;
        this.strings = strings;
        this.storage = storage;
        this.geolocations = geolocations;
    }
    showLoader(msg = 'Please wait...') {
        return this.loading.showLoader(msg);
    }
    hideLoader() {
        return this.loading.hideLoader();
    }
    showAlert(msg) {
        return this.alerts.showAlert(msg);
    }
    presentToast(msg) {
        return this.alerts.presentToast(msg);
    }
    presentSuccessToast(msg) {
        return this.alerts.presentSuccessToast(msg);
    }
    presentFailureToast(msg) {
        return this.alerts.presentFailureToast(msg);
    }
    presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = '') {
        return this.alerts.presentConfirm((okText = okText), (cancelText = cancelText), (title = title), (message = message));
    }
    isOverThirteen(dob) {
        return this.times.isOverThirteen(dob);
    }
    /** Storage Service */
    setKey(key, value) {
        return this.storage.set(key, value);
    }
    getKey(key) {
        return this.storage.get(key);
    }
    /** Strings Service */
    capitalizeEachFirst(str) {
        return this.strings.capitalizeEachFirst(str);
    }
    onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
        return this.strings.onkeyupFormatPhoneNumberRuntime(phoneNumber, last);
    }
    onkeyupFormatCardNumberRuntime(phoneNumber, last = true) {
        return this.strings.onkeyupFormatCardNumberRuntime(phoneNumber, last);
    }
    formatPhoneNumberRuntime(phoneNumber) {
        return this.strings.formatPhoneNumberRuntime(phoneNumber);
    }
    isPhoneNumberValid(num) {
        return this.strings.isPhoneNumberValid(num);
    }
    checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
        return this.strings.checkIfMatchingPasswords(passwordKey, passwordConfirmationKey);
    }
    parseAddressFromProfile(profile) {
        return this.strings.parseAddressFromProfile(profile);
    }
    isLastNameExist(input) {
        return this.strings.isLastNameExist(input);
    }
    /* Immage Service */
    snapImage(type) {
        return this.images.snapImage(type);
    }
    convertImageUrltoBase64(url) {
        return this.images.convertImageUrltoBase64(url);
    }
    /* Time Service */
    showDatePicker(date, mode = 'date') {
        return this.times.showDatePicker(date, mode);
    }
    diffInHours(startDate, endDate) {
        return this.times.diffInHours(startDate, endDate);
    }
    formatDate(date) {
        return this.times.formatDate(date);
    }
    formatDateTime(date) {
        return this.times.formatDateTime(date);
    }
    formatDateMDYHM(date) {
        return this.times.formatDateMDYHM(date);
    }
    formatAMPM(_dt) {
        return this.times.formatAMPM(_dt);
    }
    customMDYHMformatDateMDYHM(_date) {
        return this.times.customMDYHMformatDateMDYHM(_date);
    }
    formatHoursToText(hour) {
        return this.times.formatHoursToText(hour);
    }
    formatDateMDY(date) {
        return this.times.formatDateMDY(date);
    }
    getCurrentLocationCoordinates() {
        return this.geolocations.getCurrentLocationCoordinates();
    }
    getCoordsForGeoAddress(address, _default = true) {
        return this.geolocations.getCoordsForGeoAddress(address, _default = true);
    }
};
UtilityService.ctorParameters = () => [
    { type: _basic_loading_service__WEBPACK_IMPORTED_MODULE_2__.LoadingService },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.Platform },
    { type: _basic_alerts_service__WEBPACK_IMPORTED_MODULE_1__.AlertsService },
    { type: _image_service__WEBPACK_IMPORTED_MODULE_4__.ImageService },
    { type: _times_service__WEBPACK_IMPORTED_MODULE_3__.TimesService },
    { type: _basic_strings_service__WEBPACK_IMPORTED_MODULE_0__.StringsService },
    { type: _basic_storage_service__WEBPACK_IMPORTED_MODULE_5__.StorageService },
    { type: _geolocation_service__WEBPACK_IMPORTED_MODULE_6__.GeolocationService }
];
UtilityService = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Injectable)({
        providedIn: 'root',
    })
], UtilityService);



/***/ }),

/***/ 92340:
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "environment": () => (/* binding */ environment)
/* harmony export */ });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    firebase: {
        apiKey: 'AIzaSyDv0QzTsHlTfp0nqWK8rPlzXLjhGd2FkcY',
        authDomain: 'bidrider-app.firebaseapp.com',
        databaseURL: 'https://bidrider-app.firebaseio.com',
        projectId: 'bidrider-app',
        storageBucket: 'bidrider-app.appspot.com',
        messagingSenderId: '945081768290',
        // apiKey: 'AIzaSyBDWkVRhZhE8P0bdfuRxmrzP9ZHqyeJl9s',
        // authDomain: 'ridebidder.firebaseapp.com',
        // databaseURL: 'https://ridebidder-default-rtdb.firebaseio.com',
        // projectId: 'ridebidder',
        // storageBucket: 'ridebidder.appspot.com',
        // messagingSenderId: '348445200010',
        // appId: '1:348445200010:web:4e25cf9475e176482e2773',
        // measurementId: 'G-7LBR4N1HL1',
    },
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ 14431:
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ 24608);
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/app.module */ 36747);
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./environments/environment */ 92340);




if (_environments_environment__WEBPACK_IMPORTED_MODULE_1__.environment.production) {
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.enableProdMode)();
}
(0,_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_3__.platformBrowserDynamic)().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_0__.AppModule)
    .catch(err => console.log(err));


/***/ }),

/***/ 50863:
/*!******************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm/ lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \******************************************************************************************************************************************/
/***/ ((module, __unused_webpack_exports, __webpack_require__) => {

var map = {
	"./ion-action-sheet.entry.js": [
		47321,
		"common",
		"node_modules_ionic_core_dist_esm_ion-action-sheet_entry_js"
	],
	"./ion-alert.entry.js": [
		36108,
		"common",
		"node_modules_ionic_core_dist_esm_ion-alert_entry_js"
	],
	"./ion-app_8.entry.js": [
		31489,
		"common",
		"node_modules_ionic_core_dist_esm_ion-app_8_entry_js"
	],
	"./ion-avatar_3.entry.js": [
		10305,
		"common",
		"node_modules_ionic_core_dist_esm_ion-avatar_3_entry_js"
	],
	"./ion-back-button.entry.js": [
		15830,
		"common",
		"node_modules_ionic_core_dist_esm_ion-back-button_entry_js"
	],
	"./ion-backdrop.entry.js": [
		37757,
		"node_modules_ionic_core_dist_esm_ion-backdrop_entry_js"
	],
	"./ion-button_2.entry.js": [
		30392,
		"common",
		"node_modules_ionic_core_dist_esm_ion-button_2_entry_js"
	],
	"./ion-card_5.entry.js": [
		66911,
		"common",
		"node_modules_ionic_core_dist_esm_ion-card_5_entry_js"
	],
	"./ion-checkbox.entry.js": [
		30937,
		"common",
		"node_modules_ionic_core_dist_esm_ion-checkbox_entry_js"
	],
	"./ion-chip.entry.js": [
		78695,
		"common",
		"node_modules_ionic_core_dist_esm_ion-chip_entry_js"
	],
	"./ion-col_3.entry.js": [
		16034,
		"node_modules_ionic_core_dist_esm_ion-col_3_entry_js"
	],
	"./ion-datetime_3.entry.js": [
		68837,
		"common",
		"node_modules_ionic_core_dist_esm_ion-datetime_3_entry_js"
	],
	"./ion-fab_3.entry.js": [
		34195,
		"common",
		"node_modules_ionic_core_dist_esm_ion-fab_3_entry_js"
	],
	"./ion-img.entry.js": [
		41709,
		"node_modules_ionic_core_dist_esm_ion-img_entry_js"
	],
	"./ion-infinite-scroll_2.entry.js": [
		33087,
		"node_modules_ionic_core_dist_esm_ion-infinite-scroll_2_entry_js"
	],
	"./ion-input.entry.js": [
		84513,
		"common",
		"node_modules_ionic_core_dist_esm_ion-input_entry_js"
	],
	"./ion-item-option_3.entry.js": [
		58056,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item-option_3_entry_js"
	],
	"./ion-item_8.entry.js": [
		10862,
		"common",
		"node_modules_ionic_core_dist_esm_ion-item_8_entry_js"
	],
	"./ion-loading.entry.js": [
		7509,
		"common",
		"node_modules_ionic_core_dist_esm_ion-loading_entry_js"
	],
	"./ion-menu_3.entry.js": [
		76272,
		"common",
		"node_modules_ionic_core_dist_esm_ion-menu_3_entry_js"
	],
	"./ion-modal.entry.js": [
		71855,
		"common",
		"node_modules_ionic_core_dist_esm_ion-modal_entry_js"
	],
	"./ion-nav_2.entry.js": [
		38708,
		"common",
		"node_modules_ionic_core_dist_esm_ion-nav_2_entry_js"
	],
	"./ion-popover.entry.js": [
		23527,
		"common",
		"node_modules_ionic_core_dist_esm_ion-popover_entry_js"
	],
	"./ion-progress-bar.entry.js": [
		24694,
		"common",
		"node_modules_ionic_core_dist_esm_ion-progress-bar_entry_js"
	],
	"./ion-radio_2.entry.js": [
		19222,
		"common",
		"node_modules_ionic_core_dist_esm_ion-radio_2_entry_js"
	],
	"./ion-range.entry.js": [
		25277,
		"common",
		"node_modules_ionic_core_dist_esm_ion-range_entry_js"
	],
	"./ion-refresher_2.entry.js": [
		39921,
		"common",
		"node_modules_ionic_core_dist_esm_ion-refresher_2_entry_js"
	],
	"./ion-reorder_2.entry.js": [
		83122,
		"common",
		"node_modules_ionic_core_dist_esm_ion-reorder_2_entry_js"
	],
	"./ion-ripple-effect.entry.js": [
		51602,
		"node_modules_ionic_core_dist_esm_ion-ripple-effect_entry_js"
	],
	"./ion-route_4.entry.js": [
		45174,
		"common",
		"node_modules_ionic_core_dist_esm_ion-route_4_entry_js"
	],
	"./ion-searchbar.entry.js": [
		7895,
		"common",
		"node_modules_ionic_core_dist_esm_ion-searchbar_entry_js"
	],
	"./ion-segment_2.entry.js": [
		76164,
		"common",
		"node_modules_ionic_core_dist_esm_ion-segment_2_entry_js"
	],
	"./ion-select_3.entry.js": [
		20592,
		"common",
		"node_modules_ionic_core_dist_esm_ion-select_3_entry_js"
	],
	"./ion-slide_2.entry.js": [
		27162,
		"node_modules_ionic_core_dist_esm_ion-slide_2_entry_js"
	],
	"./ion-spinner.entry.js": [
		81374,
		"common",
		"node_modules_ionic_core_dist_esm_ion-spinner_entry_js"
	],
	"./ion-split-pane.entry.js": [
		97896,
		"node_modules_ionic_core_dist_esm_ion-split-pane_entry_js"
	],
	"./ion-tab-bar_2.entry.js": [
		25043,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab-bar_2_entry_js"
	],
	"./ion-tab_2.entry.js": [
		77802,
		"common",
		"node_modules_ionic_core_dist_esm_ion-tab_2_entry_js"
	],
	"./ion-text.entry.js": [
		29072,
		"common",
		"node_modules_ionic_core_dist_esm_ion-text_entry_js"
	],
	"./ion-textarea.entry.js": [
		32191,
		"common",
		"node_modules_ionic_core_dist_esm_ion-textarea_entry_js"
	],
	"./ion-toast.entry.js": [
		40801,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toast_entry_js"
	],
	"./ion-toggle.entry.js": [
		67110,
		"common",
		"node_modules_ionic_core_dist_esm_ion-toggle_entry_js"
	],
	"./ion-virtual-scroll.entry.js": [
		10431,
		"node_modules_ionic_core_dist_esm_ion-virtual-scroll_entry_js"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(() => {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(() => {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = () => (Object.keys(map));
webpackAsyncContext.id = 50863;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 43069:
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-menu ion-content {\n  --background: var(--ion-item-background, var(--ion-background-color, #fff));\n}\n\nion-menu.md ion-content {\n  --padding-start: 8px;\n  --padding-end: 8px;\n  --padding-top: 20px;\n  --padding-bottom: 20px;\n}\n\nion-menu.md ion-list {\n  padding: 20px 0;\n}\n\nion-menu.md ion-note {\n  margin-bottom: 30px;\n}\n\nion-menu.md ion-list-header,\nion-menu.md ion-note {\n  padding-left: 10px;\n}\n\nion-menu.md ion-list#inbox-list {\n  border-bottom: 1px solid var(--ion-color-step-150, #d7d8da);\n}\n\nion-menu.md ion-list#inbox-list ion-list-header {\n  font-size: 22px;\n  font-weight: 600;\n  min-height: 20px;\n}\n\nion-menu.md ion-list#labels-list ion-list-header {\n  font-size: 16px;\n  margin-bottom: 18px;\n  color: #757575;\n  min-height: 26px;\n}\n\nion-menu.md ion-item {\n  --padding-start: 10px;\n  --padding-end: 10px;\n  border-radius: 4px;\n}\n\nion-menu.md ion-item.selected {\n  --background: rgba(var(--ion-color-primary-rgb), 0.14);\n}\n\nion-menu.md ion-item.selected ion-icon {\n  color: var(--ion-color-primary);\n}\n\nion-menu.md ion-item ion-icon {\n  color: #21599c;\n}\n\nion-menu ion-item ion-label {\n  font-weight: 500;\n  font-size: small;\n  color: var(--ion-color-gray);\n}\n\nion-menu.ios ion-content {\n  --padding-bottom: 20px;\n}\n\nion-menu.ios ion-list {\n  padding: 20px 0 0 0;\n}\n\nion-menu.ios ion-note {\n  line-height: 24px;\n  margin-bottom: 20px;\n}\n\nion-menu.ios ion-item {\n  --padding-start: 16px;\n  --padding-end: 16px;\n  --min-height: 50px;\n}\n\nion-menu.ios ion-item.selected ion-icon {\n  color: var(--ion-color-primary);\n}\n\nion-menu.ios ion-item ion-icon {\n  font-size: 20px;\n  color: var(--ion-color-light);\n}\n\nion-menu.ios ion-list#labels-list ion-list-header {\n  margin-bottom: 8px;\n}\n\nion-menu.ios ion-list-header,\nion-menu.ios ion-note {\n  padding-left: 16px;\n  padding-right: 16px;\n}\n\nion-menu.ios ion-note {\n  margin-bottom: 8px;\n}\n\nion-note {\n  display: inline-block;\n  font-size: 16px;\n  color: var(--ion-color-medium-shade);\n}\n\nion-item.selected {\n  --color: var(--ion-color-primary);\n}\n\n.user-img {\n  width: 100px;\n  height: 100px;\n  margin: auto;\n  margin-top: 20px;\n  border-radius: 15px;\n}\n\n.horizontal-centered-flex {\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n  width: 100%;\n}\n\n.user-name {\n  font-weight: bold;\n  font-size: 14px;\n  margin-top: 15px;\n}\n\n.ccid {\n  margin-bottom: 30px;\n  font-size: 8px;\n  margin-top: 7px;\n}\n\n.menu-icon {\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFwcC5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLDJFQUFBO0FBQ0Y7O0FBRUE7RUFDRSxvQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtBQUNGOztBQUVBO0VBQ0UsZUFBQTtBQUNGOztBQUVBO0VBQ0UsbUJBQUE7QUFDRjs7QUFFQTs7RUFFRSxrQkFBQTtBQUNGOztBQUVBO0VBQ0UsMkRBQUE7QUFDRjs7QUFFQTtFQUNFLGVBQUE7RUFDQSxnQkFBQTtFQUVBLGdCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBRUEsbUJBQUE7RUFFQSxjQUFBO0VBRUEsZ0JBQUE7QUFIRjs7QUFNQTtFQUNFLHFCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQUhGOztBQU1BO0VBQ0Usc0RBQUE7QUFIRjs7QUFNQTtFQUNFLCtCQUFBO0FBSEY7O0FBTUE7RUFDRSxjQUFBO0FBSEY7O0FBTUE7RUFDRSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0EsNEJBQUE7QUFIRjs7QUFNQTtFQUNFLHNCQUFBO0FBSEY7O0FBTUE7RUFDRSxtQkFBQTtBQUhGOztBQU1BO0VBQ0UsaUJBQUE7RUFDQSxtQkFBQTtBQUhGOztBQU1BO0VBQ0UscUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBSEY7O0FBTUE7RUFDRSwrQkFBQTtBQUhGOztBQU1BO0VBQ0UsZUFBQTtFQUNBLDZCQUFBO0FBSEY7O0FBTUE7RUFDRSxrQkFBQTtBQUhGOztBQU1BOztFQUVFLGtCQUFBO0VBQ0EsbUJBQUE7QUFIRjs7QUFNQTtFQUNFLGtCQUFBO0FBSEY7O0FBTUE7RUFDRSxxQkFBQTtFQUNBLGVBQUE7RUFFQSxvQ0FBQTtBQUpGOztBQU9BO0VBQ0UsaUNBQUE7QUFKRjs7QUFPQTtFQUNFLFlBQUE7RUFDQSxhQUFBO0VBQ0EsWUFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFKRjs7QUFPQTtFQUNFLGFBQUE7RUFDQSxzQkFBQTtFQUNBLG1CQUFBO0VBQ0EsdUJBQUE7RUFDQSxXQUFBO0FBSkY7O0FBT0E7RUFDRSxpQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtBQUpGOztBQU9BO0VBQ0UsbUJBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtBQUpGOztBQU9BO0VBQ0Usa0JBQUE7QUFKRiIsImZpbGUiOiJhcHAuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tbWVudSBpb24tY29udGVudCB7XHJcbiAgLS1iYWNrZ3JvdW5kOiB2YXIoLS1pb24taXRlbS1iYWNrZ3JvdW5kLCB2YXIoLS1pb24tYmFja2dyb3VuZC1jb2xvciwgI2ZmZikpO1xyXG59XHJcblxyXG5pb24tbWVudS5tZCBpb24tY29udGVudCB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiA4cHg7XHJcbiAgLS1wYWRkaW5nLWVuZDogOHB4O1xyXG4gIC0tcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgLS1wYWRkaW5nLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuaW9uLW1lbnUubWQgaW9uLWxpc3Qge1xyXG4gIHBhZGRpbmc6IDIwcHggMDtcclxufVxyXG5cclxuaW9uLW1lbnUubWQgaW9uLW5vdGUge1xyXG4gIG1hcmdpbi1ib3R0b206IDMwcHg7XHJcbn1cclxuXHJcbmlvbi1tZW51Lm1kIGlvbi1saXN0LWhlYWRlcixcclxuaW9uLW1lbnUubWQgaW9uLW5vdGUge1xyXG4gIHBhZGRpbmctbGVmdDogMTBweDtcclxufVxyXG5cclxuaW9uLW1lbnUubWQgaW9uLWxpc3QjaW5ib3gtbGlzdCB7XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1zdGVwLTE1MCwgI2Q3ZDhkYSk7XHJcbn1cclxuXHJcbmlvbi1tZW51Lm1kIGlvbi1saXN0I2luYm94LWxpc3QgaW9uLWxpc3QtaGVhZGVyIHtcclxuICBmb250LXNpemU6IDIycHg7XHJcbiAgZm9udC13ZWlnaHQ6IDYwMDtcclxuXHJcbiAgbWluLWhlaWdodDogMjBweDtcclxufVxyXG5cclxuaW9uLW1lbnUubWQgaW9uLWxpc3QjbGFiZWxzLWxpc3QgaW9uLWxpc3QtaGVhZGVyIHtcclxuICBmb250LXNpemU6IDE2cHg7XHJcblxyXG4gIG1hcmdpbi1ib3R0b206IDE4cHg7XHJcblxyXG4gIGNvbG9yOiAjNzU3NTc1O1xyXG5cclxuICBtaW4taGVpZ2h0OiAyNnB4O1xyXG59XHJcblxyXG5pb24tbWVudS5tZCBpb24taXRlbSB7XHJcbiAgLS1wYWRkaW5nLXN0YXJ0OiAxMHB4O1xyXG4gIC0tcGFkZGluZy1lbmQ6IDEwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNHB4O1xyXG59XHJcblxyXG5pb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCB7XHJcbiAgLS1iYWNrZ3JvdW5kOiByZ2JhKHZhcigtLWlvbi1jb2xvci1wcmltYXJ5LXJnYiksIDAuMTQpO1xyXG59XHJcblxyXG5pb24tbWVudS5tZCBpb24taXRlbS5zZWxlY3RlZCBpb24taWNvbiB7XHJcbiAgY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuaW9uLW1lbnUubWQgaW9uLWl0ZW0gaW9uLWljb24ge1xyXG4gIGNvbG9yOiAjMjE1OTljO1xyXG59XHJcblxyXG5pb24tbWVudSBpb24taXRlbSBpb24tbGFiZWwge1xyXG4gIGZvbnQtd2VpZ2h0OiA1MDA7XHJcbiAgZm9udC1zaXplOiBzbWFsbDtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWdyYXkpO1xyXG59XHJcblxyXG5pb24tbWVudS5pb3MgaW9uLWNvbnRlbnQge1xyXG4gIC0tcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbmlvbi1tZW51LmlvcyBpb24tbGlzdCB7XHJcbiAgcGFkZGluZzogMjBweCAwIDAgMDtcclxufVxyXG5cclxuaW9uLW1lbnUuaW9zIGlvbi1ub3RlIHtcclxuICBsaW5lLWhlaWdodDogMjRweDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG5pb24tbWVudS5pb3MgaW9uLWl0ZW0ge1xyXG4gIC0tcGFkZGluZy1zdGFydDogMTZweDtcclxuICAtLXBhZGRpbmctZW5kOiAxNnB4O1xyXG4gIC0tbWluLWhlaWdodDogNTBweDtcclxufVxyXG5cclxuaW9uLW1lbnUuaW9zIGlvbi1pdGVtLnNlbGVjdGVkIGlvbi1pY29uIHtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLXByaW1hcnkpO1xyXG59XHJcblxyXG5pb24tbWVudS5pb3MgaW9uLWl0ZW0gaW9uLWljb24ge1xyXG4gIGZvbnQtc2l6ZTogMjBweDtcclxuICBjb2xvcjogdmFyKC0taW9uLWNvbG9yLWxpZ2h0KTtcclxufVxyXG5cclxuaW9uLW1lbnUuaW9zIGlvbi1saXN0I2xhYmVscy1saXN0IGlvbi1saXN0LWhlYWRlciB7XHJcbiAgbWFyZ2luLWJvdHRvbTogOHB4O1xyXG59XHJcblxyXG5pb24tbWVudS5pb3MgaW9uLWxpc3QtaGVhZGVyLFxyXG5pb24tbWVudS5pb3MgaW9uLW5vdGUge1xyXG4gIHBhZGRpbmctbGVmdDogMTZweDtcclxuICBwYWRkaW5nLXJpZ2h0OiAxNnB4O1xyXG59XHJcblxyXG5pb24tbWVudS5pb3MgaW9uLW5vdGUge1xyXG4gIG1hcmdpbi1ib3R0b206IDhweDtcclxufVxyXG5cclxuaW9uLW5vdGUge1xyXG4gIGRpc3BsYXk6IGlubGluZS1ibG9jaztcclxuICBmb250LXNpemU6IDE2cHg7XHJcblxyXG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItbWVkaXVtLXNoYWRlKTtcclxufVxyXG5cclxuaW9uLWl0ZW0uc2VsZWN0ZWQge1xyXG4gIC0tY29sb3I6IHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcclxufVxyXG5cclxuLnVzZXItaW1nIHtcclxuICB3aWR0aDogMTAwcHg7XHJcbiAgaGVpZ2h0OiAxMDBweDtcclxuICBtYXJnaW46IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMjBweDtcclxuICBib3JkZXItcmFkaXVzOiAxNXB4O1xyXG59XHJcblxyXG4uaG9yaXpvbnRhbC1jZW50ZXJlZC1mbGV4IHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgYWxpZ24taXRlbXM6IGNlbnRlcjtcclxuICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnVzZXItbmFtZSB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuXHJcbi5jY2lkIHtcclxuICBtYXJnaW4tYm90dG9tOiAzMHB4O1xyXG4gIGZvbnQtc2l6ZTogOHB4O1xyXG4gIG1hcmdpbi10b3A6IDdweDtcclxufVxyXG5cclxuLm1lbnUtaWNvbiB7XHJcbiAgbWFyZ2luLXJpZ2h0OiAxMHB4O1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 22985:
/*!*****************************************************!*\
  !*** ./src/app/components/gmap/gmap.component.scss ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("ion-content {\n  overflow: hidden;\n}\n\n#map_canvas {\n  height: 100vh;\n  width: 100vw;\n}\n\n#map {\n  height: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdtYXAuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxnQkFBQTtBQUNGOztBQUVBO0VBQ0UsYUFBQTtFQUNBLFlBQUE7QUFDRjs7QUFDQTtFQUNFLFlBQUE7QUFFRiIsImZpbGUiOiJnbWFwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWNvbnRlbnR7XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxufVxyXG5cclxuI21hcF9jYW52YXN7XHJcbiAgaGVpZ2h0OiAxMDB2aDtcclxuICB3aWR0aDogMTAwdnc7XHJcbn1cclxuI21hcCB7XHJcbiAgaGVpZ2h0OiAxMDAlO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 91106:
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-app>\r\n  <ion-split-pane contentId=\"main-content\">\r\n    <ion-menu contentId=\"main-content\" type=\"overlay\" menuId=\"drawer\">\r\n      <ion-content>\r\n        <ion-list id=\"inbox-list\">\r\n          <ion-list-header>\r\n            <div class=\"horizontal-centered-flex\">\r\n              <img class=\"user-img\" onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\"\r\n                [src]=\"user?.profile_image ?? 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAb1BMVEX///9UWV1PVVmBhIZKUFSztbdCSE1FS09OU1dGTFBARkv8/Pzh4uJKT1RESU5NUlfKy8z39/fx8fFaX2NobG+JjI7q6+umqKqQk5VgZGjExcbV1tducnWanJ6Dhoh0eHu6vL2ho6Xc3d17foGur7GvHrXYAAAGTklEQVR4nO2d65KqOhBGJRPDHREEL4yCyvs/45HxOO4ZRQmk6WbvXlVW+TNfpdOXkHRmM4ZhGIZhGIb5ZnmK5+tNdvg4ZJv1PD4tsQdkEr+oP1LbDuXCcRxx+S1kaEfWuS587KGZIKnOF3HCekRINzrPc+wBDsOvPqOn6r5VhtFnNd2ZzPehfCXvJtLdT3Mi84NavJV3ZaEOAfZwtUky5XTU1+CoLMEesh5rLX3XeVxjD1qDUyo19TXI9IQ98K7svR76Grw99tA7kWz7TOCVcDWB1Vi47wNEO8ItsAW8Y97XQm94c2wJr9mrgQItKyK9GDfuYIGW5W6wZbSTmRB4kZhhC2nDyAx+SSRqqHVkSOBlLdbYYp6xG+5k7ng7bDmPBCYFWpYiV2z4RvU1UKuLD7q1xDucA7aknxhdhFcUqaW47J9styMpbTgat9EGSnZ6GppuP8ejUxGvhhRM7YgVtrAbOxtEoGXZVJxNCiTQslJsaVdiUwn3I3aMLe6LT5hV2CA+scU1nMwH+zuKgjvdQMTCGw6Bet+HcqRXXPwEfBeCKgzxAwZIwnaHQOoGFyquuNgCC3CF2JvgR1gjvZjpEVkhYLi/gh30fWgjRY8Xgbk90jYi3F034GjYgBwR112PW/Rngft9P4N2pRdnivudBtyVojtTuPL+Dm6hDx8ssPM2mG3En3iYApeQ9f0Nhbn3zQpZIX2Ff7+nmUF8VfvNAlXhGBF/i6qwHCFrK1EVbuBrC+RN4Rp+IUrc00PxCBUw7venfIRdDOTLGPAVMG6wmM3O4LuJZ2SFNfRCDLGPKQawH9fQNxMvgFsptkDYT8Do8b6hgDVTG/vT0wzaTPGNdDZbQyZuksKFthz0tAmJ26WAX/IJfMVvADwyROLA0AywDEYufu+ATSKVKbysRKATtDRWYUMCdAqa0IXSNUSFEVKIhd9szdupwN1F/E1g3k499LLpJ7Xpb6UuduX7QGk2s3GohMI7vmV2KeKfnH0gN1ko2iQy7t8U5ryNR6DufcbOlESKd2SvVGYkehW2kHaMzCLdGWyIh5cZisZNoFaCDr2vXiFCYqnMI8lqSBY+iQY1Q/qbKPz9307ETr8MznGIL8E7fubpr0bhZQQztVaKrW6t4W6J5jGtVJbOXrgUFfaAe1CldjdbFW5aYQ+2J3Gp3k+kVJ+TcTBPyNdb9aK9pwjVdk2yUNIhqEvbls5vmWIhbbusyWcw3fBP881KKtuVMgxDKV1bhavN/DSl6NCFJCh2VTWvql0RTCE3YxiGYRiG+Rfxl0meB1fyPFn+HTlpEsRVvc/KVSpcpaILdkPzRylXpKsy26+reIpZalJUx4+tGzXVxMIRbQWiEM6iqTQiNz0fq2IiQvPd8WwpN3woCF8jnNBVVnncka6H/aI+29FjsaulM7Kpvs5yKeVDe+BHi/9lStstazIHvr7w443z/C2Z3irDaLGJqUxlnIUuSHdPV2YEduFOGznoqYB3IsMNqrn61TYC76IUbedY1prv3TGuAVuWxHlMKDhoPyXTH4THhILziPoaHHUeU2N+8MbV1yDUYTRb3UfwN7ifaoz2o/icnRzHvzxDjtAWKynhb/6+IiqBK5AKyUDvCAX51M6yhL4x2gW7BGt2Ugyoi0wiHKBDDfUYzWi6oUAOgWcULPSGbb5NnT/ouJp55MpwaExSGkvwjpMaDRs5ER/zJ8IxmMTlrfuBmIiFMYkJSYHNLBoy1CW5NXhDpGZi/2r8SqkrjpFXTA54pcR7pIFrpjWlQP+IPTi7Keikas8Zehvap+pk7ohhyU0G349tKMM6KRu4AgPPoEs29Gewwekv8EirnmhD9n4+IcHddOpO1Dd7G6Hhuhn6OhvQ1jpm8fpVGcBdvEzSryPYkna69pOoT5ExQmtSc/RqcjpGi2Bz9Hhe4DQlI73UGPoJOPgzR2bp8WjStIy0h5lOKBhe0e5dV03JkzbISlPhCI26zaId9LfYI9ZGs62UP4266U+k3m7GCH26TaPZ9xv8WUPzaD6UuJuaK71Yqd52TTW9dRhWWgrnE5xDvVMorJAgrJAV0ocVskL6sEJWSB9WyArpwwpZIX1YISukDytkhfRhhayQPqyQFdKHFbJC+ugqtMXUsPUU7s4fU+NM+vEWhmEYhmEY5jX/ASVYkKOp66h3AAAAAElFTkSuQmCC'\" />\r\n              <ion-label class=\"user-name\">\r\n                Hi, {{user?.fullname}}\r\n              </ion-label>\r\n\r\n              <!-- <ion-label class=\"ccid\">CCID: 12345 6789</ion-label> -->\r\n\r\n            </div>\r\n\r\n          </ion-list-header>\r\n\r\n          <ion-menu-toggle auto-hide=\"false\" *ngFor=\"let p of appPages; let i = index\">\r\n            <ion-item routerDirection=\"root\" [routerLink]=\"[p.url]\" lines=\"none\" detail=\"true\">\r\n              <ion-icon [name]=\"p.icon\" class=\"menu-icon\" color=\"black\"></ion-icon>\r\n              <!-- <ion-icon [name]=\"p.icon\" class=\"menu-icon\"> </ion-icon> -->\r\n              <!-- <ion-icon slot=\"start\" [ios]=\"p.icon + '-outline'\" [md]=\"p.icon + '-sharp'\" class=\"menu-icon\"></ion-icon> -->\r\n              <ion-label (click)=\"handleClicked(p.title)\" color=\"gray\">{{ p.title }}</ion-label>\r\n            </ion-item>\r\n          </ion-menu-toggle>\r\n        </ion-list>\r\n      </ion-content>\r\n    </ion-menu>\r\n    <ion-router-outlet id=\"main-content\"></ion-router-outlet>\r\n  </ion-split-pane>\r\n</ion-app>");

/***/ }),

/***/ 1921:
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/gmap/gmap.component.html ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-buttons slot=\"start\">\r\n      <ion-button (click)=\"closeModal({ data: 'A' })\">\r\n        <ion-icon name=\"arrow-back\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>Map Place</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button *ngIf=\"!isDirections\" (click)=\"getMarkerLocation()\">\r\n        <!-- <ion-icon name=\"add\"></ion-icon> -->\r\n        <ion-label>Done</ion-label>\r\n      </ion-button>\r\n      <ion-button *ngIf=\"isBothDirectionsAvailable\" (click)=\"getDirection()\">\r\n        <ion-icon name=\"navigate\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n  <ion-toolbar color=\"primary\">\r\n    <ion-searchbar #searchbox></ion-searchbar>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-no-bounce\">\r\n  <div #map id=\"map\"></div>\r\n</ion-content>");

/***/ })

},
/******/ __webpack_require__ => { // webpackRuntimeModules
/******/ "use strict";
/******/ 
/******/ var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
/******/ __webpack_require__.O(0, ["vendor"], () => (__webpack_exec__(14431)));
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);
//# sourceMappingURL=main.js.map