(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_currentbookings_currentbookings_module_ts"],{

/***/ 42498:
/*!******************************************!*\
  !*** ./src/app/Services/data.service.ts ***!
  \******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DataService": () => (/* binding */ DataService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);


let DataService = class DataService {
    constructor() { }
};
DataService.ctorParameters = () => [];
DataService = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.Injectable)({
        providedIn: 'root'
    })
], DataService);



/***/ }),

/***/ 92456:
/*!*************************************************************!*\
  !*** ./src/app/components/emptyview/emptyview.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EmptyviewComponent": () => (/* binding */ EmptyviewComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_emptyview_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./emptyview.component.html */ 17862);
/* harmony import */ var _emptyview_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./emptyview.component.scss */ 8572);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);




let EmptyviewComponent = class EmptyviewComponent {
    constructor() {
        this.type = 'icon';
    }
    ngOnInit() { }
};
EmptyviewComponent.ctorParameters = () => [];
EmptyviewComponent.propDecorators = {
    message: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    icon: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }],
    type: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
EmptyviewComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-emptyview',
        template: _raw_loader_emptyview_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_emptyview_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], EmptyviewComponent);



/***/ }),

/***/ 92207:
/*!**********************************************************!*\
  !*** ./src/app/components/emptyview/emptyview.module.ts ***!
  \**********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "EmptyviewModule": () => (/* binding */ EmptyviewModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _emptyview_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./emptyview.component */ 92456);






let EmptyviewModule = class EmptyviewModule {
};
EmptyviewModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_emptyview_component__WEBPACK_IMPORTED_MODULE_0__.EmptyviewComponent],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.ReactiveFormsModule],
        exports: [_emptyview_component__WEBPACK_IMPORTED_MODULE_0__.EmptyviewComponent],
    })
], EmptyviewModule);



/***/ }),

/***/ 39269:
/*!*************************************************************************!*\
  !*** ./src/app/pages/currentbookings/currentbookings-routing.module.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentbookingsPageRoutingModule": () => (/* binding */ CurrentbookingsPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _currentbookings_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./currentbookings.page */ 33721);




const routes = [
    {
        path: '',
        component: _currentbookings_page__WEBPACK_IMPORTED_MODULE_0__.CurrentbookingsPage
    }
];
let CurrentbookingsPageRoutingModule = class CurrentbookingsPageRoutingModule {
};
CurrentbookingsPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], CurrentbookingsPageRoutingModule);



/***/ }),

/***/ 40566:
/*!*****************************************************************!*\
  !*** ./src/app/pages/currentbookings/currentbookings.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentbookingsPageModule": () => (/* binding */ CurrentbookingsPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _currentbookings_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./currentbookings-routing.module */ 39269);
/* harmony import */ var _currentbookings_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./currentbookings.page */ 33721);
/* harmony import */ var src_app_components_emptyview_emptyview_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/emptyview/emptyview.module */ 92207);








let CurrentbookingsPageModule = class CurrentbookingsPageModule {
};
CurrentbookingsPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _currentbookings_routing_module__WEBPACK_IMPORTED_MODULE_0__.CurrentbookingsPageRoutingModule,
            src_app_components_emptyview_emptyview_module__WEBPACK_IMPORTED_MODULE_2__.EmptyviewModule,
        ],
        declarations: [_currentbookings_page__WEBPACK_IMPORTED_MODULE_1__.CurrentbookingsPage],
    })
], CurrentbookingsPageModule);



/***/ }),

/***/ 33721:
/*!***************************************************************!*\
  !*** ./src/app/pages/currentbookings/currentbookings.page.ts ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CurrentbookingsPage": () => (/* binding */ CurrentbookingsPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_currentbookings_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./currentbookings.page.html */ 54131);
/* harmony import */ var _currentbookings_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./currentbookings.page.scss */ 38370);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/Services/data.service */ 42498);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 24282);






let CurrentbookingsPage = class CurrentbookingsPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    constructor(injector, dataService) {
        super(injector);
        this.dataService = dataService;
        this.bookingList = [];
    }
    openActiveBooking(position) {
        //  if(this.bookingList[position].driver==null){
        //  alert("Driver not assigned");
        // }else{
        console.log(this.bookingList[position]);
        if (this.bookingList[position].status == 'Bidding Start') {
            this.dataService.data = {
                booking: this.bookingList[position],
                bookingKey: this.bookingList[position].uid,
            };
            this.nav.push('pages/bid-driver');
        }
        else if (this.bookingList[position].status == 'REVIEW') {
            let geoRef = this.getDb().ref('geofire');
            geoRef.child(this.bookingList[position].uid).remove();
            this.nav.push('pages/reviewdriver', {
                bookingKey: this.bookingList[position].uid,
            });
        }
        else {
            this.nav.push('pages/activebooking', {
                bookingKey: this.bookingList[position].uid,
            });
        }
        //  }
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            let ref = this.getDb().ref('/users/' + this.curUser.uid);
            console.log("currentBooking -> ", ref);
            ref.on('value', (snapshot) => {
                console.log("snapshot", snapshot.val());
                if (snapshot.val()) {
                    console.log(snapshot.val());
                    this.bookingList = [];
                    this.zone.run(() => {
                        this.userProfile = snapshot.val();
                        for (let key in this.userProfile.bookings) {
                            this.userProfile.bookings[key].uid = key;
                            if (this.userProfile.bookings[key].status != 'ENDED' &&
                                this.userProfile.bookings[key].status != 'CANCELLED' &&
                                this.userProfile.bookings[key].customer == this.curUser.uid) {
                                console.log(this.userProfile.bookings[key]);
                                if (this.userProfile.bookings[key].driver == null) {
                                    this.userProfile.bookings[key].image_url =
                                        'assets/images/notfound.png';
                                }
                                else {
                                    this.userProfile.bookings[key].image_url =
                                        this.userProfile.bookings[key].driver_image;
                                }
                                this.bookingList.push(this.userProfile.bookings[key]);
                            }
                        }
                        this.bookingList.sort(function (a, b) {
                            let aa = Number(new Date(a.tripdate));
                            let bb = Number(new Date(b.tripdate));
                            console.log(bb - aa);
                            return bb - aa;
                        });
                    });
                }
            });
        });
    }
    getDb() {
        return this.firebaseService.getDatabase();
    }
};
CurrentbookingsPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector },
    { type: src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_2__.DataService }
];
CurrentbookingsPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-currentbookings',
        template: _raw_loader_currentbookings_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_currentbookings_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], CurrentbookingsPage);



/***/ }),

/***/ 8572:
/*!***************************************************************!*\
  !*** ./src/app/components/emptyview/emptyview.component.scss ***!
  \***************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".center-dflex-white {\n  position: absolute;\n  top: 50%;\n  width: 50%;\n  /* margin: 0 auto; */\n  /* margin: 0 auto; */\n  left: 50%;\n  transform: translate(-50%, -50%);\n  text-align: center;\n}\n\n.center-dflex-white ion-img {\n  width: 100px;\n  margin: 0 auto;\n}\n\n.center-dflex-white ion-icon {\n  font-size: 4.5em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImVtcHR5dmlldy5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFBO0VBQ0EsUUFBQTtFQUNBLFVBQUE7RUFDQSxvQkFBQTtFQUNBLG9CQUFBO0VBQ0EsU0FBQTtFQUNBLGdDQUFBO0VBQ0Esa0JBQUE7QUFDRjs7QUFHQTtFQUNFLFlBQUE7RUFDQSxjQUFBO0FBQUY7O0FBR0E7RUFDRSxnQkFBQTtBQUFGIiwiZmlsZSI6ImVtcHR5dmlldy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5jZW50ZXItZGZsZXgtd2hpdGUge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICB0b3A6IDUwJTtcclxuICB3aWR0aDogNTAlO1xyXG4gIC8qIG1hcmdpbjogMCBhdXRvOyAqL1xyXG4gIC8qIG1hcmdpbjogMCBhdXRvOyAqL1xyXG4gIGxlZnQ6IDUwJTtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZSgtNTAlLCAtNTAlKTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgLy8gY29sb3I6ICNmZmY7XHJcbn1cclxuXHJcbi5jZW50ZXItZGZsZXgtd2hpdGUgaW9uLWltZyB7XHJcbiAgd2lkdGg6IDEwMHB4O1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG59XHJcblxyXG4uY2VudGVyLWRmbGV4LXdoaXRlIGlvbi1pY29uIHtcclxuICBmb250LXNpemU6IDQuNWVtO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 38370:
/*!*****************************************************************!*\
  !*** ./src/app/pages/currentbookings/currentbookings.page.scss ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".bookings {\n  width: 100%;\n  margin: 0 auto;\n}\n\n.bookings img {\n  width: 100%;\n  display: block;\n  margin: 0 auto;\n  border-radius: 50%;\n}\n\n.list-ios .item-block .item-inner {\n  padding-right: 0;\n}\n\n/*.item-md.item-block .item-inner{padding-right: 0;}*/\n\n.bookings h4 {\n  font-family: \"PT Sans\";\n  font-size: 13px;\n  text-align: left;\n  color: #222;\n  padding-bottom: 3px;\n  margin: 0;\n  padding-left: 2%;\n}\n\n.bookings p {\n  margin: 0;\n  font-family: \"PT Sans\";\n  font-size: 13px;\n  text-align: left;\n  color: #2e2e2e;\n  padding-bottom: 2px;\n  white-space: normal;\n  padding-top: 2%;\n  padding-left: 2%;\n}\n\n.bookings p span {\n  font-weight: bold;\n  color: #222;\n}\n\n.bookings h3 {\n  text-align: right;\n  font-family: \"PT Sans\";\n  font-size: 12px;\n  color: #2e2e2e;\n  margin: 0;\n  font-weight: bold;\n}\n\n.no-padding {\n  padding: 0 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImN1cnJlbnRib29raW5ncy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxXQUFBO0VBQ0EsY0FBQTtBQUNGOztBQUNBO0VBQ0UsV0FBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFFRjs7QUFBQTtFQUNFLGdCQUFBO0FBR0Y7O0FBREEscURBQUE7O0FBQ0E7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLFNBQUE7RUFDQSxnQkFBQTtBQUlGOztBQUZBO0VBQ0UsU0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7QUFLRjs7QUFIQTtFQUNFLGlCQUFBO0VBQ0EsV0FBQTtBQU1GOztBQUhBO0VBQ0UsaUJBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLGlCQUFBO0FBTUY7O0FBSkE7RUFDRSxxQkFBQTtBQU9GIiwiZmlsZSI6ImN1cnJlbnRib29raW5ncy5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuYm9va2luZ3Mge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG59XHJcbi5ib29raW5ncyBpbWcge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGJvcmRlci1yYWRpdXM6IDUwJTtcclxufVxyXG4ubGlzdC1pb3MgLml0ZW0tYmxvY2sgLml0ZW0taW5uZXIge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDA7XHJcbn1cclxuLyouaXRlbS1tZC5pdGVtLWJsb2NrIC5pdGVtLWlubmVye3BhZGRpbmctcmlnaHQ6IDA7fSovXHJcbi5ib29raW5ncyBoNCB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTNweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGNvbG9yOiAjMjIyO1xyXG4gIHBhZGRpbmctYm90dG9tOiAzcHg7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmctbGVmdDogMiU7XHJcbn1cclxuLmJvb2tpbmdzIHAge1xyXG4gIG1hcmdpbjogMDtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxM3B4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgcGFkZGluZy1ib3R0b206IDJweDtcclxuICB3aGl0ZS1zcGFjZTogbm9ybWFsO1xyXG4gIHBhZGRpbmctdG9wOiAyJTtcclxuICBwYWRkaW5nLWxlZnQ6IDIlO1xyXG59XHJcbi5ib29raW5ncyBwIHNwYW4ge1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIGNvbG9yOiAjMjIyO1xyXG59XHJcblxyXG4uYm9va2luZ3MgaDMge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDEycHg7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5uby1wYWRkaW5nIHtcclxuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 17862:
/*!*****************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/emptyview/emptyview.component.html ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div class=\"center-dflex-white\">\r\n  <ion-icon *ngIf=\"type == 'icon'\" [name]=\"icon\"></ion-icon>\r\n  <img *ngIf=\"type == 'image'\" [src]=\"icon\" />\r\n  <h2 [innerHTML]=\"message\"></h2>\r\n</div>");

/***/ }),

/***/ 54131:
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/currentbookings/currentbookings.page.html ***!
  \*******************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Bookings page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!--<ion-header>\r\n  <ion-toolbarbar color=\"headerblue\">\r\n      <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">BOOKINGS</span></ion-title>\r\n  </ion-toolbarbar>\r\n</ion-header>-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title>BOOKINGS</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content color=\"light\">\r\n  <app-emptyview *ngIf=\"bookingList.length == 0 \" [icon]=\"'file-tray-outline'\" [type]=\"'icon'\"\r\n    [message]=\"'You Have No Current Bookings'\"></app-emptyview>\r\n\r\n  <ion-grid *ngIf=\"bookingList.length != 0 \">\r\n    <ion-row *ngFor=\"let booking of bookingList; let i = index;\" (click)=\"openActiveBooking(i)\">\r\n      <ion-col size=\"12\" class=\"ion-padding\">\r\n        <ion-item lines=\"none\">\r\n          <ion-avatar slot=\"start\">\r\n            <img [src]=\"booking.image_url !== '' ? booking.image_url : 'assets/images/notfound.png'\"\r\n              onerror=\"this.onerror=null;this.src='assets/images/notfound.png';\" />\r\n          </ion-avatar>\r\n          <ion-label>\r\n            <p *ngIf=\"booking.tripdate\">\r\n              <small>{{booking.tripdate.substring(8,10)}}{{booking.tripdate.substring(4,8)}}{{booking.tripdate.substring(0,4)}}\r\n                {{booking.tripdate.substring(11,16)}}</small>\r\n            </p>\r\n            <p *ngIf=\"booking.status === 'REVIEW'\">PLEASE REVIEW YOUR DRIVER</p>\r\n          </ion-label>\r\n          <ion-badge slot=\"end\" *ngIf=\"booking.status !== 'REVIEW'\">{{booking.status}}</ion-badge>\r\n        </ion-item>\r\n        <ion-item lines=\"none\">\r\n          <ion-label class=\"ion-text-wrap\">\r\n            <h2>{{booking.serviceType}}</h2>\r\n\r\n            <h2 *ngIf=\"booking.dropAddress\" style=\"margin-top: 10px\">\r\n              Pickup Address:\r\n            </h2>\r\n            <p style=\"color: gray\">{{booking.pickupAddress}}</p>\r\n            <div *ngIf=\"booking.dropAddress\" style=\"margin-top: 10px\">\r\n              <h2>Drop Address:</h2>\r\n              <p style=\"color: gray\">{{booking.dropAddress}}</p>\r\n            </div>\r\n          </ion-label>\r\n          <ion-icon slot=\"end\" name=\"location-outline\" class=\"ion-margin-end\"></ion-icon>\r\n        </ion-item>\r\n\r\n        <ion-button expand=\"block\" (click)=\"openActiveBooking(i)\">Open Active Booking</ion-button>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_currentbookings_currentbookings_module_ts.js.map