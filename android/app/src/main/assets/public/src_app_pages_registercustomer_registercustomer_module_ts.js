(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_registercustomer_registercustomer_module_ts"],{

/***/ 9300:
/*!***************************************************************************!*\
  !*** ./src/app/pages/registercustomer/registercustomer-routing.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistercustomerPageRoutingModule": () => (/* binding */ RegistercustomerPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _registercustomer_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registercustomer.page */ 86481);




const routes = [
    {
        path: '',
        component: _registercustomer_page__WEBPACK_IMPORTED_MODULE_0__.RegistercustomerPage
    }
];
let RegistercustomerPageRoutingModule = class RegistercustomerPageRoutingModule {
};
RegistercustomerPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], RegistercustomerPageRoutingModule);



/***/ }),

/***/ 58182:
/*!*******************************************************************!*\
  !*** ./src/app/pages/registercustomer/registercustomer.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistercustomerPageModule": () => (/* binding */ RegistercustomerPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _registercustomer_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./registercustomer-routing.module */ 9300);
/* harmony import */ var _registercustomer_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registercustomer.page */ 86481);







let RegistercustomerPageModule = class RegistercustomerPageModule {
};
RegistercustomerPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _registercustomer_routing_module__WEBPACK_IMPORTED_MODULE_0__.RegistercustomerPageRoutingModule
        ],
        declarations: [_registercustomer_page__WEBPACK_IMPORTED_MODULE_1__.RegistercustomerPage]
    })
], RegistercustomerPageModule);



/***/ }),

/***/ 86481:
/*!*****************************************************************!*\
  !*** ./src/app/pages/registercustomer/registercustomer.page.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RegistercustomerPage": () => (/* binding */ RegistercustomerPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_registercustomer_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./registercustomer.page.html */ 57188);
/* harmony import */ var _registercustomer_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./registercustomer.page.scss */ 45687);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var _terms_terms_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../terms/terms.page */ 32278);






let RegistercustomerPage = class RegistercustomerPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            if (this.curUser) {
                this.showInput = false;
                this.fullname = this.curUser.displayName;
                this.email = this.curUser.email;
            }
            else {
                this.showInput = true;
            }
        });
    }
    backClick() {
        console.log('backCliked');
    }
    onSignUpSuccess() {
        console.log('Restration Succesfull');
    }
    onSignUpFailed() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Registration',
                subHeader: 'Failed',
                buttons: ['OK'],
            });
            alert.present();
        });
    }
    onFirebaseFailed(error) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Registration',
                subHeader: error,
                buttons: ['OK'],
            });
            alert.present();
        });
    }
    wrongUserInfo() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Registration',
                subHeader: 'Please Enter Vaild Details',
                buttons: ['OK'],
            });
            alert.present();
        });
    }
    doRegistration() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            if (this.curUser) {
                if (this.fullname == undefined ||
                    this.email == undefined ||
                    this.phone == undefined ||
                    this.cityname == undefined ||
                    this.fullname == '' ||
                    this.email == '' ||
                    this.phone == '' ||
                    this.cityname == '') {
                    this.wrongUserInfo();
                    //     ||
                    // this.vehicle== ""
                }
                else {
                    this.usersService
                        .insertUser(this.fullname, this.email, this.phone, this.cityname, 12345, this.curUser)
                        .then(() => this.nav.setRoot('pages/locationloader'))
                        .catch((_error) => this.onSignUpFailed());
                }
            }
            else {
                if (this.fullname == undefined ||
                    this.email == undefined ||
                    this.password == undefined ||
                    this.confirmPassword == undefined ||
                    this.phone == undefined ||
                    this.cityname == undefined ||
                    this.fullname == '' ||
                    this.email == '' ||
                    this.password == '' ||
                    this.confirmPassword == '' ||
                    this.phone == '' ||
                    this.cityname == '') {
                    this.wrongUserInfo();
                    //      ||
                    // this.vehicle== ""
                }
                else {
                    if (this.password == this.confirmPassword) {
                        this.usersService
                            .signUpUser(this.fullname, this.email, this.password, this.phone, this.cityname, 12345)
                            .then(() => (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
                            console.log('Registration Success');
                            let alert = yield this.alertCtrl.create({
                                header: 'Registration',
                                subHeader: 'Registration Success',
                                buttons: ['OK'],
                            });
                            alert.present().then(() => {
                                this.nav.pop();
                            });
                        }))
                            .catch((_error) => this.onFirebaseFailed(_error.toString()));
                    }
                    else {
                        let alert = yield this.alertCtrl.create({
                            header: 'Registration',
                            subHeader: 'Password do not match',
                            buttons: ['OK'],
                        });
                        alert.present();
                    }
                }
            }
        });
    }
    goToSignin() {
        this.nav.push('pages/login');
    }
    modalopen() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__awaiter)(this, void 0, void 0, function* () {
            console.log('works');
            let modal = yield this.modals.present({ component: _terms_terms_page__WEBPACK_IMPORTED_MODULE_3__.TermsPage });
        });
    }
    typePhoneNumber(ev) {
        // let v = $event.target.value;
        // console.log(v)
        if (ev.inputType !== 'deleteContentBackward') {
            const utel = this.utility.onkeyupFormatPhoneNumberRuntime(ev.target.value, false);
            console.log(utel);
            ev.target.value = utel;
            this.phone = utel;
            // this.aForm.controls['phone_number'].patchValue(utel);
            // ev.target.value = utel;
        }
    }
};
RegistercustomerPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Injector }
];
RegistercustomerPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-registercustomer',
        template: _raw_loader_registercustomer_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_registercustomer_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], RegistercustomerPage);



/***/ }),

/***/ 45687:
/*!*******************************************************************!*\
  !*** ./src/app/pages/registercustomer/registercustomer.page.scss ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".ion-button.activated {\n  background-color: #000 !important;\n}\n\nion-button:hover {\n  background-color: #000 !important;\n}\n\n.support-section {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n  margin-top: 15%;\n}\n\n.heavy {\n  font-weight: bold;\n}\n\n.bannerterm {\n  width: 100%;\n  padding-top: 180px;\n  background: url(\"/assets/images/term.png\") no-repeat;\n  position: relative;\n  background-size: cover;\n}\n\n.aboutlogo {\n  position: absolute;\n  display: block;\n  width: auto;\n  margin: auto;\n  top: 72%;\n  left: 0;\n  right: 0;\n}\n\n.headerabout {\n  color: #2e2e2e;\n  font-size: 18px;\n  text-align: center;\n  font-family: \"PT Sans\";\n  padding-top: 1%;\n  font-weight: bold;\n}\n\n.blueline {\n  display: block !important;\n  margin: 0 auto !important;\n  width: auto !important;\n  border-radius: 0 !important;\n}\n\n.aboutpara {\n  font-family: \"PT Sans\";\n  color: #919191;\n  font-size: 16px;\n  text-align: left;\n  line-height: 18px;\n  padding: 12px 20px 0 20px;\n  margin: 0px;\n}\n\n.aboutpara span {\n  color: #222;\n}\n\n.ashrectangle, .ashrectangle-review {\n  width: 90%;\n  margin: 12px auto;\n  background: #f4f4f4;\n  border-radius: 10px;\n  border: 1px solid #d4d4d4;\n  padding: 12px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 19px;\n  font-weight: bold;\n}\n\n.liststyle {\n  width: 90%;\n  margin: 0 auto;\n  margin-top: 15px;\n}\n\n.liststyle ul {\n  margin: 0px;\n  padding: 0px;\n}\n\n.liststyle ul li {\n  list-style: none;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 20px;\n  color: #919191;\n  padding-left: 9%;\n  background: url(\"/assets/images/carbullet.png\") no-repeat;\n  margin-bottom: 20px;\n}\n\n.font {\n  font-weight: bold;\n  color: #737373;\n}\n\n.acceptBtn {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n\n.btnaccept {\n  background: #09738d !important;\n  color: white;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n.btnaccept:hover {\n  background: white !important;\n  color: black;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n@media (min-width: 568px) {\n  .headerabout {\n    padding-top: 7%;\n    font-weight: bold;\n  }\n}\n\n.review-title {\n  font-family: \"PT Sans\";\n  font-size: 23px;\n  color: #2e2e2e;\n  text-align: center;\n  margin: 0;\n  padding-top: 3%;\n}\n\nion-rating {\n  --color: gray;\n  --color-filled: green;\n}\n\n.ashrectangle-review {\n  width: 90%;\n  padding-bottom: 0px;\n  margin-bottom: 5px;\n}\n\n.no-border {\n  border: none !important;\n}\n\n.checkboxwrapper {\n  width: 53%;\n  margin: 0 auto;\n  display: block;\n}\n\n.checkboxtext {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #2e2e2e;\n}\n\n.darkbutton, .blackbutton {\n  border-radius: 10px;\n  padding: 10px 100px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.padding-back {\n  padding-right: 5px;\n}\n\n.reviewimage img {\n  padding-bottom: 10px !important;\n}\n\n.rate {\n  color: #ffae00;\n  font-size: 25px;\n  padding-bottom: 20px;\n  display: inline;\n  text-align: center;\n}\n\n.rate ul {\n  margin: 0 auto !important;\n  padding: 0;\n  display: block;\n  width: 100%;\n}\n\n@media screen and (orientation: landscape) {\n  .rate ul {\n    margin: 0 auto !important;\n    padding: 0;\n    display: block;\n    width: 100%;\n  }\n}\n\n@media (max-width: 320px) {\n  .checkboxwrapper {\n    width: 60%;\n  }\n}\n\n@media (min-width: 640px) {\n  .checkboxwrapper {\n    width: 30%;\n  }\n}\n\n.loginimage {\n  width: 30% !important;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 10px;\n}\n\n.formwrapper {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n  margin-top: 15%;\n}\n\n.field {\n  padding: 4%;\n  background: #fff;\n  color: black;\n  border: 1px solid #f9f9f9;\n  border-radius: 10px;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  width: 100%;\n  display: block;\n  margin: 0px !important;\n  margin-bottom: 15px !important;\n}\n\n.blackbutton {\n  width: 50%;\n  background: #09738d !important;\n  border-radius: 10px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.blackbutton:hover {\n  background: white !important;\n  width: 50%;\n  color: #000;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.scroll-content {\n  margin-top: 13%;\n}\n\n.fixed-content {\n  margin-top: 13%;\n}\n\n.left-float {\n  float: left;\n  margin-right: 10px;\n}\n\n.logo-text {\n  color: #fff;\n  font-weight: 700;\n  font-size: 50px;\n  text-align: center;\n  text-shadow: #000;\n}\n\n.logo-text span {\n  padding: 4px;\n  font-weight: 300 !important;\n  color: #d3d3d3;\n}\n\n.signup-container {\n  text-align: center;\n  margin-top: 22%;\n}\n\n.logo-font {\n  font-size: 3.4rem !important;\n}\n\n.signup-container p {\n  color: #fff;\n  margin-top: -50px;\n}\n\n.signup-container span {\n  color: #969494;\n}\n\n.login-container {\n  background-color: #000;\n}\n\n@media (min-width: 568px) {\n  .loginimage {\n    width: 15% !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZ2lzdGVyY3VzdG9tZXIucGFnZS5zY3NzIiwiLi5cXHRlcm1zXFx0ZXJtcy5wYWdlLnNjc3MiLCIuLlxccmV2aWV3ZHJpdmVyXFxyZXZpZXdkcml2ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsaUNBQUE7QUFDRjs7QUFFQTtFQUNFLGlDQUFBO0FBQ0Y7O0FBQ0E7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUFFRjs7QUNkQTtFQUNFLGlCQUFBO0FEaUJGOztBQ2ZBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0VBQ0Esb0RBQUE7RUFDQSxrQkFBQTtFQUNBLHNCQUFBO0FEa0JGOztBQ2hCQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FEbUJGOztBQ2pCQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBRG9CRjs7QUNsQkE7RUFDRSx5QkFBQTtFQUNBLHlCQUFBO0VBQ0Esc0JBQUE7RUFDQSwyQkFBQTtBRHFCRjs7QUNuQkE7RUFDRSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtBRHNCRjs7QUNwQkE7RUFDRSxXQUFBO0FEdUJGOztBQ3JCQTtFQUNFLFVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSx5QkFBQTtFQUNBLGFBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLGlCQUFBO0FEd0JGOztBQ3RCQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUR5QkY7O0FDdkJBO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUQwQkY7O0FDeEJBO0VBQ0UsZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EseURBQUE7RUFDQSxtQkFBQTtBRDJCRjs7QUN6QkE7RUFDRSxpQkFBQTtFQUNBLGNBQUE7QUQ0QkY7O0FDMUJBO0VBQ0UsZUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBRDZCRjs7QUMzQkE7RUFDRSw4QkFBQTtFQUNBLFlBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7QUQ4QkY7O0FDNUJBO0VBQ0UsNEJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FEK0JGOztBQzVCQTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGlCQUFBO0VEK0JGO0FBQ0Y7O0FFN0lBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUYrSUY7O0FFNUlBO0VBQ0UsYUFBQTtFQUNBLHFCQUFBO0FGK0lGOztBRTVJQTtFQUVFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FGOElGOztBRTNJQTtFQUNFLHVCQUFBO0FGOElGOztBRTNJQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBRjhJRjs7QUUzSUE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FGOElGOztBRTNJQTtFQUNFLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0FGOElGOztBRTNJQTtFQUNFLGtCQUFBO0FGOElGOztBRTVJQTtFQUNFLCtCQUFBO0FGK0lGOztBRTdJQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUZnSkY7O0FFOUlBO0VBQ0UseUJBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QUZpSkY7O0FFOUlBO0VBQ0U7SUFDRSx5QkFBQTtJQUNBLFVBQUE7SUFDQSxjQUFBO0lBQ0EsV0FBQTtFRmlKRjtBQUNGOztBRTlJQTtFQUNFO0lBQ0UsVUFBQTtFRmdKRjtBQUNGOztBRTdJQTtFQUNFO0lBQ0UsVUFBQTtFRitJRjtBQUNGOztBQXpOQTtFQUNFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtBQTJORjs7QUF4TkE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUEyTkY7O0FBdk5BO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLHlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsOEJBQUE7QUEwTkY7O0FBeE5BO0VBRUUsVUFBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBME5GOztBQXhOQTtFQUNFLDRCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7QUEyTkY7O0FBek5BO0VBQ0UsZUFBQTtBQTRORjs7QUExTkE7RUFDRSxlQUFBO0FBNk5GOztBQTFOQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQTZORjs7QUEzTkE7RUFDRSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxpQkFBQTtBQThORjs7QUE1TkE7RUFDRSxZQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0FBK05GOztBQTdOQTtFQUNFLGtCQUFBO0VBQ0EsZUFBQTtBQWdPRjs7QUE5TkE7RUFDRSw0QkFBQTtBQWlPRjs7QUEvTkE7RUFDRSxXQUFBO0VBQ0EsaUJBQUE7QUFrT0Y7O0FBaE9BO0VBQ0UsY0FBQTtBQW1PRjs7QUFqT0E7RUFDRSxzQkFBQTtBQW9PRjs7QUFqT0E7RUFDRTtJQUNFLHFCQUFBO0VBb09GO0FBQ0YiLCJmaWxlIjoicmVnaXN0ZXJjdXN0b21lci5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuaW9uLWJ1dHRvbi5hY3RpdmF0ZWQge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDAgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW9uLWJ1dHRvbjpob3ZlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzAwMCAhaW1wb3J0YW50O1xyXG59XHJcbi5zdXBwb3J0LXNlY3Rpb24ge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcclxuICBoZWlnaHQ6IGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMTUlO1xyXG59XHJcbkBpbXBvcnQgXCIuLi9yZXZpZXdkcml2ZXIvcmV2aWV3ZHJpdmVyLnBhZ2Uuc2Nzc1wiO1xyXG5cclxuLmxvZ2luaW1hZ2Uge1xyXG4gIHdpZHRoOiAzMCUgIWltcG9ydGFudDtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuLmZvcm13cmFwcGVyIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1JTtcclxufVxyXG5cclxuLy8gLmZpZWxke3BhZGRpbmc6IDE1cHg7IGJhY2tncm91bmQ6ICNmNGY0ZjQ7IGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7IGJvcmRlci1yYWRpdXM6IDI1cHg7IGNvbG9yOiAjOGQ4ZDhkOyBmb250LWZhbWlseTonUFQgU2Fucyc7IGZvbnQtc2l6ZTogMTZweDsgd2lkdGg6IDEwMCU7IGRpc3BsYXk6IGJsb2NrO31cclxuLmZpZWxkIHtcclxuICBwYWRkaW5nOiA0JTtcclxuICBiYWNrZ3JvdW5kOiAjZmZmO1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZjlmOWY5O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1ib3R0b206IDE1cHggIWltcG9ydGFudDtcclxufVxyXG4uYmxhY2tidXR0b24ge1xyXG4gIEBleHRlbmQgLmRhcmtidXR0b247XHJcbiAgd2lkdGg6IDUwJTtcclxuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIHBhZGRpbmc6IDE1cHggMzJweDtcclxufVxyXG4uYmxhY2tidXR0b246aG92ZXIge1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IDUwJTtcclxuICBjb2xvcjogIzAwMDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgcGFkZGluZzogMTVweCAzMnB4O1xyXG59XHJcbi5zY3JvbGwtY29udGVudCB7XHJcbiAgbWFyZ2luLXRvcDogMTMlO1xyXG59XHJcbi5maXhlZC1jb250ZW50IHtcclxuICBtYXJnaW4tdG9wOiAxMyU7XHJcbn1cclxuXHJcbi5sZWZ0LWZsb2F0IHtcclxuICBmbG9hdDogbGVmdDtcclxuICBtYXJnaW4tcmlnaHQ6IDEwcHg7XHJcbn1cclxuLmxvZ28tdGV4dCB7XHJcbiAgY29sb3I6ICNmZmY7XHJcbiAgZm9udC13ZWlnaHQ6IDcwMDtcclxuICBmb250LXNpemU6IDUwcHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIHRleHQtc2hhZG93OiAjMDAwO1xyXG59XHJcbi5sb2dvLXRleHQgc3BhbiB7XHJcbiAgcGFkZGluZzogNHB4O1xyXG4gIGZvbnQtd2VpZ2h0OiAzMDAgIWltcG9ydGFudDtcclxuICBjb2xvcjogI2QzZDNkMztcclxufVxyXG4uc2lnbnVwLWNvbnRhaW5lciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbi10b3A6IDIyJTtcclxufVxyXG4ubG9nby1mb250IHtcclxuICBmb250LXNpemU6IDMuNHJlbSAhaW1wb3J0YW50O1xyXG59XHJcbi5zaWdudXAtY29udGFpbmVyIHAge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIG1hcmdpbi10b3A6IC01MHB4O1xyXG59XHJcbi5zaWdudXAtY29udGFpbmVyIHNwYW4ge1xyXG4gIGNvbG9yOiAjOTY5NDk0O1xyXG59XHJcbi5sb2dpbi1jb250YWluZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xyXG4gIC5sb2dpbmltYWdlIHtcclxuICAgIHdpZHRoOiAxNSUgIWltcG9ydGFudDtcclxuICB9XHJcbn1cclxuIiwiLmhlYXZ5IHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4uYmFubmVydGVybSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZy10b3A6IDE4MHB4O1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3Rlcm0ucG5nXCIpIG5vLXJlcGVhdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG4uYWJvdXRsb2dvIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHRvcDogNzIlO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuLmhlYWRlcmFib3V0IHtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBwYWRkaW5nLXRvcDogMSU7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLmJsdWVsaW5lIHtcclxuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbjogMCBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICBib3JkZXItcmFkaXVzOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLmFib3V0cGFyYSB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGNvbG9yOiAjOTE5MTkxO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIHBhZGRpbmc6IDEycHggMjBweCAwIDIwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbn1cclxuLmFib3V0cGFyYSBzcGFuIHtcclxuICBjb2xvcjogIzIyMjtcclxufVxyXG4uYXNocmVjdGFuZ2xlIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1hcmdpbjogMTJweCBhdXRvO1xyXG4gIGJhY2tncm91bmQ6ICNmNGY0ZjQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5saXN0c3R5bGUge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG4ubGlzdHN0eWxlIHVsIHtcclxuICBtYXJnaW46IDBweDtcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuLmxpc3RzdHlsZSB1bCBsaSB7XHJcbiAgbGlzdC1zdHlsZTogbm9uZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgY29sb3I6ICM5MTkxOTE7XHJcbiAgcGFkZGluZy1sZWZ0OiA5JTtcclxuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jYXJidWxsZXQucG5nXCIpIG5vLXJlcGVhdDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5mb250IHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjb2xvcjogIzczNzM3MztcclxufVxyXG4uYWNjZXB0QnRuIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuLmJ0bmFjY2VwdHtcclxuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4uYnRuYWNjZXB0OmhvdmVye1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDU2OHB4KSB7XHJcbiAgLmhlYWRlcmFib3V0IHtcclxuICAgIHBhZGRpbmctdG9wOiA3JTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxufVxyXG4iLCJAaW1wb3J0IFwiLi4vdGVybXMvL3Rlcm1zLnBhZ2Uuc2Nzc1wiO1xyXG5cclxuLnJldmlldy10aXRsZSB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMjNweDtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmctdG9wOiAzJTtcclxufVxyXG5cclxuaW9uLXJhdGluZyB7XHJcbiAgLS1jb2xvcjogZ3JheTtcclxuICAtLWNvbG9yLWZpbGxlZDogZ3JlZW47XHJcbn1cclxuXHJcbi5hc2hyZWN0YW5nbGUtcmV2aWV3IHtcclxuICBAZXh0ZW5kIC5hc2hyZWN0YW5nbGU7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDVweDtcclxufVxyXG5cclxuLm5vLWJvcmRlciB7XHJcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5jaGVja2JveHdyYXBwZXIge1xyXG4gIHdpZHRoOiA1MyU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi5jaGVja2JveHRleHQge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbn1cclxuXHJcbi5kYXJrYnV0dG9uIHtcclxuICBib3JkZXItcmFkaXVzOiAxMHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMTAwcHg7XHJcbiAgbWFyZ2luOiAxMHB4IGF1dG87XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgYmFja2dyb3VuZDogIzJlMmUyZTtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE5cHg7XHJcbn1cclxuXHJcbi5wYWRkaW5nLWJhY2sge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDVweDtcclxufVxyXG4ucmV2aWV3aW1hZ2UgaW1nIHtcclxuICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xyXG59XHJcbi5yYXRlIHtcclxuICBjb2xvcjogI2ZmYWUwMDtcclxuICBmb250LXNpemU6IDI1cHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgZGlzcGxheTogaW5saW5lO1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ucmF0ZSB1bCB7XHJcbiAgbWFyZ2luOiAwIGF1dG8gIWltcG9ydGFudDtcclxuICBwYWRkaW5nOiAwO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xyXG4gIC5yYXRlIHVsIHtcclxuICAgIG1hcmdpbjogMCBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xyXG4gIC5jaGVja2JveHdyYXBwZXIge1xyXG4gICAgd2lkdGg6IDYwJTtcclxuICB9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA2NDBweCkge1xyXG4gIC5jaGVja2JveHdyYXBwZXIge1xyXG4gICAgd2lkdGg6IDMwJTtcclxuICB9XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 57188:
/*!*********************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/registercustomer/registercustomer.page.html ***!
  \*********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Registercustomer page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<!--<ion-header>\r\n  <ion-toolbarbar color=\"headerblue\">\r\n    <button ion-button menuToggle>\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button>\r\n    <ion-title class=\"toptitle\"><span class=\"heavy\">REGISTER CUSTOMER</span></ion-title>\r\n  </ion-toolbarbar>\r\n</ion-header>-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <!-- <button ion-button (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </button> -->\r\n    <!-- <ion-title><span class=\"heavy toptitle\">REGISTER CUSTOMER</span></ion-title>-->\r\n    <ion-title class=\"toptitle\"\r\n      ><span class=\"heavy\">REGISTER CUSTOMER</span></ion-title\r\n    >\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content color=\"dark\">\r\n  <section class=\"support-section\">\r\n    <!-- <img src=\"assets/images/supportlogo.png\"> -->\r\n    <p class=\"logo-text\" style=\"color: rgb(252, 252, 252) !important\">\r\n      RideBidder\r\n    </p>\r\n    <section class=\"profileformwrapper\">\r\n      <form>\r\n        <div style=\"margin-bottom: 5px\">\r\n          <input\r\n            class=\"field\"\r\n            type=\"text\"\r\n            name=\"fullname\"\r\n            placeholder=\"Name\"\r\n            [(ngModel)]=\"fullname\"\r\n          />\r\n        </div>\r\n        <div style=\"margin-bottom: 5px\">\r\n          <input\r\n            class=\"field\"\r\n            type=\"text\"\r\n            name=\"Email\"\r\n            placeholder=\"Email\"\r\n            [(ngModel)]=\"email\"\r\n          />\r\n        </div>\r\n        <div style=\"margin-bottom: 5px\">\r\n          <input\r\n            class=\"field\"\r\n            *ngIf=\"showInput\"\r\n            type=\"password\"\r\n            name=\"Password\"\r\n            placeholder=\"Password\"\r\n            [(ngModel)]=\"password\"\r\n          />\r\n        </div>\r\n        <div style=\"margin-bottom: 5px\">\r\n          <input\r\n            class=\"field\"\r\n            type=\"password\"\r\n            *ngIf=\"showInput\"\r\n            name=\"Confirm Password\"\r\n            placeholder=\"Confirm Password\"\r\n            [(ngModel)]=\"confirmPassword\"\r\n          />\r\n        </div>\r\n        <div style=\"margin-bottom: 5px\">\r\n          <input\r\n            class=\"field\"\r\n            type=\"text\"\r\n            name=\"Phone\"\r\n            placeholder=\"Contact Number\"\r\n            [(ngModel)]=\"phone\"\r\n            (input)=\"typePhoneNumber($event)\"\r\n          />\r\n        </div>\r\n        <div style=\"margin-bottom: 5px\">\r\n          <input\r\n            class=\"field\"\r\n            type=\"text\"\r\n            name=\"City\"\r\n            placeholder=\"City\"\r\n            [(ngModel)]=\"cityname\"\r\n          />\r\n        </div>\r\n        <!-- <input class=\"field\" type=\"text\" name=\"Vehicle Number\" placeholder=\"Vehicle Number\" [(ngModel)]=\"vehicle\"> -->\r\n        <!-- <ion-item>\r\n      <ion-label>I agree to terms & Conditions</ion-label>\r\n      <ion-checkbox color=\"dark\" ></ion-checkbox>\r\n      </ion-item>-->\r\n        <button class=\"blackbutton\" ion-button round (click)=\"doRegistration()\">\r\n          Register\r\n        </button>\r\n      </form>\r\n      <!--<p style=\"text-align:center\"> Read our <a (click)=\"modalopen()\">Terms and Conditions</a></p>-->\r\n    </section>\r\n\r\n    <div class=\"signup-container\">\r\n      <p color=\"bright\" (click)=\"goToSignin()\">\r\n        <span color=\"light\" style=\"margin-top: -100px\"\r\n          >Already Have An Account? <b> Sign in </b>\r\n        </span>\r\n      </p>\r\n    </div>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_registercustomer_registercustomer_module_ts.js.map