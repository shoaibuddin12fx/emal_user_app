(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_support_support_module_ts"],{

/***/ 27544:
/*!*********************************************************!*\
  !*** ./src/app/pages/support/support-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SupportPageRoutingModule": () => (/* binding */ SupportPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _support_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./support.page */ 66227);




const routes = [
    {
        path: '',
        component: _support_page__WEBPACK_IMPORTED_MODULE_0__.SupportPage
    }
];
let SupportPageRoutingModule = class SupportPageRoutingModule {
};
SupportPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], SupportPageRoutingModule);



/***/ }),

/***/ 82602:
/*!*************************************************!*\
  !*** ./src/app/pages/support/support.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SupportPageModule": () => (/* binding */ SupportPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _support_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./support-routing.module */ 27544);
/* harmony import */ var _support_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./support.page */ 66227);







let SupportPageModule = class SupportPageModule {
};
SupportPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _support_routing_module__WEBPACK_IMPORTED_MODULE_0__.SupportPageRoutingModule
        ],
        declarations: [_support_page__WEBPACK_IMPORTED_MODULE_1__.SupportPage]
    })
], SupportPageModule);



/***/ }),

/***/ 66227:
/*!***********************************************!*\
  !*** ./src/app/pages/support/support.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SupportPage": () => (/* binding */ SupportPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_support_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./support.page.html */ 46853);
/* harmony import */ var _support_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./support.page.scss */ 93366);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);





let SupportPage = class SupportPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.selectedTab = 'tab1';
    }
    ionViewDidLoad() {
        console.log('ionViewDidLoad SupportPage');
    }
    ngOnInit() { }
};
SupportPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
SupportPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-support',
        template: _raw_loader_support_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_support_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], SupportPage);



/***/ }),

/***/ 93366:
/*!*************************************************!*\
  !*** ./src/app/pages/support/support.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".toptitle {\n  padding-right: 16% !important;\n}\n\n.support-section {\n  padding-top: 15px;\n  width: 90%;\n  margin: 0 auto;\n}\n\n.support-section input {\n  opacity: 1 !important;\n  margin-bottom: 20 !important;\n}\n\n.support-section img {\n  width: auto;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 20px;\n}\n\n.supportpara {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #2e2e2e;\n  text-align: left;\n  line-height: 22px;\n  margin: 0;\n}\n\n.tab {\n  position: relative;\n  margin-bottom: 1px;\n  width: 100%;\n  color: #fff;\n  overflow: hidden;\n  margin-bottom: 10px;\n}\n\ninput {\n  position: absolute;\n  opacity: 0;\n  z-index: -1;\n}\n\nlabel {\n  position: relative;\n  display: block;\n  padding: 0 0 0 1em;\n  background: #16a085;\n  font-weight: bold;\n  line-height: 3;\n  cursor: pointer;\n}\n\n.blue label {\n  background: #f4f4f4;\n  border: 1px solid #d4d4d4;\n  border-radius: 5px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 15px;\n  line-height: 2rem;\n  font-weight: normal;\n  padding: 10px;\n}\n\n.tab-content {\n  max-height: 0;\n  overflow: hidden;\n  background: #1abc9c;\n  transition: max-height 0.35s;\n}\n\n.blue .tab-content {\n  background: #fff;\n  color: #919191;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  line-height: 22px;\n  text-align: left;\n}\n\n.tab-content p {\n  margin: 0.5em;\n}\n\n/* :checked */\n\ninput:checked ~ .tab-content {\n  max-height: 10em;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN1cHBvcnQucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsNkJBQUE7QUFDRjs7QUFDQTtFQUNFLGlCQUFBO0VBQ0EsVUFBQTtFQUNBLGNBQUE7QUFFRjs7QUFBQTtFQUNFLHFCQUFBO0VBQ0EsNEJBQUE7QUFHRjs7QUFEQTtFQUNFLFdBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtFQUNBLG9CQUFBO0FBSUY7O0FBRkE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLFNBQUE7QUFLRjs7QUFGQTtFQUNFLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7QUFLRjs7QUFGQTtFQUNFLGtCQUFBO0VBQ0EsVUFBQTtFQUNBLFdBQUE7QUFLRjs7QUFIQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0FBTUY7O0FBSkE7RUFDRSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0Esa0JBQUE7RUFDQSxjQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7QUFPRjs7QUFMQTtFQUNFLGFBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBR0EsNEJBQUE7QUFRRjs7QUFOQTtFQUNFLGdCQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFTRjs7QUFOQTtFQUNFLGFBQUE7QUFTRjs7QUFQQSxhQUFBOztBQUNBO0VBQ0UsZ0JBQUE7QUFVRiIsImZpbGUiOiJzdXBwb3J0LnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50b3B0aXRsZSB7XHJcbiAgcGFkZGluZy1yaWdodDogMTYlICFpbXBvcnRhbnQ7XHJcbn1cclxuLnN1cHBvcnQtc2VjdGlvbiB7XHJcbiAgcGFkZGluZy10b3A6IDE1cHg7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG4uc3VwcG9ydC1zZWN0aW9uIGlucHV0IHtcclxuICBvcGFjaXR5OiAxICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjAgIWltcG9ydGFudDtcclxufVxyXG4uc3VwcG9ydC1zZWN0aW9uIGltZyB7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbn1cclxuLnN1cHBvcnRwYXJhIHtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGNvbG9yOiAjMmUyZTJlO1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbGluZS1oZWlnaHQ6IDIycHg7XHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4udGFiIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgbWFyZ2luLWJvdHRvbTogMXB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbiAgbWFyZ2luLWJvdHRvbTogMTBweDtcclxufVxyXG5cclxuaW5wdXQge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvcGFjaXR5OiAwO1xyXG4gIHotaW5kZXg6IC0xO1xyXG59XHJcbmxhYmVsIHtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgcGFkZGluZzogMCAwIDAgMWVtO1xyXG4gIGJhY2tncm91bmQ6ICMxNmEwODU7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgbGluZS1oZWlnaHQ6IDM7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG59XHJcbi5ibHVlIGxhYmVsIHtcclxuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XHJcbiAgYm9yZGVyLXJhZGl1czogNXB4O1xyXG4gIGNvbG9yOiAjMmUyZTJlO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbiAgbGluZS1oZWlnaHQ6IDJyZW07XHJcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcbi50YWItY29udGVudCB7XHJcbiAgbWF4LWhlaWdodDogMDtcclxuICBvdmVyZmxvdzogaGlkZGVuO1xyXG4gIGJhY2tncm91bmQ6ICMxYWJjOWM7XHJcbiAgLXdlYmtpdC10cmFuc2l0aW9uOiBtYXgtaGVpZ2h0IDAuMzVzO1xyXG4gIC1vLXRyYW5zaXRpb246IG1heC1oZWlnaHQgMC4zNXM7XHJcbiAgdHJhbnNpdGlvbjogbWF4LWhlaWdodCAwLjM1cztcclxufVxyXG4uYmx1ZSAudGFiLWNvbnRlbnQge1xyXG4gIGJhY2tncm91bmQ6ICNmZmY7XHJcbiAgY29sb3I6ICM5MTkxOTE7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICBsaW5lLWhlaWdodDogMjJweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG59XHJcblxyXG4udGFiLWNvbnRlbnQgcCB7XHJcbiAgbWFyZ2luOiAwLjVlbTtcclxufVxyXG4vKiA6Y2hlY2tlZCAqL1xyXG5pbnB1dDpjaGVja2VkIH4gLnRhYi1jb250ZW50IHtcclxuICBtYXgtaGVpZ2h0OiAxMGVtO1xyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 46853:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/support/support.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Support page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <!-- <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n      <ion-icon name=\"menu\"></ion-icon>\r\n    </ion-button> -->\r\n      <ion-button (click)=\"nav.pop()\">\r\n        <ion-icon name=\"chevron-back-outline\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    <ion-title><span class=\"heavy toptitle\">SUPPORT</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n\r\n<ion-content>\r\n\r\n\r\n  <div padding>\r\n    <ion-segment [(ngModel)]=\"selectedTab\">\r\n      <ion-segment-button value=\"tab1\">\r\n        FAQ\r\n      </ion-segment-button>\r\n      <ion-segment-button value=\"tab2\">\r\n        Code of Conduct\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n  </div>\r\n\r\n  <div [ngSwitch]=\"selectedTab\">\r\n    <section class=\"support-section\" *ngSwitchCase=\"'tab1'\">\r\n      <div class=\"tab blue\">\r\n        <label>How can I contact the driver?</label>\r\n        <div>\r\n          <p style=\"color:black\">Once the job has been assigned to the driver, under the booking tab you select the\r\n            contact button which will call the driver.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label> Is there a charge if I cancel the driver?</label>\r\n        <div>\r\n          <p style=\"color:black\">No, you will not be charged if you cancel the job straight away. However, if you cancel\r\n            the job 5 minutes after accepting a bid - you will be charged $7.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label>Can I request a specific driver?</label>\r\n        <div>\r\n          <p style=\"color:black\">Its better to let all the drivers who are free and close by to give you offers. It’s\r\n            possible that driver is heading on the direction of your trip, which means you can score a big bargain.</p>\r\n        </div>\r\n      </div>\r\n      <!--<div class=\"tab blue\">-->\r\n      <!--<label>Can I request a specific driver? </label>-->\r\n      <!--<div>-->\r\n      <!--<p style=\"color:black\">We automatically connect you with the drivers in the area to get you picked up as quickly as possible. As such, it is not possible to request a specific driver at the moment.</p>-->\r\n      <!--</div>-->\r\n      <!--</div>-->\r\n      <div class=\"tab blue\">\r\n        <label>How do I pay the driver?</label>\r\n        <div>\r\n          <p style=\"color:black\"><strong>Ridebidder</strong> does not use cash, all the payments are made using the app\r\n            platform. This a safe and reliable way to make a transaction.</p>\r\n        </div>\r\n      </div>\r\n      <!-- <div class=\"tab blue\">\r\n         <label>In which city is Ridebidder available?</label>\r\n         <div>\r\n         <p style=\"color:black\">Ridebidder is available in Sydney, Australia. Find updated coverage area on www.pickmyride.com.au</p>\r\n         </div>\r\n       </div>-->\r\n      <div class=\"tab blue\">\r\n        <label>Which City is <strong> Ridebidder </strong>available?</label>\r\n        <div>\r\n          <p style=\"color:black\">Currently the App is being launched in Sydney. We will be expanding to other countries\r\n            and cities, so keep an eye out for our presence in your location.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label> Who are your drivers?</label>\r\n        <div>\r\n          <p style=\"color:black\">Our drivers are contracted with us to provide you with the best service that an agreed\r\n            price. We have ensured that are drivers meet our strict requirements of code of conduct to ensure your safe\r\n            and satisfaction.</p>\r\n        </div>\r\n      </div>\r\n      <div class=\"tab blue\">\r\n        <label>Can I track the location of the driver?</label>\r\n        <div>\r\n          <p style=\"color:black\">Yes, you can track where the driver is at any time, this allows you to get ready to\r\n            start your trip when the driver arrives at your location.</p>\r\n        </div>\r\n      </div>\r\n\r\n      <div class=\"tab blue\">\r\n        <label>How does your support work?</label>\r\n        <div>\r\n          <p style=\"color:black\">In case if any issues, please use contact us and someone will be on contact to\r\n            remediate your concerns as soon as possible.\r\n          </p>\r\n        </div>\r\n\r\n        <div class=\"tab blue\">\r\n          <label>How secure is my personal information?</label>\r\n          <div>\r\n            <p style=\"color:black\">Riddbidder doesn't store any credit card specific data, not even in encrypted\r\n              fashion. Payment is via stripe, stripe hold at details and are PCI compliant payment gateway.\r\n            </p>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </section>\r\n\r\n    <section class=\"support-section\" *ngSwitchCase=\"'tab2'\">\r\n      <p><strong>1. Instruction</strong> - While technology helps ensure the speed, dependability, and safety of our\r\n        drivers and customers, people matter most to the Ridebidder experience. And it is a two-way street between our\r\n        Driver Partners and Riders. We created this Code of Conduct so that everyone in the vehicle has a shared\r\n        standard for respect, accountability, and common courtesy.</p>\r\n      <p><strong>2. Professionalism perfect</strong> - Ridebidder maintains a zero-tolerance policy regarding all forms\r\n        of discrimination, harassment or abuse. It is unacceptable to refuse to provide or accept services based on a\r\n        person is race, religion, national origin, disability, sexual orientation, sex, marital status, gender identity,\r\n        age or any other characteristic protected under applicable federal or state law. This type of behavior can\r\n        result in permanent loss of access to the Ridebidder app. No aggressive behavior, it is disrespectful to make\r\n        derogatory remarks about a person or group. Furthermore, commenting on appearance, asking overly personal\r\n        questions and making unwanted physical contact are all inappropriate. We encourage you to be mindful of other\r\n        users privacy and personal space. The violence of any kind will not be tolerated. Human Kindness Calm and clear\r\n        communication is the most effective way to defuse any disagreement that may arise between you and another\r\n        Ridebidder user. Ridebidder expects that all riders and drivers will treat one another with respect and\r\n        courtesy.</p>\r\n\r\n      <p><strong>3. Safety</strong> - The safety of riders and drivers on the Ridebidder platform is of utmost concern.\r\n        In order to best protect everyone in the vehicle, we require the following: Compliance with the local law at all\r\n        time. Furthermore, Ridebidder does not tolerate drug or alcohol by drivers while using the Ridebidder app. If a\r\n        rider believes a driver may be under the influence of drugs or alcohol, please request that the driver ends the\r\n        trip immediately and alert Ridebidder Support on contact@ridebidder.com.au. As a driver, it is your\r\n        responsibility to transport riders safely in accordance with the rules of the road in your city. As a rider, it\r\n        is your responsibility to abide by the seat belt laws in your state. However, we recommend that you always wear\r\n        a seatbelt while riding in any vehicle. Following the rules, We require partners to keep documents up to date to\r\n        remain active. Riders, likewise, must maintain active payment information. Riders are responsible for guests\r\n        traveling with them or anyone they request a ride for. It is your responsibility to ensure everyone adheres to\r\n        Ridebidder Code of Conduct. Violations of this Code of Conduct could result in loss of your Ridebidder Account.\r\n        Please report any violations to Ridebidder at contact@ridebidder.com.au.</p>\r\n\r\n      <p><strong>4. Emergencies</strong> - If at any time you feel that you are faced with a situation that requires\r\n        immediate emergency attention, please call the emergency service number in your area. Once all parties are safe\r\n        and the authorities have handled the situation, please then notify Ridebidder at contact@ridebidder.com.au. We\r\n        will assist and take appropriate action as needed, including cooperating with law enforcement.</p>\r\n    </section>\r\n  </div>\r\n</ion-content>");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_support_support_module_ts.js.map