(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_login_login_module_ts"],{

/***/ 73403:
/*!*****************************************************!*\
  !*** ./src/app/pages/login/login-routing.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageRoutingModule": () => (/* binding */ LoginPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login.page */ 3058);




const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_0__.LoginPage
    }
];
let LoginPageRoutingModule = class LoginPageRoutingModule {
};
LoginPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], LoginPageRoutingModule);



/***/ }),

/***/ 21053:
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPageModule": () => (/* binding */ LoginPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _login_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./login-routing.module */ 73403);
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page */ 3058);







let LoginPageModule = class LoginPageModule {
};
LoginPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _login_routing_module__WEBPACK_IMPORTED_MODULE_0__.LoginPageRoutingModule
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_1__.LoginPage]
    })
], LoginPageModule);



/***/ }),

/***/ 3058:
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LoginPage": () => (/* binding */ LoginPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./login.page.html */ 31021);
/* harmony import */ var _login_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./login.page.scss */ 28781);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/google-plus/ngx */ 19342);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var _terms_terms_page__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../terms/terms.page */ 32278);
/* harmony import */ var _firebase_app_compat__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @firebase/app-compat */ 50156);








// import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';

let LoginPage = class LoginPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_3__.BasePage {
    /*    <plugin name="cordova-plugin-googleplus" spec="~5.1.1">
          <variable name="REVERSED_CLIENT_ID" value="com.googleusercontent.apps.735130465689-2r35luq5cqbmq47animao1neuscedd2t" />
      </plugin>*/
    //private oauth: OauthCordova = new OauthCordova();
    constructor(injector, alertCtrl, modalCtrl, googlePlus) {
        super(injector);
        this.alertCtrl = alertCtrl;
        this.modalCtrl = modalCtrl;
        this.googlePlus = googlePlus;
        this.email = 'shoaibuddin12fx@gmail.com';
        this.password = '123456';
        /*  if(this.navParams.get('login_type')){
                          console.log(this.navParams.get('login_type'));
                          this.connectlogin(this.navParams.get('login_type'));
                      }*/
        /*  if(this.navParams.get('terms')){
                          this.navCtrl.push(RegistercustomerPage);
                      }*/
    }
    ionViewDidLoad() {
        console.log('Login Page');
    }
    onSignInFaild() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Login',
                subHeader: 'Email or Password is Invalid',
                buttons: ['OK'],
            });
            alert.present();
        });
    }
    detailsFailed() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Login',
                subHeader: 'Please Enter Valid Details',
                buttons: ['OK'],
            });
            alert.present();
        });
    }
    doLogin() {
        if (this.email == undefined ||
            this.password == undefined ||
            this.email == '' ||
            this.password == '') {
            this.detailsFailed();
        }
        else {
            this.usersService
                .loginUser(this.email, this.password)
                .then(() => {
                this.events.publish('login_event');
                this.nav.setRoot('pages/locationloader');
            })
                .catch((_error) => this.onSignInFaild());
        }
    }
    doRegistration(login_type) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            // this.navCtrl.push(RegistercustomerPage);
            // this.navCtrl.setRoot(TermsPage);
            // let profileModal = this.modals.present(TermsPage,{type:"basic"});
            // profileModal.present();
            console.log('presengint modal');
            var profileModal = yield this.modalCtrl.create({
                component: _terms_terms_page__WEBPACK_IMPORTED_MODULE_4__.TermsPage,
                componentProps: { login: login_type },
            });
            profileModal.present();
            const { data } = yield profileModal.onDidDismiss();
            console.log('login type is : ', data);
            if ((data === null || data === void 0 ? void 0 : data.param) === 'basic') {
                this.nav.push('pages/registercustomer');
            }
        });
    }
    connectlogin(login_type) {
        let provider;
        this.usersService.loginType = login_type;
        if (login_type != 'Facebook') {
            // this.fb.logout().then((response) => {
            //   console.log(response);
            // });
            // this.fb
            //   .login(['email'])
            //   .then((response) => {
            //     let res: any;
            //     let credential: any;
            //     res = response;
            //     console.log('no: -> 1');
            //     console.log(response);
            //     this.getDb().ref;
            //     credential = firebase.auth.FacebookAuthProvider.credential(
            //       response.authResponse.accessToken
            //     );
            //     console.log('no: -> 2');
            //     console.log(credential);
            //     this.firebaseService.fireAuth
            //       .signInWithCredential(credential)
            //       .then((success) => {
            //         console.log('no: -> 3');
            //         console.log(success);
            //         this.user = success.user;
            //         let ref = this.getDb().ref('/users/' + success.user.uid);
            //         ref.child('fullname').set(success.user.displayName);
            //         ref.child('email').set(success.user.email);
            //         ref.child('profile_image').set(success.user.photoURL);
            //         ref.child('authToken').set(response.authResponse.accessToken);
            //         ref.child('customer').set(true);
            //       })
            //       .catch(async (error) => {
            //         console.log(error);
            //         let alert = await this.alertCtrl.create({
            //           header: 'Alert',
            //           subHeader:
            //             'Sign In Failed. You may have previous account with same email id.',
            //           buttons: ['OK'],
            //         });
            //         alert.present();
            //       });
            //   })
            //   .catch(async (error) => {
            //     console.log(error);
            //     let facebookAlrt = await this.alertCtrl.create({
            //       header: 'ALERT',
            //       subHeader: 'fail to login',
            //       buttons: ['OK'],
            //     });
            //     facebookAlrt.present();
            //   });
        }
        else {
            console.log('login with google...');
            // GooglePlus.login({'webClientId':'735130465689-o89h3d2gsahss4h9atf24o2vcqrmjh2o.apps.googleusercontent.com'}).then( (response) => {
            //   console.log("login with google access token...");
            //   var provider = firebase.auth.GoogleAuthProvider.credential(response.idToken);
            //   console.log(response);
            //   console.log(provider);
            //   firebase.auth().signInWithCredential(provider)
            //                 .then((success) => {
            //                     console.log(success);
            //                         this.user = success;
            //                         let ref = firebase.database().ref('/users/' + success.uid);
            //                         ref.child('fullname').set(response.displayName);
            //                         ref.child('email').set(success.email);
            //                         ref.child('profile_image').set(response.imageUrl);
            //                         ref.child('authToken').set(response.idToken);
            //                         ref.child('customer').set(true);
            //                 })
            //                 .catch((error) => {
            //                     console.log(error);
            //                     let alert = this.alertCtrl.create({
            //                         title: 'Login',
            //                         subTitle: 'Sign In Failed. You may have previous account with same email id.',
            //                         buttons: ['OK']
            //                         });
            //                     alert.present();
            //                 });
            //   })
            // .catch((error)=>{
            //   let openAlrt = this.alertCtrl.create({
            //       title: 'ALERT',
            //       subTitle:'fail to login',
            //       buttons: ['OK']
            //   });
            //   openAlrt.present();
            // })
            this.googlePlus
                .login({
                webClientId: '945081768290-im80pg5742lk2b1sh9rt256q7os1238p.apps.googleusercontent.com',
                offline: true,
            })
                .then((response) => {
                console.log('login detail', response);
                var provider = _firebase_app_compat__WEBPACK_IMPORTED_MODULE_5__.default.auth.GoogleAuthProvider.credential(response.idToken);
                this.firebaseService.fireAuth
                    .signInWithCredential(provider)
                    .then((success) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                    console.log(success);
                    this.user = success.user;
                    let ref = this.getDb().ref('/users/' + success.user.uid);
                    ref.child('fullname').set(success.user.displayName);
                    ref.child('email').set(success.user.email);
                    ref.child('profile_image').set(success.user.photoURL);
                    ref.child('authToken').set(yield success.user.getIdToken());
                    ref.child('customer').set(true);
                }))
                    .catch((error) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                    console.log(error);
                    let alert = yield this.alertCtrl.create({
                        header: 'Login',
                        subHeader: 'Sign In Failed. You may have previous account with same email id.',
                        buttons: ['OK'],
                    });
                    alert.present();
                }));
            })
                .catch((err) => (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
                console.error(err);
                let openAlrt = yield this.alertCtrl.create({
                    header: 'ALERT',
                    subHeader: 'fail to login',
                    buttons: ['OK'],
                });
                openAlrt.present();
            }));
        }
        //   this.googlePlus.logout().then(response =>{
        //        console.log(response);
        //   });
    }
    loginSocial(login_type) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            console.log(login_type);
            let profileModal = yield this.modals.modal.create({
                component: _terms_terms_page__WEBPACK_IMPORTED_MODULE_4__.TermsPage,
                componentProps: { login: login_type },
            });
            console.log('created');
            profileModal.present();
            const { data } = yield profileModal.onDidDismiss();
            this.connectlogin(data);
            /*   let provider:any;
              if(login_type=="Facebook"){
        
                    Facebook.login(['email']).then( (response) => {
                        let res:any;
                        let credential:any;
        
                        res = response;
                        console.log("no: -> 1");
                        console.log(response);
        
                        credential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
                        console.log("no: -> 2");
                        console.log(credential);
        
                        firebase.auth().signInWithCredential(credential)
                        .then((success) => {
                            console.log("no: -> 3");
                            console.log(success);
                                this.user = success;
                                let ref = firebase.database().ref('/users/' + success.uid);
                                ref.child('fullname').set(success.displayName);
                                ref.child('email').set(success.email);
                                ref.child('profile_image').set(success.photoURL);
                                ref.child('authToken').set(response.authResponse.accessToken);
                                ref.child('customer').set(true);
                        })
                        .catch((error) => {
                            console.log(error);
                            let alert = this.alertCtrl.create({
                                title: 'Alert',
                                subTitle: 'Sign In Failed. You may have previous account with same email id.',
                                buttons: ['OK']
                            });
                            alert.present();
                        });
                    }).catch((error) => { alert(error) });
        
              }
              else{
                    GooglePlus.login({'webClientId':'735130465689-q7pu4kvidom52nqp17vg1nc8lhq6cfni.apps.googleusercontent.com'}).then( (response) => {
                    var provider = firebase.auth.GoogleAuthProvider.credential(response.idToken);
                     console.log(response);
                     console.log(provider);
                            firebase.auth().signInWithCredential(provider)
                                .then((success) => {
                                    console.log(success);
                                        this.user = success;
                                        let ref = firebase.database().ref('/users/' + success.uid);
                                        ref.child('fullname').set(response.displayName);
                                        ref.child('email').set(success.email);
                                        ref.child('profile_image').set(response.imageUrl);
                                        ref.child('authToken').set(response.idToken);
                                        ref.child('customer').set(true);
                                })
                                .catch((error) => {
                                    let alert = this.alertCtrl.create({
                                        title: 'Login',
                                        subTitle: 'Sign In Failed. You may have previous account with same email id.',
                                        buttons: ['OK']
                                        });
                                    alert.present();
                                });
                    })
                    .catch((error)=>{
                        alert("fail to login");
                        console.log(error);
                    })
              }   */
        });
    }
    //forgot password
    resetPassword() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'Forgot password',
                inputs: [
                    {
                        type: 'text',
                        placeholder: 'Please Enter Your Email',
                    },
                ],
                buttons: [
                    'Cancel',
                    {
                        text: 'OK',
                        handler: (data) => {
                            let passData = data[0];
                            this.changepass(passData);
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    pswreset() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__awaiter)(this, void 0, void 0, function* () {
            let psw = yield this.alertCtrl.create({
                header: 'Success',
                subHeader: 'Password reset email has been sent to your email address',
                buttons: ['OK'],
            });
            psw.present();
        });
    }
    changepass(data) {
        let email = data;
        console.log('OnChangePass');
        // this.usersService.passReset(email).then(() => alert("password reset"))
        this.usersService
            .passReset(email)
            .then((msg) => {
            if (msg === 'Success')
                this.pswreset();
            else {
                this.utility.presentFailureToast('Invalid / incomplete email, please try again');
            } //alert('Invalid or incomplete email');
        })
            .catch((_error) => alert(_error));
    }
    ngOnInit() {
        this.menuCtrl.enable(false, 'drawer');
    }
    getDb() {
        return this.firebaseService.getDatabase();
    }
};
LoginPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.Injector },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.AlertController },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_8__.ModalController },
    { type: _ionic_native_google_plus_ngx__WEBPACK_IMPORTED_MODULE_2__.GooglePlus }
];
LoginPage = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-login',
        template: _raw_loader_login_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_login_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], LoginPage);



/***/ }),

/***/ 28781:
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".heavy {\n  font-weight: bold;\n}\n\n.bannerterm {\n  width: 100%;\n  padding-top: 180px;\n  background: url(\"/assets/images/term.png\") no-repeat;\n  position: relative;\n  background-size: cover;\n}\n\n.aboutlogo {\n  position: absolute;\n  display: block;\n  width: auto;\n  margin: auto;\n  top: 72%;\n  left: 0;\n  right: 0;\n}\n\n.headerabout {\n  color: #2e2e2e;\n  font-size: 18px;\n  text-align: center;\n  font-family: \"PT Sans\";\n  padding-top: 1%;\n  font-weight: bold;\n}\n\n.blueline {\n  display: block !important;\n  margin: 0 auto !important;\n  width: auto !important;\n  border-radius: 0 !important;\n}\n\n.aboutpara {\n  font-family: \"PT Sans\";\n  color: #919191;\n  font-size: 16px;\n  text-align: left;\n  line-height: 18px;\n  padding: 12px 20px 0 20px;\n  margin: 0px;\n}\n\n.aboutpara span {\n  color: #222;\n}\n\n.ashrectangle, .ashrectangle-review {\n  width: 90%;\n  margin: 12px auto;\n  background: #f4f4f4;\n  border-radius: 10px;\n  border: 1px solid #d4d4d4;\n  padding: 12px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 19px;\n  font-weight: bold;\n}\n\n.liststyle {\n  width: 90%;\n  margin: 0 auto;\n  margin-top: 15px;\n}\n\n.liststyle ul {\n  margin: 0px;\n  padding: 0px;\n}\n\n.liststyle ul li {\n  list-style: none;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 20px;\n  color: #919191;\n  padding-left: 9%;\n  background: url(\"/assets/images/carbullet.png\") no-repeat;\n  margin-bottom: 20px;\n}\n\n.font {\n  font-weight: bold;\n  color: #737373;\n}\n\n.acceptBtn {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n\n.btnaccept {\n  background: #09738d !important;\n  color: white;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n.btnaccept:hover {\n  background: white !important;\n  color: black;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n\n@media (min-width: 568px) {\n  .headerabout {\n    padding-top: 7%;\n    font-weight: bold;\n  }\n}\n\n.review-title {\n  font-family: \"PT Sans\";\n  font-size: 23px;\n  color: #2e2e2e;\n  text-align: center;\n  margin: 0;\n  padding-top: 3%;\n}\n\nion-rating {\n  --color: gray;\n  --color-filled: green;\n}\n\n.ashrectangle-review {\n  width: 90%;\n  padding-bottom: 0px;\n  margin-bottom: 5px;\n}\n\n.no-border {\n  border: none !important;\n}\n\n.checkboxwrapper {\n  width: 53%;\n  margin: 0 auto;\n  display: block;\n}\n\n.checkboxtext {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  color: #2e2e2e;\n}\n\n.darkbutton, .facebookbtn:hover, .facebookbtn, .googlebtn:hover, .googlebtn, .lightgreenbutton:hover, .lightgreenbutton, .bluebutton, .redbutton, .blackbutton {\n  border-radius: 10px;\n  padding: 10px 100px;\n  margin: 10px auto;\n  display: block;\n  background: #2e2e2e;\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  font-family: \"PT Sans\";\n  font-size: 19px;\n}\n\n.padding-back {\n  padding-right: 5px;\n}\n\n.reviewimage img {\n  padding-bottom: 10px !important;\n}\n\n.rate {\n  color: #ffae00;\n  font-size: 25px;\n  padding-bottom: 20px;\n  display: inline;\n  text-align: center;\n}\n\n.rate ul {\n  margin: 0 auto !important;\n  padding: 0;\n  display: block;\n  width: 100%;\n}\n\n@media screen and (orientation: landscape) {\n  .rate ul {\n    margin: 0 auto !important;\n    padding: 0;\n    display: block;\n    width: 100%;\n  }\n}\n\n@media (max-width: 320px) {\n  .checkboxwrapper {\n    width: 60%;\n  }\n}\n\n@media (min-width: 640px) {\n  .checkboxwrapper {\n    width: 30%;\n  }\n}\n\n.back {\n  background: url(\"/assets/images/background.png\") repeat;\n}\n\n.loginimage {\n  width: 30% !important;\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 10px;\n}\n\n.formwrapper {\n  width: 90%;\n  margin: 0 auto;\n  overflow: hidden;\n  height: auto;\n  margin-top: 15%;\n}\n\n.field {\n  padding: 20px;\n  background: #fff;\n  border: 1px solid #f9f9f9;\n  /* border-radius: 25px; */\n  color: #333131;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  width: 100%;\n  display: block;\n  margin: 0px !important;\n  border-radius: 4px 4px 0px 0px;\n  border-radius: 50px;\n}\n\n.field ::placeholder {\n  color: black !important;\n}\n\n.blackbutton {\n  width: 100%;\n}\n\n.clearbutton {\n  display: block;\n  margin-top: 10px;\n  font-size: 15px;\n  text-align: center;\n  margin-left: 100px;\n  text-transform: none !important;\n  height: 1.5em;\n}\n\n.redbutton {\n  background: #ea4335 !important;\n  padding: 25px 95px !important;\n  width: 100%;\n}\n\n.bluebutton {\n  width: 100%;\n  background: #3b5998 !important;\n  margin-bottom: 25px !important;\n}\n\n.lightgreenbutton {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 10px;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.lightgreenbutton:hover {\n  background: white !important;\n  margin-bottom: 25px !important;\n  font-size: 15px;\n  font-weight: bold;\n  margin-top: 10px;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.googlebtn {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  overflow-x: hidden;\n  padding: 15px 32px;\n}\n\n.googlebtn:hover {\n  background: #ffffff !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  overflow-x: hidden;\n  padding: 15px 32px;\n  color: #000;\n}\n\n.facebookbtn {\n  background: #09738d !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n}\n\n.facebookbtn:hover {\n  background: #fdfdfd !important;\n  margin-bottom: 25px !important;\n  text-align: center;\n  font-size: 15px;\n  font-weight: bold;\n  border-radius: 50px;\n  white-space: nowrap;\n  padding: 15px 32px;\n  color: #000;\n}\n\n.lightgreenbutton .login-btn {\n  text-align: center;\n  display: block;\n}\n\n.lightgreenbutton:hover {\n  background: #d3d3d3 !important;\n  color: #000;\n  font-weight: bold;\n  font-size: 15px;\n}\n\n.alert-md .alert-head {\n  padding: 10px;\n  text-align: center;\n  color: #222;\n  background: none;\n  font-family: \"PT Sans\";\n}\n\n.alert-md .alert-title {\n  font-size: 18px;\n  text-transform: uppercase;\n}\n\n.alert-md .alert-button {\n  padding: 0;\n  margin: 0 auto;\n  display: block;\n}\n\n.scroll-content {\n  margin-top: 13%;\n}\n\n.fixed-content {\n  margin-top: 13%;\n}\n\n.redlogo {\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  background: #ea4335 !important;\n  border-radius: 50px;\n}\n\n.bluelogo {\n  font-weight: normal;\n  text-transform: none;\n  color: #fff;\n  background: #3b5998 !important;\n  border-radius: 50px;\n}\n\n.left-float {\n  float: left;\n  margin-right: 10px;\n}\n\n.social-login {\n  margin-left: 31%;\n  margin-top: 3%;\n}\n\n.logo-text {\n  color: #fff;\n  font-weight: 700;\n  font-size: 50px;\n  text-align: center;\n  text-shadow: #000;\n}\n\n.logo-text span {\n  padding: 4px;\n  font-weight: 300 !important;\n  color: #d3d3d3;\n}\n\n.signup-container {\n  text-align: center;\n  margin-top: 22%;\n}\n\n.formwrapper input:email {\n  border-radius: 6px 6px 0px 0px;\n}\n\n.formwrapper Password {\n  border-radius: 0px 0px 6px 6px;\n}\n\n.logo-font {\n  font-size: 3.4rem !important;\n}\n\n.signup-container p {\n  color: #fff;\n  margin-top: -50px;\n}\n\n.signup-container span {\n  color: #969494;\n}\n\n.login-container {\n  background-color: #000;\n}\n\n@media (min-width: 568px) {\n  .loginimage {\n    width: 15% !important;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFx0ZXJtc1xcdGVybXMucGFnZS5zY3NzIiwibG9naW4ucGFnZS5zY3NzIiwiLi5cXHJldmlld2RyaXZlclxccmV2aWV3ZHJpdmVyLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFBO0FDQ0Y7O0FEQ0E7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxvREFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUNFRjs7QURBQTtFQUNFLGtCQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7RUFDQSxZQUFBO0VBQ0EsUUFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FDR0Y7O0FEREE7RUFDRSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7QUNJRjs7QURGQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0FDS0Y7O0FESEE7RUFDRSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxpQkFBQTtFQUNBLHlCQUFBO0VBQ0EsV0FBQTtBQ01GOztBREpBO0VBQ0UsV0FBQTtBQ09GOztBRExBO0VBQ0UsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUNRRjs7QUROQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUNTRjs7QURQQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0FDVUY7O0FEUkE7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5REFBQTtFQUNBLG1CQUFBO0FDV0Y7O0FEVEE7RUFDRSxpQkFBQTtFQUNBLGNBQUE7QUNZRjs7QURWQTtFQUNFLGVBQUE7RUFDQSxTQUFBO0VBQ0EsT0FBQTtFQUNBLFFBQUE7QUNhRjs7QURYQTtFQUNFLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBQ2NGOztBRFpBO0VBQ0UsNEJBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0FDZUY7O0FEWkE7RUFDRTtJQUNFLGVBQUE7SUFDQSxpQkFBQTtFQ2VGO0FBQ0Y7O0FDN0hBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0EsY0FBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLGVBQUE7QUQrSEY7O0FDNUhBO0VBQ0UsYUFBQTtFQUNBLHFCQUFBO0FEK0hGOztBQzVIQTtFQUVFLFVBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FEOEhGOztBQzNIQTtFQUNFLHVCQUFBO0FEOEhGOztBQzNIQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0VBQ0EsY0FBQTtBRDhIRjs7QUMzSEE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxjQUFBO0FEOEhGOztBQzNIQTtFQUNFLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxpQkFBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esb0JBQUE7RUFDQSxXQUFBO0VBQ0Esc0JBQUE7RUFDQSxlQUFBO0FEOEhGOztBQzNIQTtFQUNFLGtCQUFBO0FEOEhGOztBQzVIQTtFQUNFLCtCQUFBO0FEK0hGOztBQzdIQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esb0JBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QURnSUY7O0FDOUhBO0VBQ0UseUJBQUE7RUFDQSxVQUFBO0VBQ0EsY0FBQTtFQUNBLFdBQUE7QURpSUY7O0FDOUhBO0VBQ0U7SUFDRSx5QkFBQTtJQUNBLFVBQUE7SUFDQSxjQUFBO0lBQ0EsV0FBQTtFRGlJRjtBQUNGOztBQzlIQTtFQUNFO0lBQ0UsVUFBQTtFRGdJRjtBQUNGOztBQzdIQTtFQUNFO0lBQ0UsVUFBQTtFRCtIRjtBQUNGOztBQXZOQTtFQUNFLHVEQUFBO0FBeU5GOztBQXZOQTtFQUNFLHFCQUFBO0VBQ0EsY0FBQTtFQUNBLGNBQUE7RUFDQSxvQkFBQTtBQTBORjs7QUF2TkE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsWUFBQTtFQUNBLGVBQUE7QUEwTkY7O0FBdE5BO0VBQ0UsYUFBQTtFQUNBLGdCQUFBO0VBQ0EseUJBQUE7RUFDQSx5QkFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0VBQ0EsY0FBQTtFQUNBLHNCQUFBO0VBQ0EsOEJBQUE7RUFBZ0MsbUJBQUE7QUEwTmxDOztBQXhOQTtFQUNFLHVCQUFBO0FBMk5GOztBQXpOQTtFQUVFLFdBQUE7QUEyTkY7O0FBek5BO0VBQ0UsY0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0Esa0JBQUE7RUFDQSwrQkFBQTtFQUNBLGFBQUE7QUE0TkY7O0FBMU5BO0VBRUUsOEJBQUE7RUFDQSw2QkFBQTtFQUNBLFdBQUE7QUE0TkY7O0FBMU5BO0VBRUUsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsOEJBQUE7QUE0TkY7O0FBMU5BO0VBRUUsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLGdCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBNE5GOztBQTFOQTtFQUVFLDRCQUFBO0VBQ0EsOEJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQTRORjs7QUExTkE7RUFFRSw4QkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtBQTRORjs7QUExTkE7RUFFRSw4QkFBQTtFQUNBLDhCQUFBO0VBQ0Esa0JBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxtQkFBQTtFQUNBLG1CQUFBO0VBQ0Esa0JBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUE0TkY7O0FBMU5BO0VBRUUsOEJBQUE7RUFDQSw4QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0FBNE5GOztBQTFOQTtFQUVFLDhCQUFBO0VBQ0EsOEJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtFQUNBLG1CQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLFdBQUE7QUE0TkY7O0FBMU5BO0VBQ0Usa0JBQUE7RUFDQSxjQUFBO0FBNk5GOztBQTFOQTtFQUVFLDhCQUFBO0VBQ0EsV0FBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtBQTRORjs7QUF6TkE7RUFDRSxhQUFBO0VBQ0Esa0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7RUFDQSxzQkFBQTtBQTRORjs7QUExTkE7RUFDRSxlQUFBO0VBQ0EseUJBQUE7QUE2TkY7O0FBMU5BO0VBQ0UsVUFBQTtFQUNBLGNBQUE7RUFDQSxjQUFBO0FBNk5GOztBQTFOQTtFQUNFLGVBQUE7QUE2TkY7O0FBM05BO0VBQ0UsZUFBQTtBQThORjs7QUE1TkE7RUFDRSxtQkFBQTtFQUNBLG9CQUFBO0VBQ0EsV0FBQTtFQUNBLDhCQUFBO0VBQ0EsbUJBQUE7QUErTkY7O0FBN05BO0VBQ0UsbUJBQUE7RUFDQSxvQkFBQTtFQUNBLFdBQUE7RUFDQSw4QkFBQTtFQUNBLG1CQUFBO0FBZ09GOztBQTdOQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQWdPRjs7QUE3TkE7RUFDRSxnQkFBQTtFQUNBLGNBQUE7QUFnT0Y7O0FBN05BO0VBQ0UsV0FBQTtFQUNBLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUFnT0Y7O0FBOU5BO0VBQ0UsWUFBQTtFQUNBLDJCQUFBO0VBQ0EsY0FBQTtBQWlPRjs7QUEvTkE7RUFDRSxrQkFBQTtFQUNBLGVBQUE7QUFrT0Y7O0FBaE9BO0VBQ0UsOEJBQUE7QUFtT0Y7O0FBak9BO0VBQ0UsOEJBQUE7QUFvT0Y7O0FBbE9BO0VBQ0UsNEJBQUE7QUFxT0Y7O0FBbk9BO0VBQ0UsV0FBQTtFQUNBLGlCQUFBO0FBc09GOztBQXBPQTtFQUNFLGNBQUE7QUF1T0Y7O0FBck9BO0VBQ0Usc0JBQUE7QUF3T0Y7O0FBck9BO0VBQ0U7SUFDRSxxQkFBQTtFQXdPRjtBQUNGIiwiZmlsZSI6ImxvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5oZWF2eSB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLmJhbm5lcnRlcm0ge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIHBhZGRpbmctdG9wOiAxODBweDtcclxuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy90ZXJtLnBuZ1wiKSBuby1yZXBlYXQ7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbn1cclxuLmFib3V0bG9nbyB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICB0b3A6IDcyJTtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcbi5oZWFkZXJhYm91dCB7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgZm9udC1zaXplOiAxOHB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgcGFkZGluZy10b3A6IDElO1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5ibHVlbGluZSB7XHJcbiAgZGlzcGxheTogYmxvY2sgIWltcG9ydGFudDtcclxuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xyXG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMCAhaW1wb3J0YW50O1xyXG59XHJcbi5hYm91dHBhcmEge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBjb2xvcjogIzkxOTE5MTtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBsaW5lLWhlaWdodDogMThweDtcclxuICBwYWRkaW5nOiAxMnB4IDIwcHggMCAyMHB4O1xyXG4gIG1hcmdpbjogMHB4O1xyXG59XHJcbi5hYm91dHBhcmEgc3BhbiB7XHJcbiAgY29sb3I6ICMyMjI7XHJcbn1cclxuLmFzaHJlY3RhbmdsZSB7XHJcbiAgd2lkdGg6IDkwJTtcclxuICBtYXJnaW46IDEycHggYXV0bztcclxuICBiYWNrZ3JvdW5kOiAjZjRmNGY0O1xyXG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XHJcbiAgYm9yZGVyOiAxcHggc29saWQgI2Q0ZDRkNDtcclxuICBwYWRkaW5nOiAxMnB4O1xyXG4gIGNvbG9yOiAjMmUyZTJlO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDE2cHg7XHJcbiAgdGV4dC1hbGlnbjogbGVmdDtcclxuICBsaW5lLWhlaWdodDogMTlweDtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4ubGlzdHN0eWxlIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuLmxpc3RzdHlsZSB1bCB7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbiAgcGFkZGluZzogMHB4O1xyXG59XHJcbi5saXN0c3R5bGUgdWwgbGkge1xyXG4gIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xyXG4gIGNvbG9yOiAjOTE5MTkxO1xyXG4gIHBhZGRpbmctbGVmdDogOSU7XHJcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2FyYnVsbGV0LnBuZ1wiKSBuby1yZXBlYXQ7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG4uZm9udCB7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbiAgY29sb3I6ICM3MzczNzM7XHJcbn1cclxuLmFjY2VwdEJ0biB7XHJcbiAgcG9zaXRpb246IGZpeGVkO1xyXG4gIGJvdHRvbTogMDtcclxuICBsZWZ0OiAwO1xyXG4gIHJpZ2h0OiAwO1xyXG59XHJcbi5idG5hY2NlcHR7XHJcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiB3aGl0ZTtcclxuICBtYXJnaW4tbGVmdDogMTQwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuLmJ0bmFjY2VwdDpob3ZlcntcclxuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xyXG4gIGNvbG9yOiBibGFjaztcclxuICBtYXJnaW4tbGVmdDogMTQwcHg7XHJcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcclxuICBwYWRkaW5nOiAxMHB4IDE1cHg7XHJcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICBmb250LXNpemU6IDE1cHg7XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xyXG4gIC5oZWFkZXJhYm91dCB7XHJcbiAgICBwYWRkaW5nLXRvcDogNyU7XHJcbiAgICBmb250LXdlaWdodDogYm9sZDtcclxuICB9XHJcbn1cclxuIiwiLmhlYXZ5IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5iYW5uZXJ0ZXJtIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBhZGRpbmctdG9wOiAxODBweDtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvdGVybS5wbmdcIikgbm8tcmVwZWF0O1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XG59XG5cbi5hYm91dGxvZ28ge1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICB3aWR0aDogYXV0bztcbiAgbWFyZ2luOiBhdXRvO1xuICB0b3A6IDcyJTtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG59XG5cbi5oZWFkZXJhYm91dCB7XG4gIGNvbG9yOiAjMmUyZTJlO1xuICBmb250LXNpemU6IDE4cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBwYWRkaW5nLXRvcDogMSU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uYmx1ZWxpbmUge1xuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICB3aWR0aDogYXV0byAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5hYm91dHBhcmEge1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGNvbG9yOiAjOTE5MTkxO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xuICBwYWRkaW5nOiAxMnB4IDIwcHggMCAyMHB4O1xuICBtYXJnaW46IDBweDtcbn1cblxuLmFib3V0cGFyYSBzcGFuIHtcbiAgY29sb3I6ICMyMjI7XG59XG5cbi5hc2hyZWN0YW5nbGUsIC5hc2hyZWN0YW5nbGUtcmV2aWV3IHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAxMnB4IGF1dG87XG4gIGJhY2tncm91bmQ6ICNmNGY0ZjQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGNvbG9yOiAjMmUyZTJlO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubGlzdHN0eWxlIHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5saXN0c3R5bGUgdWwge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4ubGlzdHN0eWxlIHVsIGxpIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBjb2xvcjogIzkxOTE5MTtcbiAgcGFkZGluZy1sZWZ0OiA5JTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2FyYnVsbGV0LnBuZ1wiKSBuby1yZXBlYXQ7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5mb250IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNzM3MzczO1xufVxuXG4uYWNjZXB0QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xufVxuXG4uYnRuYWNjZXB0IHtcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5idG5hY2NlcHQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xuICAuaGVhZGVyYWJvdXQge1xuICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxufVxuLnJldmlldy10aXRsZSB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAyM3B4O1xuICBjb2xvcjogIzJlMmUyZTtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBtYXJnaW46IDA7XG4gIHBhZGRpbmctdG9wOiAzJTtcbn1cblxuaW9uLXJhdGluZyB7XG4gIC0tY29sb3I6IGdyYXk7XG4gIC0tY29sb3ItZmlsbGVkOiBncmVlbjtcbn1cblxuLmFzaHJlY3RhbmdsZS1yZXZpZXcge1xuICB3aWR0aDogOTAlO1xuICBwYWRkaW5nLWJvdHRvbTogMHB4O1xuICBtYXJnaW4tYm90dG9tOiA1cHg7XG59XG5cbi5uby1ib3JkZXIge1xuICBib3JkZXI6IG5vbmUgIWltcG9ydGFudDtcbn1cblxuLmNoZWNrYm94d3JhcHBlciB7XG4gIHdpZHRoOiA1MyU7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLmNoZWNrYm94dGV4dCB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICBjb2xvcjogIzJlMmUyZTtcbn1cblxuLmRhcmtidXR0b24sIC5mYWNlYm9va2J0bjpob3ZlciwgLmZhY2Vib29rYnRuLCAuZ29vZ2xlYnRuOmhvdmVyLCAuZ29vZ2xlYnRuLCAubGlnaHRncmVlbmJ1dHRvbjpob3ZlciwgLmxpZ2h0Z3JlZW5idXR0b24sIC5ibHVlYnV0dG9uLCAucmVkYnV0dG9uLCAuYmxhY2tidXR0b24ge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICBwYWRkaW5nOiAxMHB4IDEwMHB4O1xuICBtYXJnaW46IDEwcHggYXV0bztcbiAgZGlzcGxheTogYmxvY2s7XG4gIGJhY2tncm91bmQ6ICMyZTJlMmU7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG4gIHRleHQtdHJhbnNmb3JtOiBub25lO1xuICBjb2xvcjogI2ZmZjtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE5cHg7XG59XG5cbi5wYWRkaW5nLWJhY2sge1xuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XG59XG5cbi5yZXZpZXdpbWFnZSBpbWcge1xuICBwYWRkaW5nLWJvdHRvbTogMTBweCAhaW1wb3J0YW50O1xufVxuXG4ucmF0ZSB7XG4gIGNvbG9yOiAjZmZhZTAwO1xuICBmb250LXNpemU6IDI1cHg7XG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xuICBkaXNwbGF5OiBpbmxpbmU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLnJhdGUgdWwge1xuICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiAwO1xuICBkaXNwbGF5OiBibG9jaztcbiAgd2lkdGg6IDEwMCU7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChvcmllbnRhdGlvbjogbGFuZHNjYXBlKSB7XG4gIC5yYXRlIHVsIHtcbiAgICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xuICAgIHBhZGRpbmc6IDA7XG4gICAgZGlzcGxheTogYmxvY2s7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cbkBtZWRpYSAobWF4LXdpZHRoOiAzMjBweCkge1xuICAuY2hlY2tib3h3cmFwcGVyIHtcbiAgICB3aWR0aDogNjAlO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogNjQwcHgpIHtcbiAgLmNoZWNrYm94d3JhcHBlciB7XG4gICAgd2lkdGg6IDMwJTtcbiAgfVxufVxuLmJhY2sge1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kLnBuZ1wiKSByZXBlYXQ7XG59XG5cbi5sb2dpbmltYWdlIHtcbiAgd2lkdGg6IDMwJSAhaW1wb3J0YW50O1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHBhZGRpbmctYm90dG9tOiAxMHB4O1xufVxuXG4uZm9ybXdyYXBwZXIge1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW46IDAgYXV0bztcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbiAgaGVpZ2h0OiBhdXRvO1xuICBtYXJnaW4tdG9wOiAxNSU7XG59XG5cbi5maWVsZCB7XG4gIHBhZGRpbmc6IDIwcHg7XG4gIGJhY2tncm91bmQ6ICNmZmY7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNmOWY5Zjk7XG4gIC8qIGJvcmRlci1yYWRpdXM6IDI1cHg7ICovXG4gIGNvbG9yOiAjMzMzMTMxO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgd2lkdGg6IDEwMCU7XG4gIGRpc3BsYXk6IGJsb2NrO1xuICBtYXJnaW46IDBweCAhaW1wb3J0YW50O1xuICBib3JkZXItcmFkaXVzOiA0cHggNHB4IDBweCAwcHg7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbi5maWVsZCA6OnBsYWNlaG9sZGVyIHtcbiAgY29sb3I6IGJsYWNrICFpbXBvcnRhbnQ7XG59XG5cbi5ibGFja2J1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uY2xlYXJidXR0b24ge1xuICBkaXNwbGF5OiBibG9jaztcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAxMDBweDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmUgIWltcG9ydGFudDtcbiAgaGVpZ2h0OiAxLjVlbTtcbn1cblxuLnJlZGJ1dHRvbiB7XG4gIGJhY2tncm91bmQ6ICNlYTQzMzUgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMjVweCA5NXB4ICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiAxMDAlO1xufVxuXG4uYmx1ZWJ1dHRvbiB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjM2I1OTk4ICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbn1cblxuLmxpZ2h0Z3JlZW5idXR0b24ge1xuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xufVxuXG4ubGlnaHRncmVlbmJ1dHRvbjpob3ZlciB7XG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgbWFyZ2luLXRvcDogMTBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xufVxuXG4uZ29vZ2xlYnRuIHtcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgb3ZlcmZsb3cteDogaGlkZGVuO1xuICBwYWRkaW5nOiAxNXB4IDMycHg7XG59XG5cbi5nb29nbGVidG46aG92ZXIge1xuICBiYWNrZ3JvdW5kOiAjZmZmZmZmICFpbXBvcnRhbnQ7XG4gIG1hcmdpbi1ib3R0b206IDI1cHggIWltcG9ydGFudDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDE1cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xuICBib3JkZXItcmFkaXVzOiA1MHB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBvdmVyZmxvdy14OiBoaWRkZW47XG4gIHBhZGRpbmc6IDE1cHggMzJweDtcbiAgY29sb3I6ICMwMDA7XG59XG5cbi5mYWNlYm9va2J0biB7XG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcbiAgbWFyZ2luLWJvdHRvbTogMjVweCAhaW1wb3J0YW50O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTVweDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XG4gIHBhZGRpbmc6IDE1cHggMzJweDtcbn1cblxuLmZhY2Vib29rYnRuOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2ZkZmRmZCAhaW1wb3J0YW50O1xuICBtYXJnaW4tYm90dG9tOiAyNXB4ICFpbXBvcnRhbnQ7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZm9udC1zaXplOiAxNXB4O1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcbiAgcGFkZGluZzogMTVweCAzMnB4O1xuICBjb2xvcjogIzAwMDtcbn1cblxuLmxpZ2h0Z3JlZW5idXR0b24gLmxvZ2luLWJ0biB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgZGlzcGxheTogYmxvY2s7XG59XG5cbi5saWdodGdyZWVuYnV0dG9uOmhvdmVyIHtcbiAgYmFja2dyb3VuZDogI2QzZDNkMyAhaW1wb3J0YW50O1xuICBjb2xvcjogIzAwMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGZvbnQtc2l6ZTogMTVweDtcbn1cblxuLmFsZXJ0LW1kIC5hbGVydC1oZWFkIHtcbiAgcGFkZGluZzogMTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBjb2xvcjogIzIyMjtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xufVxuXG4uYWxlcnQtbWQgLmFsZXJ0LXRpdGxlIHtcbiAgZm9udC1zaXplOiAxOHB4O1xuICB0ZXh0LXRyYW5zZm9ybTogdXBwZXJjYXNlO1xufVxuXG4uYWxlcnQtbWQgLmFsZXJ0LWJ1dHRvbiB7XG4gIHBhZGRpbmc6IDA7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBkaXNwbGF5OiBibG9jaztcbn1cblxuLnNjcm9sbC1jb250ZW50IHtcbiAgbWFyZ2luLXRvcDogMTMlO1xufVxuXG4uZml4ZWQtY29udGVudCB7XG4gIG1hcmdpbi10b3A6IDEzJTtcbn1cblxuLnJlZGxvZ28ge1xuICBmb250LXdlaWdodDogbm9ybWFsO1xuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6ICNlYTQzMzUgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbn1cblxuLmJsdWVsb2dvIHtcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC10cmFuc2Zvcm06IG5vbmU7XG4gIGNvbG9yOiAjZmZmO1xuICBiYWNrZ3JvdW5kOiAjM2I1OTk4ICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDUwcHg7XG59XG5cbi5sZWZ0LWZsb2F0IHtcbiAgZmxvYXQ6IGxlZnQ7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn1cblxuLnNvY2lhbC1sb2dpbiB7XG4gIG1hcmdpbi1sZWZ0OiAzMSU7XG4gIG1hcmdpbi10b3A6IDMlO1xufVxuXG4ubG9nby10ZXh0IHtcbiAgY29sb3I6ICNmZmY7XG4gIGZvbnQtd2VpZ2h0OiA3MDA7XG4gIGZvbnQtc2l6ZTogNTBweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICB0ZXh0LXNoYWRvdzogIzAwMDtcbn1cblxuLmxvZ28tdGV4dCBzcGFuIHtcbiAgcGFkZGluZzogNHB4O1xuICBmb250LXdlaWdodDogMzAwICFpbXBvcnRhbnQ7XG4gIGNvbG9yOiAjZDNkM2QzO1xufVxuXG4uc2lnbnVwLWNvbnRhaW5lciB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgbWFyZ2luLXRvcDogMjIlO1xufVxuXG4uZm9ybXdyYXBwZXIgaW5wdXQ6ZW1haWwge1xuICBib3JkZXItcmFkaXVzOiA2cHggNnB4IDBweCAwcHg7XG59XG5cbi5mb3Jtd3JhcHBlciBQYXNzd29yZCB7XG4gIGJvcmRlci1yYWRpdXM6IDBweCAwcHggNnB4IDZweDtcbn1cblxuLmxvZ28tZm9udCB7XG4gIGZvbnQtc2l6ZTogMy40cmVtICFpbXBvcnRhbnQ7XG59XG5cbi5zaWdudXAtY29udGFpbmVyIHAge1xuICBjb2xvcjogI2ZmZjtcbiAgbWFyZ2luLXRvcDogLTUwcHg7XG59XG5cbi5zaWdudXAtY29udGFpbmVyIHNwYW4ge1xuICBjb2xvcjogIzk2OTQ5NDtcbn1cblxuLmxvZ2luLWNvbnRhaW5lciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICMwMDA7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xuICAubG9naW5pbWFnZSB7XG4gICAgd2lkdGg6IDE1JSAhaW1wb3J0YW50O1xuICB9XG59IiwiQGltcG9ydCBcIi4uL3Rlcm1zLy90ZXJtcy5wYWdlLnNjc3NcIjtcclxuXHJcbi5yZXZpZXctdGl0bGUge1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBmb250LXNpemU6IDIzcHg7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1hcmdpbjogMDtcclxuICBwYWRkaW5nLXRvcDogMyU7XHJcbn1cclxuXHJcbmlvbi1yYXRpbmcge1xyXG4gIC0tY29sb3I6IGdyYXk7XHJcbiAgLS1jb2xvci1maWxsZWQ6IGdyZWVuO1xyXG59XHJcblxyXG4uYXNocmVjdGFuZ2xlLXJldmlldyB7XHJcbiAgQGV4dGVuZCAuYXNocmVjdGFuZ2xlO1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgcGFkZGluZy1ib3R0b206IDBweDtcclxuICBtYXJnaW4tYm90dG9tOiA1cHg7XHJcbn1cclxuXHJcbi5uby1ib3JkZXIge1xyXG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uY2hlY2tib3h3cmFwcGVyIHtcclxuICB3aWR0aDogNTMlO1xyXG4gIG1hcmdpbjogMCBhdXRvO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4uY2hlY2tib3h0ZXh0IHtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGNvbG9yOiAjMmUyZTJlO1xyXG59XHJcblxyXG4uZGFya2J1dHRvbiB7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBwYWRkaW5nOiAxMHB4IDEwMHB4O1xyXG4gIG1hcmdpbjogMTBweCBhdXRvO1xyXG4gIGRpc3BsYXk6IGJsb2NrO1xyXG4gIGJhY2tncm91bmQ6ICMyZTJlMmU7XHJcbiAgZm9udC13ZWlnaHQ6IG5vcm1hbDtcclxuICB0ZXh0LXRyYW5zZm9ybTogbm9uZTtcclxuICBjb2xvcjogI2ZmZjtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxOXB4O1xyXG59XHJcblxyXG4ucGFkZGluZy1iYWNrIHtcclxuICBwYWRkaW5nLXJpZ2h0OiA1cHg7XHJcbn1cclxuLnJldmlld2ltYWdlIGltZyB7XHJcbiAgcGFkZGluZy1ib3R0b206IDEwcHggIWltcG9ydGFudDtcclxufVxyXG4ucmF0ZSB7XHJcbiAgY29sb3I6ICNmZmFlMDA7XHJcbiAgZm9udC1zaXplOiAyNXB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiAyMHB4O1xyXG4gIGRpc3BsYXk6IGlubGluZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuLnJhdGUgdWwge1xyXG4gIG1hcmdpbjogMCBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgcGFkZGluZzogMDtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuQG1lZGlhIHNjcmVlbiBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpIHtcclxuICAucmF0ZSB1bCB7XHJcbiAgICBtYXJnaW46IDAgYXV0byAhaW1wb3J0YW50O1xyXG4gICAgcGFkZGluZzogMDtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1heC13aWR0aDogMzIwcHgpIHtcclxuICAuY2hlY2tib3h3cmFwcGVyIHtcclxuICAgIHdpZHRoOiA2MCU7XHJcbiAgfVxyXG59XHJcblxyXG5AbWVkaWEgKG1pbi13aWR0aDogNjQwcHgpIHtcclxuICAuY2hlY2tib3h3cmFwcGVyIHtcclxuICAgIHdpZHRoOiAzMCU7XHJcbiAgfVxyXG59XHJcbiJdfQ== */");

/***/ }),

/***/ 31021:
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/login/login.page.html ***!
  \***********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-content class=\"login-container\" color=\"dark\">\r\n  <section class=\"support-section\">\r\n    <!-- <img src=\"assets/images/supportlogo.png\">  -->\r\n    <p class=\"logo-text\">RideBidder</p>\r\n    <!-- <img class=\"loginimage\" src=\"assets/images/login.png\"> -->\r\n  </section>\r\n  <section class=\"formwrapper\">\r\n    <form>\r\n\r\n      <!-- <ion-icon ios=\"ios-mail\" md=\"md-mail\"></ion-icon> -->\r\n      <div style=\"margin-bottom: 20px;\">\r\n        <input class=\"field\" type=\"text\" autcomplete=\"off\" name=\"Email\"\r\n          style=\"border-radius: 4px 4px 0px 0px; border-radius: 50px;\" placeholder=\"Email\" [(ngModel)]=\"email\">\r\n      </div>\r\n      <!-- <ion-icon ios=\"ios-lock\" md=\"md-lock\"></ion-icon> -->\r\n      <div>\r\n        <input class=\"field\" type=\"password\" autcomplete=\"off\" name=\"Password\"\r\n          style=\"border-radius: 0px 0px 4px 4px; border-radius: 50px;\" placeholder=\"Password\" [(ngModel)]=\"password\">\r\n      </div>\r\n      <ion-button color=\"light\" fill=\"clear\" expand=\"block\" (click)='resetPassword()'>Forgot Password ?</ion-button>\r\n      <button class=\"lightgreenbutton login-btn\" ion-button color=\"primary\" (click)='doLogin()'>SIGN IN</button>\r\n      <ion-grid hidden>\r\n        <ion-row>\r\n          <ion-col size=\"6\">\r\n            <ion-button style=\"font-size: 1.5em;\" class=\"ion-float-end\" fill=\"clear\" color=\"danger\"\r\n              (click)=\"loginSocial('Google')\">\r\n              <ion-icon name=\"logo-google\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <ion-button style=\"font-size: 1.5em;\" class=\"ion-float-start\" fill=\"clear\" color=\"primary\"\r\n              (click)=\"loginSocial('Facebook')\">\r\n              <ion-icon name=\"logo-facebook\"></ion-icon>\r\n            </ion-button>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n      <div class=\"signup-container\">\r\n        <p color=\"bright\" (click)=\"doRegistration('basic')\"><span color=\"light\" style=\"margin-top: -100px;\">Don`t Have\r\n            An Account?</span> Sign Up\r\n        </p>\r\n\r\n      </div>\r\n    </form>\r\n  </section>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_login_login_module_ts.js.map