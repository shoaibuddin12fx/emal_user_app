(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_pages_module_ts"],{

/***/ 18950:
/*!***************************************!*\
  !*** ./src/app/pages/pages.module.ts ***!
  \***************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesModule": () => (/* binding */ PagesModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages.routing.module */ 8639);





let PagesModule = class PagesModule {
};
PagesModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [],
        entryComponents: [],
        imports: [_angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule, _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule, _pages_routing_module__WEBPACK_IMPORTED_MODULE_0__.PagesRoutingModule],
        //   providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
        providers: [],
    })
], PagesModule);



/***/ }),

/***/ 8639:
/*!***********************************************!*\
  !*** ./src/app/pages/pages.routing.module.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoutingModule": () => (/* binding */ PagesRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 39895);



const routes = [
    {
        path: '',
        redirectTo: 'login',
        pathMatch: 'full',
    },
    {
        path: 'home',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_payments_payments_page_ts"), __webpack_require__.e("default-src_app_pages_autocomplete_autocomplete_page_ts"), __webpack_require__.e("default-node_modules_geofire_dist_geofire_geofire_min_js-src_app_Services_data_service_ts"), __webpack_require__.e("default-src_app_pages_home_fare-now_fare-now_component_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_home_home_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./home/home.module */ 57994)).then((m) => m.HomePageModule),
    },
    {
        path: 'terms',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_terms_terms_page_ts"), __webpack_require__.e("src_app_pages_terms_terms_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./terms/terms.module */ 27498)).then((m) => m.TermsPageModule),
    },
    {
        path: 'support',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_support_support_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./support/support.module */ 82602)).then((m) => m.SupportPageModule),
    },
    {
        path: 'share',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_share_share_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./share/share.module */ 10630)).then((m) => m.SharePageModule),
    },
    {
        path: 'reviewdriver',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_reviewdriver_reviewdriver_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./reviewdriver/reviewdriver.module */ 5462)).then((m) => m.ReviewdriverPageModule),
    },
    {
        path: 'registercustomer',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_terms_terms_page_ts"), __webpack_require__.e("src_app_pages_registercustomer_registercustomer_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./registercustomer/registercustomer.module */ 58182)).then((m) => m.RegistercustomerPageModule),
    },
    {
        path: 'receipt',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_receipt_receipt_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./receipt/receipt.module */ 27583)).then((m) => m.ReceiptPageModule),
    },
    {
        path: 'profile',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_profile_profile_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./profile/profile.module */ 88558)).then((m) => m.ProfilePageModule),
    },
    {
        path: 'popover',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_popover_popover_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./popover/popover.module */ 78949)).then((m) => m.PopoverPageModule),
    },
    {
        path: 'payments',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_payments_payments_page_ts"), __webpack_require__.e("src_app_pages_payments_payments_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./payments/payments.module */ 15795)).then((m) => m.PaymentsPageModule),
    },
    {
        path: 'map',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_autocomplete_autocomplete_page_ts"), __webpack_require__.e("common"), __webpack_require__.e("src_app_pages_map_map_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./map/map.module */ 96016)).then((m) => m.MapPageModule),
    },
    {
        path: 'makepayment',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_makepayment_makepayment_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./makepayment/makepayment.module */ 21478)).then((m) => m.MakepaymentPageModule),
    },
    {
        path: 'login',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_terms_terms_page_ts"), __webpack_require__.e("src_app_pages_login_login_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./login/login.module */ 21053)).then((m) => m.LoginPageModule),
    },
    {
        path: 'locationloader',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_locationloader_locationloader_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./locationloader/locationloader.module */ 76147)).then((m) => m.LocationloaderPageModule),
    },
    {
        path: 'fare',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_payments_payments_page_ts"), __webpack_require__.e("default-src_app_pages_autocomplete_autocomplete_page_ts"), __webpack_require__.e("default-node_modules_geofire_dist_geofire_geofire_min_js-src_app_Services_data_service_ts"), __webpack_require__.e("src_app_pages_fare_fare_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./fare/fare.module */ 92615)).then((m) => m.FarePageModule),
    },
    {
        path: 'currentbookings',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_currentbookings_currentbookings_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./currentbookings/currentbookings.module */ 40566)).then((m) => m.CurrentbookingsPageModule),
    },
    {
        path: 'bookings',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_bookings_bookings_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./bookings/bookings.module */ 13582)).then((m) => m.BookingsPageModule),
    },
    {
        path: 'bid-driver',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_bid-driver_bid-driver_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./bid-driver/bid-driver.module */ 57145)).then((m) => m.BidDriverPageModule),
    },
    {
        path: 'autocomplete',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_autocomplete_autocomplete_page_ts"), __webpack_require__.e("common")]).then(__webpack_require__.bind(__webpack_require__, /*! ./autocomplete/autocomplete.module */ 55147)).then((m) => m.AutocompletePageModule),
    },
    {
        path: 'activebooking',
        loadChildren: () => Promise.all(/*! import() */[__webpack_require__.e("default-src_app_pages_payments_payments_page_ts"), __webpack_require__.e("default-node_modules_geofire_dist_geofire_geofire_min_js-src_app_Services_data_service_ts"), __webpack_require__.e("default-src_app_pages_home_fare-now_fare-now_component_ts"), __webpack_require__.e("src_app_pages_activebooking_activebooking_module_ts")]).then(__webpack_require__.bind(__webpack_require__, /*! ./activebooking/activebooking.module */ 84162)).then((m) => m.ActivebookingPageModule),
    },
    {
        path: 'about',
        loadChildren: () => __webpack_require__.e(/*! import() */ "src_app_pages_about_about_module_ts").then(__webpack_require__.bind(__webpack_require__, /*! ./about/about.module */ 18114)).then((m) => m.AboutPageModule),
    },
];
let PagesRoutingModule = class PagesRoutingModule {
};
PagesRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_0__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_1__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__.RouterModule],
    })
], PagesRoutingModule);



/***/ })

}]);
//# sourceMappingURL=src_app_pages_pages_module_ts.js.map