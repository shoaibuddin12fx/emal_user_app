(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_home_home_module_ts"],{

/***/ 31619:
/*!********************************************************!*\
  !*** ./src/app/pages/home/fare-now/fare-now.module.ts ***!
  \********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FareNowModule": () => (/* binding */ FareNowModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _fare_now_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fare-now.component */ 69753);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);







let FareNowModule = class FareNowModule {
};
FareNowModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        declarations: [_fare_now_component__WEBPACK_IMPORTED_MODULE_0__.FareNowComponent],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__.IonicModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
        ],
        providers: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.DatePipe
        ],
        exports: [_fare_now_component__WEBPACK_IMPORTED_MODULE_0__.FareNowComponent]
    })
], FareNowModule);



/***/ }),

/***/ 96610:
/*!***************************************************!*\
  !*** ./src/app/pages/home/home-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageRoutingModule": () => (/* binding */ HomePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 10678);




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage,
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule]
    })
], HomePageRoutingModule);



/***/ }),

/***/ 57994:
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePageModule": () => (/* binding */ HomePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./home.page */ 10678);
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home-routing.module */ 96610);
/* harmony import */ var _fare_now_fare_now_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./fare-now/fare-now.module */ 31619);








let HomePageModule = class HomePageModule {
};
HomePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _home_routing_module__WEBPACK_IMPORTED_MODULE_1__.HomePageRoutingModule,
            _fare_now_fare_now_module__WEBPACK_IMPORTED_MODULE_2__.FareNowModule
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_0__.HomePage]
    })
], HomePageModule);



/***/ }),

/***/ 10678:
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HomePage": () => (/* binding */ HomePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./home.page.html */ 78102);
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./home.page.scss */ 17603);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var _book_now_book_now_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./book-now/book-now.component */ 23790);
/* harmony import */ var _fare_now_fare_now_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./fare-now/fare-now.component */ 69753);
var HomePage_1;







let HomePage = HomePage_1 = class HomePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.isBookingConfirmed = false;
        let params = this.getQueryParams();
        //end
        if ((params === null || params === void 0 ? void 0 : params.add) == undefined || (params === null || params === void 0 ? void 0 : params.add) == null) {
            // call map here for assign pin location to map shown
            // this.nav.setRoot('pages/locationloader');
        }
        this.mapReady = false;
        this.mapService.showmap = true;
        this.pickup = {
            lat: params === null || params === void 0 ? void 0 : params.lat,
            lng: params === null || params === void 0 ? void 0 : params.lng,
            add: params === null || params === void 0 ? void 0 : params.add,
        };
        this.events.subscribe('add_address', this.setMarkerToMap.bind(this));
        this.events.subscribe('redirect_to_fare_page', this.redirectToFarePage.bind(this));
    }
    ionViewWillEnter() {
        this.openBookNow();
    }
    setMarkerToMap(data) {
        var _a, _b, _c, _d, _e;
        console.log('marker set', data);
        const bounds = new google.maps.LatLngBounds();
        if (data.pick.add) {
            if (((_a = this.oldPickup) === null || _a === void 0 ? void 0 : _a.lat) != data.pick.lat &&
                ((_b = this.oldPickup) === null || _b === void 0 ? void 0 : _b.lng) != data.pick.lng) {
                this.pick_marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data.pick.lat, data.pick.lng),
                    map: this.map,
                    draggable: true,
                    title: data.pick.add,
                });
                this.oldPickup = data.pick;
                bounds.extend(data.pick);
            }
        }
        else
            this.pick_marker = null;
        console.log('isSame');
        if (data.drop.add) {
            if (((_c = this.oldDrop) === null || _c === void 0 ? void 0 : _c.lat) != data.drop.lat &&
                ((_d = this.oldDrop) === null || _d === void 0 ? void 0 : _d.lng) != data.drop.lng) {
                this.drop_marker = new google.maps.Marker({
                    position: new google.maps.LatLng(data.drop.lat, data.drop.lng),
                    map: this.map,
                    draggable: true,
                    title: data.drop.add,
                });
                bounds.extend(data.drop);
                this.oldDrop = data.drop;
            }
        }
        else
            this.drop_marker = null;
        this.map.fitBounds(bounds);
        if (this.pick_marker && this.drop_marker) {
            console.log('we can work from here', this.pick_marker.position.lat());
            this.directionsService = new google.maps.DirectionsService();
            (_e = this.directionsDisplay) === null || _e === void 0 ? void 0 : _e.setMap(null);
            this.directionsDisplay = new google.maps.DirectionsRenderer({
                map: this.map,
            });
            this.pick_marker.setMap(null);
            this.drop_marker.setMap(null);
            this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay, this.pick_marker.position, this.drop_marker.position);
        }
    }
    calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB) {
        directionsService.route({
            origin: pointA,
            destination: pointB,
            travelMode: google.maps.TravelMode.DRIVING,
        }, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            }
            else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }
    redirectToFarePage(data) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            console.log('redirect ', data);
            // let data = {
            //   pickupDet: this.pick,
            //   dropDet: this.drop,
            //   pickupService: pickupService,
            //   jobtype: 'pickup'
            // }
            // let data = {
            //   pickupDet: this.pick,
            //   pickupService: pickupService,
            //   jobtype: 'personal'
            // }
            // this.nav.push('pages/fare', data);
            yield this.modals.dismiss({ data: 'A' });
            yield this.modals.present(_fare_now_fare_now_component__WEBPACK_IMPORTED_MODULE_4__.FareNowComponent, { data });
            // this.openBookNow();
        });
    }
    ngOnInit() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            this.curUser = yield this.firebaseService.getCurrentUser();
            this.menuCtrl.enable(true, 'drawer');
            this.events.subscribe('BOOKING_CONFIRMED', () => {
                console.log('BOOKING_CONFIRMED event recieved');
                this.isBookingConfirmed = true;
                this.loadMap();
            });
        });
    }
    ngAfterViewInit() {
        this.platform.ready().then(() => {
            this.initializeMapBeforeSetCoordinates().then((v) => {
                this.initMap();
                setTimeout(() => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                    const active = localStorage.getItem('google_map');
                    if (!active) {
                        const flag = yield this.utility.presentConfirm('Thanks', 'Remind Later', 'How To Use', 'In the search field, type in the new address and press enter. You can also hold your finger and drag to new location, then press the + symbol in the top right corner');
                        localStorage.setItem('google_map', `${flag}`);
                    }
                    else {
                        // this.openBookNow();
                    }
                }), 1000);
            });
        });
    }
    initializeMapBeforeSetCoordinates() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            return new Promise((resolve) => (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
                console.log('initializeMapBeforeSetCoordinates');
                const mylocation = yield this.utility.getCurrentLocationCoordinates();
                console.log('My Location', mylocation);
                this.userLocation = mylocation;
                resolve(mylocation);
            }));
        });
    }
    initMap() {
        this.loadMap();
    }
    loadMap() {
        var _a, _b;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            //  const mylocation = await this.utility.getCurrentLocationCoordinates();
            // console.log("My Location", mylocation)
            let params = this.getQueryParams();
            if (!HomePage_1.locationParams)
                HomePage_1.locationParams = params;
            params = HomePage_1.locationParams;
            let myLocation = {};
            if (params) {
                myLocation = {
                    lat: parseFloat(params === null || params === void 0 ? void 0 : params.lat),
                    lng: parseFloat(params.lng),
                };
            }
            else
                myLocation = {
                    lat: (_a = this.userLocation) === null || _a === void 0 ? void 0 : _a.lat,
                    lng: (_b = this.userLocation) === null || _b === void 0 ? void 0 : _b.lng,
                };
            console.log('userLocation', this.userLocation);
            this.map = new google.maps.Map(this.mapElement.nativeElement, {
                zoom: 15,
                center: myLocation,
            });
            // this.map = new GoogleMap('map', {
            //   backgroundColor: 'white',
            //   controls: {
            //     compass: false,
            //     myLocationButton: false,
            //     indoorPicker: false,
            //     zoom: true,
            //   },
            //   gestures: {
            //     scroll: true,
            //     tilt: true,
            //     rotate: true,
            //     zoom: true,
            //   },
            //   camera: {
            //     target: location,
            //     tilt: 30,
            //     zoom: 15, //,
            //     // 'bearing': 50
            //   },
            // });
            let info = this.setMapInfo;
            let self = this;
            // * CAMERA_CHANGE * //
            google.maps.event.addListener(this.map, 'dragend', (event) => {
                if (!event)
                    return;
                const lt = event.latLng.lat();
                const lg = event.latLng.lng();
                const coords = { lat: lt, lng: lg };
                if (this.autoChange == true) {
                    clearTimeout(this.searchBoundsTimer);
                    this.searchBoundsTimer = setTimeout(() => {
                        this.geocodeAddress(lt, lg, (data) => {
                            this.setMapInfo(data.geometry.location.lat(), data.geometry.location.lng(), data.formatted_address);
                            // if (this.pickup.add && this.drop.add)
                            //   this.setDistance(this.pickup, this.drop);
                            console.log('map data: ');
                            console.log(data);
                        });
                    }, 500);
                }
                else {
                    this.autoChange = true;
                }
            });
            this.map.addListener('click', (mapsMouseEvent) => {
                let position = JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2);
                console.log('OnMapClicked', position);
            });
            // this.map.on(GoogleMapsEvent.CAMERA_MOVE).subscribe((res) => {
            //   if (this.autoChange == true) {
            //     clearTimeout(this.searchBoundsTimer);
            //     this.searchBoundsTimer = setTimeout(() => {
            //       this.geocodeAddress(res.target.lat, res.target.lng, (data) => {
            //         this.setMapInfo(
            //           data.geometry.location.lat(),
            //           data.geometry.location.lng(),
            //           data.formatted_address
            //         );
            //         if (this.pickup.add && this.drop.add)
            //           this.setDistance(this.pickup, this.drop);
            //         console.log('map data: ');
            //         console.log(data);
            //       });
            //     }, 500);
            //   } else {
            //     this.autoChange = true;
            //   }
            // });
            google.maps.event.addListenerOnce(this.map, 'idle', function () {
                var _a;
                // do something only the first time the map is loaded
                (_a = this.map) === null || _a === void 0 ? void 0 : _a.markers.clear();
                this.mapReady = true;
                console.log('Map is ready!');
                this.markerSetTo = 'pickup';
            });
        });
    }
    geocodeAddress(lat, lng, callback) {
        let geocoder = new google.maps.Geocoder();
        let latlng = { lat: lat, lng: lng };
        let request = {
            latLng: latlng,
        };
        geocoder.geocode({ latLng: latlng }, function (data, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                callback(data[0]);
            }
            else {
                console.log('Geocode Error');
            }
        });
    }
    openBookNow() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__awaiter)(this, void 0, void 0, function* () {
            let self = this;
            console.log('isBookingConfirmed', this.isBookingConfirmed);
            const res = yield this.modals.present(_book_now_book_now_component__WEBPACK_IMPORTED_MODULE_3__.BookNowComponent, self.isBookingConfirmed
                ? {
                    drop: {
                        lat: 0,
                        lng: 0,
                        add: null,
                    },
                    pick: {
                        lat: 0,
                        lng: 0,
                        add: null,
                    },
                }
                : {}, 'half-modal-book-now');
            this.isBookingConfirmed = false;
        });
    }
};
HomePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Injector }
];
HomePage.propDecorators = {
    mapElement: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: ['map', { static: false },] }]
};
HomePage = HomePage_1 = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HomePage);



/***/ }),

/***/ 17603:
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("#container {\n  text-align: center;\n  position: absolute;\n  left: 0;\n  right: 0;\n  top: 50%;\n  transform: translateY(-50%);\n}\n\n#container strong {\n  font-size: 20px;\n  line-height: 26px;\n}\n\n#container p {\n  font-size: 16px;\n  line-height: 22px;\n  color: #8c8c8c;\n  margin: 0;\n}\n\n#container a {\n  text-decoration: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImhvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0Usa0JBQUE7RUFFQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsUUFBQTtFQUNBLDJCQUFBO0FBQUY7O0FBR0E7RUFDRSxlQUFBO0VBQ0EsaUJBQUE7QUFBRjs7QUFHQTtFQUNFLGVBQUE7RUFDQSxpQkFBQTtFQUVBLGNBQUE7RUFFQSxTQUFBO0FBRkY7O0FBS0E7RUFDRSxxQkFBQTtBQUZGIiwiZmlsZSI6ImhvbWUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiI2NvbnRhaW5lciB7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgbGVmdDogMDtcclxuICByaWdodDogMDtcclxuICB0b3A6IDUwJTtcclxuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XHJcbn1cclxuXHJcbiNjb250YWluZXIgc3Ryb25nIHtcclxuICBmb250LXNpemU6IDIwcHg7XHJcbiAgbGluZS1oZWlnaHQ6IDI2cHg7XHJcbn1cclxuXHJcbiNjb250YWluZXIgcCB7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIGxpbmUtaGVpZ2h0OiAyMnB4O1xyXG5cclxuICBjb2xvcjogIzhjOGM4YztcclxuXHJcbiAgbWFyZ2luOiAwO1xyXG59XHJcblxyXG4jY29udGFpbmVyIGEge1xyXG4gIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcclxufSJdfQ== */");

/***/ }),

/***/ 78102:
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/home/home.page.html ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<ion-header>\r\n  <ion-toolbar color=\"dark\">\r\n    <ion-buttons>\r\n      <ion-button fill=\"clear\" (click)=\"ToggleMenuBar()\">\r\n        <ion-icon name=\"menu\"></ion-icon>\r\n      </ion-button>\r\n    </ion-buttons>\r\n    \r\n    <ion-title>\r\n      RideBidder\r\n    </ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <div #map id=\"map\" style=\"height: 100%;\"></div>\r\n  <ion-fab vertical=\"bottom\" horizontal=\"start\" slot=\"fixed\">\r\n    <ion-fab-button>\r\n      <ion-icon name=\"settings\"></ion-icon>\r\n    </ion-fab-button>\r\n    <ion-fab-list side=\"end\">\r\n      <ion-fab-button>\r\n        <ion-icon name=\"locate-outline\"></ion-icon>\r\n      </ion-fab-button>\r\n      <ion-fab-button (click)=\"openBookNow()\">\r\n        <ion-icon name=\"options-outline\"></ion-icon>\r\n      </ion-fab-button>\r\n    </ion-fab-list>\r\n  </ion-fab>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_home_home_module_ts.js.map