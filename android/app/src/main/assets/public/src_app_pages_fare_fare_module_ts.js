(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_fare_fare_module_ts"],{

/***/ 59930:
/*!**************************************************************!*\
  !*** ./src/app/components/header/header.component.module.ts ***!
  \**************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderModule": () => (/* binding */ HeaderModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _header_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./header.component */ 43646);






// import { ForgotPasswordPageRoutingModule } from './forgot-password-routing.module';
// import { ForgotPasswordPage } from './forgot-password.page';
let HeaderModule = class HeaderModule {
};
HeaderModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__.IonicModule,
            //    ForgotPasswordPageRoutingModule
        ],
        exports: [_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent],
        declarations: [_header_component__WEBPACK_IMPORTED_MODULE_0__.HeaderComponent],
    })
], HeaderModule);



/***/ }),

/***/ 43646:
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "HeaderComponent": () => (/* binding */ HeaderComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./header.component.html */ 97911);
/* harmony import */ var _header_component_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./header.component.scss */ 64993);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);




let HeaderComponent = class HeaderComponent {
    constructor() {
        this.title = 'Unknown';
    }
    ngOnInit() { }
};
HeaderComponent.ctorParameters = () => [];
HeaderComponent.propDecorators = {
    title: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_2__.Input }]
};
HeaderComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.Component)({
        selector: 'app-header',
        template: _raw_loader_header_component_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_header_component_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], HeaderComponent);



/***/ }),

/***/ 58588:
/*!***************************************************!*\
  !*** ./src/app/pages/fare/fare-routing.module.ts ***!
  \***************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FarePageRoutingModule": () => (/* binding */ FarePageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _fare_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fare.page */ 57757);




const routes = [
    {
        path: '',
        component: _fare_page__WEBPACK_IMPORTED_MODULE_0__.FarePage
    }
];
let FarePageRoutingModule = class FarePageRoutingModule {
};
FarePageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], FarePageRoutingModule);



/***/ }),

/***/ 92615:
/*!*******************************************!*\
  !*** ./src/app/pages/fare/fare.module.ts ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FarePageModule": () => (/* binding */ FarePageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _fare_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./fare-routing.module */ 58588);
/* harmony import */ var _fare_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fare.page */ 57757);
/* harmony import */ var src_app_components_header_header_component_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/components/header/header.component.module */ 59930);








let FarePageModule = class FarePageModule {
};
FarePageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_5__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__.IonicModule,
            _fare_routing_module__WEBPACK_IMPORTED_MODULE_0__.FarePageRoutingModule,
            src_app_components_header_header_component_module__WEBPACK_IMPORTED_MODULE_2__.HeaderModule
        ],
        declarations: [_fare_page__WEBPACK_IMPORTED_MODULE_1__.FarePage]
    })
], FarePageModule);



/***/ }),

/***/ 57757:
/*!*****************************************!*\
  !*** ./src/app/pages/fare/fare.page.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FarePage": () => (/* binding */ FarePage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_fare_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./fare.page.html */ 41850);
/* harmony import */ var _fare_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./fare.page.scss */ 42778);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);
/* harmony import */ var _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../autocomplete/autocomplete.page */ 86302);
/* harmony import */ var src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/Services/data.service */ 42498);
/* harmony import */ var geofire__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! geofire */ 58809);
/* harmony import */ var geofire__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(geofire__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _payments_payments_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../payments/payments.page */ 42061);










let FarePage = class FarePage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector, platform, alertCtrl, dataService) {
        var _a, _b, _c;
        super(injector);
        this.platform = platform;
        this.alertCtrl = alertCtrl;
        this.dataService = dataService;
        this.personal_hour = 0;
        this.personal_minimum = 0;
        this.pickup_base_km = 0;
        this.pickup_hour = 0;
        this.pickup_km = 0;
        this.pickup_minimum = 0;
        this.allDriver = [];
        this.notificationData = [];
        this.totalBill = 0;
        this.count = 0;
        this.requestDone = false;
        this.drivermap = true;
        this.params = this.getParams();
        console.log('getParams', this.params);
        this.pickup = (_a = this.params) === null || _a === void 0 ? void 0 : _a.pick;
        this.drop = (_b = this.params) === null || _b === void 0 ? void 0 : _b.drop;
        console.log('Pickup -> ', this.pickup);
        console.log('Drop ->', this.drop);
        // let cDate = new Date();
        // let isoDate = cDate.toISOString();
        // this.yesterdayDate = this.formatLocalDate("yesterday").substring(0, 10);
        let cDate = this.formatLocalDate('');
        // this.laterDate = cDate;
        this.currentDate = this.formatLocalDate('cuurent').substring(0, 10);
        this.showDate = cDate.substring(0, 10);
        this.showTime = cDate.substring(11, 19);
        this.calculateDateTime();
        //new added by me..
        if ((_c = this.params) === null || _c === void 0 ? void 0 : _c.PickupService) {
            this.zone.run(() => {
                this.PickupService = this.params.PickupService;
            });
        }
        //end
        this.mapReady = false;
        this.mapService.showmap = true;
        this.initialize();
    }
    ionViewDidLoad() {
        // this.navBar.backButtonClick = (e:UIEvent)=>{
        //   this.navCtrl.setRoot(CurrentbookingsPage);
        // }
    }
    ngDoCheck() {
        if (this.mapReady) {
            if (this.mapService.showmap) {
                this.map.setClickable(true);
            }
            else {
                this.map.setClickable(false);
            }
        }
    }
    ngOnInit() {
        this.platform.ready().then(() => {
            setTimeout(() => {
                // this.loadMap();
            }, 1000);
        });
    }
    initialize() {
        var _a, _b;
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            let params = this.getParams();
            this.curUser = yield this.firebaseService.getCurrentUser();
            this.refcode = this.getDb().ref('/users/' + this.curUser.uid + '/promo_code');
            this.refcode.on('value', (snapshot) => {
                this.customerPromocode = snapshot.val();
            });
            this.bookingKey = (_a = this.params) === null || _a === void 0 ? void 0 : _a.bookingId;
            if (this.bookingKey) {
                this.bookingDetails = (_b = this.params) === null || _b === void 0 ? void 0 : _b.bookingDetails;
                this.trip = this.bookingDetails.tripType;
                this.pickup = this.bookingDetails.pickup;
                if (this.bookingDetails.drop) {
                    this.zone.run(() => {
                        this.PickupService = true;
                        this.drop = this.bookingDetails.drop;
                    });
                }
                else {
                    this.zone.run(() => {
                        this.PickupService = false;
                    });
                }
                this.distance = this.bookingDetails.distance;
                this.transmission = this.bookingDetails.transmission;
                this.myDate = this.bookingDetails.tripdate;
                //new changes
                this.showDate = this.bookingDetails.tripdate.substring(0, 10);
                this.showTime = this.bookingDetails.tripdate.substring(11, 19);
                //new changes end
                this.triptime = this.bookingDetails.triptime;
                this.estimate = this.bookingDetails.estimate;
                this.serviceType = this.bookingDetails.serviceType;
                if (this.bookingDetails.promo_code != undefined) {
                    this.promo_code = this.bookingDetails.promo_code;
                }
            }
            else {
                //new added by me..
                if (this.PickupService) {
                    this.trip = 'Oneway';
                    this.pickup = params === null || params === void 0 ? void 0 : params.pickupDet;
                    this.drop = params === null || params === void 0 ? void 0 : params.dropDet;
                    this.distance = params === null || params === void 0 ? void 0 : params.distRoute;
                    this.googleTime = params === null || params === void 0 ? void 0 : params.googleTime;
                    this.transmission = 'Automatic';
                    //this.myDate =  this.formatLocalDate();
                    this.myDate = this.originalDate; //new line added
                    // this.triptime = 4;
                    this.triptime = params === null || params === void 0 ? void 0 : params.googleTime;
                    this.serviceType = 'Pickup';
                }
                else {
                    this.trip = 'Oneway';
                    this.pickup = params === null || params === void 0 ? void 0 : params.pickupDet;
                    this.drop = {
                        lat: null,
                        lng: null,
                        add: null,
                    };
                    this.distance = null;
                    this.transmission = 'Automatic';
                    // this.myDate =  this.formatLocalDate();
                    this.myDate = this.originalDate; //new line added
                    this.triptime = '2';
                    // this.serviceType = "Personal"; // add to Sach23
                    this.serviceType = 'Personal driver'; // add to Hemant
                }
                this.changeEstimate();
            }
            var ref = this.getDb().ref('rates');
            ref.on('value', (snapshot) => {
                if (snapshot.val()) {
                    this.personal_hour = snapshot.val().personal_hour;
                    this.personal_minimum = snapshot.val().personal_minimum;
                    this.pickup_base_km = snapshot.val().pickup_base_km;
                    this.pickup_hour = snapshot.val().pickup_hour;
                    this.pickup_km = snapshot.val().pickup_km;
                    this.pickup_minimum = snapshot.val().pickup_minimum;
                    this.pickup_minute = snapshot.val().pickup_minute;
                    this.zone.run(() => {
                        setTimeout(() => {
                            this.loadMap();
                        }, 1000);
                        this.changeEstimate();
                    });
                }
            });
        });
    }
    setMapInfo(lat, lng, add) {
        this.zone.run(() => {
            if (this.markerSetTo == 'pickup') {
                this.pickup.lat = lat;
                this.pickup.lng = lng;
                this.pickup.add = add;
            }
            else {
                this.drop.lat = lat;
                this.drop.lng = lng;
                this.drop.add = add;
            }
        });
    }
    calculateAndDisplayRoute(lat, lng) {
        var directionsService = new google.maps.DirectionsService();
        var directionsDisplay = new google.maps.DirectionsRenderer();
        var map = new google.maps.Map(document.getElementById('map3'), {
            zoom: 7,
            center: { lat: lat, lng: lng },
        });
        directionsDisplay.setMap(map);
        directionsService.route({
            origin: this.pickup.add,
            destination: this.drop.add,
            travelMode: 'DRIVING',
        }, function (response, status) {
            if (status === 'OK') {
                directionsDisplay.setDirections(response);
            }
            else {
                // window.alert('Directions request failed due to ' + status);
            }
        });
    }
    animateMarker(lat, lng) {
        var map = new google.maps.Map(document.getElementById('map3'), {
            zoom: 13,
            center: { lat: lat, lng: lng },
        });
        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: { lat: lat, lng: lng },
        });
    }
    loadMap() {
        let location = { lat: this.pickup.lat, lng: this.pickup.lng };
        this.map = new google.maps.Map(document.getElementById('map3'), {
            zoom: 13,
            center: location,
        });
        // this.map = new GoogleMap('map3', {
        //   backgroundColor: 'white',
        //   controls: {
        //     compass: false,
        //     myLocationButton: false,
        //     indoorPicker: false,
        //     zoom: true,
        //   },
        //   gestures: {
        //     scroll: true,
        //     tilt: true,
        //     rotate: true,
        //     zoom: true,
        //   },
        //   camera: {
        //     target: location,
        //     tilt: 30,
        //     zoom: 15, //,
        //     // 'bearing': 50
        //   },
        // });
        let info = this.setMapInfo;
        // this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
        //   this.map.clear();
        //   this.mapReady = true;
        //   console.log('Map is ready!');
        //   this.markerSetTo = 'pickup';
        // });
        if (this.PickupService == true) {
            this.custHeight = '210px !important';
            this.calculateAndDisplayRoute(this.pickup.lat, this.pickup.lng);
        }
        else {
            this.custHeight = '280px !important';
            this.animateMarker(this.pickup.lat, this.pickup.lng);
        }
    }
    formatLocalDate(state) {
        var finalDate;
        var now = new Date(), tzo = -now.getTimezoneOffset(), dif = tzo >= 0 ? '+' : '-', pad = function (num) {
            var norm = Math.abs(Math.floor(num));
            return (norm < 10 ? '0' : '') + norm;
        };
        if (state == 'later') {
            finalDate =
                now.getFullYear() +
                    '-' +
                    pad(now.getMonth() + 1) +
                    '-' +
                    pad(now.getDate()) +
                    'T' +
                    pad(now.getHours() + 1) +
                    ':' +
                    pad(now.getMinutes()) +
                    ':' +
                    pad(now.getSeconds()) +
                    '.000Z';
        }
        else if (state == 'later-com') {
            finalDate =
                now.getFullYear() +
                    '-' +
                    pad(now.getMonth() + 1) +
                    '-' +
                    pad(now.getDate()) +
                    'T' +
                    pad(now.getHours()) +
                    ':' +
                    pad(now.getMinutes() - 2) +
                    ':' +
                    pad(now.getSeconds()) +
                    '.000Z';
        }
        else if (state == 'yesterday') {
            finalDate =
                now.getFullYear() +
                    '-' +
                    pad(now.getMonth() + 1) +
                    '-' +
                    pad(now.getDate() - 1) +
                    'T' +
                    pad(now.getHours()) +
                    ':' +
                    pad(now.getMinutes()) +
                    ':' +
                    pad(now.getSeconds()) +
                    '.000Z';
        }
        else {
            finalDate =
                now.getFullYear() +
                    '-' +
                    pad(now.getMonth() + 1) +
                    '-' +
                    pad(now.getDate()) +
                    'T' +
                    pad(now.getHours()) +
                    ':' +
                    pad(now.getMinutes()) +
                    ':' +
                    pad(now.getSeconds()) +
                    '.000Z';
        }
        return finalDate;
    }
    addCard() {
        // this.nav.push('pages/payments');
        alert('open');
        this.modals.present(_payments_payments_page__WEBPACK_IMPORTED_MODULE_6__.PaymentsPage);
    }
    selectTransmission() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create();
            alert.title = 'Transmission Type';
            alert.inputs.push({
                type: 'radio',
                label: 'Automatic',
                value: 'Automatic',
                checked: true,
            });
            alert.inputs.push({
                type: 'radio',
                label: 'Manual',
                value: 'Manual',
                checked: false,
            });
            alert.buttons.push('Cancel');
            alert.buttons.push({
                text: 'OK',
                handler: (data) => {
                    this.testRadioOpen = false;
                    this.transmission = data;
                    if (this.bookingKey) {
                        this.bookingDetails.transmission = data;
                    }
                },
            });
            alert.present();
        });
    }
    updateDate() {
        if (this.bookingKey) {
            this.bookingDetails.tripdate = this.myDate;
        }
    }
    //triptype oneway or return
    tripType() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create();
            alert.title = 'Journey Type';
            alert.inputs.push({
                type: 'radio',
                label: 'Oneway',
                value: 'Oneway',
                checked: true,
            });
            alert.inputs.push({
                type: 'radio',
                label: 'Return',
                value: 'Return',
                checked: false,
            });
            alert.buttons.push('Cancel');
            alert.buttons.push({
                text: 'OK',
                handler: (data) => {
                    this.testRadioOpen = false;
                    this.trip = data;
                    if (this.bookingKey) {
                        this.bookingDetails.tripType = data;
                    }
                },
            });
            alert.present();
        });
    }
    showAddressModal(addressType) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            if (this.bookingKey) {
                const modal = yield this.modals.present({
                    component: _autocomplete_autocomplete_page__WEBPACK_IMPORTED_MODULE_3__.AutocompletePage,
                });
                const { data } = yield modal.onDidDismiss();
                if (data != null) {
                    if (addressType == 'pickup') {
                        this.pickup.add = data.add;
                        this.pickup.lat = data.lat;
                        this.pickup.lng = data.lng;
                        if (this.bookingKey) {
                            this.bookingDetails.pickup = this.pickup;
                        }
                        if (this.PickupService) {
                            this.setDistance(this.pickup, this.drop);
                            this.changeEstimate();
                        }
                    }
                    else {
                        this.drop.add = data.add;
                        this.drop.lat = data.lat;
                        this.drop.lng = data.lng;
                        if (this.bookingKey) {
                            this.bookingDetails.drop = this.drop;
                        }
                        this.setDistance(this.pickup, this.drop);
                        this.changeEstimate();
                    }
                }
                modal.present();
            }
        });
    }
    setDistance(pick, drop) {
        var directionsService = new google.maps.DirectionsService();
        let routeDistance;
        directionsService.route({
            origin: pick,
            destination: drop,
            travelMode: 'DRIVING',
        }, (response, status) => {
            if (status === 'OK') {
                this.distance = response.routes[0].legs[0].distance.value;
                if (this.bookingKey) {
                    this.bookingDetails.distance = this.distance;
                    this.timeDistance = response.routes[0].legs[0].duration.value / 60;
                    this.googleTime = this.timeDistance.toFixed();
                    this.triptime = this.googleTime;
                }
                this.changeEstimate();
            }
            else {
                //window.alert('Directions request failed due to ' + status);
            }
        });
    }
    addPromo() {
        if (this.bookingKey) {
            let booking = this.getDb().ref('bookings/' + this.bookingKey);
            booking.once('value', (snapshot) => (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
                if (snapshot.val()) {
                    if (snapshot.val().promo_code) {
                        //alert("Promo Code Already Added");
                        let promocodeadd = yield this.alertCtrl.create({
                            header: 'Alert',
                            subHeader: 'Promo Code Already Added',
                            buttons: ['OK'],
                        });
                        promocodeadd.present();
                    }
                    else {
                        this.showPromopopup();
                    }
                }
            }));
        }
        else {
            this.showPromopopup();
        }
        /* if(this.bookingKey){
          let booking = firebase.database().ref('bookings/' + this.bookingKey) ;
          booking.once('value',(snap1:any)=>{
            if(snap1.val()){
              if(snap1.val().promo_code){
                  this.newPromoCode = snap1.val().promo_code;
                  alert("you already added One Promo Code");
              }else{
                this.showPromopopup();
              }
            }
          })
        }else{
        this.showPromopopup();
        }*/
    }
    showPromopopup() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            let prompt = yield this.alertCtrl.create({
                header: 'Add Promo Code',
                subHeader: 'Promo Code wiil be applied in your bill after End of your Trip',
                inputs: [
                    {
                        name: 'title',
                        placeholder: 'Code',
                    },
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        handler: (data) => { },
                    },
                    {
                        text: 'Save',
                        handler: (data) => {
                            let pCode = data.title;
                            let promoRef = this.getDb().ref('promoCodes');
                            promoRef.once('value', (snapData) => (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
                                if (snapData.val()) {
                                    let codes = snapData.val();
                                    let foundPromo = true;
                                    for (let promokey in codes) {
                                        if (pCode == codes[promokey].promoName) {
                                            foundPromo = false;
                                            if (this.customerPromocode) {
                                                let foundCust_Promo = true;
                                                for (let matchcode in this.customerPromocode) {
                                                    if (pCode == this.customerPromocode[matchcode].promoName) {
                                                        // foundCust_Promo = true;
                                                    }
                                                    else {
                                                        foundCust_Promo = false;
                                                        this.codeFound = codes[promokey];
                                                        if (this.bookingDetails) {
                                                            this.bookingDetails.promo_code = codes[promokey];
                                                        }
                                                        let promo = yield this.alertCtrl.create({
                                                            header: 'Alert',
                                                            subHeader: 'promo Code Added Successfully.',
                                                            buttons: ['OK'],
                                                        });
                                                        promo.present();
                                                    }
                                                    if (foundCust_Promo) {
                                                        let promo1 = yield this.alertCtrl.create({
                                                            header: 'promo1',
                                                            subHeader: 'Promo Code Already Added.',
                                                            buttons: ['OK'],
                                                        });
                                                        promo1.present();
                                                    }
                                                }
                                            }
                                            else {
                                                this.codeFound = codes[promokey];
                                                if (this.bookingDetails) {
                                                    this.bookingDetails.promo_code = codes[promokey];
                                                }
                                                let promo = yield this.alertCtrl.create({
                                                    header: 'Alert',
                                                    subHeader: 'promo Code Added Successfully.',
                                                    buttons: ['OK'],
                                                });
                                                promo.present();
                                            }
                                        }
                                    }
                                    if (foundPromo) {
                                        let nopromocode = yield this.alertCtrl.create({
                                            header: 'Promocode',
                                            subHeader: 'Promo Code not found',
                                            buttons: ['OK'],
                                        });
                                        nopromocode.present();
                                    }
                                }
                                else {
                                    let nopromocode = yield this.alertCtrl.create({
                                        header: 'Promocode',
                                        subHeader: 'Promo Code not found',
                                        buttons: ['OK'],
                                    });
                                    nopromocode.present();
                                }
                            }));
                        },
                    },
                ],
            });
            prompt.present();
        });
    }
    changeEstimate() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            this.count += 1;
            // var totalBill;
            if (this.PickupService) {
                let totalDistance = this.distance / 1000;
                // var pickup_base = '28';
                let billableKms = 0;
                if (totalDistance > parseFloat(this.pickup_base_km)) {
                    billableKms = totalDistance - parseFloat(this.pickup_base_km);
                }
                //let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (parseInt(this.triptime) * parseInt(this.pickup_hour));
                // let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (parseInt(this.triptime) * parseInt(this.pickup_minute));
                this.totalBill =
                    parseFloat(this.pickup_minimum) +
                        billableKms * parseFloat(this.pickup_km) +
                        parseFloat(this.triptime) * parseFloat(this.pickup_minute);
                // if(this.totalBill < parseFloat(this.pickup_minimum)){
                //      this.totalBill = parseFloat(this.pickup_minimum);
                // }
                this.estimate = Math.round(this.totalBill * 100) / 100;
            }
            else {
                this.totalBill =
                    parseFloat(this.personal_minimum) +
                        parseFloat(this.triptime) * parseFloat(this.personal_hour);
                this.estimate = Math.round(this.totalBill * 100) / 100;
            }
            if (this.bookingKey) {
                this.bookingDetails['triptime'] = this.triptime;
                this.bookingDetails.estimate = this.estimate;
            }
            if (isNaN(this.totalBill) && this.count >= 2) {
                let alert = yield this.alertCtrl.create({
                    header: 'Inavlid destination',
                    subHeader: 'Please select a valid drop location',
                    buttons: [
                        {
                            text: 'OK',
                            role: 'cancel',
                            handler: (data) => {
                                this.nav.pop();
                            },
                        },
                    ],
                });
                alert.present();
            }
        });
    }
    //dateTime workflow
    dateShow() {
        // this.sDate = this.showDate.substring(0, 10);
        this.sDate = this.showDate;
        this.calculateDateTime();
    }
    timeShow() {
        // this.sTime = this.showTime.substring(11, 19);
        this.sTime = this.showTime;
        this.calculateDateTime();
    }
    calculateDateTime() {
        //this.originalDate = this.sDate + 'T' + this.sTime + '.000Z';
        this.originalDate = this.showDate + 'T' + this.showTime + '.000Z';
        console.log('calculate datetime =>' + this.originalDate);
        // this.bookingDetails.tripdate = this.originalDate;
        /*    + '-' + pad(now.getMonth()+1)
            + '-' + pad(now.getDate())
            + 'T' + pad(now.getHours())
            + ':' + pad(now.getMinutes())
            + ':' + pad(now.getSeconds())
            + '.000Z'*/
    }
    /* requestDriver(){
      console.log(this.myDate);
      this.confirmBooking();
    }*/
    //open alert
    showAlert(msg) {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            let alert = yield this.alertCtrl.create({
                header: 'ALERT',
                subHeader: msg,
                buttons: [
                    {
                        text: 'OK',
                        role: 'cancel',
                    },
                ],
            });
            alert.present();
        });
    }
    requestDriver() {
        // let selectedTime = new Date(this.showDate +" "+this.showTime);
        let selectedTime = this.showDate + 'T' + this.showTime + '.000Z';
        // let currentTime = new Date();
        console.log(selectedTime);
        let timeNow = this.formatLocalDate('later-com');
        if (Number(new Date(timeNow)) > Number(new Date(selectedTime))) {
            this.showAlert('You cannot book driver for past time.');
            return false;
        }
        // console.log(this.laterDate);
        //let cDate = this.formatLocalDate("later");
        // console.log(afterTwohr <= selectedTime);
        // if (afterTwohr > selectedTime) {
        //   this.showAlert("Please select time after 1 hours from now.");
        //   return false;
        // }
        //return false;
        let fireRef = this.getDb().ref('users/' + this.curUser.uid);
        fireRef.once('value', (snapdetails) => {
            if (snapdetails.val()) {
                if (snapdetails.val().payment_methods) {
                    this.confirmBooking();
                }
                else {
                    let sndmsg = 'Please add a valid credit card';
                    this.showAlert(sndmsg);
                }
            }
        });
    }
    bookingConfirmAlert() {
        return (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__awaiter)(this, void 0, void 0, function* () {
            this.requestDone = true;
            let alert = yield this.alertCtrl.create({
                header: 'ALERT',
                subHeader: 'Your booking is scheduled for ' +
                    this.showTime +
                    '. You will receive a notification when a driver is assigned.',
                buttons: [
                    {
                        text: 'OK',
                        handler: (data) => {
                            this.Notification();
                            // this.navCtrl.setRoot(CurrentbookingsPage);
                        },
                    },
                ],
            });
            alert.present();
        });
    }
    confirmBooking() {
        if (this.codeFound) {
            this.newPromoCode = this.codeFound;
            // this.bookingDetails.promo_code = this.codeFound;
            console.log('promo code found : => ' + this.newPromoCode);
        }
        else {
            this.newPromoCode = null;
            // this.bookingDetails.promo_code = null ;
            console.log('promo code not found : => ' + this.newPromoCode);
        }
        if (this.bookingKey) {
            if (this.originalDate) {
                this.bookingDetails.tripdate = this.originalDate;
            }
            else {
                this.bookingDetails.tripdate = this.bookingDetails.tripdate;
            }
            if (this.bookingDetails.promo_code) {
                this.bookingDetails.promo_code = this.bookingDetails.promo_code;
            }
            if (this.newPromoCode) {
                console.log('edit time call promocode required.........', this.newPromoCode);
                this.bookingDetails.promo_code = this.newPromoCode;
                let ref = this.getDb().ref('/users/' + this.curUser.uid);
                ref.once('value', (snap1) => {
                    console.log('edit time call promocode *********.........');
                    let addpromocode = ref.child('promo_code').push().key;
                    ref.child('promo_code/' + addpromocode).set(this.newPromoCode);
                });
            }
            else {
                this.bookingDetails.promo_code = null;
            }
            let ref = this.getDb().ref('bookings/' + this.bookingKey);
            ref.set(this.bookingDetails);
            let updates = {};
            if (this.bookingDetails.driver) {
                //new created by me.. part2
                if (this.PickupService) {
                    let customerData = {
                        customer: this.bookingDetails.customer,
                        driver: this.bookingDetails.driver,
                        driver_name: this.bookingDetails.driver_name,
                        driver_image: this.bookingDetails.driver_image,
                        status: this.bookingDetails.status,
                        pickupAddress: this.bookingDetails.pickup.add,
                        dropAddress: this.bookingDetails.drop.add,
                        tripdate: this.bookingDetails.tripdate,
                        customer_push_token: this.bookingDetails.customer_push_token,
                        serviceType: this.bookingDetails.serviceType,
                        // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
                    };
                    let driverData = {
                        customer: this.bookingDetails.customer,
                        customer_name: this.bookingDetails.customer_name,
                        customer_image: this.bookingDetails.customer_image,
                        driver: this.bookingDetails.driver,
                        status: this.bookingDetails.status,
                        pickupAddress: this.bookingDetails.pickup.add,
                        dropAddress: this.bookingDetails.drop.add,
                        customer_push_token: this.bookingDetails.customer_push_token,
                        serviceType: this.bookingDetails.serviceType,
                        // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
                    };
                    updates['/users/' +
                        this.bookingDetails.driver +
                        '/bookings/' +
                        this.bookingKey] = driverData;
                    updates['/users/' +
                        this.bookingDetails.customer +
                        '/bookings/' +
                        this.bookingKey] = customerData;
                }
                else {
                    let customerData = {
                        customer: this.bookingDetails.customer,
                        driver: this.bookingDetails.driver,
                        driver_name: this.bookingDetails.driver_name,
                        driver_image: this.bookingDetails.driver_image,
                        status: this.bookingDetails.status,
                        pickupAddress: this.bookingDetails.pickup.add,
                        // dropAddress: this.bookingDetails.drop.add,
                        tripdate: this.bookingDetails.tripdate,
                        customer_push_token: this.bookingDetails.customer_push_token,
                        serviceType: this.bookingDetails.serviceType,
                        // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
                    };
                    let driverData = {
                        customer: this.bookingDetails.customer,
                        customer_name: this.bookingDetails.customer_name,
                        customer_image: this.bookingDetails.customer_image,
                        driver: this.bookingDetails.driver,
                        status: this.bookingDetails.status,
                        //pickupAddress: this.bookingDetails.pickup.add,
                        dropAddress: this.bookingDetails.drop.add,
                        tripdate: this.bookingDetails.tripdate,
                        customer_push_token: this.bookingDetails.customer_push_token,
                        serviceType: this.bookingDetails.serviceType,
                        // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
                    };
                    updates['/users/' +
                        this.bookingDetails.driver +
                        '/bookings/' +
                        this.bookingKey] = driverData;
                    updates['/users/' +
                        this.bookingDetails.customer +
                        '/bookings/' +
                        this.bookingKey] = customerData;
                }
            }
            else {
                if (this.PickupService) {
                    let customerData = {
                        customer: this.bookingDetails.customer,
                        status: this.bookingDetails.status,
                        pickupAddress: this.bookingDetails.pickup.add,
                        dropAddress: this.bookingDetails.drop.add,
                        tripdate: this.bookingDetails.tripdate,
                        tripType: this.trip,
                        customer_push_token: this.bookingDetails.customer_push_token,
                        serviceType: this.bookingDetails.serviceType,
                        // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
                    };
                    updates['/users/' +
                        this.bookingDetails.customer +
                        '/bookings/' +
                        this.bookingKey] = customerData;
                }
                else {
                    let customerData = {
                        customer: this.bookingDetails.customer,
                        status: this.bookingDetails.status,
                        pickupAddress: this.bookingDetails.pickup.add,
                        // dropAddress: this.bookingDetails.drop.add,
                        tripdate: this.bookingDetails.tripdate,
                        tripType: this.trip,
                        customer_push_token: this.bookingDetails.customer_push_token,
                        serviceType: this.bookingDetails.serviceType,
                        // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
                    };
                    updates['/users/' +
                        this.bookingDetails.customer +
                        '/bookings/' +
                        this.bookingKey] = customerData;
                }
            }
            console.log(updates);
            this.getDb().ref().update(updates);
            // this.bookingConfirmAlert();
            this.nav.setRoot('pages/currentbookings');
            // this.navCtrl.setRoot(BidDriverPagePage,{
            //                         from:this.bookingDetails.pickup.add,
            //                         to:this.bookingDetails.drop.add,
            //                         pickupservice:this.bookingDetails.serviceType
            //                     });
        }
        else {
            console.log('first time booking........................', this.newPromoCode);
            let ref = this.getDb().ref('/users/' + this.curUser.uid);
            ref.once('value', (snap1) => {
                var _a, _b;
                if (snap1.val() != null) {
                    this.userProfile = snap1.val();
                    var shortBookingData = {
                        // tripdate:this.myDate,
                        tripdate: this.originalDate,
                        pickupAddress: this.pickup.add,
                        dropAddress: this.drop.add,
                        customer: this.curUser.uid,
                        tripType: this.trip,
                        // status:'NEW' , //added by Sach23
                        status: 'Finding your driver',
                        customer_push_token: (_a = this.userProfile.pushToken) !== null && _a !== void 0 ? _a : '',
                        serviceType: this.serviceType,
                        // promo_code:this.newPromoCode   //new added for promo code
                    };
                    var bookingData = {
                        customer: this.curUser.uid,
                        customer_name: this.userProfile.fullname,
                        customer_push_token: (_b = this.userProfile.pushToken) !== null && _b !== void 0 ? _b : '',
                        pickup: this.pickup,
                        drop: this.drop,
                        distance: this.distance,
                        // tripdate:this.myDate,
                        tripdate: this.originalDate,
                        triptime: this.triptime,
                        transmission: this.transmission,
                        estimate: this.estimate,
                        tripType: this.trip,
                        // status:'NEW' , //added by Sach23
                        status: 'Finding your driver',
                        serviceType: this.serviceType,
                        promo_code: this.newPromoCode, //new added for promo code
                    };
                    if (this.userProfile.profile_image)
                        bookingData['customer_image'] = this.userProfile.profile_image;
                    let newBooking = ref.child('bookings').push().key;
                    let addpromocode = ref.child('promo_code').push().key;
                    console.log('bookingData', bookingData);
                    this.getDb()
                        .ref('bookings/' + newBooking)
                        .set(bookingData);
                    ref.child('bookings/' + newBooking).set(shortBookingData);
                    ref.child('promo_code/' + addpromocode).set(this.newPromoCode);
                    //promo code
                    /*  if(this.newPromoCode!=undefined || this.newPromoCode!=null){
                          firebase.database().ref('bookings/' + newBooking).child('/promo_code').set(this.newPromoCode);
                      }
                    */
                    //end
                    var dbref = this.getDb().ref();
                    var geoFire = new geofire__WEBPACK_IMPORTED_MODULE_5__.GeoFire(dbref.child('geofire'));
                    geoFire.set(newBooking, [this.pickup.lat, this.pickup.lng]).then(function () {
                        console.log('Provided key has been added to GeoFire');
                    }, function (error) {
                        console.log('Error: ' + error);
                    });
                    this.bookingConfirmAlert();
                    // this.navCtrl.setRoot(CurrentbookingsPage);
                }
            });
        }
    }
    tapEvent(e) {
        console.log(this.showDate);
        // this.dateShow();
    }
    tapEventT(e) {
        console.log(this.showTime);
        // this.timeShow();
    }
    date() {
        console.log('my date format is' + this.myDate);
    }
    ionViewWillEnter() {
        this.allDriver = [];
        this.notificationData = [];
        console.log('ionViewDidLoad FarePage');
        /*------------------------------ Push Token Save -----------------------------------*/
        let refToken = this.getDb().ref('users');
        refToken.on('value', (snapshot) => {
            this.allDriver = snapshot.val();
            this.notificationData = [];
            for (let key in this.allDriver) {
                if (this.allDriver[key].islogin == 'driver' &&
                    this.allDriver[key].active == true &&
                    this.allDriver[key].pushToken) {
                    // if(this.allDriver[key].islogin == 'driver' && this.allDriver[key].pushToken && this.allDriver[key].email == 'bhavesh9562@gmail.com' ){
                    this.notificationData.push({ token: this.allDriver[key].pushToken });
                    console.log('ionViewDidLoad notificationData...............', this.notificationData);
                }
            }
        });
        /*------------------------------ Push Token Save -----------------------------------*/
    }
    // NotificationDriver(){
    //     let alert = this.alertCtrl.create({
    //       title: 'ALERT',
    //       subTitle: 'You will receive a notification when a driver is assigned.',
    //       buttons: [
    //        {
    //           text: 'OK',
    //           handler: data => {
    //             this.Notification();
    //           }
    //         }
    //         ]
    //     });
    //     alert.present();
    // }
    Notification() {
        // console.log("notification...................", this.notificationData);
        var cnt = 0;
        for (let key in this.notificationData) {
            cnt++;
            this.usersService
                .sendPushDriver('Customer Booking', this.pickup.add, this.notificationData[key].token)
                .subscribe((response) => {
                console.log('response........', response);
                if (this.notificationData.length == cnt) {
                    this.notificationData = [];
                    cnt = 0;
                    // this.navCtrl.setRoot(BidDriverPagePage,{
                    //                       from:this.pickup.add,
                    //                       to:this.drop.add,
                    //                       pickupservice:this.serviceType
                    //                   });
                }
            });
        }
        this.nav.setRoot('pages/currentbookings');
    }
    getDb() {
        return this.firebaseService.getDatabase();
    }
    getParams() {
        let data = this.dataService.farePageData;
        console.log('getParams', data);
        return data;
    }
};
FarePage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_8__.Injector },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.Platform },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_9__.AlertController },
    { type: src_app_Services_data_service__WEBPACK_IMPORTED_MODULE_4__.DataService }
];
FarePage = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-fare',
        template: _raw_loader_fare_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_fare_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], FarePage);



/***/ }),

/***/ 64993:
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJoZWFkZXIuY29tcG9uZW50LnNjc3MifQ== */");

/***/ }),

/***/ 42778:
/*!*******************************************!*\
  !*** ./src/app/pages/fare/fare.page.scss ***!
  \*******************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("@charset \"UTF-8\";\npage-activebooking #map2 {\n  height: 100%;\n}\n.rightmenu {\n  color: #fff;\n  background: none !important;\n  padding: 0 !important;\n}\n.sebm-google-map-container {\n  width: 100%;\n  height: 100%;\n  opacity: 0;\n  transition: opacity 150ms ease-in;\n  position: relative;\n}\n.gmap {\n  width: 100%;\n  position: relative;\n}\n.gmap-button-wrapper {\n  position: absolute;\n  width: 100%;\n  display: block;\n  height: auto;\n}\n.cusb {\n  position: absolute;\n  top: 0;\n  left: 0;\n  width: 20%;\n  height: 40px;\n}\n.darkbutton1 {\n  width: 100%;\n  padding: 22px 0;\n  font-family: \"PT Sans\";\n  font-size: 18px;\n  text-transform: capitalize;\n  font-weight: normal;\n}\n.button-md-dark.activated,\n.button-md.activated {\n  background: #222 !important;\n}\n.button-md:hover:not(.disable-hover) {\n  background: #222 !important;\n}\n.mapcontainer {\n  width: 100%;\n  height: 100%;\n  position: absolute;\n  top: 0;\n}\n.topnotification-wrapper {\n  width: 100%;\n  position: absolute;\n  top: 0;\n  overflow: hidden;\n}\n.mapbutton-wrapper {\n  width: 80%;\n  margin: auto;\n  position: absolute;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.topnotification, .topnotification-fare {\n  width: 100%;\n  background: #fff;\n  margin-bottom: 2px;\n  padding: 0px 2px;\n}\n.subbluetext-map {\n  font-family: \"PT Sans\";\n  font-size: 14px;\n  text-align: left;\n  color: #2b2b2b;\n  margin: 0;\n  padding-bottom: 8px;\n}\n.bluetext-map {\n  color: #222;\n  margin-left: 0;\n  margin-right: 0;\n  margin-top: 0;\n  margin-bottom: 2px;\n  font-size: 14px;\n  line-height: normal;\n  text-overflow: inherit;\n  overflow: inherit;\n}\n.marker img {\n  display: block;\n  margin: 0 auto;\n  padding-top: 5px;\n}\n.driver-snippet {\n  position: fixed;\n  bottom: 0px;\n  width: 100%;\n  background-color: #fff;\n}\n.nopadding-avataar .item-md ion-avatar[item-left] {\n  margin: 8px 16px 8px 6px;\n}\nion-col {\n  flex: 1 0 100px !important;\n}\n.driver-snippet .item-md {\n  padding: 0 !important;\n}\n.driver-snippet .item-md ion-avatar[item-left] {\n  margin: 8px 16px 8px 12px;\n}\n.wrapper-driversnippet .label-md {\n  margin: 0 !important;\n}\n.no-border {\n  border: none !important;\n}\n.padding-status {\n  padding-top: 6.5%;\n}\n.no-margin {\n  margin: 0 !important;\n}\n​.customer-icons {\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: center;\n  color: #2b2b2b;\n}\n.customer-icons img {\n  display: block;\n  margin: 0 auto;\n  padding-bottom: 2px;\n}\n​ .wrapper-driversnippet {\n  width: 95%;\n  margin: 0 auto;\n}\n.onlinetoppadding {\n  padding-top: 8px;\n  padding-left: 80px;\n}\n.sidepadding-status {\n  padding-left: 3px;\n}\n.item-md {\n  padding: 0 0 0 22px !important;\n}\n.no-padding {\n  padding: 0;\n}\n.no-padding-bottom {\n  padding-bottom: 0;\n}\n.no-padding-top {\n  padding-top: 0;\n}\n.centertext {\n  text-align: center;\n}\n.paddingright {\n  padding-right: 10px;\n}\n@media screen and (orientation: landscape) {\n  .onlinetoppadding {\n    padding-left: 69%;\n  }\n}\n@media (min-width: 1024px) {\n  .darkbutton1 {\n    padding: 30px 0;\n  }\n}\nion-app._gmaps_cdv_ .nav-decor {\n  background-color: transparent !important;\n}\n.heavy {\n  font-weight: bold;\n}\n.bannerterm {\n  width: 100%;\n  padding-top: 180px;\n  background: url(\"/assets/images/term.png\") no-repeat;\n  position: relative;\n  background-size: cover;\n}\n.aboutlogo {\n  position: absolute;\n  display: block;\n  width: auto;\n  margin: auto;\n  top: 72%;\n  left: 0;\n  right: 0;\n}\n.headerabout {\n  color: #2e2e2e;\n  font-size: 18px;\n  text-align: center;\n  font-family: \"PT Sans\";\n  padding-top: 1%;\n  font-weight: bold;\n}\n.blueline {\n  display: block !important;\n  margin: 0 auto !important;\n  width: auto !important;\n  border-radius: 0 !important;\n}\n.aboutpara {\n  font-family: \"PT Sans\";\n  color: #919191;\n  font-size: 16px;\n  text-align: left;\n  line-height: 18px;\n  padding: 12px 20px 0 20px;\n  margin: 0px;\n}\n.aboutpara span {\n  color: #222;\n}\n.ashrectangle, .ashrectangle-fare {\n  width: 90%;\n  margin: 12px auto;\n  background: #f4f4f4;\n  border-radius: 10px;\n  border: 1px solid #d4d4d4;\n  padding: 12px;\n  color: #2e2e2e;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 19px;\n  font-weight: bold;\n}\n.liststyle {\n  width: 90%;\n  margin: 0 auto;\n  margin-top: 15px;\n}\n.liststyle ul {\n  margin: 0px;\n  padding: 0px;\n}\n.liststyle ul li {\n  list-style: none;\n  font-family: \"PT Sans\";\n  font-size: 16px;\n  text-align: left;\n  line-height: 20px;\n  color: #919191;\n  padding-left: 9%;\n  background: url(\"/assets/images/carbullet.png\") no-repeat;\n  margin-bottom: 20px;\n}\n.font {\n  font-weight: bold;\n  color: #737373;\n}\n.acceptBtn {\n  position: fixed;\n  bottom: 0;\n  left: 0;\n  right: 0;\n}\n.btnaccept {\n  background: #09738d !important;\n  color: white;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n.btnaccept:hover {\n  background: white !important;\n  color: black;\n  margin-left: 140px;\n  border-radius: 50px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n}\n@media (min-width: 568px) {\n  .headerabout {\n    padding-top: 7%;\n    font-weight: bold;\n  }\n}\n.topnotification-fare {\n  background: #e8e8e8 !important;\n  padding: 4px 0 !important;\n}\n.cartypedate {\n  width: 80%;\n  overflow: hidden;\n  margin: 0 auto;\n}\n.calendar {\n  font-size: 30px;\n  text-align: center;\n  margin-left: 20px;\n}\n.farebutton {\n  color: #fff;\n  font-size: 12px;\n  padding: 2% 0;\n  background: #222;\n  border-radius: 10px;\n}\n/*.cartypedate-left{float: left; background: #e8e8e8;}\n.cartypedate-left span{width: auto; display: block; position: absolute; top: 30%; left: 20%; background: url('../assets/images/calendar.png') no-repeat; line-height: 21px; font-family: 'PT Sans'; color: #2b2b2b; font-size: 14px; padding-left: 28px; font-weight: bold;}*/\n/*.cartypedate-right{float: right; width: 44.5%; background: #e8e8e8; position: relative;}\n.cartypedate-right span{width: auto; display: block; position: absolute; top: 30%; left: 20%; background: url('../assets/images/car1.png') no-repeat; line-height: 21px; font-family: 'PT Sans'; color: #2b2b2b; font-size: 14px; padding-left: 28px; font-weight: bold;}*/\n.fareestimate, .currency {\n  font-family: \"PT Sans\";\n  color: #2b2b2b;\n  text-align: center;\n  font-size: 20px;\n  padding: 5px 0 0 0;\n  margin: 0;\n  font-weight: bold;\n}\n.currency {\n  color: #222;\n  font-size: 35px;\n  padding: 0 0 0px 0;\n}\n.triptime, .timeslider {\n  font-family: \"PT Sans\";\n  font-size: 17px;\n  text-align: center;\n  color: #2b2b2b;\n  margin: 0;\n}\n.timeslider {\n  font-size: 22px;\n  padding: 10px 0;\n  font-weight: bold;\n}\n.farebutton-wrapper {\n  width: 90%;\n  margin: 0 auto;\n}\n.no-padding-top {\n  padding-top: 0;\n}\n.no-padding-bottom {\n  padding-bottom: 0;\n}\n.ashrectangle-fare {\n  margin: 4px auto !important;\n  width: 80% !important;\n}\n.datetime-md {\n  font-size: 14px;\n  padding: 0;\n  background: none !important;\n}\n.item-cover {\n  width: 0;\n  height: 0;\n}\n.select-md {\n  padding: 0;\n}\n.item-md.item-block .item-inner {\n  border-bottom: none;\n}\n.ashfare {\n  margin: 4px auto !important;\n  width: 90%;\n  background: #f4f4f4;\n  padding: 5px;\n  border-radius: 10px;\n}\n.ashfare .list-md {\n  margin: 0 !important;\n}\n.ashfare .item-md {\n  padding: 0 0 0 8px !important;\n}\n.ashfare .list-ios {\n  margin: 0 !important;\n}\n.ashfare .item-ios {\n  padding: 0 0 0 8px !important;\n}\n.center-allign {\n  text-align: center;\n  padding: 1%;\n  background: #e8e8e8;\n  font-size: 13px;\n  margin: 2px;\n}\n.ashrectangle-fare .list-md {\n  margin: 0 !important;\n}\n.ashrectangle-fare .item-md {\n  padding-left: 0 !important;\n}\n.select-text {\n  font-size: 14px !important;\n}\n.shortwrapper {\n  width: 36% !important;\n  margin: 0 auto;\n}\nion-select {\n  max-width: 100%;\n  font-weight: bold;\n}\n.select-md .select-icon {\n  width: 22px;\n}\n.custom-outline {\n  border-radius: 10px;\n  width: 100%;\n  background: none;\n  border: 1px solid #222;\n  color: #2b2b2b;\n  box-shadow: none;\n}\n.custom-col {\n  width: 100%;\n}\n.custom-col ion-col {\n  flex: 1 0 100px;\n  min-width: 100px;\n}\n.custom-col .button-ios {\n  padding: 0.3rem;\n}\n.custom-icon-padding [icon-left] ion-icon {\n  padding-left: 0.5em;\n}\n.rqstDvr {\n  color: #fff;\n  background: #3b5998;\n}\n.textCenter {\n  text-align: center;\n}\n.smallFont {\n  font-size: 1.4rem !important;\n}\n.padding-btm {\n  padding-bottom: 0px !important;\n}\n.fareCalBtn {\n  position: relative;\n}\n.fareCalBtn ion-icon {\n  position: absolute;\n}\n.fareCalBtn ion-datetime {\n  width: 100%;\n  position: relative;\n}\n.fareCalBtn .fareDateIcon {\n  left: 21%;\n  top: 25%;\n}\n.fareCalBtn .fareTimeIcon {\n  left: 21%;\n  top: 27%;\n}\n.note {\n  margin-left: 14px;\n  margin-bottom: 0px;\n  font: italic bold 11px PT, Sans;\n}\n@media screen and (orientation: landscape) {\n  .shortwrapper {\n    width: 24% !important;\n  }\n\n  .fareCalBtn .fareDateIcon {\n    left: 25% !important;\n  }\n  .fareCalBtn .fareTimeIcon {\n    left: 32% !important;\n  }\n}\n@media (min-width: 320px) {\n  .cartypedate-left span {\n    left: 15%;\n  }\n\n  .cartypedate-right span {\n    left: 7%;\n  }\n\n  [icon-left] ion-icon {\n    padding-right: 0.5em;\n  }\n\n  .fareCalBtn .fareDateIcon {\n    left: 1%;\n  }\n  .fareCalBtn .fareTimeIcon {\n    left: 17%;\n  }\n}\n@media (min-width: 360px) {\n  .cartypedate-left span {\n    left: 8%;\n  }\n\n  .cartypedate-right span {\n    left: 11%;\n  }\n\n  .fareCalBtn .fareDateIcon {\n    left: 4%;\n  }\n  .fareCalBtn .fareTimeIcon {\n    left: 18%;\n  }\n}\n@media (min-width: 412px) {\n  .cartypedate-left span {\n    left: 23%;\n  }\n\n  .cartypedate-right span {\n    left: 16%;\n  }\n\n  .fareCalBtn .fareDateIcon {\n    left: 20%;\n  }\n  .fareCalBtn .fareTimeIcon {\n    left: 30%;\n  }\n}\n@media (min-width: 568px) {\n  .cartypedate-left span {\n    left: 30%;\n  }\n\n  .cartypedate-right span {\n    left: 30%;\n  }\n}\n@media (min-width: 768px) {\n  .fareCalBtn .fareDateIcon {\n    left: 36%;\n  }\n  .fareCalBtn .fareTimeIcon {\n    left: 39%;\n  }\n}\n@media (min-width: 1024px) {\n  .cartypedate-left span {\n    left: 40%;\n  }\n\n  .cartypedate-right span {\n    left: 40%;\n  }\n}\n.time {\n  font-size: 30px;\n  text-align: center;\n  margin-left: 20px;\n}\n.dateset {\n  font-size: 16px !important;\n  margin-left: -10px;\n}\n.dateicon {\n  font-size: 18px !important;\n}\n.map3 {\n  height: 250px;\n}\n.map4 {\n  height: 387px;\n}\n.button-trans {\n  background: rgba(224, 226, 226, 0.767);\n  color: #000;\n  width: 88%;\n  border: 1px solid #dedddd;\n  padding: 10px 15px;\n  font-size: 16px;\n  border-radius: 20px;\n  box-shadow: none !important;\n  font-weight: 400 !important;\n  box-shadow: #c7c7c7;\n}\n.apprx-dis {\n  font-size: 1.4rem !important;\n  margin: 0px;\n  margin-top: 12px;\n}\n.apprx-dis-p {\n  margin: 10px;\n}\n.datetime-ios {\n  padding: 11px 0px 11px 5px !important;\n}\n.darkbutton1 {\n  background: #09738d !important;\n  border-radius: 60px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImZhcmUucGFnZS5zY3NzIiwiLi5cXGFjdGl2ZWJvb2tpbmdcXGFjdGl2ZWJvb2tpbmcucGFnZS5zY3NzIiwiLi5cXHRlcm1zXFx0ZXJtcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQ2Q7RUFDRSxZQUFBO0FEQ0o7QUNHQTtFQUNFLFdBQUE7RUFDQSwyQkFBQTtFQUNBLHFCQUFBO0FEQUY7QUNHQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0EsVUFBQTtFQUNBLGlDQUFBO0VBQ0Esa0JBQUE7QURBRjtBQ0dBO0VBQ0UsV0FBQTtFQUNBLGtCQUFBO0FEQUY7QUNFQTtFQUNFLGtCQUFBO0VBQ0EsV0FBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0FEQ0Y7QUNDQTtFQUNFLGtCQUFBO0VBQ0EsTUFBQTtFQUNBLE9BQUE7RUFDQSxVQUFBO0VBQ0EsWUFBQTtBREVGO0FDQUE7RUFDRSxXQUFBO0VBQ0EsZUFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLDBCQUFBO0VBQ0EsbUJBQUE7QURHRjtBQ0RBOztFQUVFLDJCQUFBO0FESUY7QUNGQTtFQUNFLDJCQUFBO0FES0Y7QUNGQTtFQUNFLFdBQUE7RUFDQSxZQUFBO0VBQ0Esa0JBQUE7RUFFQSxNQUFBO0FESUY7QUNGQTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxnQkFBQTtBREtGO0FDSEE7RUFDRSxVQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsU0FBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0FETUY7QUNKQTtFQUNFLFdBQUE7RUFDQSxnQkFBQTtFQUNBLGtCQUFBO0VBQ0EsZ0JBQUE7QURPRjtBQ0xBO0VBQ0Usc0JBQUE7RUFDQSxlQUFBO0VBQ0EsZ0JBQUE7RUFDQSxjQUFBO0VBQ0EsU0FBQTtFQUNBLG1CQUFBO0FEUUY7QUNOQTtFQUdFLFdBQUE7RUFHQSxjQUFBO0VBQ0UsZUFBQTtFQUNBLGFBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLHNCQUFBO0VBQ0EsaUJBQUE7QURLSjtBQ0hBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxnQkFBQTtBRE1GO0FDSEE7RUFDRSxlQUFBO0VBQ0EsV0FBQTtFQUNBLFdBQUE7RUFDQSxzQkFBQTtBRE1GO0FDSEE7RUFDRSx3QkFBQTtBRE1GO0FDSkE7RUFDRSwwQkFBQTtBRE9GO0FDTEE7RUFDRSxxQkFBQTtBRFFGO0FDTkE7RUFDRSx5QkFBQTtBRFNGO0FDUEE7RUFDRSxvQkFBQTtBRFVGO0FDUEE7RUFDRSx1QkFBQTtBRFVGO0FDUkE7RUFDRSxpQkFBQTtBRFdGO0FDVEE7RUFDRSxvQkFBQTtBRFlGO0FDVEE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7QURZRjtBQ1ZBO0VBQ0UsY0FBQTtFQUNBLGNBQUE7RUFDQSxtQkFBQTtBRGFGO0FDWEE7RUFDRSxVQUFBO0VBQ0EsY0FBQTtBRGNGO0FDWkE7RUFDRSxnQkFBQTtFQUNBLGtCQUFBO0FEZUY7QUNiQTtFQUNFLGlCQUFBO0FEZ0JGO0FDZEE7RUFDRSw4QkFBQTtBRGlCRjtBQ2ZBO0VBQ0UsVUFBQTtBRGtCRjtBQ2hCQTtFQUNFLGlCQUFBO0FEbUJGO0FDakJBO0VBQ0UsY0FBQTtBRG9CRjtBQ2xCQTtFQUNFLGtCQUFBO0FEcUJGO0FDbkJBO0VBQ0UsbUJBQUE7QURzQkY7QUNuQkE7RUFDRTtJQUNFLGlCQUFBO0VEc0JGO0FBQ0Y7QUNuQkE7RUFDRTtJQUNFLGVBQUE7RURxQkY7QUFDRjtBQ2xCQTtFQUNFLHdDQUFBO0FEb0JGO0FFeE5BO0VBQ0UsaUJBQUE7QUYyTkY7QUV6TkE7RUFDRSxXQUFBO0VBQ0Esa0JBQUE7RUFDQSxvREFBQTtFQUNBLGtCQUFBO0VBQ0Esc0JBQUE7QUY0TkY7QUUxTkE7RUFDRSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFFBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBRjZORjtBRTNOQTtFQUNFLGNBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBRjhORjtBRTVOQTtFQUNFLHlCQUFBO0VBQ0EseUJBQUE7RUFDQSxzQkFBQTtFQUNBLDJCQUFBO0FGK05GO0FFN05BO0VBQ0Usc0JBQUE7RUFDQSxjQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSx5QkFBQTtFQUNBLFdBQUE7QUZnT0Y7QUU5TkE7RUFDRSxXQUFBO0FGaU9GO0FFL05BO0VBQ0UsVUFBQTtFQUNBLGlCQUFBO0VBQ0EsbUJBQUE7RUFDQSxtQkFBQTtFQUNBLHlCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxnQkFBQTtFQUNBLGlCQUFBO0VBQ0EsaUJBQUE7QUZrT0Y7QUVoT0E7RUFDRSxVQUFBO0VBQ0EsY0FBQTtFQUNBLGdCQUFBO0FGbU9GO0FFak9BO0VBQ0UsV0FBQTtFQUNBLFlBQUE7QUZvT0Y7QUVsT0E7RUFDRSxnQkFBQTtFQUNBLHNCQUFBO0VBQ0EsZUFBQTtFQUNBLGdCQUFBO0VBQ0EsaUJBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7RUFDQSx5REFBQTtFQUNBLG1CQUFBO0FGcU9GO0FFbk9BO0VBQ0UsaUJBQUE7RUFDQSxjQUFBO0FGc09GO0FFcE9BO0VBQ0UsZUFBQTtFQUNBLFNBQUE7RUFDQSxPQUFBO0VBQ0EsUUFBQTtBRnVPRjtBRXJPQTtFQUNFLDhCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBRndPRjtBRXRPQTtFQUNFLDRCQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtBRnlPRjtBRXRPQTtFQUNFO0lBQ0UsZUFBQTtJQUNBLGlCQUFBO0VGeU9GO0FBQ0Y7QUF0VkE7RUFFRSw4QkFBQTtFQUNBLHlCQUFBO0FBdVZGO0FBcFZBO0VBQ0UsVUFBQTtFQUNBLGdCQUFBO0VBQ0EsY0FBQTtBQXVWRjtBQXJWQTtFQUNFLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGlCQUFBO0FBd1ZGO0FBdFZBO0VBQ0UsV0FBQTtFQUNBLGVBQUE7RUFDQSxhQUFBO0VBQ0EsZ0JBQUE7RUFDQSxtQkFBQTtBQXlWRjtBQXRWQTs2UUFBQTtBQUdBOzBRQUFBO0FBR0E7RUFDRSxzQkFBQTtFQUNBLGNBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLFNBQUE7RUFDQSxpQkFBQTtBQXVWRjtBQXJWQTtFQUVFLFdBQUE7RUFDQSxlQUFBO0VBQ0Esa0JBQUE7QUF1VkY7QUFuVkE7RUFDRSxzQkFBQTtFQUNBLGVBQUE7RUFDQSxrQkFBQTtFQUNBLGNBQUE7RUFDQSxTQUFBO0FBc1ZGO0FBcFZBO0VBRUUsZUFBQTtFQUNBLGVBQUE7RUFDQSxpQkFBQTtBQXNWRjtBQW5WQTtFQUNFLFVBQUE7RUFDQSxjQUFBO0FBc1ZGO0FBcFZBO0VBQ0UsY0FBQTtBQXVWRjtBQXJWQTtFQUNFLGlCQUFBO0FBd1ZGO0FBdFZBO0VBRUUsMkJBQUE7RUFDQSxxQkFBQTtBQXdWRjtBQXRWQTtFQUNFLGVBQUE7RUFDQSxVQUFBO0VBQ0EsMkJBQUE7QUF5VkY7QUF2VkE7RUFDRSxRQUFBO0VBQ0EsU0FBQTtBQTBWRjtBQXZWQTtFQUNFLFVBQUE7QUEwVkY7QUF4VkE7RUFDRSxtQkFBQTtBQTJWRjtBQXhWQTtFQUNFLDJCQUFBO0VBQ0EsVUFBQTtFQUNBLG1CQUFBO0VBQ0EsWUFBQTtFQUNBLG1CQUFBO0FBMlZGO0FBelZBO0VBQ0Usb0JBQUE7QUE0VkY7QUExVkE7RUFDRSw2QkFBQTtBQTZWRjtBQTNWQTtFQUNFLG9CQUFBO0FBOFZGO0FBNVZBO0VBQ0UsNkJBQUE7QUErVkY7QUE3VkE7RUFDRSxrQkFBQTtFQUNBLFdBQUE7RUFDQSxtQkFBQTtFQUNBLGVBQUE7RUFDQSxXQUFBO0FBZ1dGO0FBN1ZBO0VBQ0Usb0JBQUE7QUFnV0Y7QUE5VkE7RUFDRSwwQkFBQTtBQWlXRjtBQTlWQTtFQUNFLDBCQUFBO0FBaVdGO0FBL1ZBO0VBQ0UscUJBQUE7RUFDQSxjQUFBO0FBa1dGO0FBaFdBO0VBQ0UsZUFBQTtFQUNBLGlCQUFBO0FBbVdGO0FBaldBO0VBQ0UsV0FBQTtBQW9XRjtBQWpXQTtFQUNFLG1CQUFBO0VBQ0EsV0FBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0EsZ0JBQUE7QUFvV0Y7QUFqV0E7RUFDRSxXQUFBO0FBb1dGO0FBbFdBO0VBQ0UsZUFBQTtFQUNBLGdCQUFBO0FBcVdGO0FBbFdBO0VBQ0UsZUFBQTtBQXFXRjtBQW5XQTtFQUNFLG1CQUFBO0FBc1dGO0FBbldBO0VBQ0UsV0FBQTtFQUNBLG1CQUFBO0FBc1dGO0FBcFdBO0VBQ0Usa0JBQUE7QUF1V0Y7QUFwV0E7RUFDRSw0QkFBQTtBQXVXRjtBQXJXQTtFQUNFLDhCQUFBO0FBd1dGO0FBcldBO0VBQ0Usa0JBQUE7QUF3V0Y7QUF2V0U7RUFDRSxrQkFBQTtBQXlXSjtBQXRXRTtFQUNFLFdBQUE7RUFDQSxrQkFBQTtBQXdXSjtBQXRXRTtFQUNFLFNBQUE7RUFDQSxRQUFBO0FBd1dKO0FBdFdFO0VBQ0UsU0FBQTtFQUNBLFFBQUE7QUF3V0o7QUFsV0E7RUFDRSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EsK0JBQUE7QUFxV0Y7QUFsV0E7RUFDRTtJQUNFLHFCQUFBO0VBcVdGOztFQWpXRTtJQUNFLG9CQUFBO0VBb1dKO0VBbFdFO0lBQ0Usb0JBQUE7RUFvV0o7QUFDRjtBQWhXQTtFQUNFO0lBQ0UsU0FBQTtFQWtXRjs7RUFoV0E7SUFDRSxRQUFBO0VBbVdGOztFQWpXQTtJQUNFLG9CQUFBO0VBb1dGOztFQWhXRTtJQUNFLFFBQUE7RUFtV0o7RUFqV0U7SUFDRSxTQUFBO0VBbVdKO0FBQ0Y7QUEvVkE7RUFDRTtJQUNFLFFBQUE7RUFpV0Y7O0VBL1ZBO0lBQ0UsU0FBQTtFQWtXRjs7RUE5VkU7SUFDRSxRQUFBO0VBaVdKO0VBL1ZFO0lBQ0UsU0FBQTtFQWlXSjtBQUNGO0FBN1ZBO0VBQ0U7SUFDRSxTQUFBO0VBK1ZGOztFQTdWQTtJQUNFLFNBQUE7RUFnV0Y7O0VBNVZFO0lBQ0UsU0FBQTtFQStWSjtFQTdWRTtJQUNFLFNBQUE7RUErVko7QUFDRjtBQTNWQTtFQUNFO0lBQ0UsU0FBQTtFQTZWRjs7RUEzVkE7SUFDRSxTQUFBO0VBOFZGO0FBQ0Y7QUEzVkE7RUFFSTtJQUNFLFNBQUE7RUE0Vko7RUExVkU7SUFDRSxTQUFBO0VBNFZKO0FBQ0Y7QUF4VkE7RUFDRTtJQUNFLFNBQUE7RUEwVkY7O0VBeFZBO0lBQ0UsU0FBQTtFQTJWRjtBQUNGO0FBelZBO0VBQ0UsZUFBQTtFQUNBLGtCQUFBO0VBQ0EsaUJBQUE7QUEyVkY7QUF6VkE7RUFDRSwwQkFBQTtFQUNBLGtCQUFBO0FBNFZGO0FBMVZBO0VBQ0UsMEJBQUE7QUE2VkY7QUExVkE7RUFDRSxhQUFBO0FBNlZGO0FBM1ZBO0VBQ0UsYUFBQTtBQThWRjtBQTNWQTtFQUNFLHNDQUFBO0VBQ0EsV0FBQTtFQUNBLFVBQUE7RUFDQSx5QkFBQTtFQUNBLGtCQUFBO0VBQ0EsZUFBQTtFQUNBLG1CQUFBO0VBQ0EsMkJBQUE7RUFDQSwyQkFBQTtFQUNBLG1CQUFBO0FBOFZGO0FBM1ZBO0VBQ0UsNEJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUE4VkY7QUE1VkE7RUFDRSxZQUFBO0FBK1ZGO0FBNVZBO0VBQ0UscUNBQUE7QUErVkY7QUE3VkE7RUFDRSw4QkFBQTtFQUNBLG1CQUFBO0FBZ1dGIiwiZmlsZSI6ImZhcmUucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xucGFnZS1hY3RpdmVib29raW5nICNtYXAyIHtcbiAgaGVpZ2h0OiAxMDAlO1xufVxuXG4ucmlnaHRtZW51IHtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDtcbiAgcGFkZGluZzogMCAhaW1wb3J0YW50O1xufVxuXG4uc2VibS1nb29nbGUtbWFwLWNvbnRhaW5lciB7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIG9wYWNpdHk6IDA7XG4gIHRyYW5zaXRpb246IG9wYWNpdHkgMTUwbXMgZWFzZS1pbjtcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xufVxuXG4uZ21hcCB7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogcmVsYXRpdmU7XG59XG5cbi5nbWFwLWJ1dHRvbi13cmFwcGVyIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB3aWR0aDogMTAwJTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIGhlaWdodDogYXV0bztcbn1cblxuLmN1c2Ige1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgbGVmdDogMDtcbiAgd2lkdGg6IDIwJTtcbiAgaGVpZ2h0OiA0MHB4O1xufVxuXG4uZGFya2J1dHRvbjEge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZzogMjJweCAwO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC10cmFuc2Zvcm06IGNhcGl0YWxpemU7XG4gIGZvbnQtd2VpZ2h0OiBub3JtYWw7XG59XG5cbi5idXR0b24tbWQtZGFyay5hY3RpdmF0ZWQsXG4uYnV0dG9uLW1kLmFjdGl2YXRlZCB7XG4gIGJhY2tncm91bmQ6ICMyMjIgIWltcG9ydGFudDtcbn1cblxuLmJ1dHRvbi1tZDpob3Zlcjpub3QoLmRpc2FibGUtaG92ZXIpIHtcbiAgYmFja2dyb3VuZDogIzIyMiAhaW1wb3J0YW50O1xufVxuXG4ubWFwY29udGFpbmVyIHtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMTAwJTtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG59XG5cbi50b3Bub3RpZmljYXRpb24td3JhcHBlciB7XG4gIHdpZHRoOiAxMDAlO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIHRvcDogMDtcbiAgb3ZlcmZsb3c6IGhpZGRlbjtcbn1cblxuLm1hcGJ1dHRvbi13cmFwcGVyIHtcbiAgd2lkdGg6IDgwJTtcbiAgbWFyZ2luOiBhdXRvO1xuICBwb3NpdGlvbjogYWJzb2x1dGU7XG4gIGJvdHRvbTogMDtcbiAgbGVmdDogMDtcbiAgcmlnaHQ6IDA7XG59XG5cbi50b3Bub3RpZmljYXRpb24sIC50b3Bub3RpZmljYXRpb24tZmFyZSB7XG4gIHdpZHRoOiAxMDAlO1xuICBiYWNrZ3JvdW5kOiAjZmZmO1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG4gIHBhZGRpbmc6IDBweCAycHg7XG59XG5cbi5zdWJibHVldGV4dC1tYXAge1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgY29sb3I6ICMyYjJiMmI7XG4gIG1hcmdpbjogMDtcbiAgcGFkZGluZy1ib3R0b206IDhweDtcbn1cblxuLmJsdWV0ZXh0LW1hcCB7XG4gIGNvbG9yOiAjMjIyO1xuICBtYXJnaW4tbGVmdDogMDtcbiAgbWFyZ2luLXJpZ2h0OiAwO1xuICBtYXJnaW4tdG9wOiAwO1xuICBtYXJnaW4tYm90dG9tOiAycHg7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgbGluZS1oZWlnaHQ6IG5vcm1hbDtcbiAgdGV4dC1vdmVyZmxvdzogaW5oZXJpdDtcbiAgb3ZlcmZsb3c6IGluaGVyaXQ7XG59XG5cbi5tYXJrZXIgaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nLXRvcDogNXB4O1xufVxuXG4uZHJpdmVyLXNuaXBwZXQge1xuICBwb3NpdGlvbjogZml4ZWQ7XG4gIGJvdHRvbTogMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmZjtcbn1cblxuLm5vcGFkZGluZy1hdmF0YWFyIC5pdGVtLW1kIGlvbi1hdmF0YXJbaXRlbS1sZWZ0XSB7XG4gIG1hcmdpbjogOHB4IDE2cHggOHB4IDZweDtcbn1cblxuaW9uLWNvbCB7XG4gIGZsZXg6IDEgMCAxMDBweCAhaW1wb3J0YW50O1xufVxuXG4uZHJpdmVyLXNuaXBwZXQgLml0ZW0tbWQge1xuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5kcml2ZXItc25pcHBldCAuaXRlbS1tZCBpb24tYXZhdGFyW2l0ZW0tbGVmdF0ge1xuICBtYXJnaW46IDhweCAxNnB4IDhweCAxMnB4O1xufVxuXG4ud3JhcHBlci1kcml2ZXJzbmlwcGV0IC5sYWJlbC1tZCB7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xufVxuXG4ubm8tYm9yZGVyIHtcbiAgYm9yZGVyOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5wYWRkaW5nLXN0YXR1cyB7XG4gIHBhZGRpbmctdG9wOiA2LjUlO1xufVxuXG4ubm8tbWFyZ2luIHtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG59XG5cbuKAiy5jdXN0b21lci1pY29ucyB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgZm9udC1zaXplOiAxNnB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiAjMmIyYjJiO1xufVxuXG4uY3VzdG9tZXItaWNvbnMgaW1nIHtcbiAgZGlzcGxheTogYmxvY2s7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBwYWRkaW5nLWJvdHRvbTogMnB4O1xufVxuXG7igIsgLndyYXBwZXItZHJpdmVyc25pcHBldCB7XG4gIHdpZHRoOiA5NSU7XG4gIG1hcmdpbjogMCBhdXRvO1xufVxuXG4ub25saW5ldG9wcGFkZGluZyB7XG4gIHBhZGRpbmctdG9wOiA4cHg7XG4gIHBhZGRpbmctbGVmdDogODBweDtcbn1cblxuLnNpZGVwYWRkaW5nLXN0YXR1cyB7XG4gIHBhZGRpbmctbGVmdDogM3B4O1xufVxuXG4uaXRlbS1tZCB7XG4gIHBhZGRpbmc6IDAgMCAwIDIycHggIWltcG9ydGFudDtcbn1cblxuLm5vLXBhZGRpbmcge1xuICBwYWRkaW5nOiAwO1xufVxuXG4ubm8tcGFkZGluZy1ib3R0b20ge1xuICBwYWRkaW5nLWJvdHRvbTogMDtcbn1cblxuLm5vLXBhZGRpbmctdG9wIHtcbiAgcGFkZGluZy10b3A6IDA7XG59XG5cbi5jZW50ZXJ0ZXh0IHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4ucGFkZGluZ3JpZ2h0IHtcbiAgcGFkZGluZy1yaWdodDogMTBweDtcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpIHtcbiAgLm9ubGluZXRvcHBhZGRpbmcge1xuICAgIHBhZGRpbmctbGVmdDogNjklO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSB7XG4gIC5kYXJrYnV0dG9uMSB7XG4gICAgcGFkZGluZzogMzBweCAwO1xuICB9XG59XG5pb24tYXBwLl9nbWFwc19jZHZfIC5uYXYtZGVjb3Ige1xuICBiYWNrZ3JvdW5kLWNvbG9yOiB0cmFuc3BhcmVudCAhaW1wb3J0YW50O1xufVxuXG4uaGVhdnkge1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmJhbm5lcnRlcm0ge1xuICB3aWR0aDogMTAwJTtcbiAgcGFkZGluZy10b3A6IDE4MHB4O1xuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy90ZXJtLnBuZ1wiKSBuby1yZXBlYXQ7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgYmFja2dyb3VuZC1zaXplOiBjb3Zlcjtcbn1cblxuLmFib3V0bG9nbyB7XG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcbiAgZGlzcGxheTogYmxvY2s7XG4gIHdpZHRoOiBhdXRvO1xuICBtYXJnaW46IGF1dG87XG4gIHRvcDogNzIlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbn1cblxuLmhlYWRlcmFib3V0IHtcbiAgY29sb3I6ICMyZTJlMmU7XG4gIGZvbnQtc2l6ZTogMThweDtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIHBhZGRpbmctdG9wOiAxJTtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5ibHVlbGluZSB7XG4gIGRpc3BsYXk6IGJsb2NrICFpbXBvcnRhbnQ7XG4gIG1hcmdpbjogMCBhdXRvICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiBhdXRvICFpbXBvcnRhbnQ7XG4gIGJvcmRlci1yYWRpdXM6IDAgIWltcG9ydGFudDtcbn1cblxuLmFib3V0cGFyYSB7XG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcbiAgY29sb3I6ICM5MTkxOTE7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbGluZS1oZWlnaHQ6IDE4cHg7XG4gIHBhZGRpbmc6IDEycHggMjBweCAwIDIwcHg7XG4gIG1hcmdpbjogMHB4O1xufVxuXG4uYWJvdXRwYXJhIHNwYW4ge1xuICBjb2xvcjogIzIyMjtcbn1cblxuLmFzaHJlY3RhbmdsZSwgLmFzaHJlY3RhbmdsZS1mYXJlIHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAxMnB4IGF1dG87XG4gIGJhY2tncm91bmQ6ICNmNGY0ZjQ7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG4gIGJvcmRlcjogMXB4IHNvbGlkICNkNGQ0ZDQ7XG4gIHBhZGRpbmc6IDEycHg7XG4gIGNvbG9yOiAjMmUyZTJlO1xuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XG4gIGZvbnQtc2l6ZTogMTZweDtcbiAgdGV4dC1hbGlnbjogbGVmdDtcbiAgbGluZS1oZWlnaHQ6IDE5cHg7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4ubGlzdHN0eWxlIHtcbiAgd2lkdGg6IDkwJTtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIG1hcmdpbi10b3A6IDE1cHg7XG59XG5cbi5saXN0c3R5bGUgdWwge1xuICBtYXJnaW46IDBweDtcbiAgcGFkZGluZzogMHB4O1xufVxuXG4ubGlzdHN0eWxlIHVsIGxpIHtcbiAgbGlzdC1zdHlsZTogbm9uZTtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE2cHg7XG4gIHRleHQtYWxpZ246IGxlZnQ7XG4gIGxpbmUtaGVpZ2h0OiAyMHB4O1xuICBjb2xvcjogIzkxOTE5MTtcbiAgcGFkZGluZy1sZWZ0OiA5JTtcbiAgYmFja2dyb3VuZDogdXJsKFwiL2Fzc2V0cy9pbWFnZXMvY2FyYnVsbGV0LnBuZ1wiKSBuby1yZXBlYXQ7XG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XG59XG5cbi5mb250IHtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG4gIGNvbG9yOiAjNzM3MzczO1xufVxuXG4uYWNjZXB0QnRuIHtcbiAgcG9zaXRpb246IGZpeGVkO1xuICBib3R0b206IDA7XG4gIGxlZnQ6IDA7XG4gIHJpZ2h0OiAwO1xufVxuXG4uYnRuYWNjZXB0IHtcbiAgYmFja2dyb3VuZDogIzA5NzM4ZCAhaW1wb3J0YW50O1xuICBjb2xvcjogd2hpdGU7XG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbi5idG5hY2NlcHQ6aG92ZXIge1xuICBiYWNrZ3JvdW5kOiB3aGl0ZSAhaW1wb3J0YW50O1xuICBjb2xvcjogYmxhY2s7XG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcbiAgYm9yZGVyLXJhZGl1czogNTBweDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xuICBmb250LXNpemU6IDE1cHg7XG59XG5cbkBtZWRpYSAobWluLXdpZHRoOiA1NjhweCkge1xuICAuaGVhZGVyYWJvdXQge1xuICAgIHBhZGRpbmctdG9wOiA3JTtcbiAgICBmb250LXdlaWdodDogYm9sZDtcbiAgfVxufVxuLnRvcG5vdGlmaWNhdGlvbi1mYXJlIHtcbiAgYmFja2dyb3VuZDogI2U4ZThlOCAhaW1wb3J0YW50O1xuICBwYWRkaW5nOiA0cHggMCAhaW1wb3J0YW50O1xufVxuXG4uY2FydHlwZWRhdGUge1xuICB3aWR0aDogODAlO1xuICBvdmVyZmxvdzogaGlkZGVuO1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLmNhbGVuZGFyIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG4uZmFyZWJ1dHRvbiB7XG4gIGNvbG9yOiAjZmZmO1xuICBmb250LXNpemU6IDEycHg7XG4gIHBhZGRpbmc6IDIlIDA7XG4gIGJhY2tncm91bmQ6ICMyMjI7XG4gIGJvcmRlci1yYWRpdXM6IDEwcHg7XG59XG5cbi8qLmNhcnR5cGVkYXRlLWxlZnR7ZmxvYXQ6IGxlZnQ7IGJhY2tncm91bmQ6ICNlOGU4ZTg7fVxuLmNhcnR5cGVkYXRlLWxlZnQgc3Bhbnt3aWR0aDogYXV0bzsgZGlzcGxheTogYmxvY2s7IHBvc2l0aW9uOiBhYnNvbHV0ZTsgdG9wOiAzMCU7IGxlZnQ6IDIwJTsgYmFja2dyb3VuZDogdXJsKCcuLi9hc3NldHMvaW1hZ2VzL2NhbGVuZGFyLnBuZycpIG5vLXJlcGVhdDsgbGluZS1oZWlnaHQ6IDIxcHg7IGZvbnQtZmFtaWx5OiAnUFQgU2Fucyc7IGNvbG9yOiAjMmIyYjJiOyBmb250LXNpemU6IDE0cHg7IHBhZGRpbmctbGVmdDogMjhweDsgZm9udC13ZWlnaHQ6IGJvbGQ7fSovXG4vKi5jYXJ0eXBlZGF0ZS1yaWdodHtmbG9hdDogcmlnaHQ7IHdpZHRoOiA0NC41JTsgYmFja2dyb3VuZDogI2U4ZThlODsgcG9zaXRpb246IHJlbGF0aXZlO31cbi5jYXJ0eXBlZGF0ZS1yaWdodCBzcGFue3dpZHRoOiBhdXRvOyBkaXNwbGF5OiBibG9jazsgcG9zaXRpb246IGFic29sdXRlOyB0b3A6IDMwJTsgbGVmdDogMjAlOyBiYWNrZ3JvdW5kOiB1cmwoJy4uL2Fzc2V0cy9pbWFnZXMvY2FyMS5wbmcnKSBuby1yZXBlYXQ7IGxpbmUtaGVpZ2h0OiAyMXB4OyBmb250LWZhbWlseTogJ1BUIFNhbnMnOyBjb2xvcjogIzJiMmIyYjsgZm9udC1zaXplOiAxNHB4OyBwYWRkaW5nLWxlZnQ6IDI4cHg7IGZvbnQtd2VpZ2h0OiBib2xkO30qL1xuLmZhcmVlc3RpbWF0ZSwgLmN1cnJlbmN5IHtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBjb2xvcjogIzJiMmIyYjtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBmb250LXNpemU6IDIwcHg7XG4gIHBhZGRpbmc6IDVweCAwIDAgMDtcbiAgbWFyZ2luOiAwO1xuICBmb250LXdlaWdodDogYm9sZDtcbn1cblxuLmN1cnJlbmN5IHtcbiAgY29sb3I6ICMyMjI7XG4gIGZvbnQtc2l6ZTogMzVweDtcbiAgcGFkZGluZzogMCAwIDBweCAwO1xufVxuXG4udHJpcHRpbWUsIC50aW1lc2xpZGVyIHtcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xuICBmb250LXNpemU6IDE3cHg7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbiAgY29sb3I6ICMyYjJiMmI7XG4gIG1hcmdpbjogMDtcbn1cblxuLnRpbWVzbGlkZXIge1xuICBmb250LXNpemU6IDIycHg7XG4gIHBhZGRpbmc6IDEwcHggMDtcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XG59XG5cbi5mYXJlYnV0dG9uLXdyYXBwZXIge1xuICB3aWR0aDogOTAlO1xuICBtYXJnaW46IDAgYXV0bztcbn1cblxuLm5vLXBhZGRpbmctdG9wIHtcbiAgcGFkZGluZy10b3A6IDA7XG59XG5cbi5uby1wYWRkaW5nLWJvdHRvbSB7XG4gIHBhZGRpbmctYm90dG9tOiAwO1xufVxuXG4uYXNocmVjdGFuZ2xlLWZhcmUge1xuICBtYXJnaW46IDRweCBhdXRvICFpbXBvcnRhbnQ7XG4gIHdpZHRoOiA4MCUgIWltcG9ydGFudDtcbn1cblxuLmRhdGV0aW1lLW1kIHtcbiAgZm9udC1zaXplOiAxNHB4O1xuICBwYWRkaW5nOiAwO1xuICBiYWNrZ3JvdW5kOiBub25lICFpbXBvcnRhbnQ7XG59XG5cbi5pdGVtLWNvdmVyIHtcbiAgd2lkdGg6IDA7XG4gIGhlaWdodDogMDtcbn1cblxuLnNlbGVjdC1tZCB7XG4gIHBhZGRpbmc6IDA7XG59XG5cbi5pdGVtLW1kLml0ZW0tYmxvY2sgLml0ZW0taW5uZXIge1xuICBib3JkZXItYm90dG9tOiBub25lO1xufVxuXG4uYXNoZmFyZSB7XG4gIG1hcmdpbjogNHB4IGF1dG8gIWltcG9ydGFudDtcbiAgd2lkdGg6IDkwJTtcbiAgYmFja2dyb3VuZDogI2Y0ZjRmNDtcbiAgcGFkZGluZzogNXB4O1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xufVxuXG4uYXNoZmFyZSAubGlzdC1tZCB7XG4gIG1hcmdpbjogMCAhaW1wb3J0YW50O1xufVxuXG4uYXNoZmFyZSAuaXRlbS1tZCB7XG4gIHBhZGRpbmc6IDAgMCAwIDhweCAhaW1wb3J0YW50O1xufVxuXG4uYXNoZmFyZSAubGlzdC1pb3Mge1xuICBtYXJnaW46IDAgIWltcG9ydGFudDtcbn1cblxuLmFzaGZhcmUgLml0ZW0taW9zIHtcbiAgcGFkZGluZzogMCAwIDAgOHB4ICFpbXBvcnRhbnQ7XG59XG5cbi5jZW50ZXItYWxsaWduIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xuICBwYWRkaW5nOiAxJTtcbiAgYmFja2dyb3VuZDogI2U4ZThlODtcbiAgZm9udC1zaXplOiAxM3B4O1xuICBtYXJnaW46IDJweDtcbn1cblxuLmFzaHJlY3RhbmdsZS1mYXJlIC5saXN0LW1kIHtcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XG59XG5cbi5hc2hyZWN0YW5nbGUtZmFyZSAuaXRlbS1tZCB7XG4gIHBhZGRpbmctbGVmdDogMCAhaW1wb3J0YW50O1xufVxuXG4uc2VsZWN0LXRleHQge1xuICBmb250LXNpemU6IDE0cHggIWltcG9ydGFudDtcbn1cblxuLnNob3J0d3JhcHBlciB7XG4gIHdpZHRoOiAzNiUgIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwIGF1dG87XG59XG5cbmlvbi1zZWxlY3Qge1xuICBtYXgtd2lkdGg6IDEwMCU7XG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xufVxuXG4uc2VsZWN0LW1kIC5zZWxlY3QtaWNvbiB7XG4gIHdpZHRoOiAyMnB4O1xufVxuXG4uY3VzdG9tLW91dGxpbmUge1xuICBib3JkZXItcmFkaXVzOiAxMHB4O1xuICB3aWR0aDogMTAwJTtcbiAgYmFja2dyb3VuZDogbm9uZTtcbiAgYm9yZGVyOiAxcHggc29saWQgIzIyMjtcbiAgY29sb3I6ICMyYjJiMmI7XG4gIGJveC1zaGFkb3c6IG5vbmU7XG59XG5cbi5jdXN0b20tY29sIHtcbiAgd2lkdGg6IDEwMCU7XG59XG5cbi5jdXN0b20tY29sIGlvbi1jb2wge1xuICBmbGV4OiAxIDAgMTAwcHg7XG4gIG1pbi13aWR0aDogMTAwcHg7XG59XG5cbi5jdXN0b20tY29sIC5idXR0b24taW9zIHtcbiAgcGFkZGluZzogMC4zcmVtO1xufVxuXG4uY3VzdG9tLWljb24tcGFkZGluZyBbaWNvbi1sZWZ0XSBpb24taWNvbiB7XG4gIHBhZGRpbmctbGVmdDogMC41ZW07XG59XG5cbi5ycXN0RHZyIHtcbiAgY29sb3I6ICNmZmY7XG4gIGJhY2tncm91bmQ6ICMzYjU5OTg7XG59XG5cbi50ZXh0Q2VudGVyIHtcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xufVxuXG4uc21hbGxGb250IHtcbiAgZm9udC1zaXplOiAxLjRyZW0gIWltcG9ydGFudDtcbn1cblxuLnBhZGRpbmctYnRtIHtcbiAgcGFkZGluZy1ib3R0b206IDBweCAhaW1wb3J0YW50O1xufVxuXG4uZmFyZUNhbEJ0biB7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mYXJlQ2FsQnRuIGlvbi1pY29uIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xufVxuLmZhcmVDYWxCdG4gaW9uLWRhdGV0aW1lIHtcbiAgd2lkdGg6IDEwMCU7XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbn1cbi5mYXJlQ2FsQnRuIC5mYXJlRGF0ZUljb24ge1xuICBsZWZ0OiAyMSU7XG4gIHRvcDogMjUlO1xufVxuLmZhcmVDYWxCdG4gLmZhcmVUaW1lSWNvbiB7XG4gIGxlZnQ6IDIxJTtcbiAgdG9wOiAyNyU7XG59XG5cbi5ub3RlIHtcbiAgbWFyZ2luLWxlZnQ6IDE0cHg7XG4gIG1hcmdpbi1ib3R0b206IDBweDtcbiAgZm9udDogaXRhbGljIGJvbGQgMTFweCBQVCwgU2Fucztcbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG9yaWVudGF0aW9uOiBsYW5kc2NhcGUpIHtcbiAgLnNob3J0d3JhcHBlciB7XG4gICAgd2lkdGg6IDI0JSAhaW1wb3J0YW50O1xuICB9XG5cbiAgLmZhcmVDYWxCdG4gLmZhcmVEYXRlSWNvbiB7XG4gICAgbGVmdDogMjUlICFpbXBvcnRhbnQ7XG4gIH1cbiAgLmZhcmVDYWxCdG4gLmZhcmVUaW1lSWNvbiB7XG4gICAgbGVmdDogMzIlICFpbXBvcnRhbnQ7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiAzMjBweCkge1xuICAuY2FydHlwZWRhdGUtbGVmdCBzcGFuIHtcbiAgICBsZWZ0OiAxNSU7XG4gIH1cblxuICAuY2FydHlwZWRhdGUtcmlnaHQgc3BhbiB7XG4gICAgbGVmdDogNyU7XG4gIH1cblxuICBbaWNvbi1sZWZ0XSBpb24taWNvbiB7XG4gICAgcGFkZGluZy1yaWdodDogMC41ZW07XG4gIH1cblxuICAuZmFyZUNhbEJ0biAuZmFyZURhdGVJY29uIHtcbiAgICBsZWZ0OiAxJTtcbiAgfVxuICAuZmFyZUNhbEJ0biAuZmFyZVRpbWVJY29uIHtcbiAgICBsZWZ0OiAxNyU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiAzNjBweCkge1xuICAuY2FydHlwZWRhdGUtbGVmdCBzcGFuIHtcbiAgICBsZWZ0OiA4JTtcbiAgfVxuXG4gIC5jYXJ0eXBlZGF0ZS1yaWdodCBzcGFuIHtcbiAgICBsZWZ0OiAxMSU7XG4gIH1cblxuICAuZmFyZUNhbEJ0biAuZmFyZURhdGVJY29uIHtcbiAgICBsZWZ0OiA0JTtcbiAgfVxuICAuZmFyZUNhbEJ0biAuZmFyZVRpbWVJY29uIHtcbiAgICBsZWZ0OiAxOCU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA0MTJweCkge1xuICAuY2FydHlwZWRhdGUtbGVmdCBzcGFuIHtcbiAgICBsZWZ0OiAyMyU7XG4gIH1cblxuICAuY2FydHlwZWRhdGUtcmlnaHQgc3BhbiB7XG4gICAgbGVmdDogMTYlO1xuICB9XG5cbiAgLmZhcmVDYWxCdG4gLmZhcmVEYXRlSWNvbiB7XG4gICAgbGVmdDogMjAlO1xuICB9XG4gIC5mYXJlQ2FsQnRuIC5mYXJlVGltZUljb24ge1xuICAgIGxlZnQ6IDMwJTtcbiAgfVxufVxuQG1lZGlhIChtaW4td2lkdGg6IDU2OHB4KSB7XG4gIC5jYXJ0eXBlZGF0ZS1sZWZ0IHNwYW4ge1xuICAgIGxlZnQ6IDMwJTtcbiAgfVxuXG4gIC5jYXJ0eXBlZGF0ZS1yaWdodCBzcGFuIHtcbiAgICBsZWZ0OiAzMCU7XG4gIH1cbn1cbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xuICAuZmFyZUNhbEJ0biAuZmFyZURhdGVJY29uIHtcbiAgICBsZWZ0OiAzNiU7XG4gIH1cbiAgLmZhcmVDYWxCdG4gLmZhcmVUaW1lSWNvbiB7XG4gICAgbGVmdDogMzklO1xuICB9XG59XG5AbWVkaWEgKG1pbi13aWR0aDogMTAyNHB4KSB7XG4gIC5jYXJ0eXBlZGF0ZS1sZWZ0IHNwYW4ge1xuICAgIGxlZnQ6IDQwJTtcbiAgfVxuXG4gIC5jYXJ0eXBlZGF0ZS1yaWdodCBzcGFuIHtcbiAgICBsZWZ0OiA0MCU7XG4gIH1cbn1cbi50aW1lIHtcbiAgZm9udC1zaXplOiAzMHB4O1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIG1hcmdpbi1sZWZ0OiAyMHB4O1xufVxuXG4uZGF0ZXNldCB7XG4gIGZvbnQtc2l6ZTogMTZweCAhaW1wb3J0YW50O1xuICBtYXJnaW4tbGVmdDogLTEwcHg7XG59XG5cbi5kYXRlaWNvbiB7XG4gIGZvbnQtc2l6ZTogMThweCAhaW1wb3J0YW50O1xufVxuXG4ubWFwMyB7XG4gIGhlaWdodDogMjUwcHg7XG59XG5cbi5tYXA0IHtcbiAgaGVpZ2h0OiAzODdweDtcbn1cblxuLmJ1dHRvbi10cmFucyB7XG4gIGJhY2tncm91bmQ6IHJnYmEoMjI0LCAyMjYsIDIyNiwgMC43NjcpO1xuICBjb2xvcjogIzAwMDtcbiAgd2lkdGg6IDg4JTtcbiAgYm9yZGVyOiAxcHggc29saWQgI2RlZGRkZDtcbiAgcGFkZGluZzogMTBweCAxNXB4O1xuICBmb250LXNpemU6IDE2cHg7XG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XG4gIGJveC1zaGFkb3c6IG5vbmUgIWltcG9ydGFudDtcbiAgZm9udC13ZWlnaHQ6IDQwMCAhaW1wb3J0YW50O1xuICBib3gtc2hhZG93OiAjYzdjN2M3O1xufVxuXG4uYXBwcngtZGlzIHtcbiAgZm9udC1zaXplOiAxLjRyZW0gIWltcG9ydGFudDtcbiAgbWFyZ2luOiAwcHg7XG4gIG1hcmdpbi10b3A6IDEycHg7XG59XG5cbi5hcHByeC1kaXMtcCB7XG4gIG1hcmdpbjogMTBweDtcbn1cblxuLmRhdGV0aW1lLWlvcyB7XG4gIHBhZGRpbmc6IDExcHggMHB4IDExcHggNXB4ICFpbXBvcnRhbnQ7XG59XG5cbi5kYXJrYnV0dG9uMSB7XG4gIGJhY2tncm91bmQ6ICMwOTczOGQgIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogNjBweDtcbn0iLCJwYWdlLWFjdGl2ZWJvb2tpbmcge1xyXG4gICNtYXAyIHtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICB9XHJcbn1cclxuXHJcbi5yaWdodG1lbnUge1xyXG4gIGNvbG9yOiAjZmZmO1xyXG4gIGJhY2tncm91bmQ6IG5vbmUgIWltcG9ydGFudDtcclxuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5zZWJtLWdvb2dsZS1tYXAtY29udGFpbmVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBoZWlnaHQ6IDEwMCU7XHJcbiAgb3BhY2l0eTogMDtcclxuICB0cmFuc2l0aW9uOiBvcGFjaXR5IDE1MG1zIGVhc2UtaW47XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcblxyXG4uZ21hcCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xyXG59XHJcbi5nbWFwLWJ1dHRvbi13cmFwcGVyIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgaGVpZ2h0OiBhdXRvO1xyXG59XHJcbi5jdXNiIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgd2lkdGg6IDIwJTtcclxuICBoZWlnaHQ6IDQwcHg7XHJcbn1cclxuLmRhcmtidXR0b24xIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwYWRkaW5nOiAyMnB4IDA7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMThweDtcclxuICB0ZXh0LXRyYW5zZm9ybTogY2FwaXRhbGl6ZTtcclxuICBmb250LXdlaWdodDogbm9ybWFsO1xyXG59XHJcbi5idXR0b24tbWQtZGFyay5hY3RpdmF0ZWQsXHJcbi5idXR0b24tbWQuYWN0aXZhdGVkIHtcclxuICBiYWNrZ3JvdW5kOiAjMjIyICFpbXBvcnRhbnQ7XHJcbn1cclxuLmJ1dHRvbi1tZDpob3Zlcjpub3QoLmRpc2FibGUtaG92ZXIpIHtcclxuICBiYWNrZ3JvdW5kOiAjMjIyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tYXBjb250YWluZXIge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG4gIGhlaWdodDogMTAwJTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgLy8gYmFja2dyb3VuZDogI2RiZGJkYjtcclxuICB0b3A6IDA7XHJcbn1cclxuLnRvcG5vdGlmaWNhdGlvbi13cmFwcGVyIHtcclxuICB3aWR0aDogMTAwJTtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgdG9wOiAwO1xyXG4gIG92ZXJmbG93OiBoaWRkZW47XHJcbn1cclxuLm1hcGJ1dHRvbi13cmFwcGVyIHtcclxuICB3aWR0aDogODAlO1xyXG4gIG1hcmdpbjogYXV0bztcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuLnRvcG5vdGlmaWNhdGlvbiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgYmFja2dyb3VuZDogI2ZmZjtcclxuICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgcGFkZGluZzogMHB4IDJweDtcclxufVxyXG4uc3ViYmx1ZXRleHQtbWFwIHtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNHB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgY29sb3I6ICMyYjJiMmI7XHJcbiAgbWFyZ2luOiAwO1xyXG4gIHBhZGRpbmctYm90dG9tOiA4cHg7XHJcbn1cclxuLmJsdWV0ZXh0LW1hcCB7XHJcbiAgLy8gQGV4dGVuZCAuc3ViYmx1ZXRleHQtbWFwICFvcHRpb25hbDtcclxuICAvLyBmb250LXNpemU6IDE2cHg7XHJcbiAgY29sb3I6ICMyMjI7XHJcbiAgLy8gcGFkZGluZy1ib3R0b206IDA7XHJcbiAgLy8gcGFkZGluZy10b3A6IDVweDtcclxuICBtYXJnaW4tbGVmdDogMDtcclxuICAgIG1hcmdpbi1yaWdodDogMDtcclxuICAgIG1hcmdpbi10b3A6IDA7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycHg7XHJcbiAgICBmb250LXNpemU6IDE0cHg7XHJcbiAgICBsaW5lLWhlaWdodDogbm9ybWFsO1xyXG4gICAgdGV4dC1vdmVyZmxvdzogaW5oZXJpdDtcclxuICAgIG92ZXJmbG93OiBpbmhlcml0O1xyXG59XHJcbi5tYXJrZXIgaW1nIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBtYXJnaW46IDAgYXV0bztcclxuICBwYWRkaW5nLXRvcDogNXB4O1xyXG59XHJcblxyXG4uZHJpdmVyLXNuaXBwZXQge1xyXG4gIHBvc2l0aW9uOiBmaXhlZDtcclxuICBib3R0b206IDBweDtcclxuICB3aWR0aDogMTAwJTtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZmZmO1xyXG59XHJcblxyXG4ubm9wYWRkaW5nLWF2YXRhYXIgLml0ZW0tbWQgaW9uLWF2YXRhcltpdGVtLWxlZnRdIHtcclxuICBtYXJnaW46IDhweCAxNnB4IDhweCA2cHg7XHJcbn1cclxuaW9uLWNvbCB7XHJcbiAgZmxleDogMSAwIDEwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLmRyaXZlci1zbmlwcGV0IC5pdGVtLW1kIHtcclxuICBwYWRkaW5nOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLmRyaXZlci1zbmlwcGV0IC5pdGVtLW1kIGlvbi1hdmF0YXJbaXRlbS1sZWZ0XSB7XHJcbiAgbWFyZ2luOiA4cHggMTZweCA4cHggMTJweDtcclxufVxyXG4ud3JhcHBlci1kcml2ZXJzbmlwcGV0IC5sYWJlbC1tZCB7XHJcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5uby1ib3JkZXIge1xyXG4gIGJvcmRlcjogbm9uZSAhaW1wb3J0YW50O1xyXG59XHJcbi5wYWRkaW5nLXN0YXR1cyB7XHJcbiAgcGFkZGluZy10b3A6IDYuNSU7XHJcbn1cclxuLm5vLW1hcmdpbiB7XHJcbiAgbWFyZ2luOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbuKAiy5jdXN0b21lci1pY29ucyB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgY29sb3I6ICMyYjJiMmI7XHJcbn1cclxuLmN1c3RvbWVyLWljb25zIGltZyB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgcGFkZGluZy1ib3R0b206IDJweDtcclxufVxyXG7igIsgLndyYXBwZXItZHJpdmVyc25pcHBldCB7XHJcbiAgd2lkdGg6IDk1JTtcclxuICBtYXJnaW46IDAgYXV0bztcclxufVxyXG4ub25saW5ldG9wcGFkZGluZyB7XHJcbiAgcGFkZGluZy10b3A6IDhweDtcclxuICBwYWRkaW5nLWxlZnQ6IDgwcHg7XHJcbn1cclxuLnNpZGVwYWRkaW5nLXN0YXR1cyB7XHJcbiAgcGFkZGluZy1sZWZ0OiAzcHg7XHJcbn1cclxuLml0ZW0tbWQge1xyXG4gIHBhZGRpbmc6IDAgMCAwIDIycHggIWltcG9ydGFudDtcclxufVxyXG4ubm8tcGFkZGluZyB7XHJcbiAgcGFkZGluZzogMDtcclxufVxyXG4ubm8tcGFkZGluZy1ib3R0b20ge1xyXG4gIHBhZGRpbmctYm90dG9tOiAwO1xyXG59XHJcbi5uby1wYWRkaW5nLXRvcCB7XHJcbiAgcGFkZGluZy10b3A6IDA7XHJcbn1cclxuLmNlbnRlcnRleHQge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4ucGFkZGluZ3JpZ2h0IHtcclxuICBwYWRkaW5nLXJpZ2h0OiAxMHB4O1xyXG59XHJcblxyXG5AbWVkaWEgc2NyZWVuIGFuZCAob3JpZW50YXRpb246IGxhbmRzY2FwZSkge1xyXG4gIC5vbmxpbmV0b3BwYWRkaW5nIHtcclxuICAgIHBhZGRpbmctbGVmdDogNjklO1xyXG4gIH1cclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDEwMjRweCkge1xyXG4gIC5kYXJrYnV0dG9uMSB7XHJcbiAgICBwYWRkaW5nOiAzMHB4IDA7XHJcbiAgfVxyXG59XHJcblxyXG5pb24tYXBwLl9nbWFwc19jZHZfIC5uYXYtZGVjb3Ige1xyXG4gIGJhY2tncm91bmQtY29sb3I6IHRyYW5zcGFyZW50ICFpbXBvcnRhbnQ7XHJcbn1cclxuIiwiLmhlYXZ5IHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxufVxyXG4uYmFubmVydGVybSB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbiAgcGFkZGluZy10b3A6IDE4MHB4O1xyXG4gIGJhY2tncm91bmQ6IHVybChcIi9hc3NldHMvaW1hZ2VzL3Rlcm0ucG5nXCIpIG5vLXJlcGVhdDtcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxufVxyXG4uYWJvdXRsb2dvIHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgd2lkdGg6IGF1dG87XHJcbiAgbWFyZ2luOiBhdXRvO1xyXG4gIHRvcDogNzIlO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuLmhlYWRlcmFib3V0IHtcclxuICBjb2xvcjogIzJlMmUyZTtcclxuICBmb250LXNpemU6IDE4cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIGZvbnQtZmFtaWx5OiBcIlBUIFNhbnNcIjtcclxuICBwYWRkaW5nLXRvcDogMSU7XHJcbiAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn1cclxuLmJsdWVsaW5lIHtcclxuICBkaXNwbGF5OiBibG9jayAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbjogMCBhdXRvICFpbXBvcnRhbnQ7XHJcbiAgd2lkdGg6IGF1dG8gIWltcG9ydGFudDtcclxuICBib3JkZXItcmFkaXVzOiAwICFpbXBvcnRhbnQ7XHJcbn1cclxuLmFib3V0cGFyYSB7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGNvbG9yOiAjOTE5MTkxO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOHB4O1xyXG4gIHBhZGRpbmc6IDEycHggMjBweCAwIDIwcHg7XHJcbiAgbWFyZ2luOiAwcHg7XHJcbn1cclxuLmFib3V0cGFyYSBzcGFuIHtcclxuICBjb2xvcjogIzIyMjtcclxufVxyXG4uYXNocmVjdGFuZ2xlIHtcclxuICB3aWR0aDogOTAlO1xyXG4gIG1hcmdpbjogMTJweCBhdXRvO1xyXG4gIGJhY2tncm91bmQ6ICNmNGY0ZjQ7XHJcbiAgYm9yZGVyLXJhZGl1czogMTBweDtcclxuICBib3JkZXI6IDFweCBzb2xpZCAjZDRkNGQ0O1xyXG4gIHBhZGRpbmc6IDEycHg7XHJcbiAgY29sb3I6ICMyZTJlMmU7XHJcbiAgZm9udC1mYW1pbHk6IFwiUFQgU2Fuc1wiO1xyXG4gIGZvbnQtc2l6ZTogMTZweDtcclxuICB0ZXh0LWFsaWduOiBsZWZ0O1xyXG4gIGxpbmUtaGVpZ2h0OiAxOXB4O1xyXG4gIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG59XHJcbi5saXN0c3R5bGUge1xyXG4gIHdpZHRoOiA5MCU7XHJcbiAgbWFyZ2luOiAwIGF1dG87XHJcbiAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG4ubGlzdHN0eWxlIHVsIHtcclxuICBtYXJnaW46IDBweDtcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuLmxpc3RzdHlsZSB1bCBsaSB7XHJcbiAgbGlzdC1zdHlsZTogbm9uZTtcclxuICBmb250LWZhbWlseTogXCJQVCBTYW5zXCI7XHJcbiAgZm9udC1zaXplOiAxNnB4O1xyXG4gIHRleHQtYWxpZ246IGxlZnQ7XHJcbiAgbGluZS1oZWlnaHQ6IDIwcHg7XHJcbiAgY29sb3I6ICM5MTkxOTE7XHJcbiAgcGFkZGluZy1sZWZ0OiA5JTtcclxuICBiYWNrZ3JvdW5kOiB1cmwoXCIvYXNzZXRzL2ltYWdlcy9jYXJidWxsZXQucG5nXCIpIG5vLXJlcGVhdDtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcbi5mb250IHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBjb2xvcjogIzczNzM3MztcclxufVxyXG4uYWNjZXB0QnRuIHtcclxuICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgYm90dG9tOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgcmlnaHQ6IDA7XHJcbn1cclxuLmJ0bmFjY2VwdHtcclxuICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IHdoaXRlO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG4uYnRuYWNjZXB0OmhvdmVye1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIG1hcmdpbi1sZWZ0OiAxNDBweDtcclxuICBib3JkZXItcmFkaXVzOiA1MHB4O1xyXG4gIHBhZGRpbmc6IDEwcHggMTVweDtcclxuICB3aGl0ZS1zcGFjZTogbm93cmFwO1xyXG4gIGZvbnQtc2l6ZTogMTVweDtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDU2OHB4KSB7XHJcbiAgLmhlYWRlcmFib3V0IHtcclxuICAgIHBhZGRpbmctdG9wOiA3JTtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gIH1cclxufVxyXG4iXX0= */");

/***/ }),

/***/ 97911:
/*!***********************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/components/header/header.component.html ***!
  \***********************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<div>\r\n\r\n</div>\r\n");

/***/ }),

/***/ 41850:
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/fare/fare.page.html ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Fare page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n\r\n<ion-header>\r\n  <ion-toolbar color=\"dark\">  \r\n    <ion-title>Estimate</ion-title>\r\n    <ion-buttons slot=\"end\">\r\n      <ion-button fill=\"clear\" >\r\n        Close\r\n      </ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n  <!-- <section>\r\n    <ion-grid>\r\n      <ion-row>\r\n\r\n    </ion-grid>\r\n  </section> -->\r\n\r\n  <section class=\"topnotification-fare\">\r\n\r\n    <ion-grid class=\"no-padding\">\r\n      <ion-row>\r\n        <ion-col width-10 class=\"marker\" size=\"3\"><div class=\"ion-margin-left\"><h4>DropOff:</h4></div> \r\n          <img src=\"assets/images/marker.png\" (click)=\"showAddressModal('pickup')\">\r\n        </ion-col>\r\n        <ion-col width-90 class=\"no-padding\" size=\"9\">\r\n          <!-- <p class=\"bluetext-map extrafontsize\" (click)=\"showAddressModal('pickup')\">Pickup location</p> -->\r\n          <p class=\"subbluetext-map smallFont padding-btm\" (click)=\"showAddressModal('pickup')\">{{pickup?.add}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </section>\r\n  <section class=\"topnotification-fare\" *ngIf=\"PickupService\">\r\n    <ion-grid class=\"no-padding\">\r\n      <ion-row>\r\n        <ion-col width-10 class=\"marker\">\r\n          <img src=\"assets/images/car.png\" (click)=\"showAddressModal('drop')\">\r\n        </ion-col>\r\n        <ion-col width-90 class=\"no-padding\">\r\n          <!-- <p class=\"bluetext-map extrafontsize\" (click)=\"showAddressModal('drop')\">Drop location</p> -->\r\n          <p class=\"subbluetext-map smallFont padding-btm\"  (click)=\"showAddressModal('drop')\">{{drop?.add}}</p>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </section>\r\n  <section>\r\n    <!-- <div id=\"map3\" data-tap-disabled=\"true\" class=\"map3\" *ngIf=\"this.PickupService === true\">\r\n    </div>\r\n    <div id=\"map3\" data-tap-disabled=\"true\" class=\"map4\" *ngIf=\"this.PickupService !== true\">\r\n    </div> -->\r\n  </section>\r\n  <section class=\"driver-snippet\">\r\n    <ion-grid style=\"border:1px solid #ddd;\">\r\n      <ion-row wrap>\r\n        <ion-col>\r\n          <!-- <ion-list no-lines class=\"no-padding no-margin\">\r\n            <ion-item class=\"no-border\"> -->\r\n          <h2 style=\"text-align:center;margin:0px\">${{estimate?.toFixed(2)}}</h2>\r\n          <!-- </ion-item>\r\n          </ion-list> -->\r\n        </ion-col>\r\n      </ion-row>\r\n      <!-- <ion-row wrap>\r\n\r\n       </ion-row> -->\r\n      <ion-row wrap>\r\n\r\n        <ion-col width-50>\r\n\r\n          <button icon-left class=\"button-trans\" ion-button (click)=\"tapEvent($event)\"\r\n            style=\"border-radius: 30px; margin-left: 10px; width: auto;\">\r\n            <ion-icon name=\"calendar-number-outline\" class=\"calendar\"></ion-icon>\r\n            <ion-datetime class=\"dateset\" displayFormat=\"D MMM YYYY\" [(ngModel)]=\"showDate\" (ionChange)=\"dateShow()\"\r\n              min=\"{{currentDate}}\" max=\"2030\"> </ion-datetime>\r\n          </button>\r\n\r\n        </ion-col>\r\n        <ion-col width-50>\r\n\r\n\r\n          <button icon-left class=\"button-trans\" ion-button style=\"margin-left:100px; border-radius: 30px; width: auto;\"\r\n            (click)=\"tapEventT($event)\">\r\n            <ion-icon name=\"time-outline\" class=\"time\"></ion-icon>\r\n            <!--<ion-datetime displayFormat=\"HH:mm:ss\"[(ngModel)]=\"showTime\" (ionChange)=\"timeShow()\"></ion-datetime>-->\r\n            <ion-datetime class=\"dateset\" displayFormat=\"h:mm A\" [(ngModel)]=\"showTime\" (ionChange)=\"timeShow()\">\r\n            </ion-datetime>\r\n          </button>\r\n\r\n        </ion-col>\r\n        <!-- <ion-col width-100 class=\"no-padding no-margin\" >\r\n          <h6 style=\"    margin: 5px;\r\n    text-align: center;\r\n    \"> {{transmission}}</h6>\r\n        </ion-col> -->\r\n        <!-- <p class=\"note\"><strong>NOTE: </strong>Minimum one hour notice period is required for all bookings.</p> -->\r\n      </ion-row>\r\n\r\n      <ion-row wrap *ngIf=\"PickupService\" style=\"text-align: center;\">\r\n        <ion-col width-50 class=\"no-padding no-margin\" style=\"border-right: 1px solid #ddd;\">\r\n\r\n          <h2 class=\"apprx-dis\"><strong>Approximate Trip time </strong> </h2>\r\n          <p class=\"apprx-dis-p\">{{triptime}} minute.</p>\r\n\r\n        </ion-col>\r\n        <ion-col width-50 class=\"no-padding no-margin\">\r\n\r\n\r\n          <h2 class=\"apprx-dis\"><strong>\r\n              Approximate Distance </strong></h2>\r\n          <p class=\"apprx-dis-p\"> {{(distance/1000)?.toFixed()}} Km</p>\r\n\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row wrap *ngIf=\"PickupService !== true\">\r\n        <ion-col width-100>\r\n          <ion-list no-lines class=\"no-padding no-margin\">\r\n            <ion-item class=\"no-border\">\r\n\r\n              <ion-label style=\"margin-left:100px;\">Trip Time</ion-label>\r\n              <ion-select [(ngModel)]=\"triptime\" (ionChange)=\"changeEstimate()\" style=\"width:138px;\">\r\n                <!--<ion-option value=\"2\" value=\"default\" selected=\"true\">2 Hours</ion-option>-->\r\n                <ion-select-option value=\"2\">2 Hours</ion-select-option>\r\n                <ion-select-option value=\"3\">3 Hours</ion-select-option>\r\n                <ion-select-option value=\"4\">4 Hours</ion-select-option>\r\n                <ion-select-option value=\"5\">5 Hours</ion-select-option>\r\n                <ion-select-option value=\"6\">6 Hours</ion-select-option>\r\n                <ion-select-option value=\"7\">7 Hours</ion-select-option>\r\n                <ion-select-option value=\"8\">8 Hours</ion-select-option>\r\n                <ion-select-option value=\"9\">9 Hours</ion-select-option>\r\n                <ion-select-option value=\"10\">10 Hours</ion-select-option>\r\n              </ion-select>\r\n\r\n\r\n            </ion-item>\r\n\r\n          </ion-list>\r\n        </ion-col>\r\n\r\n      </ion-row>\r\n\r\n      <ion-row wrap>\r\n        <ion-col width-50 class=\"no-padding no-margin\">\r\n          <!-- style=\"border-right: 1px solid #ddd;\" -->\r\n          <button ion-button icon-left class=\"button-trans\" (click)=\"addPromo()\"\r\n            style=\"margin-bottom: 5px; margin-left: 10px;\">Promo Code</button>\r\n          <!-- </ion-item> -->\r\n          <!-- </ion-list> -->\r\n        </ion-col>\r\n        <ion-col width-50 class=\"no-padding no-margin\">\r\n          <!-- <ion-list no-lines class=\"no-padding no-margin\"> -->\r\n          <!-- <ion-item class=\"no-border\"> -->\r\n          <button ion-button icon-left class=\"button-trans\" (click)=\"addCard()\" style=\"margin-left:18px\"> Add Credit\r\n            Card</button>\r\n          <!-- </ion-item> -->\r\n          <!-- </ion-list> -->\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row wrap>\r\n        <ion-col style=\"padding: 0px 2px;\">\r\n          <button class=\"darkbutton1\" ion-button (click)=\"requestDriver()\" [disabled]=\"requestDone\">Request\r\n            Driver</button>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n    <!--<ion-grid style=\"padding:0px\">-->\r\n    <!---->\r\n    <!--</ion-grid>-->\r\n\r\n\r\n\r\n\r\n\r\n  </section>\r\n\r\n\r\n\r\n\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_fare_fare_module_ts.js.map