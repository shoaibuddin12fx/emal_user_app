(self["webpackChunkRidebidder"] = self["webpackChunkRidebidder"] || []).push([["src_app_pages_popover_popover_module_ts"],{

/***/ 17853:
/*!*********************************************************!*\
  !*** ./src/app/pages/popover/popover-routing.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PopoverPageRoutingModule": () => (/* binding */ PopoverPageRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 39895);
/* harmony import */ var _popover_page__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./popover.page */ 17034);




const routes = [
    {
        path: '',
        component: _popover_page__WEBPACK_IMPORTED_MODULE_0__.PopoverPage
    }
];
let PopoverPageRoutingModule = class PopoverPageRoutingModule {
};
PopoverPageRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_1__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_2__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_3__.RouterModule],
    })
], PopoverPageRoutingModule);



/***/ }),

/***/ 78949:
/*!*************************************************!*\
  !*** ./src/app/pages/popover/popover.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PopoverPageModule": () => (/* binding */ PopoverPageModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ 38583);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 3679);
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic/angular */ 80476);
/* harmony import */ var _popover_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./popover-routing.module */ 17853);
/* harmony import */ var _popover_page__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./popover.page */ 17034);







let PopoverPageModule = class PopoverPageModule {
};
PopoverPageModule = (0,tslib__WEBPACK_IMPORTED_MODULE_2__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.NgModule)({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_4__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormsModule,
            _ionic_angular__WEBPACK_IMPORTED_MODULE_6__.IonicModule,
            _popover_routing_module__WEBPACK_IMPORTED_MODULE_0__.PopoverPageRoutingModule
        ],
        declarations: [_popover_page__WEBPACK_IMPORTED_MODULE_1__.PopoverPage]
    })
], PopoverPageModule);



/***/ }),

/***/ 17034:
/*!***********************************************!*\
  !*** ./src/app/pages/popover/popover.page.ts ***!
  \***********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PopoverPage": () => (/* binding */ PopoverPage)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 64762);
/* harmony import */ var _raw_loader_popover_page_html__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! !raw-loader!./popover.page.html */ 44557);
/* harmony import */ var _popover_page_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./popover.page.scss */ 2528);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 37716);
/* harmony import */ var _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../base-page/base-page */ 24282);





let PopoverPage = class PopoverPage extends _base_page_base_page__WEBPACK_IMPORTED_MODULE_2__.BasePage {
    constructor(injector) {
        super(injector);
        this.addcard = { cardno: '', month: '', year: '', cvv: '' };
        this.years = [];
        let year = 2017;
        let till = 2042;
        for (let i = year; i <= till; i++) {
            //this.options += "<option>"+ y +"</option>";
            this.years.push(i);
        }
        console.log(this.years);
    }
    payNow() {
        console.log(this.addcard.cardno);
        console.log(this.addcard.month);
        console.log(this.addcard.year);
        console.log(this.addcard.cvv);
    }
    close() {
        this.viewCtrl.dismiss();
    }
    ngOnInit() { }
};
PopoverPage.ctorParameters = () => [
    { type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Injector }
];
PopoverPage = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-popover',
        template: _raw_loader_popover_page_html__WEBPACK_IMPORTED_MODULE_0__.default,
        styles: [_popover_page_scss__WEBPACK_IMPORTED_MODULE_1__.default]
    })
], PopoverPage);



/***/ }),

/***/ 2528:
/*!*************************************************!*\
  !*** ./src/app/pages/popover/popover.page.scss ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = (".textcolor {\n  color: #696767;\n}\n\n.payBtn {\n  background: #09738d !important;\n  border-radius: 20px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n  margin-left: 15px;\n  margin-top: 15px;\n}\n\n.payBtn:hover {\n  background: white !important;\n  color: black;\n  border-radius: 20px;\n  padding: 10px 15px;\n  white-space: nowrap;\n  font-size: 15px;\n  margin-left: 15px;\n  margin-top: 15px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBvcG92ZXIucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0UsY0FBQTtBQUNGOztBQUVBO0VBQ0ksOEJBQUE7RUFDQSxtQkFBQTtFQUNBLGtCQUFBO0VBQ0EsbUJBQUE7RUFDQSxlQUFBO0VBQ0EsaUJBQUE7RUFDQSxnQkFBQTtBQUNKOztBQUNBO0VBQ0UsNEJBQUE7RUFDQSxZQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtFQUNBLG1CQUFBO0VBQ0EsZUFBQTtFQUNBLGlCQUFBO0VBQ0EsZ0JBQUE7QUFFRiIsImZpbGUiOiJwb3BvdmVyLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi50ZXh0Y29sb3J7XHJcbiAgY29sb3I6cmdiKDEwNSwgMTAzLCAxMDMpXHJcbn1cclxuXHJcbi5wYXlCdG57XHJcbiAgICBiYWNrZ3JvdW5kOiAjMDk3MzhkICFpbXBvcnRhbnQ7XHJcbiAgICBib3JkZXItcmFkaXVzOiAyMHB4O1xyXG4gICAgcGFkZGluZzogMTBweCAxNXB4O1xyXG4gICAgd2hpdGUtc3BhY2U6IG5vd3JhcDtcclxuICAgIGZvbnQtc2l6ZTogMTVweDtcclxuICAgIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gICAgbWFyZ2luLXRvcDogMTVweDtcclxufVxyXG4ucGF5QnRuOmhvdmVye1xyXG4gIGJhY2tncm91bmQ6IHdoaXRlICFpbXBvcnRhbnQ7XHJcbiAgY29sb3I6IGJsYWNrO1xyXG4gIGJvcmRlci1yYWRpdXM6IDIwcHg7XHJcbiAgcGFkZGluZzogMTBweCAxNXB4O1xyXG4gIHdoaXRlLXNwYWNlOiBub3dyYXA7XHJcbiAgZm9udC1zaXplOiAxNXB4O1xyXG4gIG1hcmdpbi1sZWZ0OiAxNXB4O1xyXG4gIG1hcmdpbi10b3A6IDE1cHg7XHJcbn1cclxuIl19 */");

/***/ }),

/***/ 44557:
/*!***************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/pages/popover/popover.page.html ***!
  \***************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "default": () => (__WEBPACK_DEFAULT_EXPORT__)
/* harmony export */ });
/* harmony default export */ const __WEBPACK_DEFAULT_EXPORT__ = ("<!--\r\n  Generated template for the Popover page.\r\n\r\n  See http://ionicframework.com/docs/v2/components/#navigation for more info on\r\n  Ionic pages and navigation.\r\n-->\r\n<ion-header>\r\n  <ion-toolbar>\r\n    <ion-title><span class=\"heavy title\">PAYMENTS</span></ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content padding>\r\n  <form>\r\n    <ion-list class=\"listCardWrapper\">\r\n      <ion-item>\r\n        <ion-label fixed>Card No :</ion-label>\r\n        <ion-input type=\"text\" class=\"textcolor\" [(ngModel)]=\"addcard.cardno\" [ngModelOptions]=\"{standalone: true}\"></ion-input>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label>Expriry Date :</ion-label>\r\n        <ion-select [(ngModel)]=\"addcard.month\" [ngModelOptions]=\"{standalone: true}\">\r\n          <ion-select-option value=\"01\">01</ion-select-option>\r\n          <ion-select-option value=\"02\">02</ion-select-option>\r\n          <ion-select-option value=\"03\">03</ion-select-option>\r\n          <ion-select-option value=\"04\">04</ion-select-option>\r\n          <ion-select-option value=\"05\">05</ion-select-option>\r\n          <ion-select-option value=\"06\">06</ion-select-option>\r\n          <ion-select-option value=\"07\">07</ion-select-option>\r\n          <ion-select-option value=\"08\">08</ion-select-option>\r\n          <ion-select-option value=\"09\">09</ion-select-option>\r\n          <ion-select-option value=\"10\">10</ion-select-option>\r\n          <ion-select-option value=\"11\">11</ion-select-option>\r\n          <ion-select-option value=\"12\" checked=\"true\">12</ion-select-option>\r\n        </ion-select>\r\n        <ion-select [(ngModel)]=\"addcard.year\" [ngModelOptions]=\"{standalone: true}\">\r\n          <ion-select-option *ngFor=\"let year of years\">{{year}}</ion-select-option>\r\n        </ion-select>\r\n      </ion-item>\r\n      <ion-item>\r\n        <ion-label fixed>CVV :</ion-label>\r\n        <ion-input type=\"text\" class=\"textcolor\" [(ngModel)]=\"addcard.cvv\" maxlength=\"3\" [ngModelOptions]=\"{standalone: true}\">\r\n        </ion-input>\r\n      </ion-item>\r\n      <button ion-button class=\"payBtn\" color=\"primary\" (click)=\"payNow()\">Add</button>\r\n    </ion-list>\r\n  </form>\r\n</ion-content>\r\n");

/***/ })

}]);
//# sourceMappingURL=src_app_pages_popover_popover_module_ts.js.map