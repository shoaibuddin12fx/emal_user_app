// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDv0QzTsHlTfp0nqWK8rPlzXLjhGd2FkcY',
    authDomain: 'bidrider-app.firebaseapp.com',
    databaseURL: 'https://bidrider-app.firebaseio.com',
    projectId: 'bidrider-app',
    storageBucket: 'bidrider-app.appspot.com',
    messagingSenderId: '945081768290',
    // apiKey: 'AIzaSyBDWkVRhZhE8P0bdfuRxmrzP9ZHqyeJl9s',
    // authDomain: 'ridebidder.firebaseapp.com',
    // databaseURL: 'https://ridebidder-default-rtdb.firebaseio.com',
    // projectId: 'ridebidder',
    // storageBucket: 'ridebidder.appspot.com',
    // messagingSenderId: '348445200010',
    // appId: '1:348445200010:web:4e25cf9475e176482e2773',
    // measurementId: 'G-7LBR4N1HL1',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
