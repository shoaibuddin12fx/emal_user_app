import { Component, Injector, OnInit } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-currentbookings',
  templateUrl: './currentbookings.page.html',
  styleUrls: ['./currentbookings.page.scss'],
})
export class CurrentbookingsPage extends BasePage implements OnInit {
  curUser: any;
  userProfile: any;
  bookingList = [];

  constructor(injector: Injector, private dataService: DataService) {
    super(injector);
  }

  openActiveBooking(position) {
    //  if(this.bookingList[position].driver==null){
    //  alert("Driver not assigned");
    // }else{
    console.log(this.bookingList[position]);
    if (this.bookingList[position].status == 'Bidding Start') {
      this.dataService.data = {
        booking: this.bookingList[position],
        bookingKey: this.bookingList[position].uid,
      }
      this.nav.push('pages/bid-driver');
    } else if (this.bookingList[position].status == 'REVIEW') {
      let geoRef = this.getDb().ref('geofire');
      geoRef.child(this.bookingList[position].uid).remove();
      this.nav.push('pages/reviewdriver', {
        bookingKey: this.bookingList[position].uid,
      });
    } else {
      this.nav.push('pages/activebooking', {
        bookingKey: this.bookingList[position].uid,
      });
    }

    //  }
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    let ref = this.getDb().ref('/users/' + this.curUser.uid);
    console.log("currentBooking -> ", ref)

    ref.on('value', (snapshot: any) => {
      console.log("snapshot", snapshot.val());
      if (snapshot.val()) {
        console.log(snapshot.val());
        this.bookingList = [];
        this.zone.run(() => {
          this.userProfile = snapshot.val();

          for (let key in this.userProfile.bookings) {
            this.userProfile.bookings[key].uid = key;

            if (
              this.userProfile.bookings[key].status != 'ENDED' &&
              this.userProfile.bookings[key].status != 'CANCELLED' &&
              this.userProfile.bookings[key].customer == this.curUser.uid
            ) {
              console.log(this.userProfile.bookings[key]);
              if (this.userProfile.bookings[key].driver == null) {
                this.userProfile.bookings[key].image_url =
                  'assets/images/notfound.png';
              } else {
                this.userProfile.bookings[key].image_url =
                  this.userProfile.bookings[key].driver_image;
              }
              this.bookingList.push(this.userProfile.bookings[key]);
            }
          }

          this.bookingList.sort(function (a, b) {
            let aa: any = Number(new Date(a.tripdate));
            let bb: any = Number(new Date(b.tripdate));
            console.log(bb - aa);
            return bb - aa;
          });
        });
      }
    });
  }

  getDb() {
    return this.firebaseService.getDatabase();
  }
}
