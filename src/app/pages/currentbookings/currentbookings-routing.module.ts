import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CurrentbookingsPage } from './currentbookings.page';

const routes: Routes = [
  {
    path: '',
    component: CurrentbookingsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CurrentbookingsPageRoutingModule {}
