import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CurrentbookingsPageRoutingModule } from './currentbookings-routing.module';

import { CurrentbookingsPage } from './currentbookings.page';
import { EmptyviewModule } from 'src/app/components/emptyview/emptyview.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CurrentbookingsPageRoutingModule,
    EmptyviewModule,
  ],
  declarations: [CurrentbookingsPage],
})
export class CurrentbookingsPageModule {}
