import { Component, Injector, OnInit } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { AutocompletePage } from '../autocomplete/autocomplete.page';
import { DataService } from 'src/app/Services/data.service';
import { GeoFire } from 'geofire';
import { PaymentsPage } from '../payments/payments.page';

declare const google;
@Component({
  selector: 'app-fare',
  templateUrl: './fare.page.html',
  styleUrls: ['./fare.page.scss'],
})
export class FarePage extends BasePage implements OnInit {
  map: any;
  testRadioOpen: boolean;
  testRadioResult;
  custHeight: any;

  pickup: {
    lat: number;
    lng: number;
    add: string;
  };

  drop: {
    lat: number;
    lng: number;
    add: string;
  };

  distance: any;

  myDate: any;

  transmission: any;

  triptime: any;

  estimate: any;
  //promo_code
  promo_code: any;

  curUser: any;

  userProfile: any;
  trip: any;
  markerSetTo: any;
  bookingDetails: any;
  bookingKey: any;
  //new added by mee...
  PickupService: any;
  serviceType: any;

  personal_hour: any = 0;
  personal_minimum: any = 0;
  pickup_base_km: any = 0;
  pickup_hour: any = 0;
  pickup_km: any = 0;
  pickup_minimum: any = 0;

  /*    <plugin name="plugin.google.maps" spec="https://github.com/mapsplugin/cordova-plugin-googlemaps.git#1.3.9">
        <variable name="API_KEY_FOR_ANDROID" value="AIzaSyDS7AxBMmoeRanMxs4-VJJ87I9hMKp-d1E" />
        <variable name="API_KEY_FOR_IOS" value="AIzaSyALFJK2yQuAxdWKBFtx6k0Ktl3sfbmhg7s" />
    </plugin>*/

  //new add for datetime
  showDate: any;
  showTime: any;
  sTime: any;
  sDate: any;
  originalDate: any;

  googleTime: any;
  pickup_minute: any;
  allDriver = [];
  notificationData = [];
  refcode: any;
  customerPromocode: any;
  laterDate: any;
  currentDate: any;
  totalBill: any = 0;
  count: any = 0;
  searchBoundsTimer: any;
  params: any;

  autoChange: any;
  requestDone: boolean = false;
  mapReady: any;

  constructor(
    injector: Injector,
    public platform: Platform,
    public alertCtrl: AlertController,
    private dataService: DataService
  ) {
    super(injector);
    this.params = this.getParams();
    console.log('getParams', this.params);
    this.pickup = this.params?.pick;
    this.drop = this.params?.drop;
    console.log('Pickup -> ', this.pickup);
    console.log('Drop ->', this.drop);
    // let cDate = new Date();
    // let isoDate = cDate.toISOString();
    // this.yesterdayDate = this.formatLocalDate("yesterday").substring(0, 10);
    let cDate = this.formatLocalDate('');
    // this.laterDate = cDate;
    this.currentDate = this.formatLocalDate('cuurent').substring(0, 10);
    this.showDate = cDate.substring(0, 10);
    this.showTime = cDate.substring(11, 19);
    this.calculateDateTime();
    //new added by me..
    if (this.params?.PickupService) {
      this.zone.run(() => {
        this.PickupService = this.params.PickupService;
      });
    }
    //end
    this.mapReady = false;
    this.mapService.showmap = true;
    this.initialize();
  }

  ionViewDidLoad() {
    // this.navBar.backButtonClick = (e:UIEvent)=>{
    //   this.navCtrl.setRoot(CurrentbookingsPage);
    // }
  }

  ngDoCheck() {
    if (this.mapReady) {
      if (this.mapService.showmap) {
        this.map.setClickable(true);
      } else {
        this.map.setClickable(false);
      }
    }
  }
  ngOnInit() {
    this.platform.ready().then(() => {
      setTimeout(() => {
        // this.loadMap();
      }, 1000);
    });
  }

  async initialize() {
    let params = this.getParams();

    this.curUser = await this.firebaseService.getCurrentUser();
    this.refcode = this.getDb().ref(
      '/users/' + this.curUser.uid + '/promo_code'
    );
    this.refcode.on('value', (snapshot: any) => {
      this.customerPromocode = snapshot.val();
    });

    this.bookingKey = this.params?.bookingId;

    if (this.bookingKey) {
      this.bookingDetails = this.params?.bookingDetails;
      this.trip = this.bookingDetails.tripType;
      this.pickup = this.bookingDetails.pickup;

      if (this.bookingDetails.drop) {
        this.zone.run(() => {
          this.PickupService = true;
          this.drop = this.bookingDetails.drop;
        });
      } else {
        this.zone.run(() => {
          this.PickupService = false;
        });
      }

      this.distance = this.bookingDetails.distance;

      this.transmission = this.bookingDetails.transmission;

      this.myDate = this.bookingDetails.tripdate;
      //new changes
      this.showDate = this.bookingDetails.tripdate.substring(0, 10);
      this.showTime = this.bookingDetails.tripdate.substring(11, 19);
      //new changes end

      this.triptime = this.bookingDetails.triptime;

      this.estimate = this.bookingDetails.estimate;

      this.serviceType = this.bookingDetails.serviceType;

      if (this.bookingDetails.promo_code != undefined) {
        this.promo_code = this.bookingDetails.promo_code;
      }
    } else {
      //new added by me..
      if (this.PickupService) {
        this.trip = 'Oneway';
        this.pickup = params?.pickupDet;
        this.drop = params?.dropDet;
        this.distance = params?.distRoute;
        this.googleTime = params?.googleTime;

        this.transmission = 'Automatic';

        //this.myDate =  this.formatLocalDate();
        this.myDate = this.originalDate; //new line added

        // this.triptime = 4;
        this.triptime = params?.googleTime;
        this.serviceType = 'Pickup';
      } else {
        this.trip = 'Oneway';
        this.pickup = params?.pickupDet;
        this.drop = {
          lat: null,
          lng: null,
          add: null,
        };
        this.distance = null;

        this.transmission = 'Automatic';

        // this.myDate =  this.formatLocalDate();
        this.myDate = this.originalDate; //new line added

        this.triptime = '2';
        // this.serviceType = "Personal"; // add to Sach23
        this.serviceType = 'Personal driver'; // add to Hemant
      }
      this.changeEstimate();
    }

    var ref = this.getDb().ref('rates');

    ref.on('value', (snapshot: any) => {
      if (snapshot.val()) {
        this.personal_hour = snapshot.val().personal_hour;
        this.personal_minimum = snapshot.val().personal_minimum;
        this.pickup_base_km = snapshot.val().pickup_base_km;
        this.pickup_hour = snapshot.val().pickup_hour;
        this.pickup_km = snapshot.val().pickup_km;
        this.pickup_minimum = snapshot.val().pickup_minimum;
        this.pickup_minute = snapshot.val().pickup_minute;
        this.zone.run(() => {
          setTimeout(() => {
            this.loadMap();
          }, 1000);
          this.changeEstimate();
        });
      }
    });
  }

  setMapInfo(lat: number, lng: number, add: string) {
    this.zone.run(() => {
      if (this.markerSetTo == 'pickup') {
        this.pickup.lat = lat;
        this.pickup.lng = lng;
        this.pickup.add = add;
      } else {
        this.drop.lat = lat;
        this.drop.lng = lng;
        this.drop.add = add;
      }
    });
  }

  calculateAndDisplayRoute(lat, lng) {
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map3'), {
      zoom: 7,
      center: { lat: lat, lng: lng },
    });
    directionsDisplay.setMap(map);
    directionsService.route(
      {
        origin: this.pickup.add,
        destination: this.drop.add,
        travelMode: 'DRIVING',
      },
      function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          // window.alert('Directions request failed due to ' + status);
        }
      }
    );
  }

  animateMarker(lat, lng) {
    var map = new google.maps.Map(document.getElementById('map3'), {
      zoom: 13,
      center: { lat: lat, lng: lng },
    });

    var marker = new google.maps.Marker({
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: { lat: lat, lng: lng },
    });
  }

  loadMap() {
    let location = { lat: this.pickup.lat, lng: this.pickup.lng };

    this.map = new google.maps.Map(document.getElementById('map3'), {
      zoom: 13,
      center: location,
    });

    // this.map = new GoogleMap('map3', {
    //   backgroundColor: 'white',
    //   controls: {
    //     compass: false,
    //     myLocationButton: false,
    //     indoorPicker: false,
    //     zoom: true,
    //   },
    //   gestures: {
    //     scroll: true,
    //     tilt: true,
    //     rotate: true,
    //     zoom: true,
    //   },
    //   camera: {
    //     target: location,
    //     tilt: 30,
    //     zoom: 15, //,
    //     // 'bearing': 50
    //   },
    // });

    let info = this.setMapInfo;

    // this.map.one(GoogleMapsEvent.MAP_READY).then(() => {
    //   this.map.clear();
    //   this.mapReady = true;
    //   console.log('Map is ready!');
    //   this.markerSetTo = 'pickup';

    // });
    if (this.PickupService == true) {
      this.custHeight = '210px !important';
      this.calculateAndDisplayRoute(this.pickup.lat, this.pickup.lng);
    } else {
      this.custHeight = '280px !important';
      this.animateMarker(this.pickup.lat, this.pickup.lng);
    }
  }

  formatLocalDate(state) {
    var finalDate;
    var now = new Date(),
      tzo = -now.getTimezoneOffset(),
      dif = tzo >= 0 ? '+' : '-',
      pad = function (num) {
        var norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
      };

    if (state == 'later') {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate()) +
        'T' +
        pad(now.getHours() + 1) +
        ':' +
        pad(now.getMinutes()) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    } else if (state == 'later-com') {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate()) +
        'T' +
        pad(now.getHours()) +
        ':' +
        pad(now.getMinutes() - 2) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    } else if (state == 'yesterday') {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate() - 1) +
        'T' +
        pad(now.getHours()) +
        ':' +
        pad(now.getMinutes()) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    } else {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate()) +
        'T' +
        pad(now.getHours()) +
        ':' +
        pad(now.getMinutes()) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    }

    return finalDate;
  }

  addCard() {
    // this.nav.push('pages/payments');
    alert('open');
    this.modals.present(PaymentsPage);
  }

  async selectTransmission() {
    let alert = await this.alertCtrl.create();
    alert.title = 'Transmission Type';

    alert.inputs.push({
      type: 'radio',
      label: 'Automatic',
      value: 'Automatic',
      checked: true,
    });

    alert.inputs.push({
      type: 'radio',
      label: 'Manual',
      value: 'Manual',
      checked: false,
    });

    alert.buttons.push('Cancel');
    alert.buttons.push({
      text: 'OK',
      handler: (data) => {
        this.testRadioOpen = false;
        this.transmission = data;
        if (this.bookingKey) {
          this.bookingDetails.transmission = data;
        }
      },
    });
    alert.present();
  }

  updateDate() {
    if (this.bookingKey) {
      this.bookingDetails.tripdate = this.myDate;
    }
  }

  //triptype oneway or return
  async tripType() {
    let alert = await this.alertCtrl.create();
    alert.title = 'Journey Type';
    alert.inputs.push({
      type: 'radio',
      label: 'Oneway',
      value: 'Oneway',
      checked: true,
    });

    alert.inputs.push({
      type: 'radio',
      label: 'Return',
      value: 'Return',
      checked: false,
    });

    alert.buttons.push('Cancel');
    alert.buttons.push({
      text: 'OK',
      handler: (data) => {
        this.testRadioOpen = false;
        this.trip = data;
        if (this.bookingKey) {
          this.bookingDetails.tripType = data;
        }
      },
    });
    alert.present();
  }

  async showAddressModal(addressType) {
    if (this.bookingKey) {
      const modal = await this.modals.present({
        component: AutocompletePage,
      });
      const { data } = await modal.onDidDismiss();
      if (data != null) {
        if (addressType == 'pickup') {
          this.pickup.add = data.add;
          this.pickup.lat = data.lat;
          this.pickup.lng = data.lng;
          if (this.bookingKey) {
            this.bookingDetails.pickup = this.pickup;
          }
          if (this.PickupService) {
            this.setDistance(this.pickup, this.drop);
            this.changeEstimate();
          }
        } else {
          this.drop.add = data.add;
          this.drop.lat = data.lat;
          this.drop.lng = data.lng;
          if (this.bookingKey) {
            this.bookingDetails.drop = this.drop;
          }
          this.setDistance(this.pickup, this.drop);
          this.changeEstimate();
        }
      }
      modal.present();
    }
  }

  //new changes
  timeDistance: any;
  setDistance(pick, drop) {
    var directionsService = new google.maps.DirectionsService();
    let routeDistance: number;
    directionsService.route(
      {
        origin: pick,
        destination: drop,
        travelMode: 'DRIVING',
      },
      (response, status) => {
        if (status === 'OK') {
          this.distance = response.routes[0].legs[0].distance.value;
          if (this.bookingKey) {
            this.bookingDetails.distance = this.distance;
            this.timeDistance = response.routes[0].legs[0].duration.value / 60;
            this.googleTime = this.timeDistance.toFixed();
            this.triptime = this.googleTime;
          }

          this.changeEstimate();
        } else {
          //window.alert('Directions request failed due to ' + status);
        }
      }
    );
  }

  newPromoCode: any;
  addPromo() {
    if (this.bookingKey) {
      let booking = this.getDb().ref('bookings/' + this.bookingKey);
      booking.once('value', async (snapshot: any) => {
        if (snapshot.val()) {
          if (snapshot.val().promo_code) {
            //alert("Promo Code Already Added");

            let promocodeadd = await this.alertCtrl.create({
              header: 'Alert',
              subHeader: 'Promo Code Already Added',
              buttons: ['OK'],
            });
            promocodeadd.present();
          } else {
            this.showPromopopup();
          }
        }
      });
    } else {
      this.showPromopopup();
    }

    /* if(this.bookingKey){
      let booking = firebase.database().ref('bookings/' + this.bookingKey) ;
      booking.once('value',(snap1:any)=>{
        if(snap1.val()){
          if(snap1.val().promo_code){
              this.newPromoCode = snap1.val().promo_code;
              alert("you already added One Promo Code");
          }else{
            this.showPromopopup();
          }
        }
      })
    }else{
    this.showPromopopup();
    }*/
  }
  codeFound: boolean;

  async showPromopopup() {
    let prompt = await this.alertCtrl.create({
      header: 'Add Promo Code',
      subHeader:
        'Promo Code wiil be applied in your bill after End of your Trip',
      inputs: [
        {
          name: 'title',
          placeholder: 'Code',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data) => {},
        },
        {
          text: 'Save',
          handler: (data) => {
            let pCode = data.title;
            let promoRef = this.getDb().ref('promoCodes');
            promoRef.once('value', async (snapData: any) => {
              if (snapData.val()) {
                let codes = snapData.val();
                let foundPromo = true;
                for (let promokey in codes) {
                  if (pCode == codes[promokey].promoName) {
                    foundPromo = false;
                    if (this.customerPromocode) {
                      let foundCust_Promo = true;
                      for (let matchcode in this.customerPromocode) {
                        if (
                          pCode == this.customerPromocode[matchcode].promoName
                        ) {
                          // foundCust_Promo = true;
                        } else {
                          foundCust_Promo = false;
                          this.codeFound = codes[promokey];
                          if (this.bookingDetails) {
                            this.bookingDetails.promo_code = codes[promokey];
                          }
                          let promo = await this.alertCtrl.create({
                            header: 'Alert',
                            subHeader: 'promo Code Added Successfully.',
                            buttons: ['OK'],
                          });
                          promo.present();
                        }
                        if (foundCust_Promo) {
                          let promo1 = await this.alertCtrl.create({
                            header: 'promo1',
                            subHeader: 'Promo Code Already Added.',
                            buttons: ['OK'],
                          });
                          promo1.present();
                        }
                      }
                    } else {
                      this.codeFound = codes[promokey];
                      if (this.bookingDetails) {
                        this.bookingDetails.promo_code = codes[promokey];
                      }
                      let promo = await this.alertCtrl.create({
                        header: 'Alert',
                        subHeader: 'promo Code Added Successfully.',
                        buttons: ['OK'],
                      });
                      promo.present();
                    }
                  }
                }
                if (foundPromo) {
                  let nopromocode = await this.alertCtrl.create({
                    header: 'Promocode',
                    subHeader: 'Promo Code not found',
                    buttons: ['OK'],
                  });
                  nopromocode.present();
                }
              } else {
                let nopromocode = await this.alertCtrl.create({
                  header: 'Promocode',
                  subHeader: 'Promo Code not found',
                  buttons: ['OK'],
                });
                nopromocode.present();
              }
            });
          },
        },
      ],
    });
    prompt.present();
  }

  drivermap: boolean = true;

  async changeEstimate() {
    this.count += 1;
    // var totalBill;
    if (this.PickupService) {
      let totalDistance = this.distance / 1000;
      // var pickup_base = '28';
      let billableKms = 0;
      if (totalDistance > parseFloat(this.pickup_base_km)) {
        billableKms = totalDistance - parseFloat(this.pickup_base_km);
      }

      //let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (parseInt(this.triptime) * parseInt(this.pickup_hour));
      // let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (parseInt(this.triptime) * parseInt(this.pickup_minute));
      this.totalBill =
        parseFloat(this.pickup_minimum) +
        billableKms * parseFloat(this.pickup_km) +
        parseFloat(this.triptime) * parseFloat(this.pickup_minute);
      // if(this.totalBill < parseFloat(this.pickup_minimum)){
      //      this.totalBill = parseFloat(this.pickup_minimum);
      // }

      this.estimate = Math.round(this.totalBill * 100) / 100;
    } else {
      this.totalBill =
        parseFloat(this.personal_minimum) +
        parseFloat(this.triptime) * parseFloat(this.personal_hour);

      this.estimate = Math.round(this.totalBill * 100) / 100;
    }

    if (this.bookingKey) {
      this.bookingDetails['triptime'] = this.triptime;

      this.bookingDetails.estimate = this.estimate;
    }
    if (isNaN(this.totalBill) && this.count >= 2) {
      let alert = await this.alertCtrl.create({
        header: 'Inavlid destination',
        subHeader: 'Please select a valid drop location',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: (data) => {
              this.nav.pop();
            },
          },
        ],
      });
      alert.present();
    }
  }

  //dateTime workflow
  dateShow() {
    // this.sDate = this.showDate.substring(0, 10);
    this.sDate = this.showDate;
    this.calculateDateTime();
  }
  timeShow() {
    // this.sTime = this.showTime.substring(11, 19);
    this.sTime = this.showTime;
    this.calculateDateTime();
  }

  calculateDateTime() {
    //this.originalDate = this.sDate + 'T' + this.sTime + '.000Z';
    this.originalDate = this.showDate + 'T' + this.showTime + '.000Z';
    console.log('calculate datetime =>' + this.originalDate);
    // this.bookingDetails.tripdate = this.originalDate;

    /*    + '-' + pad(now.getMonth()+1)
        + '-' + pad(now.getDate())
        + 'T' + pad(now.getHours())
        + ':' + pad(now.getMinutes())
        + ':' + pad(now.getSeconds())
        + '.000Z'*/
  }

  /* requestDriver(){
    console.log(this.myDate);
    this.confirmBooking();
  }*/

  //open alert
  async showAlert(msg) {
    let alert = await this.alertCtrl.create({
      header: 'ALERT',
      subHeader: msg,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ],
    });
    alert.present();
  }

  requestDriver() {
    // let selectedTime = new Date(this.showDate +" "+this.showTime);
    let selectedTime = this.showDate + 'T' + this.showTime + '.000Z';
    // let currentTime = new Date();
    console.log(selectedTime);
    let timeNow = this.formatLocalDate('later-com');
    if (Number(new Date(timeNow)) > Number(new Date(selectedTime))) {
      this.showAlert('You cannot book driver for past time.');
      return false;
    }

    // console.log(this.laterDate);
    //let cDate = this.formatLocalDate("later");
    // console.log(afterTwohr <= selectedTime);

    // if (afterTwohr > selectedTime) {
    //   this.showAlert("Please select time after 1 hours from now.");
    //   return false;
    // }
    //return false;
    let fireRef = this.getDb().ref('users/' + this.curUser.uid);
    fireRef.once('value', (snapdetails: any) => {
      if (snapdetails.val()) {
        if (snapdetails.val().payment_methods) {
          this.confirmBooking();
        } else {
          let sndmsg = 'Please add a valid credit card';
          this.showAlert(sndmsg);
        }
      }
    });
  }

  async bookingConfirmAlert() {
    this.requestDone = true;
    let alert = await this.alertCtrl.create({
      header: 'ALERT',
      subHeader:
        'Your booking is scheduled for ' +
        this.showTime +
        '. You will receive a notification when a driver is assigned.',
      buttons: [
        {
          text: 'OK',
          handler: (data) => {
            this.Notification();
            // this.navCtrl.setRoot(CurrentbookingsPage);
          },
        },
      ],
    });
    alert.present();
  }

  confirmBooking() {
    if (this.codeFound) {
      this.newPromoCode = this.codeFound;
      // this.bookingDetails.promo_code = this.codeFound;
      console.log('promo code found : => ' + this.newPromoCode);
    } else {
      this.newPromoCode = null;
      // this.bookingDetails.promo_code = null ;
      console.log('promo code not found : => ' + this.newPromoCode);
    }

    if (this.bookingKey) {
      if (this.originalDate) {
        this.bookingDetails.tripdate = this.originalDate;
      } else {
        this.bookingDetails.tripdate = this.bookingDetails.tripdate;
      }

      if (this.bookingDetails.promo_code) {
        this.bookingDetails.promo_code = this.bookingDetails.promo_code;
      }

      if (this.newPromoCode) {
        console.log(
          'edit time call promocode required.........',
          this.newPromoCode
        );
        this.bookingDetails.promo_code = this.newPromoCode;
        let ref = this.getDb().ref('/users/' + this.curUser.uid);
        ref.once('value', (snap1: any) => {
          console.log('edit time call promocode *********.........');
          let addpromocode = ref.child('promo_code').push().key;
          ref.child('promo_code/' + addpromocode).set(this.newPromoCode);
        });
      } else {
        this.bookingDetails.promo_code = null;
      }

      let ref = this.getDb().ref('bookings/' + this.bookingKey);

      ref.set(this.bookingDetails);

      let updates = {};

      if (this.bookingDetails.driver) {
        //new created by me.. part2
        if (this.PickupService) {
          let customerData = {
            customer: this.bookingDetails.customer,
            driver: this.bookingDetails.driver,
            driver_name: this.bookingDetails.driver_name,
            driver_image: this.bookingDetails.driver_image,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate,
            customer_push_token: this.bookingDetails.customer_push_token,
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };

          let driverData = {
            customer: this.bookingDetails.customer,
            customer_name: this.bookingDetails.customer_name,
            customer_image: this.bookingDetails.customer_image,
            driver: this.bookingDetails.driver,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop.add,
            customer_push_token: this.bookingDetails.customer_push_token,
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.driver +
              '/bookings/' +
              this.bookingKey
          ] = driverData;
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        } else {
          let customerData = {
            customer: this.bookingDetails.customer,
            driver: this.bookingDetails.driver,
            driver_name: this.bookingDetails.driver_name,
            driver_image: this.bookingDetails.driver_image,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            // dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate,
            customer_push_token: this.bookingDetails.customer_push_token,
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };

          let driverData = {
            customer: this.bookingDetails.customer,
            customer_name: this.bookingDetails.customer_name,
            customer_image: this.bookingDetails.customer_image,
            driver: this.bookingDetails.driver,
            status: this.bookingDetails.status,
            //pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate,
            customer_push_token: this.bookingDetails.customer_push_token,
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.driver +
              '/bookings/' +
              this.bookingKey
          ] = driverData;
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        }
      } else {
        if (this.PickupService) {
          let customerData = {
            customer: this.bookingDetails.customer,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate,
            tripType: this.trip,
            customer_push_token: this.bookingDetails.customer_push_token,
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        } else {
          let customerData = {
            customer: this.bookingDetails.customer,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            // dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate,
            tripType: this.trip,
            customer_push_token: this.bookingDetails.customer_push_token,
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        }
      }

      console.log(updates);

      this.getDb().ref().update(updates);
      // this.bookingConfirmAlert();
      this.nav.setRoot('pages/currentbookings');
      // this.navCtrl.setRoot(BidDriverPagePage,{
      //                         from:this.bookingDetails.pickup.add,
      //                         to:this.bookingDetails.drop.add,
      //                         pickupservice:this.bookingDetails.serviceType
      //                     });
    } else {
      console.log(
        'first time booking........................',
        this.newPromoCode
      );
      let ref = this.getDb().ref('/users/' + this.curUser.uid);

      ref.once('value', (snap1: any) => {
        if (snap1.val() != null) {
          this.userProfile = snap1.val();

          var shortBookingData = {
            // tripdate:this.myDate,
            tripdate: this.originalDate,
            pickupAddress: this.pickup.add,
            dropAddress: this.drop.add,
            customer: this.curUser.uid,
            tripType: this.trip,
            // status:'NEW' , //added by Sach23
            status: 'Finding your driver', // added by Hemant
            customer_push_token: this.userProfile.pushToken ?? '',
            serviceType: this.serviceType,
            // promo_code:this.newPromoCode   //new added for promo code
          };

          var bookingData = {
            customer: this.curUser.uid,
            customer_name: this.userProfile.fullname,
            customer_push_token: this.userProfile.pushToken ?? '',
            pickup: this.pickup,
            drop: this.drop,
            distance: this.distance,
            // tripdate:this.myDate,
            tripdate: this.originalDate,
            triptime: this.triptime,
            transmission: this.transmission,
            estimate: this.estimate,
            tripType: this.trip,
            // status:'NEW' , //added by Sach23
            status: 'Finding your driver', // added by Hemant
            serviceType: this.serviceType, //new created by me....
            promo_code: this.newPromoCode, //new added for promo code
          };
          if (this.userProfile.profile_image)
            bookingData['customer_image'] = this.userProfile.profile_image;

          let newBooking = ref.child('bookings').push().key;
          let addpromocode = ref.child('promo_code').push().key;
          console.log('bookingData', bookingData);

          this.getDb()
            .ref('bookings/' + newBooking)
            .set(bookingData);
          ref.child('bookings/' + newBooking).set(shortBookingData);
          ref.child('promo_code/' + addpromocode).set(this.newPromoCode);
          //promo code
          /*  if(this.newPromoCode!=undefined || this.newPromoCode!=null){
                firebase.database().ref('bookings/' + newBooking).child('/promo_code').set(this.newPromoCode);
            }
          */
          //end
          var dbref = this.getDb().ref();
          var geoFire = new GeoFire(dbref.child('geofire'));

          geoFire.set(newBooking, [this.pickup.lat, this.pickup.lng]).then(
            function () {
              console.log('Provided key has been added to GeoFire');
            },
            function (error) {
              console.log('Error: ' + error);
            }
          );
          this.bookingConfirmAlert();
          // this.navCtrl.setRoot(CurrentbookingsPage);
        }
      });
    }
  }

  tapEvent(e) {
    console.log(this.showDate);
    // this.dateShow();
  }
  tapEventT(e) {
    console.log(this.showTime);
    // this.timeShow();
  }

  date() {
    console.log('my date format is' + this.myDate);
  }

  ionViewWillEnter() {
    this.allDriver = [];
    this.notificationData = [];
    console.log('ionViewDidLoad FarePage');
    /*------------------------------ Push Token Save -----------------------------------*/
    let refToken = this.getDb().ref('users');
    refToken.on('value', (snapshot) => {
      this.allDriver = snapshot.val();
      this.notificationData = [];
      for (let key in this.allDriver) {
        if (
          this.allDriver[key].islogin == 'driver' &&
          this.allDriver[key].active == true &&
          this.allDriver[key].pushToken
        ) {
          // if(this.allDriver[key].islogin == 'driver' && this.allDriver[key].pushToken && this.allDriver[key].email == 'bhavesh9562@gmail.com' ){
          this.notificationData.push({ token: this.allDriver[key].pushToken });
          console.log(
            'ionViewDidLoad notificationData...............',
            this.notificationData
          );
        }
      }
    });
    /*------------------------------ Push Token Save -----------------------------------*/
  }

  // NotificationDriver(){
  //     let alert = this.alertCtrl.create({
  //       title: 'ALERT',
  //       subTitle: 'You will receive a notification when a driver is assigned.',
  //       buttons: [
  //        {
  //           text: 'OK',
  //           handler: data => {
  //             this.Notification();
  //           }
  //         }
  //         ]
  //     });
  //     alert.present();

  // }
  Notification() {
    // console.log("notification...................", this.notificationData);
    var cnt = 0;
    for (let key in this.notificationData) {
      cnt++;
      this.usersService
        .sendPushDriver(
          'Customer Booking',
          this.pickup.add,
          this.notificationData[key].token
        )
        .subscribe((response) => {
          console.log('response........', response);
          if (this.notificationData.length == cnt) {
            this.notificationData = [];
            cnt = 0;
            // this.navCtrl.setRoot(BidDriverPagePage,{
            //                       from:this.pickup.add,
            //                       to:this.drop.add,
            //                       pickupservice:this.serviceType
            //                   });
          }
        });
    }
    this.nav.setRoot('pages/currentbookings');
  }
  getDb() {
    return this.firebaseService.getDatabase();
  }

  getParams() {
    let data = this.dataService.farePageData;
    console.log('getParams', data);

    return data;
  }
}
