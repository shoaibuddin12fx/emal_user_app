import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FarePageRoutingModule } from './fare-routing.module';

import { FarePage } from './fare.page';
import { HeaderModule } from 'src/app/components/header/header.component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FarePageRoutingModule,
    HeaderModule

  ],
  declarations: [FarePage]
})
export class FarePageModule { }
