import { Component, Injector, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { DataService } from 'src/app/Services/data.service';
import { BookNowComponent } from '../home/book-now/book-now.component';
declare const google;
@Component({
  selector: 'app-map',
  templateUrl: './map.page.html',
  styleUrls: ['./map.page.scss'],
})
export class MapPage extends BasePage implements OnInit, AfterViewInit {

  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  item: any;

  driver: 'driver';
  customer: 'customer';
  showCustomer: boolean;
  showDriver: boolean;
  usertype: any;
  accptCustomar: any;
  isClickable = false;

  pickup: {
    lat: number;
    lng: number;
    add: string;
  };


  distance: number;

  curUser: any;

  markerSetTo: any;

  that = this;

  searchBoundsTimer: any;

  autoChange: any;

  mapReady: any;


  jobtype: string;

  constructor(injector: Injector, public dataService: DataService) {
    super(injector);

    //      const options: PushOptions = {
    //    android: {},
    //    ios: {
    //        alert: 'true',
    //        badge: true,
    //        sound: 'false'
    //    },
    //    windows: {},
    //    browser: {
    //        PushOptions: 'http://push.api.phonegap.com/v1/push'
    //    }
    // };
    //                             const pushObject: PushObject = this.push.init(options);

    // pushObject.on('notification').subscribe((notification: any) => console.log('Received a notification', notification));

    //new created by me

    this.jobtype = 'pickup';
    let params = this.getQueryParams();
    //end
    if (params?.add == undefined || params?.add == null) {
      // call map here for assign pin location to map shown

      // this.nav.setRoot('pages/locationloader');
    }


    this.mapReady = false;
    this.mapService.showmap = true;

    this.pickup = {
      lat: params?.lat,
      lng: params?.lng,
      add: params?.add,
    };

  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
  }

  openBookNow() {
    this.modals.present(BookNowComponent, {}, 'half-modal')
  }

  ngAfterViewInit() {

    // this.platform.ready().then(() => {
    //   this.initializeMapBeforeSetCoordinates().then(v => {
    //     this.initMap(v);

    //     setTimeout(async () => {


    //       const active = localStorage.getItem('google_map');
    //       if (!active) {
    //         const flag = await this.utility.presentConfirm('Thanks', 'Remind Later', 'How To Use', 'In the search field, type in the new address and press enter. You can also hold your finger and drag to new location, then press the + symbol in the top right corner');
    //         localStorage.setItem('google_map', `${flag}`);

    //       }else{
    //         this.modals.present(BookNowComponent, {}, 'half-modal')
    //       }



    //     }, 1000);

    //   });
    // });
  }

  async initializeMapBeforeSetCoordinates() {

    return new Promise(async resolve => {
      console.log("initializeMapBeforeSetCoordinates");
      const mylocation = await this.utility.getCurrentLocationCoordinates();
      console.log("My Location", mylocation)
      resolve(mylocation)
    })


  }

  initMap(val) {


  }

  //showdata:any;
  //logout related changes
  ngDoCheck() {
    if (this.mapReady) {
      if (this.mapService.showmap) {
        this.setIsClickable(true);
      } else {
        this.setIsClickable(false);
      }
    }
  }

  // ionViewWillEnter() {
  //   console.log('in');
  //   //ngOnInit(){
  //   this.platform.ready().then(() => {
  //     setTimeout(() => {
  //       this.loadMap();
  //     }, 1000);
  //   });
  // }

  loc: any;
  // currentLocation() {
  //   this.geoLocation.getCurrentPosition().then(
  //     (position) => {
  //       let geocoder = new google.maps.Geocoder();
  //       this.loc = {
  //         lat: position.coords.latitude,
  //         lng: position.coords.longitude,
  //       };
  //       let request = {
  //         latLng: this.loc,
  //       };

  //       this.autoChange = false;
  //       this.markerSetTo = 'pickup';
  //       this.map.setCenter(position);

  //       this.geocodeAddress(
  //         position.coords.latitude,
  //         position.coords.longitude,
  //         (data) => {
  //           //  this.pickup.add = data.formatted_address;  // this.pickup.lat = data.geometry.location.lat();  // this.pickup.lng = data.geometry.location.lng();
  //           this.setMapInfo(
  //             data.geometry.location.lat(),
  //             data.geometry.location.lng(),
  //             data.formatted_address
  //           );
  //           if (this.pickup.add && this.drop.add)
  //             this.setDistance(this.pickup, this.drop);
  //           console.log('data loaded');
  //         }
  //       );
  //     },
  //     (error) => {
  //       console.log('Error getting location', error);
  //     }
  //   );
  // }

  setMapInfo(lat: number, lng: number, add: string) {
    this.zone.run(() => {
      if (this.markerSetTo == 'pickup') {
        this.pickup.lat = lat;
        this.pickup.lng = lng;
        this.pickup.add = add;
        console.log('pickup : ');
        console.log(this.pickup);
      } else {
        // this.drop.lat = lat;
        // this.drop.lng = lng;
        // this.drop.add = add;
        // console.log('drop : ');
        // console.log(this.drop);
      }
    });
  }

  async loadMap() {
    //  const mylocation = await this.utility.getCurrentLocationCoordinates();
    // console.log("My Location", mylocation)
    let params = this.getQueryParams();
    const myLocation = { lat: parseFloat(params?.lat), lng: parseFloat(params.lng) }

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 15,
      center: myLocation
    });

    // this.map = new GoogleMap('map', {
    //   backgroundColor: 'white',
    //   controls: {
    //     compass: false,
    //     myLocationButton: false,
    //     indoorPicker: false,
    //     zoom: true,
    //   },
    //   gestures: {
    //     scroll: true,
    //     tilt: true,
    //     rotate: true,
    //     zoom: true,
    //   },
    //   camera: {
    //     target: location,
    //     tilt: 30,
    //     zoom: 15, //,
    //     // 'bearing': 50
    //   },
    // });

    let info = this.setMapInfo;
    let self = this;

    // * CAMERA_CHANGE * //

    google.maps.event.addListener(this.map, 'dragend', ((event) => {
      if (!event) return;
      const lt = event.latLng.lat();
      const lg = event.latLng.lng();
      const coords = { lat: lt, lng: lg };
      if (this.autoChange == true) {
        clearTimeout(this.searchBoundsTimer);

        this.searchBoundsTimer = setTimeout(() => {
          this.geocodeAddress(lt, lg, (data) => {
            this.setMapInfo(
              data.geometry.location.lat(),
              data.geometry.location.lng(),
              data.formatted_address
            );
            // if (this.pickup.add && this.drop.add)
            //   this.setDistance(this.pickup, this.drop);
            console.log('map data: ');
            console.log(data);
          });
        }, 500);
      } else {
        this.autoChange = true;
      }

    }));

    this.map.addListener("click", (mapsMouseEvent) => {
      let position = JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2)
      console.log("OnMapClicked", position);

    });

    // this.map.on(GoogleMapsEvent.CAMERA_MOVE).subscribe((res) => {
    //   if (this.autoChange == true) {
    //     clearTimeout(this.searchBoundsTimer);

    //     this.searchBoundsTimer = setTimeout(() => {
    //       this.geocodeAddress(res.target.lat, res.target.lng, (data) => {
    //         this.setMapInfo(
    //           data.geometry.location.lat(),
    //           data.geometry.location.lng(),
    //           data.formatted_address
    //         );
    //         if (this.pickup.add && this.drop.add)
    //           this.setDistance(this.pickup, this.drop);
    //         console.log('map data: ');
    //         console.log(data);
    //       });
    //     }, 500);
    //   } else {
    //     this.autoChange = true;
    //   }
    // });

    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      // do something only the first time the map is loaded
      this.map?.markers.clear();
      this.mapReady = true;
      console.log('Map is ready!');
      this.markerSetTo = 'pickup';
    });

  }

  geocodeAddress(lat, lng, callback) {
    let geocoder = new google.maps.Geocoder();
    let latlng = { lat: lat, lng: lng };
    let request = {
      latLng: latlng,
    };
    geocoder.geocode({ latLng: latlng }, function (data, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        callback(data[0]);
      } else {
        console.log('Geocode Error');
      }
    });
  }



  timeDistance: any;
  googleTime: any;
  setDistance(pick, drop) {
    var directionsService = new google.maps.DirectionsService();
    let routeDistance: number;
    directionsService.route(
      {
        origin: pick,
        destination: drop,
        travelMode: 'DRIVING',
      },
      (response, status) => {
        if (status === 'OK') {

          this.distance = response.routes[0].legs[0].distance.value;
          this.timeDistance = response.routes[0].legs[0].duration.value / 60;
          this.googleTime = this.timeDistance.toFixed();
        } else {
          // window.alert('Directions request failed due to ' + status);
        }
      }
    );
  }

  // personalService() {
  //   this.zone.run(() => {
  //     this.PickupService = false;
  //   });
  // }

  // pickupService() {
  //   this.zone.run(() => {
  //     this.PickupService = true;
  //   });
  // }

  async showAlert(msg) {
    this.setIsClickable(false);
    this.mapService.showmap = false;
    let alert = await this.alertCtrl.create({
      header: 'ALERT',
      subHeader: msg,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
          handler: () => {
            this.setIsClickable(true);
            this.mapService.showmap = true;
          },
        },
      ],
    });
    alert.present();
  }

  // bookNow() {
  //   //new added by me
  //   console.log(this.PickupService);
  //   console.log("Pickup", this.pickup);
  //   if (this.PickupService == true) {
  //     //new change
  //     // this.map.remove();
  //     if (this.pickup.add && this.drop.add) {
  //       console.log("Pickup", this.pickup);
  //       this.dataService.farePageData = {
  //         pickupDet: this.pickup,
  //         dropDet: this.drop,
  //         distRoute: this.distance,
  //         PickupService: this.PickupService,
  //         googleTime: this.googleTime,
  //       }
  //       this.nav.push('pages/fare');
  //     } else if (!this.drop.add) {
  //       let showMsg = 'please select destination';
  //       this.showAlert(showMsg);
  //     }
  //   } else {
  //     if (this.pickup.add) {
  //       //this.map.remove();
  //       this.nav.push('pages/fare', {
  //         pickupDet: this.pickup,
  //         PickupService: this.PickupService,
  //       });
  //     }
  //   }
  // }

  ngOnDestroy() {
    console.log('page destroy successfully');
    if (this.map != undefined) {
      this.map.remove();
    }
  }

  setIsClickable(val) {
    this.isClickable = val;
  }

  getDbRef(ref) {
    return this.firebaseService.getDatabase().ref(ref);
  }
}
