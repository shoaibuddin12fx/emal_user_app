import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MapPageRoutingModule } from './map-routing.module';

import { MapPage } from './map.page';
import { BookNowModule } from '../home/book-now/book-now.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BookNowModule,
    MapPageRoutingModule
  ],
  declarations: [MapPage]
})
export class MapPageModule { }
