import { Component, Injector, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AlertController, ModalController } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { TermsPage } from '../terms/terms.page';
// import { Facebook, FacebookLoginResponse } from '@ionic-native/facebook/ngx';
import firebase from '@firebase/app-compat';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage extends BasePage implements OnInit {
  email: any = '';
  password: any = '';

  testRadioOpen: boolean;
  testRadioResult;
  user: any;
  /*    <plugin name="cordova-plugin-googleplus" spec="~5.1.1">
        <variable name="REVERSED_CLIENT_ID" value="com.googleusercontent.apps.735130465689-2r35luq5cqbmq47animao1neuscedd2t" />
    </plugin>*/
  //private oauth: OauthCordova = new OauthCordova();

  constructor(
    injector: Injector,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
    private googlePlus: GooglePlus //   private fb: Facebook
  ) {
    super(injector);

    /*  if(this.navParams.get('login_type')){
                      console.log(this.navParams.get('login_type'));
                      this.connectlogin(this.navParams.get('login_type'));
                  }*/
    /*  if(this.navParams.get('terms')){
                      this.navCtrl.push(RegistercustomerPage);
                  }*/
  }

  ionViewDidLoad() {
    console.log('Login Page');
  }

  public async onSignInFaild() {
    let alert = await this.alertCtrl.create({
      header: 'Login',
      subHeader: 'Email or Password is Invalid',
      buttons: ['OK'],
    });
    alert.present();
  }

  private async detailsFailed() {
    let alert = await this.alertCtrl.create({
      header: 'Login',
      subHeader: 'Please Enter Valid Details',
      buttons: ['OK'],
    });
    alert.present();
  }

  doLogin() {
    if (
      this.email == undefined ||
      this.password == undefined ||
      this.email == '' ||
      this.password == ''
    ) {
      this.detailsFailed();
    } else {
      this.usersService
        .loginUser(this.email, this.password)
        .then(() => {
          this.events.publish('login_event');
          this.nav.setRoot('pages/locationloader');
        })
        .catch((_error) => this.onSignInFaild());
    }
  }

  async doRegistration(login_type) {
    // this.navCtrl.push(RegistercustomerPage);
    // this.navCtrl.setRoot(TermsPage);
    // let profileModal = this.modals.present(TermsPage,{type:"basic"});
    // profileModal.present();
    console.log('presengint modal');
    var profileModal = await this.modalCtrl.create({
      component: TermsPage,
      componentProps: { login: login_type },
    });
    profileModal.present();
    const { data } = await profileModal.onDidDismiss();
    console.log('login type is : ', data);
    if (data?.param === 'basic') {
      this.nav.push('pages/registercustomer');
    }
  }

  connectlogin(login_type) {
    let provider: any;
    this.usersService.loginType = login_type;
    if (login_type != 'Facebook') {
      // this.fb.logout().then((response) => {
      //   console.log(response);
      // });
      // this.fb
      //   .login(['email'])
      //   .then((response) => {
      //     let res: any;
      //     let credential: any;
      //     res = response;
      //     console.log('no: -> 1');
      //     console.log(response);
      //     this.getDb().ref;
      //     credential = firebase.auth.FacebookAuthProvider.credential(
      //       response.authResponse.accessToken
      //     );
      //     console.log('no: -> 2');
      //     console.log(credential);
      //     this.firebaseService.fireAuth
      //       .signInWithCredential(credential)
      //       .then((success) => {
      //         console.log('no: -> 3');
      //         console.log(success);
      //         this.user = success.user;
      //         let ref = this.getDb().ref('/users/' + success.user.uid);
      //         ref.child('fullname').set(success.user.displayName);
      //         ref.child('email').set(success.user.email);
      //         ref.child('profile_image').set(success.user.photoURL);
      //         ref.child('authToken').set(response.authResponse.accessToken);
      //         ref.child('customer').set(true);
      //       })
      //       .catch(async (error) => {
      //         console.log(error);
      //         let alert = await this.alertCtrl.create({
      //           header: 'Alert',
      //           subHeader:
      //             'Sign In Failed. You may have previous account with same email id.',
      //           buttons: ['OK'],
      //         });
      //         alert.present();
      //       });
      //   })
      //   .catch(async (error) => {
      //     console.log(error);
      //     let facebookAlrt = await this.alertCtrl.create({
      //       header: 'ALERT',
      //       subHeader: 'fail to login',
      //       buttons: ['OK'],
      //     });
      //     facebookAlrt.present();
      //   });
    } else {
      console.log('login with google...');
      // GooglePlus.login({'webClientId':'735130465689-o89h3d2gsahss4h9atf24o2vcqrmjh2o.apps.googleusercontent.com'}).then( (response) => {
      //   console.log("login with google access token...");
      //   var provider = firebase.auth.GoogleAuthProvider.credential(response.idToken);
      //   console.log(response);
      //   console.log(provider);
      //   firebase.auth().signInWithCredential(provider)
      //                 .then((success) => {
      //                     console.log(success);
      //                         this.user = success;
      //                         let ref = firebase.database().ref('/users/' + success.uid);
      //                         ref.child('fullname').set(response.displayName);
      //                         ref.child('email').set(success.email);
      //                         ref.child('profile_image').set(response.imageUrl);
      //                         ref.child('authToken').set(response.idToken);
      //                         ref.child('customer').set(true);
      //                 })
      //                 .catch((error) => {
      //                     console.log(error);
      //                     let alert = this.alertCtrl.create({
      //                         title: 'Login',
      //                         subTitle: 'Sign In Failed. You may have previous account with same email id.',
      //                         buttons: ['OK']
      //                         });
      //                     alert.present();
      //                 });
      //   })
      // .catch((error)=>{
      //   let openAlrt = this.alertCtrl.create({
      //       title: 'ALERT',
      //       subTitle:'fail to login',
      //       buttons: ['OK']
      //   });
      //   openAlrt.present();
      // })

      this.googlePlus
        .login({
          webClientId:
            '945081768290-im80pg5742lk2b1sh9rt256q7os1238p.apps.googleusercontent.com',
          offline: true,
        })
        .then((response) => {
          console.log('login detail', response);

          var provider = firebase.auth.GoogleAuthProvider.credential(
            response.idToken
          );
          this.firebaseService.fireAuth
            .signInWithCredential(provider)
            .then(async (success) => {
              console.log(success);
              this.user = success.user;
              let ref = this.getDb().ref('/users/' + success.user.uid);
              ref.child('fullname').set(success.user.displayName);
              ref.child('email').set(success.user.email);
              ref.child('profile_image').set(success.user.photoURL);
              ref.child('authToken').set(await success.user.getIdToken());
              ref.child('customer').set(true);
            })
            .catch(async (error) => {
              console.log(error);
              let alert = await this.alertCtrl.create({
                header: 'Login',
                subHeader:
                  'Sign In Failed. You may have previous account with same email id.',
                buttons: ['OK'],
              });
              alert.present();
            });
        })
        .catch(async (err) => {
          console.error(err);
          let openAlrt = await this.alertCtrl.create({
            header: 'ALERT',
            subHeader: 'fail to login',
            buttons: ['OK'],
          });
          openAlrt.present();
        });
    }

    //   this.googlePlus.logout().then(response =>{
    //        console.log(response);
    //   });
  }

  async loginSocial(login_type) {
    console.log(login_type);
    let profileModal = await this.modals.modal.create({
      component: TermsPage,
      componentProps: { login: login_type },
    });

    console.log('created');
    profileModal.present();
    const { data } = await profileModal.onDidDismiss();
    this.connectlogin(data);

    /*   let provider:any;
      if(login_type=="Facebook"){

            Facebook.login(['email']).then( (response) => {
                let res:any;
                let credential:any;

                res = response;
                console.log("no: -> 1");
                console.log(response);

                credential = firebase.auth.FacebookAuthProvider.credential(response.authResponse.accessToken);
                console.log("no: -> 2");
                console.log(credential);

                firebase.auth().signInWithCredential(credential)
                .then((success) => {
                    console.log("no: -> 3");
                    console.log(success);
                        this.user = success;
                        let ref = firebase.database().ref('/users/' + success.uid);
                        ref.child('fullname').set(success.displayName);
                        ref.child('email').set(success.email);
                        ref.child('profile_image').set(success.photoURL);
                        ref.child('authToken').set(response.authResponse.accessToken);
                        ref.child('customer').set(true);
                })
                .catch((error) => {
                    console.log(error);
                    let alert = this.alertCtrl.create({
                        title: 'Alert',
                        subTitle: 'Sign In Failed. You may have previous account with same email id.',
                        buttons: ['OK']
                    });
                    alert.present();
                });
            }).catch((error) => { alert(error) });

      }
      else{
            GooglePlus.login({'webClientId':'735130465689-q7pu4kvidom52nqp17vg1nc8lhq6cfni.apps.googleusercontent.com'}).then( (response) => {
            var provider = firebase.auth.GoogleAuthProvider.credential(response.idToken);
             console.log(response);
             console.log(provider);
                    firebase.auth().signInWithCredential(provider)
                        .then((success) => {
                            console.log(success);
                                this.user = success;
                                let ref = firebase.database().ref('/users/' + success.uid);
                                ref.child('fullname').set(response.displayName);
                                ref.child('email').set(success.email);
                                ref.child('profile_image').set(response.imageUrl);
                                ref.child('authToken').set(response.idToken);
                                ref.child('customer').set(true);
                        })
                        .catch((error) => {
                            let alert = this.alertCtrl.create({
                                title: 'Login',
                                subTitle: 'Sign In Failed. You may have previous account with same email id.',
                                buttons: ['OK']
                                });
                            alert.present();
                        });
            })
            .catch((error)=>{
                alert("fail to login");
                console.log(error);
            })
      }   */
  }

  //forgot password
  async resetPassword() {
    let alert = await this.alertCtrl.create({
      header: 'Forgot password',
      inputs: [
        {
          type: 'text',
          placeholder: 'Please Enter Your Email',
        },
      ],
      buttons: [
        'Cancel',
        {
          text: 'OK',
          handler: (data) => {
            let passData = data[0];
            this.changepass(passData);
          },
        },
      ],
    });
    alert.present();
  }

  async pswreset() {
    let psw = await this.alertCtrl.create({
      header: 'Success',
      subHeader: 'Password reset email has been sent to your email address',
      buttons: ['OK'],
    });
    psw.present();
  }

  changepass(data) {
    let email = data;
    console.log('OnChangePass');

    // this.usersService.passReset(email).then(() => alert("password reset"))
    this.usersService
      .passReset(email)
      .then((msg) => {
        if (msg === 'Success') this.pswreset();
        else {
          this.utility.presentFailureToast(
            'Invalid / incomplete email, please try again'
          );
        } //alert('Invalid or incomplete email');
      })
      .catch((_error) => alert(_error));
  }

  ngOnInit() {
    this.menuCtrl.enable(false, 'drawer');
  }
  getDb() {
    return this.firebaseService.getDatabase();
  }
}
