import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import {
  Camera,
  CameraResultType,
  CameraSource,
  ImageOptions,
} from '@capacitor/camera';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage extends BasePage implements OnInit {
  usertype: any;
  showCustomer: boolean;
  showDriver: boolean;
  driver = 'driver';
  customer = 'customer';
  data: any;

  //profileimage
  profileImage: any;

  curUser: any;

  constructor(injector: Injector) {
    super(injector);

    this.data = {
      fullname: '',
      email: '',
      phone: '',
      registration: '',
      cityname: '',
    };
  }

  //constructor end
  async doAdd() {
    // console.log(this.data,userId);
    // this.usersService.updateData(this.data,this.curUser.uid)
    let profileRef = this.getDbRef('/users/' + this.curUser.uid);
    profileRef.child('fullname').set(this.data.fullname);
    profileRef.child('email').set(this.data.email);
    profileRef.child('phone').set(this.data.phone);
    profileRef.child('cityname').set(this.data.cityname);
    profileRef.child('profile_image').set(this.profileImage);
    let alert = await this.alertCtrl.create({
      header: 'Success', subHeader: 'Profile updated successfully',
      buttons: [{
        text: 'Ok',
        handler: () => {
          this.events.publish("login_event");
          this.nav.pop();
        }
      }]
    })
    alert.present();

  }

  captureDataUrl: any;
  //open camera picture
  openCamera() {
    const cameraOptions: ImageOptions = {
      height: 150,
      width: 150,
      allowEditing: true,
      quality: 50,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Camera,
    };
    Camera.getPhoto(cameraOptions).then(
      (imageData) => {
        console.log(
          "imageData", imageData
        )
        this.captureDataUrl = imageData.dataUrl;
        //  console.log('CaptureDataUrl:' + this.captureDataUrl);
        this.upload();
      },
      (err) => { }
    );
  }
  //open galary picture
  openGalary() {
    const cameraOptions: ImageOptions = {
      height: 150,
      width: 150,
      allowEditing: true,
      quality: 50,
      resultType: CameraResultType.DataUrl,
      source: CameraSource.Photos,
    };
    Camera.getPhoto(cameraOptions).then(
      (imageData) => {
        console.log(
          "imageData", imageData
        )
        this.captureDataUrl = imageData.dataUrl;
        // console.log('CaptureDataUrl:' + this.captureDataUrl);
        this.upload();
      },
      (err) => { }
    );
  }
  //upload firebase
  upload() {
    this.profileImage = this.captureDataUrl;
    return;
    let storageRef = this.firebaseService.fireStorage.ref('/');
    const filename = Math.floor(Date.now() / 1000);
    const imageRef = storageRef.child(`images/${filename}.jpg`);
    console.log('Upload Const ImageRef:', imageRef);
    console.log("upload", this.captureDataUrl);
    //*TODO*//
    // firebase.storage.StringFormat.DATA_URL
    imageRef
      .put(this.captureDataUrl)
      .then((snapshot) => {
        console.log(
          'SnapShot:' + JSON.stringify(snapshot.ref.getDownloadURL())
        );
        this.saveDatabase(snapshot.ref);
      });
  }
  //upload database
  users = [];
  async saveDatabase(snapshot) {
    let imgUrl = await snapshot.getDownloadURL();
    var ref = this.getDbRef('users/' + this.curUser.uid);
    ref.child('profile_image').set(snapshot.downloadURL);

    var ref = this.getDbRef('/users/' + this.curUser?.uid + '/bookings');
    // var bookingref = firebase.database().ref('bookings');
    ref.on('value', (snap1) => {
      let userdata = snap1.val();
      for (var key in userdata) {
        userdata[key].uid = key;
        if (key != this.curUser?.uid) {
          this.users.push(userdata[key]);
        }
      }
      this.bookingsUpdateData(this.users, imgUrl);
    });
  }

  bookingsUpdateData(userIds, imgUrl) {
    var ref = this.getDbRef('/users/' + this.curUser.uid);
    ref.once('value', (snap2) => {
      let userType = snap2.val().type;
      if (userType == 'customer') {
        for (let i = 0; i < userIds.length; i++) {
          console.log(userIds[i].uid);
          var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
          let userBookings = this.getDbRef('/users/' + userIds[i].driver + '/bookings/' + userIds[i].uid);
          bookingref.child('customer_image').set(imgUrl);
          userBookings.child('customer_image').set(imgUrl);
        }
      } else {
        for (let i = 0; i < userIds.length; i++) {
          let id = userIds[i].uid;
          var bookingref = this.getDbRef('/bookings/' + userIds[i].uid);
          let userBookings =
            this.getDbRef(
              '/users/' + userIds[i].customer + '/bookings/' + userIds[i].uid
            );
          bookingref.child('driver_image').set(imgUrl);
          userBookings.child('driver_image').set(imgUrl);
        }
      }
    });
  }

  //option Select Camera or Album
  async doGetPicture() {
    let actionSheet = await this.actionSheetController.create({
      buttons: [
        {
          text: 'Camera',
          role: 'camera',
          handler: () => {
            this.openCamera();
          },
        },
        {
          text: 'Album',
          handler: () => {
            this.openGalary();
          },
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          },
        },
      ],
    });
    actionSheet.present();
  }
  //End Camera and Album related work

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    var ref = this.getDbRef('/users/' + this.curUser.uid);
    ref.on('value', (_snapshot: any) => {
      if (_snapshot.val()) {
        this.data = _snapshot.val();
        this.profileImage = _snapshot.val().profile_image;
        // this.usertype = _snapshot.val().type;
        // console.log(this.usertype);
        /*   if(this.usertype==this.customer){
                            this.showCustomer= true;
                        }else if((this.usertype==this.driver)) {
                            this.showCustomer= false;
                        }else{
                          alert("no type found");
                        }*/
      } else {
        console.log('no type added');
      }
    });
  }

  getDbRef(ref) {
    return this.firebaseService.getDatabase().ref(ref)
  }
}
