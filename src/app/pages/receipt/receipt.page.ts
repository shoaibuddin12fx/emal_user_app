import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-receipt',
  templateUrl: './receipt.page.html',
  styleUrls: ['./receipt.page.scss'],
})
export class ReceiptPage extends BasePage implements OnInit {
  constructor(injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReceiptPage');
  }

  postReview() {
    this.nav.setRoot('pages/reviewdriver');
  }

  ngOnInit() { }
}
