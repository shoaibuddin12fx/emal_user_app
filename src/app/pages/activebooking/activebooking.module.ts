import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ActivebookingPageRoutingModule } from './activebooking-routing.module';

import { ActivebookingPage } from './activebooking.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ActivebookingPageRoutingModule
  ],
  declarations: [ActivebookingPage]
})
export class ActivebookingPageModule {}
