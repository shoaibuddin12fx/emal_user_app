import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ActivebookingPage } from './activebooking.page';

const routes: Routes = [
  {
    path: '',
    component: ActivebookingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ActivebookingPageRoutingModule {}
