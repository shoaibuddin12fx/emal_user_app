import {
  AfterViewInit,
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { CallNumber } from '@ionic-native/call-number/ngx';

import { DataService } from 'src/app/Services/data.service';
import { FareNowComponent } from '../home/fare-now/fare-now.component';

declare const google;
@Component({
  selector: 'app-activebooking',
  templateUrl: './activebooking.page.html',
  styleUrls: ['./activebooking.page.scss'],
})
export class ActivebookingPage
  extends BasePage
  implements OnInit, AfterViewInit
{
  @ViewChild('map2', { static: false }) mapElement: ElementRef;
  curUser: any;
  userProfile: any;
  clickable = false;

  bookingKey: any;

  booking: any;

  bookingStatus: any;

  map: any;

  trackMarker: any;

  callDriver: any;
  driverUserId: any;
  customerID: any;
  driverID: any;
  showImage: boolean;

  mapReady: boolean;

  latestStatus: string;
  stripeID: string;

  constructor(
    injector: Injector,
    private callNumber: CallNumber,
    public dataService: DataService
  ) {
    super(injector);
    this.mapReady = false;
    this.mapService.showmap = true;
    this.bookingKey = this.getQueryParams()?.bookingKey;
    this.latestStatus = 'None';
    this.booking = {
      pickup: {
        add: '',
      },
      image_url: 'assets/images/notfound.png',
      status: '',
    };
    console.log(this.bookingKey);

    let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
    //bookingRef.once('value', (snapshot:any) => {
    bookingRef.on('value', (snapshot: any) => {
      if (!snapshot.val()) return;
      this.zone.run(() => {
        console.log('BOOKING_DETAIL', snapshot.val());

        this.booking = snapshot.val();
        console.log('Booking', this.booking);
        if (this.booking != undefined && this.booking != null) {
          //  this.driverUserId= snapshot.val().driver;
          this.customerID = snapshot.val().customer;
          //alert("hiii");
          this.platform.ready().then(() => {
            console.log('Loading map');
            //  this.loadMap();
          });

          if (snapshot.val().driver == undefined) {
            this.showImage = true;
          } else {
            this.driverID = snapshot.val().driver;
          }

          if (
            snapshot.val()?.hasOwnProperty('driver_image') &&
            snapshot.val()?.driver_image != undefined &&
            snapshot.val().driver_image != ''
          ) {
            this.booking.image_url = snapshot.val().driver_image;
          } else {
            this.booking.image_url = 'assets/images/notfound.png';
          }
          this.bookingStatus = snapshot.val().status;
          if (this.booking.status == 'REVIEW') {
            let geoRef = this.getDb().ref('geofire');
            console.log(this.bookingKey);
            //  let geoRef = firebase.database().ref('/geofire/' + this.bookingKey);
            geoRef.child(this.bookingKey).remove();
            this.nav.setRoot('pages/reviewdriver', {
              bookingKey: this.bookingKey,
            });
          }
        } else {
          this.nav.setRoot('pages/currentbookings');
        }
      });
    });
  }
  ngAfterViewInit(): void {
    if (this.bookingKey) this.loadMap();
  }
  loadMap() {
    const location = {
      lat: parseFloat(this.booking.pickup?.lat),
      lng: parseFloat(this.booking.pickup?.lng),
    };
    console.log('Map before', this.map);
    this.map = new google.maps.Map(this.mapElement?.nativeElement, {
      zoom: 15,
      center: location,
    });

    console.log('Map After', this.map);

    // this.map = new GoogleMap('map2', {
    //   backgroundColor: 'white',
    //   controls: {
    //     compass: false,
    //     myLocationButton: false,
    //     indoorPicker: false,
    //     zoom: true,
    //   },
    //   gestures: {
    //     scroll: true,
    //     tilt: true,
    //     rotate: true,
    //     zoom: true,
    //   },
    //   camera: {
    //     target: location,
    //     tilt: 30,
    //     zoom: 15, //,
    //     // 'bearing': 50
    //   },
    // });
    // console.log(this.map);
    let self = this;

    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      // do something only the first time the map is loaded
      self.onMapReady();
    });
  }

  onMapReady() {
    this.mapReady = true;
    let self = this;
    let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
    bookingRef.on('value', (snapshot: any) => {
      self.booking = snapshot.val();
      console.log('booking', self.booking);

      if (
        snapshot.val().hasOwnProperty('driver_image') &&
        snapshot.val().driver_image != undefined &&
        snapshot.val().driver_image != ''
      ) {
        self.booking.image_url = snapshot.val().driver_image;
      } else {
        self.booking.image_url = 'assets/images/notfound.png';
      }
      self.bookingStatus = snapshot.val().status;

      console.log(self.latestStatus, self.bookingStatus);
      if (
        self.latestStatus != self.bookingStatus ||
        self.latestStatus == 'None'
      ) {
        console.log('Latest status', self.latestStatus);
        // self.map.clear();
        self.latestStatus = self.bookingStatus;
        console.log('map clear');

        let location = {
          lat: parseFloat(self.booking.pickup?.lat),
          lng: parseFloat(self.booking.pickup?.lng),
        };
        console.log('location', location);
        if (
          self.bookingStatus == 'ARRIVING' ||
          self.bookingStatus == 'STARTED'
        ) {
          let marker = new google.maps.Marker({
            position: location,
            title: 'Driver Current Location',
            icon: 'www/assets/images/driver.png',
          });
          marker.setMap(self.map);
          self.addAndShowMarkerWindow(marker, 'Driver Current Location');

          let geoRef = self
            .getDb()
            .ref('users/' + self.booking.driver + '/location');

          geoRef.on('value', (snapshot: any) => {
            let pos = {
              lat: parseFloat(snapshot.val()?.lat),
              lng: parseFloat(snapshot.val()?.lng),
            };
            console.log(
              'latitude: ' +
                snapshot.val()?.lat +
                ' , ' +
                'longitude: ' +
                snapshot.val()?.lng
            );
            self.map.setCenter(pos);
            marker.setPosition(pos);
          });
        } else if (self.bookingStatus == 'ARRIVED') {
          let marker = new google.maps.Marker({
            position: location,
            title: 'Driver Arrived',
            icon: 'www/assets/images/driver.png',
          });

          self.addAndShowMarkerWindow(marker, 'Driver Arrived');
          self.map.setCenter(location);
        } else {
          console.log('Else -> Pickup Location');
          let trackMarker = new google.maps.Marker({
            position: location,
            title: 'Pickup Location',
          });
          trackMarker.setMap(self.map);
          self.addAndShowMarkerWindow(trackMarker, 'Pickup Location');
          self.map.setCenter(location);
        }
      }
    });
  }
  addAndShowMarkerWindow(marker, content) {
    var infoWindow = new google.maps.InfoWindow({
      content: content,
    });
    marker.setMap(this.map);
    infoWindow.open(this.map, marker);
  }

  getHelp() {
    this.nav.push('pages/support', {
      bookingKey: this.bookingKey,
    });
  }

  async cancelBooking() {
    let setStatus = 'CANCELLED';
    if (this.booking.status == setStatus) return;
    if (this.booking.status == 'Finding your driver') {
      let alertCustom = await this.alertCtrl.create({
        header: 'Do you want to Cancel your booking',
        buttons: [
          {
            text: 'Cancel',
            handler: (data) => {
              this.setClickable(true);
              this.mapService.showmap = true;
              alertCustom.dismiss();
            },
          },
          {
            text: 'OK',
            handler: (data) => {
              this.setClickable(true);
              this.mapService.showmap = true;
              let geocancleRef = this.getDb().ref('geofire');
              let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
              let customerRef = this.getDb().ref(
                '/users/' + this.customerID + '/bookings/' + this.bookingKey
              );
              // let driverRef = firebase.database().ref('/users/'+ this.driverID + '/bookings/'+ this.bookingKey);

              bookingRef.child('status').set(setStatus);
              customerRef.child('status').set(setStatus);
              //  driverRef.child('status').set(setStatus);
              geocancleRef.child(this.bookingKey).remove();
              //*TODO
              // this.nav.setRoot('pages/bookings');
              this.nav.pop();
            },
          },
        ],
      });
      this.setClickable(false);
      this.mapService.showmap = false;
      alertCustom.present();
      // this.navCtrl.setRoot(BookingsPage);
    } else {
      let alertCustom = await this.alertCtrl.create({
        header: 'Driver  ' + this.booking.status,
        subHeader: 'Cancellation Charge $7',
        buttons: [
          {
            text: 'Cancel',
            handler: (data) => {
              this.setClickable(true);
              this.mapService.showmap = true;
              alertCustom.dismiss();
            },
          },
          {
            text: 'OK',
            handler: (data) => {
              this.setClickable(true);
              this.mapService.showmap = true;
              let geocancleRef = this.getDb().ref('geofire');
              let bookingRef = this.getDb().ref('/bookings/' + this.bookingKey);
              let customerRef = this.getDb().ref(
                '/users/' + this.customerID + '/bookings/' + this.bookingKey
              );
              let driverRef = this.getDb().ref(
                '/users/' + this.driverID + '/bookings/' + this.bookingKey
              );

              bookingRef.child('status').set(setStatus);
              customerRef.child('status').set(setStatus);
              driverRef.child('status').set(setStatus);
              geocancleRef.child(this.bookingKey).remove();

              /* Cancelllation charge */
              var totalBill = 7;
              let customerRef1 = this.getDb().ref('/users/' + this.customerID);
              var paystripe = this.getDb().ref('/users/' + this.customerID);
              paystripe.on('value', (_snapshot: any) => {
                if (_snapshot.val()) {
                  this.stripeID = _snapshot.val().stripeID;
                  console.log('this.stripeID.................', this.stripeID);
                }
              });

              var payRef = this.getDb().ref(
                '/users/' + this.customerID + '/payment_methods'
              );
              payRef.once('value', (snapshot) => {
                if (snapshot.val()) {
                  let cards = snapshot.val();
                  let cardID = '';
                  console.log('cards.........', cards);
                  let GotIt = false;
                  for (let i = 0; i < cards.length; i++) {
                    if (cards[i].primary) {
                      GotIt = true;
                      cardID = cards[i].stripeCardID;
                    }
                  }
                  if (GotIt) {
                    console.log(cardID, totalBill);
                    this.usersService
                      .stripePayment(cardID, this.stripeID, totalBill)
                      .subscribe(
                        async (response) => {
                          console.log('stripe payment response', response);
                          let temp: any;
                          temp = response;
                          if (temp.status == 'succeeded') {
                            this.getDb()
                              .ref(
                                '/bookings/' + this.bookingKey + '/trip_cost'
                              )
                              .set(totalBill);
                            this.getDb()
                              .ref(
                                '/users/' +
                                  this.booking.customer +
                                  '/bookings/' +
                                  this.bookingKey +
                                  '/trip_cost'
                              )
                              .set(totalBill);
                            this.getDb()
                              .ref(
                                '/users/' +
                                  this.booking.driver +
                                  '/bookings/' +
                                  this.bookingKey +
                                  '/trip_cost'
                              )
                              .set(totalBill);
                            // alert("succeeded payment");
                            let alert = await this.alertCtrl.create({
                              header: 'Payment',
                              subHeader: 'Payment succeeded',
                              buttons: ['OK'],
                            });
                            alert.present();
                            //alert('Payment succeeded');
                            this.nav.setRoot('pages/bookings');
                            //   this.usersService.sendPush('Trip Ended - Payment Success !!',"Your cost for the trip is $" + totalBill ,this.booking.customer_push_token).subscribe(response=>console.log(response));
                            //   this.paymentclick=true;
                            // firebase.database().ref('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                            // firebase.database().ref('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                            //   this.navCtrl.setRoot(JobcompletePage,{
                            //     bookingKey:this.bookingKey
                            //   });
                          } else {
                            // let alert = this.alertCtrl.create({
                            //   title: 'Payment',
                            //   subTitle: temp.message,
                            //   buttons: ['OK']
                            // });
                            // let alert = this.alertCtrl.create();
                            // alert.present();
                            //   alert(temp.message);
                            // firebase.database().ref('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('REVIEW');
                            // firebase.database().ref('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('REVIEW');
                            //   this.usersService.sendPush('Payment Not Approve !!',"No Payment Method Found. Your cost for the trip is $" + totalBill ,this.booking.customer_push_token).subscribe(response=>console.log(response));
                          }
                        },
                        (err) => {
                          // alert('Server Error ! Try again');
                          // this.firebaseService
                          //   .getDatabase()
                          //   .ref(
                          //     'users/' +
                          //       this.booking.customer +
                          //       '/bookings/' +
                          //       this.bookingKey +
                          //       '/status'
                          //   )
                          //   .set('PAYMENT');
                          // this.firebaseService
                          //   .getDatabase()
                          //   .ref(
                          //     'users/' +
                          //       this.booking.driver +
                          //       '/bookings/' +
                          //       this.bookingKey +
                          //       '/status'
                          //   )
                          //   .set('PAYMENT');
                        }
                      );
                  } else {
                    //   alert("Customer Not Selected Primary card");
                    // firebase.database().ref('users/' + customer + '/bookings/' + this.bookingKey + '/status' ).set('PAYMENT');
                    // firebase.database().ref('users/' + driver + '/bookings/' + this.bookingKey + '/status').set('PAYMENT');
                    //   this.usersService.sendPush('Payment Failed !!',"Not Selected Primary card. Your cost for the trip is $" + totalBill ,this.booking.customer_push_token).subscribe(response=>console.log(response));
                  }
                } else {
                  //   alert("Customer Card Not Available");
                  //   this.usersService.sendPush('Payment Failed !!'," Card Not Available. Your cost for the trip is $" + totalBill,this.booking.customer_push_token).subscribe(response=>console.log(response));
                }
              });

              /* Cancelllation charge */
            },
          },
        ],
      });
      this.setClickable(false);
      this.mapService.showmap = false;
      // alertCustom.setSubTitle('Do you want to Cancel your Booking');
      alertCustom.present();
    }
  }

  disable: any;

  async giveCall() {
    if (this.driverID != undefined) {
      let alertCustom = await this.alertCtrl.create({
        header: 'Do you want to call Driver',
        buttons: [
          {
            text: 'Cancel',
            handler: (data) => {
              this.setClickable(true);
              this.mapService.showmap = true;
              alertCustom.dismiss();
            },
          },
          {
            text: 'OK',
            handler: (data) => {
              this.setClickable(true);
              this.mapService.showmap = true;
              let driverRef = this.getDb().ref('/users/' + this.driverID);

              driverRef.once('value', (snapshot: any) => {
                this.callDriver = snapshot.val().phone;

                this.callNumber
                  .callNumber(this.callDriver, true)
                  .then(() => console.log('Launched dialer!'))
                  .catch((reason) =>
                    console.log('Error launching dialer', reason)
                  );
              });
            },
          },
        ],
      });
      this.setClickable(false);
      this.mapService.showmap = false;

      alertCustom.present();
    } else {
      console.log('Driver not Select');
    }
  }

  ngDoCheck() {
    if (this.mapReady) {
      if (this.mapService.showmap) {
        this.setClickable(true);
      } else {
        this.setClickable(false);
      }
    }
  }

  editLocation() {
    console.log('edit location');
    console.log(this.booking);
    let details = this.booking;
    delete details.image_url;
    this.dataService.farePageData = {
      bookingId: this.bookingKey,
      bookingDetails: this.booking,
    };

    this.modals.present(FareNowComponent, { details });
    // this.nav.push('pages/fare');
  }

  ngOnDestroy() {
    console.log('page destroy successfully');
    if (this.map != undefined) {
      this.map = null;
    }
  }
  ngOnInit() {}

  getDb() {
    return this.firebaseService.getDatabase();
  }

  setClickable(clickable) {
    this.clickable = clickable;
  }
}
