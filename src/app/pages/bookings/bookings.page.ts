import { Component, Injector, NgZone, OnInit } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage extends BasePage implements OnInit {
  curUser: any;
  userProfile: any;
  bookingList = [];

  constructor(private injector: Injector) {
    super(injector);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BookingsPage');
  }
  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    let ref = this.getDb()
      .ref('/users/' + this.curUser.uid)
      .orderByChild('tripdate:');

    ref.once('value', (snapshot: any) => {
      if (snapshot.val() != null) {
        this.bookingList = [];

        this.zone.run(() => {
          console.log(snapshot.val());
          this.userProfile = snapshot.val();
          for (let key in this.userProfile.bookings) {
            let bs = this.userProfile.bookings[key].status;
            this.userProfile.bookings[key].uid = key;
            if (bs == 'ENDED' || bs == 'CANCELLED') {
              if (this.userProfile.bookings[key].driver == null) {
                this.userProfile.bookings[key].image_url =
                  'assets/images/notfound.png';
              } else {
                this.userProfile.bookings[key].image_url =
                  this.userProfile.bookings[key].driver_image;
              }

              this.bookingList.push(this.userProfile.bookings[key]);
            }
          }

          this.bookingList.sort(function (a, b) {
            let aa: any = Number(new Date(a.tripdate));
            let bb: any = Number(new Date(b.tripdate));
            console.log(bb - aa);
            return bb - aa;
          });
        });
      }
    });
  }

  getDb() {
    return this.firebaseService.getDatabase();
  }
}
