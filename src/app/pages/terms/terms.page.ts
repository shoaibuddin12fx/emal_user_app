import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.page.html',
  styleUrls: ['./terms.page.scss'],
})
export class TermsPage extends BasePage implements OnInit {
  @Input() login: string;

  deviceHeight: any = 85;
  constructor(injector: Injector) {
    super(injector);
    this.deviceHeight = this.platform.height();
    console.log(this.deviceHeight);
  }

  modalclose() {
    this.modals.dismiss({
      param: this.login,
    });
  }

  ngOnInit() {}
}
