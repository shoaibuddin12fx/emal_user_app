import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { GmapComponent } from 'src/app/components/gmap/gmap.component';
import { BasePage } from '../base-page/base-page';
declare const google;
@Component({
  selector: 'app-autocomplete',
  templateUrl: './autocomplete.page.html',
  styleUrls: ['./autocomplete.page.scss'],
})
export class AutocompletePage extends BasePage implements OnInit {

  GoogleAutocomplete;
  @ViewChild('searchbox', { read: ElementRef }) searchbar: ElementRef;
  
  address: any;
  lstAdd: any = [];
  lstPlaces = [];

  constructor(injector: Injector) {
    super(injector);
    
  }

  ionViewDidEnter(){
    this.storage.getData().then((data) => {
      console.log(data);
      if (data?.entries) {
        this.lstAdd = data?.entries;
      }
    });
    this.setSearchBox();
  }

  closeModal() {
    this.modals.dismiss({});
  }

  onCancel($event) {
    this.modals.dismiss({});
  }

  getLatLngAndGotoAddress(data){

    console.log(data); 
    
    // get lat long from place id 
    const geocoder = new google.maps.Geocoder(); 
    geocoder
      .geocode({ placeId: data.place_id })
      .then(async ({ results }) => {
        console.log(results);
        if(results[0]){ 
          var location = results[0].geometry.location

          var obj = {
            lat: location.lat(),
            lng: location.lng(),
            add: data.description
          }

          this.modals.dismiss(obj);
          this.storage.saveData(obj);

        }
      })
      .catch((e) => window.alert("Geocoder failed due to: " + e));

    // this.modals.dismiss(data);
  }

  gotoAddress(data) {
    this.modals.dismiss(data);
  }

  ngAfterViewInit() {
    // var options = {
    //   // componentRestrictions: {country: 'au'}
    //   componentRestrictions: { country: this.mapService.currentCountrycode },
    // };
    // //let autocomplete = new google.maps.places.Autocomplete( document.getElementBy

    // //  let autocomplete = new google.maps.places.Autocomplete( document.getElementById('PickupLocation'), {});
    // let autocomplete = new google.maps.places.Autocomplete(
    //   document.getElementById('PickupLocation'),
    //   options,
    //   {}
    // );
    // google.maps.event.addListener(autocomplete, 'place_changed', () => {
    //   this.address = autocomplete.getPlace();
    //   let data = { add: null, lat: null, lng: null };
    //   data.add = this.address.formatted_address;
    //   data.lat = this.address.geometry.location.lat();
    //   data.lng = this.address.geometry.location.lng();
    //   this.storage.saveData(data);
    //   this.modals.dismiss(data);
    // });
  }
  ngOnInit() { }

  async openLocation() {

    const address = "";
    const res = await this.modals.present(GmapComponent, { myAddress: address });
    const data = res.data;
    console.log(data);

    if (data.address) {
      data.add = data.address;

      await this.storage.saveData(data);
      this.gotoAddress(data);
      
    }

    

  }

  searchChange($event){
    let v = $event.target.value;
    console.log(v);

    let vItems = [];

    if (v == '') {
      vItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: v },
    (predictions, status) => {
      vItems = [];
      this.zone.run(() => {
        predictions.forEach((prediction) => {
          vItems.push(prediction);
        });
        this.lstPlaces = vItems;
      });
    });

  }

  setSearchBox() {

    const self = this;
    this.GoogleAutocomplete = new google.maps.places.AutocompleteService;
    // const searchInput = this.searchbar.nativeElement.querySelector('.searchbar-input');
    // // console.log("Search input", searchInput);
    // const searchBox = new google.maps.places.SearchBox(searchInput);
    // let markers = [];

    // searchBox.addListener('places_changed', (() => {
    //   console.log('places_changed')
    //   const places = searchBox.getPlaces();
    //   if (places.length === 0) {
    //     this.lstPlaces = [];
    //     return;
    //   }


    //   places.forEach((place) => {
    //     this.lstPlaces = place.geometry.location;
    //   })

      



      // self.mmarker.setMap(null);
      // markers = [];

      // For each place, get the icon, name and location.
      // const bounds = new google.maps.LatLngBounds();
      // places.forEach(((place) => {
      //   if (!place.geometry) {
      //     // console.log("Returned place contains no geometry");
      //     return;
      //   }

      //   // Create a marker for each place.
      //   self.mmarker = new google.maps.Marker({
      //     position: place.geometry.location,
      //     map: self.map,
      //     draggable: true,
      //     title: 'Destination'
      //   });

      //   if (place.geometry.viewport) {
      //     // Only geocodes have viewport.
      //     bounds.union(place.geometry.viewport);
      //   } else {
      //     bounds.extend(place.geometry.location);
      //   }
      // }));
      // self.map.fitBounds(bounds);

    // }));
  }

  // locdata(data){
  //   let lat = "123.4";
  //   let lng = "5648.66";
  //   let address = this.address;
  // }

}
