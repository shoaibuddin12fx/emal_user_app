import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationloaderPage } from './locationloader.page';

const routes: Routes = [
  {
    path: '',
    component: LocationloaderPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationloaderPageRoutingModule {}
