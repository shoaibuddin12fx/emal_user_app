import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocationloaderPageRoutingModule } from './locationloader-routing.module';

import { LocationloaderPage } from './locationloader.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationloaderPageRoutingModule
  ],
  declarations: [LocationloaderPage]
})
export class LocationloaderPageModule {}
