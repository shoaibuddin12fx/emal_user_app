import {
  AfterViewInit,
  Component,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { BasePage } from '../base-page/base-page';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  CameraPosition,
  MarkerOptions,
  Marker,
  Environment,
  ILatLng,
} from '@ionic-native/google-maps';
import {
  NativeGeocoder,
  NativeGeocoderResult,
} from '@ionic-native/native-geocoder/ngx';
import { IonContent } from '@ionic/angular';

declare const google;
@Component({
  selector: 'app-locationloader',
  templateUrl: './locationloader.page.html',
  styleUrls: ['./locationloader.page.scss'],
})
export class LocationloaderPage
  extends BasePage
  implements OnInit, AfterViewInit
{
  curUser: any;
  image = 'assets/images/splash.png';
  msg = 'Getting Location...';

  constructor(private injector: Injector) {
    super(injector);

    this.platform.ready().then(async () => {
      this.initialize();
    });
  }

  ngAfterViewInit(): void {
    console.log(
      'size of content',
      this.platform.width(),
      this.platform.height()
    );
  }

  ionViewDidEnter() {
    this.platform.ready().then(async () => {
      this.initialize();
    });
  }

  ngOnInit() {}

  timer;

  async initialize() {
    var self = this;
    this.geolocation.getCurrentPosition().then((loc) => {
      console.log(loc);
      if (loc) {
        clearInterval(this.timer);
        self.msg = 'Setting Contrycode by reverse geocoding...';

        setTimeout(async () => {
          const res = await self.setCountryCodeViaLocation(loc);
          self.reverseGeoCode(res);
        }, 1000);
      } else {
        // alert("Hello world")
        self.msg =
          'Getting Location permission, please allow it in your phone settings';

        this.timer = setInterval(() => {
          this.initialize();
        }, 1000);
      }
    }),
      (reason) => {
        self.msg = reason?.toString();
        //alert("Rejected : "+ reason);
      };

    // this.utility.presentFailureToast('Location permission required, please allow ');

    // const user = await this.firebaseService.getCurrentUser();
    // if (user) {
    //   this.usersService.curUser = user;
    //   const loc = await this.geolocation.getCurrentPosition();
    //   if (loc) {
    //     const res = await this.setCountryCodeViaLocation(loc);
    //     this.reverseGeoCode(res);
    //   } else {
    //     this.utility.presentFailureToast('Error getting location');
    //     this.reverseGeoCode({ lat: -33.865143, lng: 151.2099, add: null });
    //   }
    // } else {
    //   this.nav.setRoot('pages/login');
    // }
  }

  async setCountryCodeViaLocation(position) {
    return new Promise((resolve) => {
      console.log('position', position);

      const res = this.geolocation.getAddressFromCoords(
        position.coords.latitude,
        position.coords.longitude
      );
      if (res) {
        if (res[0] != undefined && res[0].hasOwnProperty('countryCode')) {
          this.mapService.currentCountrycode = res[0].countryCode;
        }
      }

      resolve(res);
    });
  }

  isUserLoggedIn() {
    return new Promise(async (resolve) => {
      const user = await this.firebaseService.getCurrentUser();
      if (user) {
        this.usersService.curUser = user;
        resolve(user);
        // const loc = await this.geolocation.getCurrentPosition();
        // if (loc) {
        //   const res = await this.setCountryCodeViaLocation(loc);
        //   this.reverseGeoCode(res);
        // } else {
        //   this.utility.presentFailureToast('Error getting location');
        //   this.reverseGeoCode({ lat: -33.865143, lng: 151.2099, add: null });
        // }
      } else {
        // this.nav.setRoot('pages/login');
        resolve(null);
      }
    });
  }

  async reverseGeoCode(obj) {
    console.log('reverseGeoCode reached');
    console.log(obj);
    var self = this;

    self.msg = 'Getting User Information ...';
    const user = await this.firebaseService.getCurrentUser(); // await this.isUserLoggedIn();

    if (user) {
      this.usersService.curUser = user;
    } else {
      self.nav.setRoot('pages/login');
      return;
    }

    const ref = self.firebaseService.getDatabase().ref('users/' + user.uid);

    if (obj)
      ref.child('location').set({
        lat: obj.lat,
        lng: obj.lng,
        add: obj.add ?? '',
      });

    ref.once('value', (snapshot) => {
      self.msg = 'User Found, Checking profile ...';
      if (snapshot.val().phone) {
        //TODO Temporary remove
        // if (!user['emailVerified'] && !snapshot.val().authToken) {
        //   user.sendEmailVerification().then(
        //     function () {
        //       self.utility.presentFailureToast(
        //         'Email has been sent. Please verify your account first.'
        //       );
        //       self.firebaseService.fireAuth.signOut();
        //       self.nav.setRoot('pages/login');
        //     },
        //     function (error) {
        //       self.utility.presentFailureToast(
        //         'Email has been sent. Please verify your account first.'
        //       );
        //       console.log(
        //         'COuld not send Verification email',
        //         error.toString()
        //       );

        //       self.nav.setRoot('pages/login');
        //     }
        //   );
        // } else {
        self.msg = 'Set Home Page ...';
        this.nav.setRoot('pages/home', {
          lat: obj.lat,
          lng: obj.lng,
          add: obj.add,
        });
        //}
      } else {
        self.msg = 'Register Customer ...';
        this.nav.setRoot('pages/registercustomer');
      }
    });
    //*TODO*//
    // geocoder.geocode(request, function (data, status) {
    //   console.log("in geocode")
    //   if (status == google.maps.GeocoderStatus.OK) {
    //     let ref = self.firebaseService.getDatabase().ref('users/' + user.uid);

    //     ref.child('location').set({
    //       lat: lat,
    //       lng: lng,
    //       add: data[0].formatted_address,
    //     });

    //     ref.once('value', (snapshot) => {
    //       if (snapshot.val().phone) {
    //         if (!user.emailVerified && !snapshot.val().authToken) {
    //           user.sendEmailVerification().then(
    //             function () {
    //               alert(
    //                 'Email has been sent. Please verify your account first.'
    //               );
    //               this.firebaseService.firebase.auth().signOut();
    //             },
    //             function (error) {
    //               console.log(error);
    //             }
    //           );
    //         } else {
    //           this.nav.setRoot('pages/map', {
    //             lat: lat,
    //             lng: lng,
    //             add: data[0].formatted_address,
    //           });
    //         }
    //       } else {
    //         this.nav.setRoot('pages/registercustomer');
    //       }
    //     });
    //   }
    // });
  }
}
