import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BidDriverPageRoutingModule } from './bid-driver-routing.module';

import { BidDriverPage } from './bid-driver.page';
import { IonicRatingModule } from 'ionic-rating';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BidDriverPageRoutingModule,
    IonicRatingModule,
  ],
  declarations: [BidDriverPage],
})
export class BidDriverPageModule {}
