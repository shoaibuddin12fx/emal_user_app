import { Component, Inject, Injector, NgZone, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { DataService } from 'src/app/Services/data.service';

declare const google;
@Component({
  selector: 'app-bid-driver',
  templateUrl: './bid-driver.page.html',
  styleUrls: ['./bid-driver.page.scss'],
})
export class BidDriverPage extends BasePage implements OnInit {
  bookingData: any;
  job: any = [];
  bidding: any;
  bookingKey: any;
  curUser: any;
  custHeight: any;

  markerSetTo: any;
  map: any;

  pickup: {
    lat: number;
    lng: number;
    add: string;
  };

  drop: {
    lat: number;
    lng: number;
    add: string;
  };

  constructor(injector: Injector, private dataService: DataService) {
    super(injector);
    let params = this.dataService.data;
    this.job = params?.booking;
    this.bookingKey = params?.bookingKey;
    let ref = this.getDb().ref('bookings/' + this.bookingKey);
    ref.on('value', (snapshot) => {
      this.bookingData = snapshot.val();
      this.pickup = this.bookingData.pickup;
      if (this.bookingData.hasOwnProperty('drop')) {
        this.drop = this.bookingData.drop;
      }

      if (snapshot.val() != undefined && snapshot.val() != '') {
        this.job.bidding = snapshot.val().bidding;
        this.job.bidding.sort(function (a, b) {
          return a.rate - b.rate;
        });
      }
    });
  }
  ionViewDidLoad() {
    this.job.bidding.sort(function (a, b) {
      return a.rate - b.rate;
    });

    setTimeout(() => {
      this.loadMap();
    }, 1000);
  }

  setMapInfo(lat: number, lng: number, add: string) {
    this.zone.run(() => {
      if (this.markerSetTo == 'pickup') {
        this.pickup.lat = lat;
        this.pickup.lng = lng;
        this.pickup.add = add;
      } else {
        this.drop.lat = lat;
        this.drop.lng = lng;
        this.drop.add = add;
      }
    });
  }

  calculateAndDisplayRoute(lat, lng) {
    var directionsService = new google.maps.DirectionsService();
    var directionsDisplay = new google.maps.DirectionsRenderer();
    var map = new google.maps.Map(document.getElementById('map5'), {
      zoom: 7,
      center: { lat: lat, lng: lng },
    });
    directionsDisplay.setMap(map);
    directionsService.route(
      {
        origin: this.pickup.add,
        destination: this.drop.add,
        travelMode: 'DRIVING',
      },
      function (response, status) {
        if (status === 'OK') {
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      }
    );
  }

  animateMarker(lat, lng) {
    var map = new google.maps.Map(document.getElementById('map5'), {
      zoom: 13,
      center: { lat: lat, lng: lng },
    });

    var marker = new google.maps.Marker({
      map: map,
      draggable: true,
      animation: google.maps.Animation.DROP,
      position: { lat: lat, lng: lng },
    });
  }

  loadMap() {
    let location = { lat: this.pickup.lat, lng: this.pickup.lng };

    var map = new google.maps.Map(document.getElementById('map3'), {
      zoom: 15,
      center: location,
    });

    // this.map = new GoogleMap('map3', {
    //   backgroundColor: 'white',
    //   controls: {
    //     compass: false,
    //     myLocationButton: false,
    //     indoorPicker: false,
    //     zoom: true,
    //   },
    //   gestures: {
    //     scroll: true,
    //     tilt: true,
    //     rotate: true,
    //     zoom: true,
    //   },
    //   camera: {
    //     target: location,
    //     tilt: 30,
    //     zoom: 15, //,
    //     // 'bearing': 50
    //   },
    // });

    let info = this.setMapInfo;

    if (this.job.serviceType == 'Pickup') {
      this.custHeight = '210px !important';
      this.calculateAndDisplayRoute(this.pickup.lat, this.pickup.lng);
    } else {
      this.custHeight = '280px !important';
      this.animateMarker(this.pickup.lat, this.pickup.lng);
    }
  }

  acceptRide(data) {
    var customerData = {
      customer: this.job.customer,
      customer_name: this.job.customer_name,
      customer_image: this.job.customer_image,
      driver: data.driver,
      status: 'ASSIGNED',
      pickupAddress: this.job.pickupAddress,
      // dropAddress: this.job.dropAddress,
      tripdate: this.job.tripdate,
      serviceType: this.job.serviceType,
      //    customer_email: this.customer_email,
      corporate_booking_name: null,
      booking_type: this.bookingData.tripType,

      driver_name: data.driver_name,
      driver_image: data.driver_image,
      trip_cost: data.rate,
    };

    var driverData = {
      customer: this.job.customer,
      customer_name: this.job.customer_name,
      customer_image: this.job.customer_image,
      driver: data.driver,
      status: 'ASSIGNED',
      pickupAddress: this.job.pickupAddress,
      //  dropAddress: this.job.dropAddress,
      tripdate: this.job.tripdate,
      serviceType: this.job.serviceType,
      trip_cost: data.rate,
    };
    if (this.job.hasOwnProperty('dropAddress')) {
      driverData['dropAddress'] = this.job.dropAddress;
      customerData['dropAddress'] = this.job.dropAddress;
    }

    let ref = this.getDb().ref('bookings/' + this.bookingKey);
    this.bookingData.status = 'ASSIGNED';
    this.bookingData.trip_cost = data.rate;
    this.bookingData.estimate = data.rate;

    this.bookingData.driver = data.driver;
    this.bookingData.driver_name = data.driver_name;
    this.bookingData.driver_image = data.driver_image;
    ref.update(this.bookingData);

    var updates = {};
    updates['/users/' + data.driver + '/bookings/' + this.bookingKey] =
      driverData;
    updates[
      '/users/' + this.bookingData.customer + '/bookings/' + this.bookingKey
    ] = customerData;
    this.getDb().ref().update(updates);
    console.log(data);
    this.usersService
      .sendPushDriver(
        'Your bid accepted by ' + this.job.customer_name,
        this.job.pickupAddress,
        data.driver_push_token
      )
      .subscribe((response) => {
        console.log('response........', response);
        this.nav.pop();
        this.nav.setRoot('pages/currentbookings');
      });
    this.getDb().ref('geofire').child(this.bookingKey).remove();
    //  this.navCtrl.setRoot(CurrentbookingsPage);
    //  this.usersService.sendPush('Driver is bid your trip', "Driver "+this.userProfile.fullname+" bid your trip.", this.bookingData.customer_push_token).subscribe(response => console.log(response));
  }

  ngOnDestroy() {
    console.log('page destroy successfully');
    if (this.map != undefined) {
      this.map.remove();
    }
  }

  ngOnInit() {}

  getDb() {
    return this.firebaseService.getDatabase();
  }
}
