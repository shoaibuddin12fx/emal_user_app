import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ReviewdriverPage } from './reviewdriver.page';

const routes: Routes = [
  {
    path: '',
    component: ReviewdriverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ReviewdriverPageRoutingModule {}
