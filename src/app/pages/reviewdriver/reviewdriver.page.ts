import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-reviewdriver',
  templateUrl: './reviewdriver.page.html',
  styleUrls: ['./reviewdriver.page.scss'],
})
export class ReviewdriverPage extends BasePage implements OnInit {
  rate: any;
  favourite: boolean = false;
  //giveReview:any;

  userId: any;
  booking: any;
  bookingKey: any;

  customerID: any;
  driverID: any;
  trip_cost: any;
  notes: any;
  refcard: any;
  stripeID: any;

  constructor(injector: Injector) {
    super(injector);
    this.notes = '';
    this.bookingKey = this.getQueryParams()?.bookingKey;
  }

  async ngOnInit() {
    let user = await this.firebaseService.getCurrentUser();
    let bookingRef = this.getDbRef('/bookings/' + this.bookingKey);
    bookingRef.once('value', (snapshot: any) => {
      this.booking = snapshot.val();
      this.customerID = snapshot.val().customer;
      this.driverID = snapshot.val().driver;

      console.log(this.booking);
      this.booking.image_url = snapshot.val().driver_image;
    });

    this.refcard = this.getDbRef('/users/' + user.uid);
    this.refcard.on('value', (_snapshot: any) => {
      if (_snapshot.val()) {
        this.stripeID = _snapshot.val().stripeID;
      }
    });
  }

  async reviewSubmit() {
    if (!this.rate) {
      var alert = await this.alertCtrl.create({
        subHeader: 'Please give rating',
        buttons: ['Ok'],
      });
      alert.present();
      return;
    }

    if (!this.notes) {
      var alert = await this.alertCtrl.create({
        subHeader: 'Please enter comment or notes',
        buttons: ['Ok'],
      });
      alert.present();
      return;
    }
    let user = await this.firebaseService.getCurrentUser();
    this.userId = user.uid;
    let reviewData = {
      rating: this.rate,
      // favourite:this.favourite//,
      notes: this.notes,
    };

    let userRef = this.getDbRef('/users/' + this.driverID);
    let bookingRef = this.getDbRef('/bookings/' + this.bookingKey);
    let customerRef = this.getDbRef(
      '/users/' + this.customerID + '/bookings/' + this.bookingKey
    );
    let driverRef = this.getDbRef(
      '/users/' + this.driverID + '/bookings/' + this.bookingKey
    );

    userRef.child('ratings').push(this.rate);
    bookingRef.child('ratings').set(reviewData);

    customerRef.child('ratings').set(reviewData);
    driverRef.child('ratings').set(reviewData);

    bookingRef.child('status').set('ENDED');
    customerRef.child('status').set('ENDED');
    driverRef.child('status').set('ENDED');
    this.nav.pop();
    setTimeout(() => {
      this.nav.setRoot('pages/currentbookings');
    }, 100);
  }
  async paymentSubmit() {
    this.userId = (await this.firebaseService.getCurrentUser())['uid'];
    /* ---added by hr  -----------*/

    var paymentbill = this.getDbRef(
      '/bookings/' + this.bookingKey + '/trip_cost'
    );
    paymentbill.once('value', (snapbill) => {
      let totalBill = snapbill.val();
      console.log('MY paypal bill.........', totalBill);
      var payRef = this.getDbRef(
        '/users/' + this.customerID + '/payment_methods'
      );
      payRef.once('value', (snapshot) => {
        if (snapshot.val()) {
          let cards = snapshot.val();
          let cardID = '';
          console.log('cards.........', cards);
          let GotIt = false;
          for (let i = 0; i < cards.length; i++) {
            if (cards[i].primary) {
              GotIt = true;
              cardID = cards[i].stripeCardID;
            }
          }
          if (GotIt) {
            this.usersService
              .stripePayment(cardID, this.stripeID, totalBill)
              .subscribe(
                async (response) => {
                  console.log('paypal payment response', response);
                  let temp: any;
                  temp = response;
                  if (temp.status == 'succeeded') {
                    let alert = await this.alertCtrl.create({
                      header: 'Payment',
                      subHeader: 'Payment succeeded',
                      buttons: ['OK'],
                    });
                    alert.present();
                  } else {
                    let alert = await this.alertCtrl.create({
                      header: 'Payment',
                      subHeader: temp.message,
                      buttons: ['OK'],
                    });
                    alert.present();
                    // this.usersService.sendPushDriver('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
                  }
                },
                async (err) => {
                  let alert = await this.alertCtrl.create({
                    header: 'Payment',
                    subHeader: 'Server Error ! Try again',
                    buttons: ['OK'],
                  });
                  alert.present();
                }
              );
          } else {
            alert('Please select Primary card');
            //  this.usersService.sendPushDriver('Payment Failed !!',"No Payment Method Found. Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
          }
        } else {
          alert('Card Not Available');
          // this.usersService.sendPush('Payment Failed !!',"Your cost for the trip is " + totalBill + "$",this.booking.customer_push_token).subscribe(response=>console.log(response));
        }
      });
    });
    /*-------------------------------*/
  }

  onRateChange(event) {
    console.log('onRateChange', event);
  }

  getDbRef(ref) {
    return this.firebaseService.getDatabase().ref(ref);
  }
}
