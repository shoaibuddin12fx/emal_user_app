import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicRatingModule } from 'ionic-rating';

import { IonicModule } from '@ionic/angular';

import { ReviewdriverPageRoutingModule } from './reviewdriver-routing.module';

import { ReviewdriverPage } from './reviewdriver.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReviewdriverPageRoutingModule,
    IonicRatingModule
  ],
  declarations: [ReviewdriverPage]
})
export class ReviewdriverPageModule { }
