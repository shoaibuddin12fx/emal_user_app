import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage extends BasePage implements OnInit {
  addcard = { cardno: '', month: '', year: '', cvv: '' };

  years = [];

  constructor(injector: Injector) {
    super(injector);

    let year: number = 2017;
    let till: number = 2042;

    for (let i = year; i <= till; i++) {
      //this.options += "<option>"+ y +"</option>";
      this.years.push(i);
    }
    console.log(this.years);
  }

  payNow() {
    console.log(this.addcard.cardno);
    console.log(this.addcard.month);
    console.log(this.addcard.year);
    console.log(this.addcard.cvv);
  }
  close() {
    this.viewCtrl.dismiss();
  }

  ngOnInit() {}
}
