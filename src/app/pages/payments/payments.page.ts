import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { CardIO } from '@ionic-native/card-io/ngx';
import { Navigation } from 'selenium-webdriver';
import { NavigationEnd } from '@angular/router';
import { FareNowModule } from '../home/fare-now/fare-now.module';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.page.html',
  styleUrls: ['./payments.page.scss'],
})
export class PaymentsPage extends BasePage implements OnInit {
  cards = [];
  emailID: any;
  refcard: any;
  ref: any;
  userId: any;
  stripeID: any;
  public stripe_custID = true;
  _isModal = false;
@Input() set isModal(value){
  this._isModal = value;
} ;

  get isModal () {
    return this._isModal;

    
  }

  constructor(injector: Injector, private cardIO: CardIO) {
    super(injector);
  }

  async ngOnInit() {
    let user = await this.firebaseService.getCurrentUser();
    this.ref = this.getDbRef('/users/' + user["uid"] + '/payment_methods');
    this.ref.on('value', (snapshot: any) => {
      if (snapshot.val()) {
        this.zone.run(() => {
          this.cards = snapshot.val();
          console.log('snap', snapshot.val());
        });
      }
    });

    this.refcard = this.getDbRef('/users/' + user["uid"]);
    this.refcard.on('value', (_snapshot: any) => {
      if (_snapshot.val()) {
        this.stripeID = _snapshot.val().stripeID;
        this.emailID = _snapshot.val().email;
        if (this.stripeID) {
          this.stripe_custID = false;
        }
      }
    });
  }

  openCardIO() {
    let res = {
      redactedCardNumber: '4242424242424242',
      cardType: 'VISA',
      expiryMonth: '02/22',
      expiryYear: '22',
    };
    let response = {
      primary: true,
      default_source: '123',
      id: '123',
      state: 'ok',
      CustomerID: '123',
      message: 'card added successfully!',
    };
    this.onStripCardAdded(res, response, true);
    return;
    this.cardIO.canScan().then((res: boolean) => {
      if (res) {
        let options = {
          requireExpiry: true,
          requireCCV: true,
          requirePostalCode: false,
          hideCardIOLogo: true,
        };
        this.cardIO.scan(options).then(async (res) => {
          console.log('res');
          let notGot = true;
          let firstCard = true;
          let cust_id;
          for (let i = 0; i < this.cards.length; i++) {
            firstCard = false;
            if (
              this.cards[i].cardEnding == res.redactedCardNumber.substr(12, 4)
            ) {
              notGot = false;
            }
          }
          if (notGot) {
            if (!this.stripe_custID) {
              cust_id = this.stripeID;
            } else {
              cust_id = '';
            }
            console.log(res, cust_id, this.emailID);
            this.usersService
              .addStripeCard(res, cust_id, this.emailID)
              .subscribe(
                async (response) => {
                  this.onStripCardAdded(res, response, firstCard);
                },
                async (err) => {
                  let alert = await this.alertCtrl.create({
                    header: 'ADD CARD',
                    subHeader: 'Server Error ! Try again',
                    buttons: ['OK'],
                  });
                  alert.present();
                }
              );
          } else {
            let alert = await this.alertCtrl.create({
              header: 'ADD CARD',
              subHeader: 'Card already added',
              buttons: ['OK'],
            });
            alert.present();
          }
        });
      }
    });
  }

  async onStripCardAdded(res, response, firstCard) {
    console.log('card response data', response);
    let card: any;
    card = response;
    card.primary = false;
    if (firstCard) {
      card.primary = true;
    }
    var data = {
      cardEnding: res.redactedCardNumber.substr(12, 4),
      type: res.cardType.toLowerCase(),
      expiryMonth: res.expiryMonth,
      expiryYear: res.expiryYear,
      stripeCardID: card.default_source ? card.default_source : card.id,
      state: card.state,
      primary: card.primary,
    };

    if (card.state == 'ok') {
      // if(this.stripe_custID){
      //   this.refcard.child('stripeID').set(card.id);
      // }
      if (card.CustomerID) {
        this.refcard.child('stripeID').set(card.id);
        this.ref.child(this.cards.length).set(data);
      } else {
        this.ref.child(this.cards.length).set(data);
      }
    } else {
      let alert = await this.alertCtrl.create({
        header: 'ADD CARD',
        subHeader: card.message,
        buttons: ['OK'],
      });
      alert.present();
      // alert("Some issue with the card.");
    }
  }

  goback() {
    console.log('goback');
    if(this.isModal)
      this.modals.dismiss();

    else this.nav.pop();
    
  }
  SelectCard(Card) {
    console.log('event', Card);
    this.modals.dismiss({ item: Card });
  }

  makePrimary(i) {
    console.log('press');
    for (let j = 0; j < this.cards.length; j++) {
      if (i == j) {
        this.cards[i].primary = true;
        this.ref.child(i).set(this.cards[i]);
      } else {
        this.cards[j].primary = false;
        this.ref.child(j).set(this.cards[j]);
      }
    }
  }
  removeCard(i) {
    console.log('press');
    if (this.cards[i].primary) {
      alert('You cannot remove the primary card');
    } else {
      for (let j = 0; j < this.cards.length; j++) {
        if (j == this.cards.length - 1) {
          this.ref.child(j).remove();
        } else {
          if (j >= i) {
            this.ref.child(j).set(this.cards[j + 1]);
          }
        }
      }
    }
  }

  // removeCard(i){
  //   if(this.cards[i].primary){
  //       alert("You cannot remove the primary card");
  //   }else{
  //     for(let j=0;j<this.cards.length;j++){
  //       if(j==this.cards.length-1){
  //         var cardID= this.cards[i].stripeCardID;
  //         this.usersService.deleteStripeCard(cardID,this.stripeID).subscribe(response=>{
  //           let temp :any;
  //           temp = response;
  //           if(temp.deleted ==true){
  //             this.ref.child(j).remove();
  //             let alert = this.alertCtrl.create({
  //                 title: 'Delete',
  //                 subTitle: 'Delete succeeded',
  //                 buttons: ['OK']
  //             });
  //             alert.present();
  //           }else{
  //             let alert = this.alertCtrl.create({
  //                 title: 'Delete',
  //                 subTitle: temp.message,
  //                 buttons: ['OK']
  //             });
  //             alert.present();
  //           }
  //         },err => {
  //           let alert = this.alertCtrl.create({
  //               title: 'Delete',
  //               subTitle: 'Server Error ! Try again',
  //               buttons: ['OK']
  //           });
  //           alert.present();
  //         });
  //       }else{
  //         if(j>=i){
  //           this.ref.child(j).set(this.cards[j+1]);
  //         }
  //       }
  //     }
  //   }
  // }

  getDbRef(ref) {
    return this.firebaseService.getDatabase().ref(ref);
  }
}
