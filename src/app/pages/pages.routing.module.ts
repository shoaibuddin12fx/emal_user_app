import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'splash',
    pathMatch: 'full',
  },
  {
    path: 'home',
    loadChildren: () =>
      import('./home/home.module').then((m) => m.HomePageModule),
  },
  {
    path: 'terms',
    loadChildren: () =>
      import('./terms/terms.module').then((m) => m.TermsPageModule),
  },
  {
    path: 'support',
    loadChildren: () =>
      import('./support/support.module').then((m) => m.SupportPageModule),
  },
  {
    path: 'share',
    loadChildren: () =>
      import('./share/share.module').then((m) => m.SharePageModule),
  },
  {
    path: 'reviewdriver',
    loadChildren: () =>
      import('./reviewdriver/reviewdriver.module').then(
        (m) => m.ReviewdriverPageModule
      ),
  },
  {
    path: 'registercustomer',
    loadChildren: () =>
      import('./registercustomer/registercustomer.module').then(
        (m) => m.RegistercustomerPageModule
      ),
  },
  {
    path: 'receipt',
    loadChildren: () =>
      import('./receipt/receipt.module').then((m) => m.ReceiptPageModule),
  },
  {
    path: 'profile',
    loadChildren: () =>
      import('./profile/profile.module').then((m) => m.ProfilePageModule),
  },
  {
    path: 'popover',
    loadChildren: () =>
      import('./popover/popover.module').then((m) => m.PopoverPageModule),
  },
  {
    path: 'payments',
    loadChildren: () =>
      import('./payments/payments.module').then((m) => m.PaymentsPageModule),
  },
  {
    path: 'map',
    loadChildren: () => import('./map/map.module').then((m) => m.MapPageModule),
  },
  {
    path: 'makepayment',
    loadChildren: () =>
      import('./makepayment/makepayment.module').then(
        (m) => m.MakepaymentPageModule
      ),
  },
  {
    path: 'login',
    loadChildren: () =>
      import('./login/login.module').then((m) => m.LoginPageModule),
  },
  {
    path: 'locationloader',
    loadChildren: () =>
      import('./locationloader/locationloader.module').then(
        (m) => m.LocationloaderPageModule
      ),
  },
  {
    path: 'fare',
    loadChildren: () =>
      import('./fare/fare.module').then((m) => m.FarePageModule),
  },
  {
    path: 'currentbookings',
    loadChildren: () =>
      import('./currentbookings/currentbookings.module').then(
        (m) => m.CurrentbookingsPageModule
      ),
  },
  {
    path: 'bookings',
    loadChildren: () =>
      import('./bookings/bookings.module').then((m) => m.BookingsPageModule),
  },
  {
    path: 'bid-driver',
    loadChildren: () =>
      import('./bid-driver/bid-driver.module').then(
        (m) => m.BidDriverPageModule
      ),
  },
  {
    path: 'autocomplete',
    loadChildren: () =>
      import('./autocomplete/autocomplete.module').then(
        (m) => m.AutocompletePageModule
      ),
  },
  {
    path: 'activebooking',
    loadChildren: () =>
      import('./activebooking/activebooking.module').then(
        (m) => m.ActivebookingPageModule
      ),
  },
  {
    path: 'about',
    loadChildren: () =>
      import('./about/about.module').then((m) => m.AboutPageModule),
  },
  {
    path: 'splash',
    loadChildren: () =>
      import('./splash/splash.module').then((m) => m.SplashPageModule),
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagesRoutingModule {}
