import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-support',
  templateUrl: './support.page.html',
  styleUrls: ['./support.page.scss'],
})
export class SupportPage extends BasePage implements OnInit {
  selectedTab: string;

  constructor(injector: Injector) {
    super(injector);

    this.selectedTab = 'tab1';
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad SupportPage');
  }
  ngOnInit() {}
}
