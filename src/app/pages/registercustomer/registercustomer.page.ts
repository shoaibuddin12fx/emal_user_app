import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { TermsPage } from '../terms/terms.page';

@Component({
  selector: 'app-registercustomer',
  templateUrl: './registercustomer.page.html',
  styleUrls: ['./registercustomer.page.scss'],
})
export class RegistercustomerPage extends BasePage implements OnInit {
  fullname: any;
  email: any;
  password: any;
  confirmPassword: any;
  phone: any;
  cityname: any;
  vehicle: any;

  //provider:any;
  showInput: boolean;

  //params item
  item: any;

  curUser: any;

  constructor(injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    if (this.curUser) {
      this.showInput = false;
      this.fullname = this.curUser.displayName;
      this.email = this.curUser.email;
    } else {
      this.showInput = true;
    }
  }

  backClick() {
    console.log('backCliked');
  }

  public onSignUpSuccess() {
    console.log('Restration Succesfull');
  }

  public async onSignUpFailed() {
    let alert = await this.alertCtrl.create({
      header: 'Registration',
      subHeader: 'Failed',
      buttons: ['OK'],
    });
    alert.present();
  }

  public async onFirebaseFailed(error) {
    let alert = await this.alertCtrl.create({
      header: 'Registration',
      subHeader: error,
      buttons: ['OK'],
    });
    alert.present();
  }

  private async wrongUserInfo() {
    let alert = await this.alertCtrl.create({
      header: 'Registration',
      subHeader: 'Please Enter Vaild Details',
      buttons: ['OK'],
    });
    alert.present();
  }

  async doRegistration() {
    if (this.curUser) {
      if (
        this.fullname == undefined ||
        this.email == undefined ||
        this.phone == undefined ||
        this.cityname == undefined ||
        this.fullname == '' ||
        this.email == '' ||
        this.phone == '' ||
        this.cityname == ''
      ) {
        this.wrongUserInfo();
        //     ||
        // this.vehicle== ""
      } else {
        this.usersService
          .insertUser(
            this.fullname,
            this.email,
            this.phone,
            this.cityname,
            12345,
            this.curUser
          )
          .then(() => this.nav.setRoot('pages/locationloader'))
          .catch((_error) => this.onSignUpFailed());
      }
    } else {
      if (
        this.fullname == undefined ||
        this.email == undefined ||
        this.password == undefined ||
        this.confirmPassword == undefined ||
        this.phone == undefined ||
        this.cityname == undefined ||
        this.fullname == '' ||
        this.email == '' ||
        this.password == '' ||
        this.confirmPassword == '' ||
        this.phone == '' ||
        this.cityname == ''
      ) {
        this.wrongUserInfo();

        //      ||
        // this.vehicle== ""
      } else {
        if (this.password == this.confirmPassword) {
          this.usersService
            .signUpUser(
              this.fullname,
              this.email,
              this.password,
              this.phone,
              this.cityname,
              12345
            )
            .then(async () => {
              console.log('Registration Success');
              let alert = await this.alertCtrl.create({
                header: 'Registration',
                subHeader: 'Registration Success',
                buttons: ['OK'],
              });
              alert.present().then(() => {
                this.nav.pop();
              });
            })
            .catch((_error) => this.onFirebaseFailed(_error.toString()));
        } else {
          let alert = await this.alertCtrl.create({
            header: 'Registration',
            subHeader: 'Password do not match',
            buttons: ['OK'],
          });
          alert.present();
        }
      }
    }
  }

  goToSignin() {
    this.nav.push('pages/login');
  }

  async modalopen() {
    console.log('works');

    let modal = await this.modals.present({ component: TermsPage });
  }

  typePhoneNumber(ev) {
    // let v = $event.target.value;
    // console.log(v)
    if (ev.inputType !== 'deleteContentBackward') {
      const utel = this.utility.onkeyupFormatPhoneNumberRuntime(
        ev.target.value,
        false
      );
      console.log(utel);
      ev.target.value = utel;
      this.phone = utel;
      // this.aForm.controls['phone_number'].patchValue(utel);
      // ev.target.value = utel;
    }
  }
}
