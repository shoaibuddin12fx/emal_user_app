import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistercustomerPage } from './registercustomer.page';

const routes: Routes = [
  {
    path: '',
    component: RegistercustomerPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RegistercustomerPageRoutingModule {}
