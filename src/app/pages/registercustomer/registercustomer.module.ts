import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RegistercustomerPageRoutingModule } from './registercustomer-routing.module';

import { RegistercustomerPage } from './registercustomer.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegistercustomerPageRoutingModule
  ],
  declarations: [RegistercustomerPage]
})
export class RegistercustomerPageModule {}
