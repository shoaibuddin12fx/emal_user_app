import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

@Component({
  selector: 'app-share',
  templateUrl: './share.page.html',
  styleUrls: ['./share.page.scss'],
})
export class SharePage extends BasePage implements OnInit {
  constructor(injector: Injector, private socialSharing: SocialSharing) {
    super(injector);
  }

  shareWithFacebook() {
    this.socialSharing.shareViaFacebook('message', 'image', 'www.Ridebidder.com')
      .then((success) => {
        console.log('ok: ' + success);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  //twitter share
  shareWithTwitter() {
    this.socialSharing.shareViaTwitter('message', 'image', 'www.Ridebidder.com')
      .then((success) => {
        console.log('ok: ' + success);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  //massage share
  shareWithMassage() {
    this.socialSharing.shareViaSMS(' ', ' ')
      .then((success) => {
        console.log('ok: ' + success);
      })
      .catch((error) => {
        console.log(error);
      });
  }
  data: any;
  shareWithEmail() {
    //  this.data='abc@email.com';
    this.data = '';
    /* SocialSharing.shareViaEmail('Body', 'Subject', 'pradipmondal7777@gmail.com').then((success) => {
    console.log("ok: "+ success);
  }).catch((error) => {
    console.log(error);
  });*/
    // Check if sharing via email is supported
    /*SocialSharing.canShareViaEmail().then((response) => {
  console.log(response);
}).catch((error) => {
  console.log(error);
});
*/
    this.socialSharing.shareViaEmail('', '', this.data)
      .then((success) => {
        console.log('ok: ' + success);
      })
      .catch((error) => {
        console.log(error);
      });
  }

  ngOnInit() { }
}
