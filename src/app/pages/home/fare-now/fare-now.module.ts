import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FareNowComponent } from './fare-now.component';
import { IonicModule } from '@ionic/angular';
import { DatePipe } from '@angular/common'
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [FareNowComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
  ],
  providers: [
    DatePipe
  ],
  exports: [FareNowComponent]
})
export class FareNowModule { }
