import { Component, Injector, Input, OnInit } from '@angular/core';
import { BasePage } from '../../base-page/base-page';
import {
  DatePickerPluginInterface,
  DatePickerPlugin,
} from '@capacitor-community/date-picker';
import { GeoFire } from 'geofire';
import { DataService } from 'src/app/Services/data.service';
import { PaymentsPage } from '../../payments/payments.page';
const DatePicker: DatePickerPluginInterface = DatePickerPlugin as any;

declare const google;
@Component({
  selector: 'app-fare-now',
  templateUrl: './fare-now.component.html',
  styleUrls: ['./fare-now.component.scss'],
})
export class FareNowComponent extends BasePage implements OnInit {
  @Input() data;
  @Input() drop: any = {
    lat: 0,
    lng: 0,
    add: null,
  };

  @Input() pick = {
    lat: 0,
    lng: 0,
    add: null,
  };
  triptime = '2';
  jobtype;
  year;
  distance;
  PickupService;

  showDate;
  showTime;

  estimate: any;
  bookingDetails;
  timeDistance;
  count;
  curUser;
  refcode;
  customerPromocode;
  bookingKey;
  trip;
  transmission: any;
  myDate: any;
  serviceType: any;
  promo_code: any;
  googleTime: any;
  originalDate: any;
  personal_hour: any;
  personal_minimum: any;
  pickup_base_km: any;
  pickup_hour: any;
  pickup_km: any;
  pickup_minimum: any;
  pickup_minute: any;
  totalBill: number;
  codeFound: any;
  notificationData = [];
  requestDone: boolean;
  cardEnding;
  cardData;
  allDriver = [];

  constructor(injector: Injector, private dataService: DataService) {
    super(injector);
    this.initialize();
  }

  ngOnInit() {}

  close() {
    this.modals.dismiss({ data: 'A' });
  }

  ionViewWillEnter() {
    this.initDefaultValues();
    this.initPush();
    console.log('ionViewWillEnter', this.data);
    if (this.data) {
      this.jobtype = this.data['jobtype'];
      this.pick = this.data['pickupDet'];
      console.log('JOB_TYPE', this.jobtype);
      this.PickupService = this.jobtype == 'pickup';
    }

    if (this.PickupService) {
      this.drop = this.data['dropDet'];

      // calculate estimates here
      // calculate distance

      // var lat1 = this.pick.lat;
      // var lon1 = this.pick.lng;
      // var lat2 = this.drop.lat;
      // var lon2 = this.drop.lng;
      // var unit = "K";
      // var dis = this.calcDistance(lat1, lon1, lat2, lon2, unit);
      // var dollar = this.formatToCurrency(dis);
      this.setDistance(this.pick, this.drop);
      //this.changeEstimate();
    }

    // calculate estimate
    // if(this.jobtype == 'pickup' ){

    // } else{

    // }
  }

  initPush() {
    this.allDriver = [];
    this.notificationData = [];
    console.log('ionViewDidLoad FarePage');
    /*------------------------------ Push Token Save -----------------------------------*/
    let refToken = this.getDb().ref('users');
    refToken.on('value', (snapshot) => {
      this.allDriver = snapshot.val();
      this.notificationData = [];
      console.log('Drivers', this.allDriver);
      for (let key in this.allDriver) {
        if (
          this.allDriver[key].islogin == 'driver' &&
          this.allDriver[key].active == true &&
          this.allDriver[key].pushToken
        ) {
          // if(this.allDriver[key].islogin == 'driver' && this.allDriver[key].pushToken && this.allDriver[key].email == 'bhavesh9562@gmail.com' ){
          this.notificationData.push({ token: this.allDriver[key].pushToken });
          console.log(
            'ionViewDidLoad notificationData...............',
            this.notificationData
          );
        }
      }
    });
    /*------------------------------ Push Token Save -----------------------------------*/
  }

  async initialize() {
    let params = this.getParams();

    // this.pickup = params?.pick;
    console.log('PickupInit', this.pick);

    this.drop = params?.drop;

    this.curUser = await this.firebaseService.getCurrentUser();
    this.refcode = this.getDb().ref(
      '/users/' + this.curUser?.uid + '/promo_code'
    );
    this.refcode.on('value', (snapshot: any) => {
      this.customerPromocode = snapshot.val();
    });

    let cDate = this.formatLocalDate('');
    this.showDate = cDate.substring(0, 10);
    this.showTime = cDate.substring(11, 19);
    this.calculateDateTime();

    this.bookingKey = params?.bookingId;

    if (this.bookingKey) {
      this.bookingDetails = params?.bookingDetails;
      this.trip = this.bookingDetails.tripType;
      this.pick = this.bookingDetails.pickup;
      console.log('bookingKey', this.bookingKey);

      if (this.bookingDetails.drop) {
        this.PickupService = true;
        this.drop = this.bookingDetails.drop;
      } else {
        this.PickupService = false;
      }

      this.distance = this.bookingDetails.distance;

      this.transmission = this.bookingDetails.transmission;

      this.myDate = this.bookingDetails.tripdate ?? this.originalDate;
      //new changes
      this.showDate = this.bookingDetails.tripdate.substring(0, 10);
      this.showTime = this.bookingDetails.tripdate.substring(11, 19);
      //new changes end

      this.triptime = this.bookingDetails.triptime;

      this.estimate = this.bookingDetails.estimate;

      this.serviceType = this.bookingDetails.serviceType;

      if (this.bookingDetails.promo_code != undefined) {
        this.promo_code = this.bookingDetails.promo_code;
      }
    } else {
      //new added by me..
      if (this.PickupService) {
        this.trip = 'Oneway';
        this.pick = params?.pickupDet;
        console.log('PickupService', this.pick);
        this.drop = params?.dropDet;
        this.distance = params?.distRoute;
        this.googleTime = params?.googleTime;

        this.transmission = 'Automatic';

        //this.myDate =  this.formatLocalDate();
        this.myDate = this.originalDate; //new line added

        // this.triptime = 4;
        this.triptime = params?.googleTime;
        this.serviceType = 'Pickup';
      } else {
        this.trip = 'Oneway';
        this.pick = params?.pickupDet;
        console.log('Oneway', this.pick);
        this.drop = {
          lat: null,
          lng: null,
          add: null,
        };
        this.distance = null;

        this.transmission = 'Automatic';

        // this.myDate =  this.formatLocalDate();
        this.myDate = this.originalDate; //new line added

        this.triptime = '2';
        // this.serviceType = "Personal"; // add to Sach23
        this.serviceType = 'Personal driver'; // add to Hemant
      }
      this.changeEstimate();
    }

    var ref = this.getDb().ref('rates');

    ref.on('value', (snapshot: any) => {
      if (snapshot.val()) {
        this.personal_hour = snapshot.val().personal_hour;
        this.personal_minimum = snapshot.val().personal_minimum;
        this.pickup_base_km = snapshot.val().pickup_base_km;
        this.pickup_hour = snapshot.val().pickup_hour;
        this.pickup_km = snapshot.val().pickup_km;
        this.pickup_minimum = snapshot.val().pickup_minimum;
        this.pickup_minute = snapshot.val().pickup_minute;
        this.zone.run(() => {
          setTimeout(() => {
            this.loadMap();
          }, 1000);
          this.changeEstimate();
        });
      }
    });
  }
  loadMap() {
    // throw new Error('Method not implemented.');
  }

  initDefaultValues() {
    let d = new Date();
    this.showDate = d;
  }

  async pickDate() {
    const res = await this.utility.showDatePicker(this.showDate, 'date');
    this.showDate = res;
  }

  async pickTime() {
    const res = await this.utility.showDatePicker(this.showDate, 'time');
    this.showDate = res;
  }

  // formatToCurrency(amount) {
  //   return "$" + amount.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
  // };

  calcDistance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = (Math.PI * lat1) / 180;
    var radlat2 = (Math.PI * lat2) / 180;
    var theta = lon1 - lon2;
    var radtheta = (Math.PI * theta) / 180;
    var dist =
      Math.sin(radlat1) * Math.sin(radlat2) +
      Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist);
    dist = (dist * 180) / Math.PI;
    dist = dist * 60 * 1.1515;
    if (unit == 'K') {
      dist = dist * 1.609344;
    }
    return dist;
  }

  formatToCurrency(value) {
    value = value * 2;
    var fixedNum = parseFloat(value).toFixed(2);
    return fixedNum;
  }

  setDistance(pick, drop) {
    var directionsService = new google.maps.DirectionsService();
    let routeDistance: number;
    directionsService.route(
      {
        origin: pick,
        destination: drop,
        travelMode: 'DRIVING',
      },
      (response, status) => {
        if (status === 'OK') {
          this.distance = response.routes[0].legs[0].distance.value;
          this.timeDistance = response.routes[0].legs[0].duration.value / 60;
          var googleTime = this.timeDistance.toFixed();
          this.triptime = googleTime;
          this.changeEstimate();
          //       let fair = this.formatToCurrency(this.distance);
          // this.estimate = fair;
          // if (this.data?.bookingKey) {
          //   this.bookingDetails.distance = this.distance;
          //   this.timeDistance = response.routes[0].legs[0].duration.value / 60;
          //
          //   this.triptime = this.googleTime;
          // }

          //  this.changeEstimate();
        } else {
          //window.alert('Directions request failed due to ' + status);
        }
      }
    );
  }

  async changeEstimate() {
    this.count += 1;
    // var totalBill;
    if (this.PickupService) {
      let totalDistance = this.distance / 1000;
      // var pickup_base = '28';
      let billableKms = 0;
      if (totalDistance > parseFloat(this.pickup_base_km)) {
        billableKms = totalDistance - parseFloat(this.pickup_base_km);
      }

      //let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (parseInt(this.triptime) * parseInt(this.pickup_hour));
      // let totalBill = parseInt(this.pickup_minimum)  + (billableKms * parseInt(this.pickup_km)) + (parseInt(this.triptime) * parseInt(this.pickup_minute));
      this.totalBill =
        parseFloat(this.pickup_minimum) +
        billableKms * parseFloat(this.pickup_km) +
        parseFloat(this.triptime) * parseFloat(this.pickup_minute);
      // if(this.totalBill < parseFloat(this.pickup_minimum)){
      //      this.totalBill = parseFloat(this.pickup_minimum);
      // }

      this.estimate = Math.round(this.totalBill * 100) / 100;
    } else {
      console.log('1');

      this.totalBill =
        parseFloat(this.personal_minimum) +
        parseFloat(this.triptime) * parseFloat(this.personal_hour);
      console.log('triptime', this.triptime);

      this.estimate = Math.round(this.totalBill * 100) / 100;
      console.log('3');
    }

    if (this.bookingKey) {
      this.bookingDetails['triptime'] = this.triptime;

      this.bookingDetails.estimate = this.estimate;
    }
    if (isNaN(this.totalBill) && this.count >= 2) {
      let alert = await this.alertCtrl.create({
        header: 'Inavlid destination',
        subHeader: 'Please select a valid drop location',
        buttons: [
          {
            text: 'OK',
            role: 'cancel',
            handler: (data) => {
              this.nav.pop();
            },
          },
        ],
      });
      alert.present();
    }
  }

  getDb() {
    return this.firebaseService.getDatabase();
  }

  newPromoCode: any;
  addPromo() {
    if (this.bookingKey) {
      let booking = this.getDb().ref('bookings/' + this.bookingKey);
      booking.once('value', async (snapshot: any) => {
        if (snapshot.val()) {
          if (snapshot.val().promo_code) {
            //alert("Promo Code Already Added");

            let promocodeadd = await this.alertCtrl.create({
              header: 'Alert',
              subHeader: 'Promo Code Already Added',
              buttons: ['OK'],
            });
            promocodeadd.present();
          } else {
            this.showPromopopup();
          }
        }
      });
    } else {
      this.showPromopopup();
    }
  }

  async addCard() {
    // this.nav.push('pages/payments');
    let data = await this.modals.present(PaymentsPage, { isModal: true });
    console.log('CARD', data.data.item);
    this.cardData = data.data.item;
  }

  async showPromopopup() {
    let prompt = await this.alertCtrl.create({
      header: 'Add Promo Code',
      subHeader:
        'Promo Code wiil be applied in your bill after End of your Trip',
      inputs: [
        {
          name: 'title',
          placeholder: 'Code',
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          handler: (data) => {},
        },
        {
          text: 'Save',
          handler: (data) => {
            let pCode = data.title;
            let promoRef = this.getDb().ref('promoCodes');
            promoRef.once('value', async (snapData: any) => {
              if (snapData.val()) {
                let codes = snapData.val();
                let foundPromo = true;
                for (let promokey in codes) {
                  if (pCode == codes[promokey].promoName) {
                    foundPromo = false;
                    if (this.customerPromocode) {
                      let foundCust_Promo = true;
                      for (let matchcode in this.customerPromocode) {
                        if (
                          pCode == this.customerPromocode[matchcode].promoName
                        ) {
                          // foundCust_Promo = true;
                        } else {
                          foundCust_Promo = false;
                          this.codeFound = codes[promokey];
                          if (this.bookingDetails) {
                            this.bookingDetails.promo_code = codes[promokey];
                          }
                          let promo = await this.alertCtrl.create({
                            header: 'Alert',
                            subHeader: 'promo Code Added Successfully.',
                            buttons: ['OK'],
                          });
                          promo.present();
                        }
                        if (foundCust_Promo) {
                          let promo1 = await this.alertCtrl.create({
                            header: 'promo1',
                            subHeader: 'Promo Code Already Added.',
                            buttons: ['OK'],
                          });
                          promo1.present();
                        }
                      }
                    } else {
                      this.codeFound = codes[promokey];
                      if (this.bookingDetails) {
                        this.bookingDetails.promo_code = codes[promokey];
                      }
                      let promo = await this.alertCtrl.create({
                        header: 'Alert',
                        subHeader: 'promo Code Added Successfully.',
                        buttons: ['OK'],
                      });
                      promo.present();
                    }
                  }
                }
                if (foundPromo) {
                  let nopromocode = await this.alertCtrl.create({
                    header: 'Promocode',
                    subHeader: 'Promo Code not found',
                    buttons: ['OK'],
                  });
                  nopromocode.present();
                }
              } else {
                let nopromocode = await this.alertCtrl.create({
                  header: 'Promocode',
                  subHeader: 'Promo Code not found',
                  buttons: ['OK'],
                });
                nopromocode.present();
              }
            });
          },
        },
      ],
    });
    prompt.present();
  }

  requestDriver() {
    let selectedTime = this.showDate + 'T' + this.showTime + '.000Z';
    console.log(selectedTime);
    let timeNow = this.formatLocalDate('later-com');
    if (Number(new Date(timeNow)) > Number(new Date(selectedTime))) {
      this.showAlert('You cannot book driver for past time.');
      return false;
    }
    this.requestDone = true;
    let fireRef = this.getDb().ref('users/' + this.curUser.uid);
    fireRef.once('value', (snapdetails: any) => {
      if (snapdetails.val()) {
        if (snapdetails.val().payment_methods) {
          this.confirmBooking();
        } else {
          this.requestDone = false;
          let sndmsg = 'Please add a valid credit card';
          this.showAlert(sndmsg);
        }
      }
    });
  }

  async showAlert(msg) {
    let alert = await this.alertCtrl.create({
      header: 'ALERT',
      subHeader: msg,
      buttons: [
        {
          text: 'OK',
          role: 'cancel',
        },
      ],
    });
    alert.present();
  }

  formatLocalDate(state) {
    var finalDate;
    var now = new Date(),
      tzo = -now.getTimezoneOffset(),
      dif = tzo >= 0 ? '+' : '-',
      pad = function (num) {
        var norm = Math.abs(Math.floor(num));
        return (norm < 10 ? '0' : '') + norm;
      };

    if (state == 'later') {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate()) +
        'T' +
        pad(now.getHours() + 1) +
        ':' +
        pad(now.getMinutes()) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    } else if (state == 'later-com') {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate()) +
        'T' +
        pad(now.getHours()) +
        ':' +
        pad(now.getMinutes() - 2) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    } else if (state == 'yesterday') {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate() - 1) +
        'T' +
        pad(now.getHours()) +
        ':' +
        pad(now.getMinutes()) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    } else {
      finalDate =
        now.getFullYear() +
        '-' +
        pad(now.getMonth() + 1) +
        '-' +
        pad(now.getDate()) +
        'T' +
        pad(now.getHours()) +
        ':' +
        pad(now.getMinutes()) +
        ':' +
        pad(now.getSeconds()) +
        '.000Z';
    }

    return finalDate;
  }

  confirmBooking() {
    //*TODO*// customer_push_token is undefined
    if (this.codeFound) {
      this.newPromoCode = this.codeFound;
      // this.bookingDetails.promo_code = this.codeFound;
      console.log('promo code found : => ' + this.newPromoCode);
    } else {
      this.newPromoCode = null;
      // this.bookingDetails.promo_code = null ;
      console.log('promo code not found : => ' + this.newPromoCode);
    }

    if (this.bookingKey) {
      if (this.originalDate) {
        this.bookingDetails.tripdate = this.originalDate;
      } else {
        this.bookingDetails.tripdate =
          this.bookingDetails.tripdate ?? this.originalDate;
      }

      if (this.bookingDetails.promo_code) {
        this.bookingDetails.promo_code = this.bookingDetails.promo_code;
      }

      if (this.newPromoCode) {
        console.log(
          'edit time call promocode required.........',
          this.newPromoCode
        );
        this.bookingDetails.promo_code = this.newPromoCode;
        let ref = this.getDb().ref('/users/' + this.curUser.uid);
        ref.once('value', (snap1: any) => {
          console.log('edit time call promocode *********.........');
          let addpromocode = ref.child('promo_code').push().key;
          ref.child('promo_code/' + addpromocode).set(this.newPromoCode);
        });
      } else {
        this.bookingDetails.promo_code = null;
      }

      let ref = this.getDb().ref('bookings/' + this.bookingKey);

      ref.set(this.bookingDetails);

      let updates = {};
      console.log('BOOKING_DETAILS', this.bookingDetails);

      if (this.bookingDetails.driver) {
        //new created by me.. part2
        if (this.PickupService) {
          let customerData = {
            customer: this.bookingDetails.customer,
            driver: this.bookingDetails.driver,
            driver_name: this.bookingDetails.driver_name,
            driver_image: this.bookingDetails.driver_image,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop?.add ?? null,
            tripdate: this.bookingDetails.tripdate ?? this.originalDate,
            customer_push_token: this.bookingDetails.customer_push_token ?? '',
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };

          let driverData = {
            customer: this.bookingDetails.customer,
            customer_name: this.bookingDetails.customer_name,
            customer_image: this.bookingDetails.customer_image,
            driver: this.bookingDetails.driver,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop?.add ?? null,
            customer_push_token: this.bookingDetails.customer_push_token ?? '',
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.driver +
              '/bookings/' +
              this.bookingKey
          ] = driverData;
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        } else {
          let customerData = {
            customer: this.bookingDetails.customer,
            driver: this.bookingDetails.driver,
            driver_name: this.bookingDetails.driver_name,
            driver_image: this.bookingDetails.driver_image,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            // dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate ?? this.originalDate,
            customer_push_token: this.bookingDetails.customer_push_token ?? '',
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };

          let driverData = {
            customer: this.bookingDetails.customer,
            customer_name: this.bookingDetails.customer_name,
            customer_image: this.bookingDetails.customer_image,
            driver: this.bookingDetails.driver,
            status: this.bookingDetails.status,
            //pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop?.add ?? null,
            tripdate: this.bookingDetails.tripdate ?? this.originalDate,
            customer_push_token: this.bookingDetails.customer_push_token ?? '',
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.driver +
              '/bookings/' +
              this.bookingKey
          ] = driverData;
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        }
      } else {
        if (this.PickupService) {
          let customerData = {
            customer: this.bookingDetails.customer,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate ?? this.originalDate,
            tripType: this.trip,
            customer_push_token: this.bookingDetails.customer_push_token ?? '',
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        } else {
          let customerData = {
            customer: this.bookingDetails.customer,
            status: this.bookingDetails.status,
            pickupAddress: this.bookingDetails.pickup.add,
            // dropAddress: this.bookingDetails.drop.add,
            tripdate: this.bookingDetails.tripdate ?? this.originalDate,
            tripType: this.trip,
            customer_push_token: this.bookingDetails.customer_push_token ?? '',
            serviceType: this.bookingDetails.serviceType,
            // promo_code:this.bookingDetails.newPromoCode   //new added for promo code
          };
          updates[
            '/users/' +
              this.bookingDetails.customer +
              '/bookings/' +
              this.bookingKey
          ] = customerData;
        }
      }

      console.log(updates);

      this.getDb().ref().update(updates);
      this.nav.setRoot('pages/currentbookings');
      this.utility.presentToast('Booking updated successfully!');
      this.modals.dismiss({ data: this.bookingDetails });
      // this.bookingConfirmAlert();

      // this.navCtrl.setRoot(BidDriverPagePage,{
      //                         from:this.bookingDetails.pickup.add,
      //                         to:this.bookingDetails.drop.add,
      //                         pickupservice:this.bookingDetails.serviceType
      //                     });
    } else {
      console.log(
        'first time booking........................',
        this.newPromoCode
      );
      let ref = this.getDb().ref('/users/' + this.curUser.uid);

      ref.once('value', (snap1: any) => {
        if (snap1.val() != null) {
          let userProfile = snap1.val();

          var shortBookingData = {
            // tripdate:this.myDate,
            tripdate: this.originalDate,
            pickupAddress: this.pick.add,
            dropAddress: this.drop.add,
            customer: this.curUser.uid,
            tripType: this.trip,
            // status:'NEW' , //added by Sach23
            status: 'Finding your driver', // added by Hemant
            customer_push_token: this.usersService.getToken(),
            serviceType: this.serviceType,
            // promo_code:this.newPromoCode   //new added for promo code
          };

          var bookingData = {
            customer: this.curUser.uid,
            customer_name: userProfile.fullname,
            customer_push_token: this.usersService.getToken(),
            pickup: this.pick,
            drop: this.drop,
            distance: this.distance,
            // tripdate:this.myDate,
            tripdate: this.originalDate,
            triptime: this.triptime,
            transmission: this.transmission,
            estimate: this.estimate,
            tripType: this.trip,
            // status:'NEW' , //added by Sach23
            status: 'Finding your driver', // added by Hemant
            serviceType: this.serviceType, //new created by me....
            promo_code: this.newPromoCode, //new added for promo code
          };
          if (userProfile.profile_image)
            bookingData['customer_image'] = userProfile.profile_image;

          let newBooking = ref.child('bookings').push().key;
          let addpromocode = ref.child('promo_code').push().key;
          console.log('bookingData', bookingData);

          this.getDb()
            .ref('bookings/' + newBooking)
            .set(bookingData);
          ref.child('bookings/' + newBooking).set(shortBookingData);
          ref.child('promo_code/' + addpromocode).set(this.newPromoCode);
          //promo code
          /*  if(this.newPromoCode!=undefined || this.newPromoCode!=null){
                firebase.database().ref('bookings/' + newBooking).child('/promo_code').set(this.newPromoCode);
            }
          */
          //end
          var dbref = this.getDb().ref();
          var geoFire = new GeoFire(dbref.child('geofire'));

          geoFire.set(newBooking, [this.pick.lat, this.pick.lng]).then(
            function () {
              console.log('Provided key has been added to GeoFire');
            },
            function (error) {
              console.log('Error: ' + error);
            }
          );
          this.bookingConfirmAlert();
          // this.navCtrl.setRoot(CurrentbookingsPage);
        }
      });
    }

    this.events.publish('BOOKING_CONFIRMED');
  }

  calculateDateTime() {
    //this.originalDate = this.sDate + 'T' + this.sTime + '.000Z';
    this.originalDate = this.showDate + 'T' + this.showTime + '.000Z';
    console.log('calculate datetime =>' + this.originalDate);
    //this.bookingDetails.tripdate = this.originalDate;

    /*    + '-' + pad(now.getMonth()+1)
        + '-' + pad(now.getDate())
        + 'T' + pad(now.getHours())
        + ':' + pad(now.getMinutes())
        + ':' + pad(now.getSeconds())
        + '.000Z'*/
  }

  async bookingConfirmAlert() {
    let alert = await this.alertCtrl.create({
      header: 'ALERT',
      subHeader:
        'Your booking is scheduled for ' +
        this.showTime +
        '. You will receive a notification when a driver is assigned.',
      buttons: [
        {
          text: 'OK',
          handler: (data) => {
            this.modals?.dismiss({ data: 'A' });
            this.Notification();
            // this.navCtrl.setRoot(CurrentbookingsPage);
          },
        },
      ],
    });
    alert.present();
  }

  Notification() {
    console.log('notification...................', this.notificationData);
    var cnt = 0;
    for (let key in this.notificationData) {
      cnt++;
      this.usersService
        .sendPushDriver(
          'Customer Booking',
          this.pick.add,
          this.notificationData[key].token
        )
        .subscribe((response) => {
          console.log('response........', response);
          if (this.notificationData.length == cnt) {
            this.notificationData = [];
            cnt = 0;
            // this.navCtrl.setRoot(BidDriverPagePage,{
            //                       from:this.pickup.add,
            //                       to:this.drop.add,
            //                       pickupservice:this.serviceType
            //                   });
          }
        });
    }
    this.nav.setRoot('pages/currentbookings');
  }
  getParams() {
    let data = this.dataService.farePageData;
    console.log('getParams', data);

    return data;
  }
}
