import { Component, Injector, Input, OnInit } from '@angular/core';
import { DataService } from 'src/app/Services/data.service';
import { AutocompletePage } from '../../autocomplete/autocomplete.page';
import { BasePage } from '../../base-page/base-page';

@Component({
  selector: 'app-book-now',
  templateUrl: './book-now.component.html',
  styleUrls: ['./book-now.component.scss'],
})
export class BookNowComponent extends BasePage implements OnInit {
  step = 1;
  pickupService = true;

  @Input() drop: any = {
    lat: 0,
    lng: 0,
    add: null,
  };

  @Input() pick = {
    lat: 0,
    lng: 0,
    add: null,
  };

  jobtype = 'pickup';

  constructor(injector: Injector, private dataService: DataService) {
    super(injector);
    let data = dataService.farePageData;
    if (data) {
      if (data.dropDet) this.drop = data.dropDet;
      if (data.pickupDet) this.pick = data.pickupDet;
    }
  }

  ngOnInit() {}

  close() {
    this.modals.dismiss();
  }

  async showAddressModal(addressType) {
    const res = await this.modals.present(AutocompletePage);
    console.log(res);

    let data = res.data;
    data.addressType = addressType ?? '';
    if (data && data.add) {
      if (addressType == 'pick') {
        this.pick = data;
      }

      if (addressType == 'drop') {
        this.drop = data;
      }
      this.events.publish('add_address', { pick: this.pick, drop: this.drop });
    }
  }

  selectedService($event) {
    let v = $event.target.value;
    console.log(v);
  }

  bookNow() {
    let pickupService = this.jobtype == 'pickup';

    // let data = {
    //   jobtype: this.jobtype,
    //   pick: this.pick,
    //   drop: this.drop
    // };
    // this.dataService.farePageData = data;

    // this.modals.dismiss();

    // this.events.publish('redirect_to_fare_page', data);

    if (pickupService == true) {
      console.log('Pickup service is true');
      //new change
      // this.map.remove();
      if (this.pick.add && this.drop.add) {
        console.log('Add & Drop found', this.pick.add, this.drop);
        let data = {
          pickupDet: this.pick,
          dropDet: this.drop,
          pickupService: pickupService,
          jobtype: 'pickup',
        };
        this.events.publish('redirect_to_fare_page', data);
        this.dataService.farePageData = data;
        this.step = 2;
        this.modals.dismiss();
      } else if (!this.drop.add) {
        let showMsg = 'please select destination';
        this.utility.presentFailureToast(showMsg);
      } else if (!this.pick.add) {
        let showMsg = 'please select pickup';
        this.utility.presentFailureToast(showMsg);
      }
    } else {
      console.log('Pickup service is false');
      if (this.pick.add) {
        console.log('Pickup add found');
        //this.map.remove();
        let data = {
          pickupDet: this.pick,
          pickupService: pickupService,
          jobtype: 'personal',
        };
        this.dataService.farePageData = data;
        this.events.publish('redirect_to_fare_page', data);
        this.step = 2;
        this.modals.dismiss();
      }
    }
  }
}
