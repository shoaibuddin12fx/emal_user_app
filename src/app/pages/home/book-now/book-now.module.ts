import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BookNowComponent } from './book-now.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AutocompletePageModule } from '../../autocomplete/autocomplete.module';



@NgModule({
  declarations: [BookNowComponent],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    AutocompletePageModule
  ],
  exports: [
    BookNowComponent
  ]
})
export class BookNowModule { }
