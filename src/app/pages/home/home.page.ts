import {
  AfterViewInit,
  Component,
  ElementRef,
  Injector,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ViewWillEnter } from '@ionic/angular';
import { BasePage } from '../base-page/base-page';
import { BookNowComponent } from './book-now/book-now.component';
import { FareNowComponent } from './fare-now/fare-now.component';
declare const google;
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage
  extends BasePage
  implements OnInit, AfterViewInit, ViewWillEnter
{
  @ViewChild('map', { static: false }) mapElement: ElementRef;
  map: any;
  mapReady: boolean;
  pickup: { lat: any; lng: any; add: any };
  curUser: any;
  setMapInfo: any;
  autoChange: boolean;
  searchBoundsTimer;
  drop_marker: any;
  pick_marker: any;
  static locationParams;
  directionsService;
  directionsDisplay;
  userLocation;
  isBookingConfirmed = false;
  oldPickup;
  oldDrop;

  constructor(injector: Injector) {
    super(injector);

    let params = this.getQueryParams();
    //end
    if (params?.add == undefined || params?.add == null) {
      // call map here for assign pin location to map shown
      // this.nav.setRoot('pages/locationloader');
    }

    this.mapReady = false;
    this.mapService.showmap = true;

    this.pickup = {
      lat: params?.lat,
      lng: params?.lng,
      add: params?.add,
    };

    this.events.subscribe('add_address', this.setMarkerToMap.bind(this));
    this.events.subscribe(
      'redirect_to_fare_page',
      this.redirectToFarePage.bind(this)
    );
  }
  ionViewWillEnter(): void {
    this.openBookNow();
  }

  setMarkerToMap(data) {
    console.log('marker set', data);

    const bounds = new google.maps.LatLngBounds();

    if (data.pick.add) {
      if (
        this.oldPickup?.lat != data.pick.lat &&
        this.oldPickup?.lng != data.pick.lng
      ) {
        this.pick_marker = new google.maps.Marker({
          position: new google.maps.LatLng(data.pick.lat, data.pick.lng),
          map: this.map,
          draggable: true,
          title: data.pick.add,
        });
        this.oldPickup = data.pick;
        bounds.extend(data.pick);
      }
    } else this.pick_marker = null;

    console.log('isSame');

    if (data.drop.add) {
      if (
        this.oldDrop?.lat != data.drop.lat &&
        this.oldDrop?.lng != data.drop.lng
      ) {
        this.drop_marker = new google.maps.Marker({
          position: new google.maps.LatLng(data.drop.lat, data.drop.lng),
          map: this.map,
          draggable: true,
          title: data.drop.add,
        });

        bounds.extend(data.drop);
        this.oldDrop = data.drop;
      }
    } else this.drop_marker = null;
    this.map.fitBounds(bounds);

    if (this.pick_marker && this.drop_marker) {
      console.log('we can work from here', this.pick_marker.position.lat());
      this.directionsService = new google.maps.DirectionsService();
      this.directionsDisplay?.setMap(null);
      this.directionsDisplay = new google.maps.DirectionsRenderer({
        map: this.map,
      });
      this.pick_marker.setMap(null);
      this.drop_marker.setMap(null);
      this.calculateAndDisplayRoute(
        this.directionsService,
        this.directionsDisplay,
        this.pick_marker.position,
        this.drop_marker.position
      );
    }
  }

  calculateAndDisplayRoute(
    directionsService,
    directionsDisplay,
    pointA,
    pointB
  ) {
    directionsService.route(
      {
        origin: pointA,
        destination: pointB,
        travelMode: google.maps.TravelMode.DRIVING,
      },
      function (response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
          directionsDisplay.setDirections(response);
        } else {
          window.alert('Directions request failed due to ' + status);
        }
      }
    );
  }

  async redirectToFarePage(data) {
    console.log('redirect ', data);

    // let data = {
    //   pickupDet: this.pick,
    //   dropDet: this.drop,
    //   pickupService: pickupService,
    //   jobtype: 'pickup'
    // }

    // let data = {
    //   pickupDet: this.pick,
    //   pickupService: pickupService,
    //   jobtype: 'personal'
    // }

    // this.nav.push('pages/fare', data);
    await this.modals.dismiss({ data: 'A' });
    await this.modals.present(FareNowComponent, { data });
    // this.openBookNow();
  }

  async ngOnInit() {
    this.curUser = await this.firebaseService.getCurrentUser();
    this.menuCtrl.enable(true, 'drawer');
    this.events.subscribe('BOOKING_CONFIRMED', () => {
      console.log('BOOKING_CONFIRMED event recieved');
      this.isBookingConfirmed = true;
      this.loadMap();
    });
  }

  ngAfterViewInit(): void {
    this.platform.ready().then(() => {
      this.initializeMapBeforeSetCoordinates().then((v) => {
        this.initMap();

        setTimeout(async () => {
          const active = localStorage.getItem('google_map');
          if (!active) {
            const flag = await this.utility.presentConfirm(
              'Thanks',
              'Remind Later',
              'How To Use',
              'In the search field, type in the new address and press enter. You can also hold your finger and drag to new location, then press the + symbol in the top right corner'
            );
            localStorage.setItem('google_map', `${flag}`);
          } else {
            // this.openBookNow();
          }
        }, 1000);
      });
    });
  }

  async initializeMapBeforeSetCoordinates() {
    return new Promise(async (resolve) => {
      console.log('initializeMapBeforeSetCoordinates');
      const mylocation = await this.utility.getCurrentLocationCoordinates();
      console.log('My Location', mylocation);
      this.userLocation = mylocation;
      resolve(mylocation);
    });
  }

  initMap() {
    this.loadMap();
  }

  async loadMap() {
    //  const mylocation = await this.utility.getCurrentLocationCoordinates();
    // console.log("My Location", mylocation)
    let params = this.getQueryParams();

    if (!HomePage.locationParams) HomePage.locationParams = params;

    params = HomePage.locationParams;
    let myLocation = {};
    if (params) {
      myLocation = {
        lat: parseFloat(params?.lat),
        lng: parseFloat(params.lng),
      };
    } else
      myLocation = {
        lat: this.userLocation?.lat,
        lng: this.userLocation?.lng,
      };

    console.log('userLocation', this.userLocation);

    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      zoom: 15,
      center: myLocation,
    });

    // this.map = new GoogleMap('map', {
    //   backgroundColor: 'white',
    //   controls: {
    //     compass: false,
    //     myLocationButton: false,
    //     indoorPicker: false,
    //     zoom: true,
    //   },
    //   gestures: {
    //     scroll: true,
    //     tilt: true,
    //     rotate: true,
    //     zoom: true,
    //   },
    //   camera: {
    //     target: location,
    //     tilt: 30,
    //     zoom: 15, //,
    //     // 'bearing': 50
    //   },
    // });

    let info = this.setMapInfo;
    let self = this;

    // * CAMERA_CHANGE * //

    google.maps.event.addListener(this.map, 'dragend', (event) => {
      if (!event) return;
      const lt = event.latLng.lat();
      const lg = event.latLng.lng();
      const coords = { lat: lt, lng: lg };
      if (this.autoChange == true) {
        clearTimeout(this.searchBoundsTimer);

        this.searchBoundsTimer = setTimeout(() => {
          this.geocodeAddress(lt, lg, (data) => {
            this.setMapInfo(
              data.geometry.location.lat(),
              data.geometry.location.lng(),
              data.formatted_address
            );
            // if (this.pickup.add && this.drop.add)
            //   this.setDistance(this.pickup, this.drop);
            console.log('map data: ');
            console.log(data);
          });
        }, 500);
      } else {
        this.autoChange = true;
      }
    });

    this.map.addListener('click', (mapsMouseEvent) => {
      let position = JSON.stringify(mapsMouseEvent.latLng.toJSON(), null, 2);
      console.log('OnMapClicked', position);
    });

    // this.map.on(GoogleMapsEvent.CAMERA_MOVE).subscribe((res) => {
    //   if (this.autoChange == true) {
    //     clearTimeout(this.searchBoundsTimer);

    //     this.searchBoundsTimer = setTimeout(() => {
    //       this.geocodeAddress(res.target.lat, res.target.lng, (data) => {
    //         this.setMapInfo(
    //           data.geometry.location.lat(),
    //           data.geometry.location.lng(),
    //           data.formatted_address
    //         );
    //         if (this.pickup.add && this.drop.add)
    //           this.setDistance(this.pickup, this.drop);
    //         console.log('map data: ');
    //         console.log(data);
    //       });
    //     }, 500);
    //   } else {
    //     this.autoChange = true;
    //   }
    // });

    google.maps.event.addListenerOnce(this.map, 'idle', function () {
      // do something only the first time the map is loaded
      this.map?.markers.clear();
      this.mapReady = true;
      console.log('Map is ready!');
      this.markerSetTo = 'pickup';
    });
  }

  geocodeAddress(lat, lng, callback) {
    let geocoder = new google.maps.Geocoder();
    let latlng = { lat: lat, lng: lng };
    let request = {
      latLng: latlng,
    };
    geocoder.geocode({ latLng: latlng }, function (data, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        callback(data[0]);
      } else {
        console.log('Geocode Error');
      }
    });
  }

  async openBookNow() {
    let self = this;
    console.log('isBookingConfirmed', this.isBookingConfirmed);

    const res = await this.modals.present(
      BookNowComponent,
      self.isBookingConfirmed
        ? {
            drop: {
              lat: 0,
              lng: 0,
              add: null,
            },
            pick: {
              lat: 0,
              lng: 0,
              add: null,
            },
          }
        : {},
      'half-modal-book-now'
    );
    this.isBookingConfirmed = false;
  }
}
