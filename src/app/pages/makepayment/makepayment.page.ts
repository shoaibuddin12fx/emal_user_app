import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal/ngx';

@Component({
  selector: 'app-makepayment',
  templateUrl: './makepayment.page.html',
  styleUrls: ['./makepayment.page.scss'],
})
export class MakepaymentPage extends BasePage implements OnInit {
  public VIPfeatureshidden = false;
  public VIPstatushidden = true;
  id: any;

  constructor(injector: Injector, private payPal: PayPal) {
    super(injector);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MakepaymentPage');
  }

  payNow() {
    this.payPal.init({
      // "PayPalEnvironmentProduction": "AdcWwdEFBzyVHgXXnrOQsKwwh5JL-uktFwU6Ppd7Yea7lLy4kCLETWuvF1bu8BUrMic2hoGddEN9REDx",
      PayPalEnvironmentProduction: '',
      // "PayPalEnvironmentSandbox": "",
      PayPalEnvironmentSandbox:
        'AYm-lSHMp7nvRnauOYWlWC1iUXqVZQ6rnihaqcBbTJozNr67JK3z5ZdgXTkp67Xi0k0Ud5x1YTU8de2w',
    }).then(
      () => {
        alert('paypal start');
        console.log('paypal start');

        this.payPal.prepareToRender(
          'PayPalEnvironmentProduction',
          new PayPalConfiguration({
            // Only needed if you get an "Internal Service Error" after PayPal login!
            //payPalShippingAddressOption: 2 // PayPalShippingAddressOptionPayPal
          })
        ).then(
          () => {
            alert('paypal rander start');
            console.log('paypal Rander start');
            let payment = new PayPalPayment(
              '9.99',
              'USD',
              '$9.99/Month',
              'sale'
            );

            this.payPal.renderSinglePaymentUI(payment).then(
              async (response) => {
                console.log(response);
                let paymentData = {
                  id: response.response.id,
                  intent: response.response.intent,
                  state: response.response.state,
                  create_time: response.response.create_time,
                };
                let useId = (await this.firebaseService.getCurrentUser()).uid;
                let reff = this.getDbRef('/users/' + useId + '/paymentDetails/');

                reff.set(paymentData);
                // alert("Successfully paid");
                this.VIPfeatureshidden = !this.VIPfeatureshidden;
                this.VIPstatushidden = !this.VIPstatushidden;
              },
              () => {
                // Error or render dialog closed without being successful
                alert('payment not possible');
              }
            );
          },
          () => {
            // Error in configuration
            alert(' configuration problem');
          }
        );
      },
      () => {
        // Error in initialization, maybe PayPal isn't supported or something else
        alert("PayPal isn't supported or something else");
      }
    );
  }
  ngOnInit() { }

  getDbRef(ref) {
    return this.firebaseService.getDatabase().ref(ref)
  }
}
