import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { EmptyviewComponent } from './emptyview.component';

@NgModule({
  declarations: [EmptyviewComponent],
  imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule],
  exports: [EmptyviewComponent],
})
export class EmptyviewModule {}
