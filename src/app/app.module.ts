import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { provideAuth, getAuth } from '@angular/fire/auth';
import { provideDatabase, getDatabase } from '@angular/fire/database';
import { getFirestore, provideFirestore } from '@angular/fire/firestore';
import { UserService } from './Services/user-service';
import { HttpClientModule } from '@angular/common/http';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AngularFireStorageModule, BUCKET } from '@angular/fire/compat/storage';
import { environment } from 'src/environments/environment';
import { DatabaseModule } from '@angular/fire/database';
import { UtilityService } from './services/utility.service';
import { NetworkService } from './services/network.service';
import { TimesService } from './services/times.service';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { ReactiveFormsModule } from '@angular/forms';
import { MapService } from './Services/map-service';
import { GeolocationService } from './Services/geolocation.service';
// import { Facebook } from '@ionic-native/facebook/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CardIO } from '@ionic-native/card-io/ngx';
import { GmapComponent } from './components/gmap/gmap.component';
import { AngularFireModule } from '@angular/fire/compat';

// import { AngularFireModule } from '@angular/fire';

@NgModule({
  declarations: [AppComponent, GmapComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot({
      mode: 'ios'
    }),
    AppRoutingModule,
    provideAuth(() => getAuth()),
    provideDatabase(() => getDatabase()),
    //  provideFirebaseApp(() => initializeApp(environment.firebase)),
    provideFirestore(() => getFirestore()),
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    DatabaseModule,
    ReactiveFormsModule,
  ],

  // AngularFireAuthModule,
  // AngularFireStorageModule,
  // AngularFireDatabaseModule
  providers: [
    {
      provide: RouteReuseStrategy,
      useClass: IonicRouteStrategy,
    },
    UserService,
    GooglePlus,
    UtilityService,
    NetworkService,
    TimesService,
    DatePicker,
    NgxPubSubService,
    Geolocation,
    GeolocationService,
    MapService,
    //  Facebook,
    NativeGeocoder,
    CallNumber,
    CardIO,
    { provide: BUCKET, useValue: 'my-bucket-name' }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
