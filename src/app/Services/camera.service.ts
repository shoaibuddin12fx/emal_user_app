import { Injectable } from '@angular/core';
import { ActionSheetController } from '@ionic/angular';
@Injectable({
  providedIn: 'root',
})
export class CameraService {
  constructor(
    private actionSheetController: ActionSheetController
  ) { }

  // async selectImage() {
  //   const actionSheet = await this.actionSheetController.create({
  //     // header: "Select Image source",
  //     cssClass: 'select-image-css',
  //     buttons: [{
  //       // text: 'Choose photo from Gallery',
  //       text: this.translateService.instant("Choose_photo_from_Gallery"),
  //       icon: 'images',
  //       cssClass: 'images-icon',
  //       handler: () => {
  //         this.pickImage(this.camera.PictureSourceType.PHOTOLIBRARY);
  //       }
  //     },
  //     {
  //       // text: 'Take photo',
  //       text: this.translateService.instant("Take_photo"),
  //       icon: 'camera',
  //       cssClass: 'camera-icon',
  //       handler: () => {
  //         this.pickImage(this.camera.PictureSourceType.CAMERA);
  //       }
  //     },
  //     {
  //       // text: 'Cancel',
  //       text: this.translateService.instant("Cancel"),
  //       role: 'cancel',
  //       icon: 'close',
  //       cssClass: 'close-icon'
  //     }
  //     ]
  //   });
  //   await actionSheet.present();
  // }

  // pickImage(sourceType,profilePic) {
  //   const options: CameraOptions = {
  //     quality: 100,
  //     sourceType: sourceType,
  //     destinationType: Camera.DestinationType.DATA_URL,
  //     encodingType: Camera.EncodingType.JPEG,
  //     mediaType: Camera.MediaType.PICTURE
  //   }
  //   this.camera.getPicture(options).then((imageData) => {
  //     let base64Image = 'data:image/jpeg;base64,' + imageData;
  //     profilePic = base64Image;
  //   }, (err) => {
  //     // Handle error
  //   });
  // }
}
