import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import 'rxjs/add/operator/map';
// import { Facebook } from 'ionic-native';

import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { Observable } from 'rxjs';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { FirebaseService } from './firebase.service';

declare var google: any;

@Injectable()
export class UserService {
  public curUser;
  public loginType: any = '';
  public userProfile: any;
  public reviewApp: any;
  ref: any;
  constructor(
    public http: HttpClient,
    private googlePlus: GooglePlus,
    // public fireAuth: AngularFireAuth,
    // public firebase: AngularFireDatabase
    private firebase: FirebaseService
  ) {
    this.userProfile = this.firebase.getDatabase().ref('users');
    this.reviewApp = firebase.getDatabase().ref('review');
    this.onInit();
  }
  async onInit() {
    let user = await this.firebase.getCurrentUser();
    if (user)
      this.ref = this.firebase
        .getDatabase()
        .ref('/users/' + user['uid'] + '/email');
  }

  //SignUpUsers
  signUpUser(
    fullname: any,
    email: any,
    password: any,
    phone: any,
    cityname: any,
    vehicle: 12345
  ) {
    return this.firebase.fireAuth
      .createUserWithEmailAndPassword(email, password)
      .then((newUser) => {
        //sign in the user
        this.firebase.fireAuth
          .signInWithEmailAndPassword(email, password)
          .then(async (authenticatedUser) => {
            //successful login, create user profile
            //new added by me
            authenticatedUser.user.sendEmailVerification().then(
              function () {
                console.log('please check email');
              },
              function (error) {
                // An error happened.
              }
            );

            this.userProfile.child(authenticatedUser.user.uid).set({
              fullname: fullname,
              email: email,
              phone: phone,
              cityname: cityname,
              // vehicle: vehicle,
              profile_image:
                'https://firebasestorage.googleapis.com/v0/b/pickmyride-9f86d.appspot.com/o/images%2Fdefault.png?alt=media&token=f9177f5d-4f9b-4d11-a668-db0674a44e32',
              customer: true,
              islogin: 'customer',
            });
          });
      });
  }

  insertUser(
    fullname: any,
    email: any,
    phone: any,
    cityname: any,
    vehicle: 12345,
    AuthUser: any
  ) {
    return this.userProfile.child(AuthUser.uid).update({
      profile_image: AuthUser.photoURL,
      fullname: fullname,
      email: email,
      phone: phone,
      cityname: cityname,
      vehicle: vehicle,
      customer: true,
    });
  }

  //LoginUser
  loginUser(email: any, password: any): any {
    //return this.fireAuth.signInWithEmailAndPassword(email, password);
    return this.firebase.fireAuth
      .signInWithEmailAndPassword(email, password)
      .then((authenticatedUser) => {
        console.log(this.userProfile);
        this.userProfile.child(authenticatedUser.user.uid).update({
          customer: true,
          islogin: 'customer',
          customer_push_token: localStorage.getItem('FCM_TOKEN'),
          pushToken: localStorage.getItem('FCM_TOKEN'),
        });
      });
  }

  //facebook insert data on database
  signFacebook(success) {
    this.userProfile.child(success.uid).set({
      // displayname:success.displayName,
      //  provider:success.providerData[0].providerId,
      email: success.email,
    });
  }

  //Google insert Data on database
  signGoogle(success, provider) {
    this.userProfile.child(success.uid).set({
      //displayname:success.displayName
      //  provider:provider.provider,
      email: success.email,
    });
  }

  getToken() {
    return localStorage.getItem('FCM_TOKEN');
  }

  setToken(token) {
    localStorage.setItem('FCM_TOKEN', token);
  }

  //logout
  logoutUser() {
    if (this.loginType == 'Google') {
      this.googlePlus.logout().then((response) => {
        console.log(response);
      });
    } else if (this.loginType == 'Facebook') {
      // Facebook.logout().then((response) => {
      //   console.log(response);
      // });
    }

    return this.firebase.fireAuth.signOut();
  }

  //update data from profile
  updateData(data, userId) {
    return this.userProfile.child(userId).update({
      fullname: data.fullname,
      // lastname:data.lastname,
      email: data.email,
      phone: data.phone,
      cityname: data.cityname,
      registration: data.registration,
    });
  }
  /*
  //review app and mark favourite
  review(rate,favourite,userId){
     return this.reviewApp.child(userId).set({
          rating:rate,
          favourite:favourite
     })
  }*/

  //password reset
  passReset(email) {
    return new Promise<string>((res) => {
      this.firebase.fireAuth
        .sendPasswordResetEmail(email)
        .then((success) => {
          console.log('Sucess', success);
          res('Success');
        })
        .catch((error) => {
          res('Invalid or incomplete email');
        });
    });
  }

  addStripeCard(card, stripeId, email): Observable<object> {
    let body = {
      number: card.cardNumber,
      exp_month: card.expiryMonth,
      exp_year: card.expiryYear,
      cvc: card.cvv,
      email: email,
      stripeId: stripeId,
    };
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.post(
      'https://us-central1-bidrider-app.cloudfunctions.net/addStripeCard',
      body,
      options
    );
    //.subscribe(res => JSON.stringify(res))
    // .map((res: Response) => res.json())
    // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  stripePayment(card, cust_id, amount): Observable<object> {
    console.log('card..................', card);
    let body = {
      customer_id: cust_id,
      card_id: card,
      amount: amount,
    };
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.post(
      'https://us-central1-bidrider-app.cloudfunctions.net/stripPayment',
      body,
      options
    );
    // .map((res: Response) => res.json())
    // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  deleteStripeCard(card, cust_id): Observable<object> {
    console.log('card..................', card);
    let body = {
      customer_id: cust_id,
      card_id: card,
    };
    let headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    let options = { headers: headers };
    return this.http.post(
      'https://us-central1-bidrider-app.cloudfunctions.net/deleteStripeCard',
      body,
      options
    );
    // .map((res: Response) => res.json())
    // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  sendPushDriver(title, msg, token): Observable<object> {
    console.log(token);
    let body = {
      to: token,
      notification: {
        body: msg,
        title: title,
      },
    };
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization:
        'key=AAAAUSDzzoo:APA91bENj5bNXyieaUcAcTDuSRfADbkWNgTekjkixWxsE4LL9QkSnSHbAtwfeozcH40H5fhk_9PtA9yWTkTOHDPuFw9ksb7aHHEdnSpx5U6JVwNzqKS7B9Y-aguSAcrnAsUTP5u5lRHr',
    });
    let options = { headers: headers };
    return this.http.post('https://fcm.googleapis.com/fcm/send', body, options);
    // .map((res: Response) => res.json())
    // .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }
}
