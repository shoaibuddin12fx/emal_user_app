import { StringsService } from './basic/strings.service';
import { Injectable } from '@angular/core';
import { Platform } from '@ionic/angular';
import { AlertsService } from './basic/alerts.service';
import { LoadingService } from './basic/loading.service';
import { TimesService } from './times.service';
import { ImageService } from './image.service';
import { StorageService } from './basic/storage.service';
import { GeolocationService } from './geolocation.service';

@Injectable({
  providedIn: 'root',
})
export class UtilityService {
  constructor(
    public loading: LoadingService,
    public plt: Platform,
    public alerts: AlertsService,
    public images: ImageService,
    public times: TimesService,
    public strings: StringsService,
    public storage: StorageService,
    public geolocations: GeolocationService,
  ) { }

  showLoader(msg = 'Please wait...') {
    return this.loading.showLoader(msg);
  }

  hideLoader() {
    return this.loading.hideLoader();
  }

  showAlert(msg) {
    return this.alerts.showAlert(msg);
  }

  presentToast(msg) {
    return this.alerts.presentToast(msg);
  }

  presentSuccessToast(msg) {
    return this.alerts.presentSuccessToast(msg);
  }

  presentFailureToast(msg) {
    return this.alerts.presentFailureToast(msg);
  }

  presentConfirm(
    okText = 'OK',
    cancelText = 'Cancel',
    title = 'Are You Sure?',
    message = ''
  ): Promise<boolean> {
    return this.alerts.presentConfirm(
      (okText = okText),
      (cancelText = cancelText),
      (title = title),
      (message = message)
    );
  }

  isOverThirteen(dob) {
    return this.times.isOverThirteen(dob);
  }

  /** Storage Service */

  setKey(key, value) {
    return this.storage.set(key, value);
  }

  getKey(key) {
    return this.storage.get(key);
  }

  /** Strings Service */

  capitalizeEachFirst(str) {
    return this.strings.capitalizeEachFirst(str);
  }

  onkeyupFormatPhoneNumberRuntime(phoneNumber, last = true) {
    return this.strings.onkeyupFormatPhoneNumberRuntime(phoneNumber, last);
  }

  onkeyupFormatCardNumberRuntime(phoneNumber, last = true) {
    return this.strings.onkeyupFormatCardNumberRuntime(phoneNumber, last);
  }

  formatPhoneNumberRuntime(phoneNumber) {
    return this.strings.formatPhoneNumberRuntime(phoneNumber);
  }

  isPhoneNumberValid(num) {
    return this.strings.isPhoneNumberValid(num);
  }

  checkIfMatchingPasswords(passwordKey, passwordConfirmationKey) {
    return this.strings.checkIfMatchingPasswords(
      passwordKey,
      passwordConfirmationKey
    );
  }

  parseAddressFromProfile(profile) {
    return this.strings.parseAddressFromProfile(profile);
  }

  isLastNameExist(input) {
    return this.strings.isLastNameExist(input);
  }

  /* Immage Service */
  snapImage(type) {
    return this.images.snapImage(type);
  }

  convertImageUrltoBase64(url) {
    return this.images.convertImageUrltoBase64(url);
  }

  /* Time Service */

  showDatePicker(date, mode = 'date'): Promise<any> {
    return this.times.showDatePicker(date, mode);
  }

  diffInHours(startDate, endDate) {
    return this.times.diffInHours(startDate, endDate);
  }

  formatDate(date) {
    return this.times.formatDate(date);
  }

  formatDateTime(date) {
    return this.times.formatDateTime(date);
  }

  formatDateMDYHM(date) {
    return this.times.formatDateMDYHM(date);
  }

  formatAMPM(_dt) {
    return this.times.formatAMPM(_dt);
  }

  customMDYHMformatDateMDYHM(_date) {
    return this.times.customMDYHMformatDateMDYHM(_date);
  }

  formatHoursToText(hour) {
    return this.times.formatHoursToText(hour);
  }

  formatDateMDY(date) {
    return this.times.formatDateMDY(date);
  }

  getCurrentLocationCoordinates() {
    return this.geolocations.getCurrentLocationCoordinates();
  }
  getCoordsForGeoAddress(address, _default = true) {
    return this.geolocations.getCoordsForGeoAddress(address, _default = true);
  }
}
