import { Injectable } from '@angular/core';
import localForage from 'localforage';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  // nativeStorage = localStorage;
  constructor() {
    localForage.config({
      name: 'MyApp',
    });
  }

  saveData(data) {
    return new Promise( resolve => {
      localForage.getItem('address', (err, value: any) => {
        let arr = [];
        if (value) {
          let hasEntry = false;
          for (var key in value) {
            if (key == 'entries') {
              hasEntry = true;
            }
          }

          if (hasEntry) {
            arr = value['entries'];
            let hasNot = true;
            arr.forEach((element) => {
              if (element.add == data.add) {
                hasNot = false;
              }
            });

            if (hasNot) {
              arr.push(data);
              let temp = {};
              temp['entries'] = arr;
              localForage.setItem('address', temp, (err) => {
                console.log('Entry added', temp);
                resolve(true)
              });
            }
          } else {
            arr.push(data);
            let temp = {};
            temp['entries'] = arr;
            localForage.setItem('address', temp, (err) => {
              console.log('Entry added', temp);
              resolve(true)
            });
          }
        } else {
          arr.push(data);
          let temp = {};
          temp['entries'] = arr;
          localForage.setItem('address', temp, (err) => {
            console.log('Entry added', temp);
            resolve(true)
          });
        }
      });
    })

  }

  getData(): Promise<any> {
    return localForage.getItem('address');
  }

  set(key, data): Promise<boolean> {
    return new Promise((resolve) => {
      localStorage.setItem(key, JSON.stringify(data));
      resolve(true);
    });
  }

  get(key): Promise<any> {
    return new Promise((resolve) => {
      let data = localStorage.getItem(key);
      resolve(data);
    });
  }
}
