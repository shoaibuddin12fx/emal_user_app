import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/compat/auth';
import { AngularFireDatabase } from '@angular/fire/compat/database';
import { AngularFireStorage } from '@angular/fire/compat/storage';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  constructor(
    public fireAuth: AngularFireAuth,
    public firebase: AngularFireDatabase,
    public fireStorage: AngularFireStorage
  ) { }
  getCurrentUser() { 
    return new Promise<any>(async res => {
      var user = await this.fireAuth.currentUser;
      if(user && user.uid)
        res(user);
        else res(null);
    });
    
  }

  getDatabase() {
    return this.firebase.database;
  }

  async signOut() {
    await this.fireAuth.signOut();
  }
}
