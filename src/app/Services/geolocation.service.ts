import { Injectable } from '@angular/core';
// import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { NativeGeocoder } from '@ionic-native/native-geocoder/ngx';
import { Platform } from '@ionic/angular';
import { Geolocation } from '@capacitor/geolocation';
declare var google;

@Injectable({
  providedIn: 'root',
})
export class GeolocationService {
  constructor(
    private platform: Platform,
   // private geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder
  ) { }

  getCurrentPosition() {
    return new Promise<any>((resolve,reject) => {
      Geolocation.getCurrentPosition().then(
        (res) => {
          resolve(res);
        },
        async (err) => {
          reject(err);
          // console.log(err);
          // const res = await this.getCoordsForGeoAddress('706 Timber Branch Drive, Alexandria, VA, USA')
          // resolve(res);
        }
      );
    });
  }

  getCoordsForGeoAddress(address, _default = true) {
    const self = this;
    return new Promise((resolve) => {
      const self = this;
      const geocoder = new google.maps.Geocoder();
      geocoder.geocode({ address }, (results, status) => {
        if (status === 'OK') {
          if (results[0]) {
            const loc = results[0].geometry.location;
            const lat = loc.lat();
            const lng = loc.lng();
            resolve({ lat, lng, coords: { latitude: lat, longitude: lng }});
          } else {
            resolve(null);
          }
        } else {
          console.log({ results, status });
          resolve(null);
        }
      });
    });
  }

  getCoordsViaHTML5Navigator() {
    return new Promise((resolve) => {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(
          (position) => {
            const pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            };
            resolve(pos);
          },
          () => {
            resolve({ lat: 51.5074, lng: 0.1278 });
          }
        );
      } else {
        // Browser doesn't support Geolocation
        resolve({ lat: 51.5074, lng: 0.1278 });
      }
    });
  }

  async getCurrentLocationCoordinates() {
    let user = await this.getCurrentPosition();
    return new Promise(async (resolve) => {
      Geolocation.getCurrentPosition().then(position => {
        const lt = position.coords.latitude;
        const lg = position.coords.longitude;

        resolve({ lat: lt, lng: lg });
      }).catch((reason) => {
        console.log("getCurrentPosition", reason);
      });


    });
  }

  getAddressFromCoords(lat, lng) {
    return new Promise((resolve) => {
      // if (this.platform.is('cordova')) {
      //   this.nativeGeocoder
      //     .reverseGeocode(lat, lng)
      //     .then((result) => {
      //       console.log(result);
      //       if (result[0] != undefined) {

      //         const obj = {
      //           lat,
      //           lng,
      //           add: result[0]['formatted_address']
      //         }




      //         resolve(obj);

      //       } else {
      //         resolve(null);
      //       }
      //     })
      //     .catch((error: any) => {
      //       console.log('reverseGeocode Error', error);
      //       resolve(null);
      //     });
      // } else {
      var geocoder = new google.maps.Geocoder();
      geocoder
        .geocode({ location: { lat, lng } })
        .then((response) => {
          if (response.results[0]) {

            const obj = {
              lat,
              lng,
              add: response.results[0].formatted_address
            }




            resolve(obj);
          } else {
            resolve(null);
          }
        })
        .catch((e) => {
          console.log('Geocoder failed due to: ' + e);
          resolve(null);
        });
      //   }
    });
  }
}
