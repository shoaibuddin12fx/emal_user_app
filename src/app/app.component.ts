import { Component, Injector, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController, Platform } from '@ionic/angular';
import { BasePage } from './pages/base-page/base-page';
import { ModalService } from './services/basic/modal.service';
import { EventsService } from './services/events.service';
import { FirebaseService } from './Services/firebase.service';
import { UserService } from './Services/user-service';
import { UtilityService } from './services/utility.service';
import {
  ActionPerformed,
  PushNotificationSchema,
  PushNotifications,
  Token,
} from '@capacitor/push-notifications';
import { NavService } from './services/nav.service';
import { Plugins } from '@capacitor/core';
const { LocalNotifications } = Plugins;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  user: any;
  public appPages = [
    { title: 'Home', url: '/pages/home', icon: 'home' },
    {
      title: 'Current Bookings',
      url: '/pages/currentbookings',
      icon: 'bookmarks',
    },
    { title: 'Booking History', url: '/pages/bookings', icon: 'bookmarks' },
    { title: 'Support', url: '/pages/support', icon: 'cog' },
    { title: 'Profile', url: '/pages/profile', icon: 'person' },
    { title: 'Payments', url: '/pages/payments', icon: 'cash' },
    { title: 'About Us', url: '/pages/about', icon: 'information' },
    { title: 'Logout', url: '', icon: 'exit' },
  ];

  constructor(
    public events: EventsService,
    public firebaseService: FirebaseService,
    public utility: UtilityService,
    public modals: ModalService,
    public menuCtrl: MenuController,
    public usersService: UserService,
    public router: Router,
    public nav: NavService,
    public platform: Platform,
    public zone: NgZone
  ) {
    this.events.subscribe('login_event', async () => {
      setTimeout(async () => {
        console.log('login_event RECEIVED');
        let user = await this.firebaseService.getCurrentUser();
        var ref = this.firebaseService
          .getDatabase()
          .ref('/users/' + user['uid']);
        ref.on('value', (_snapshot: any) => {
          if (_snapshot.val()) {
            var data = _snapshot.val();
            console.log(data);
            this.user = _snapshot.val();
          }
        });
      }, 1000);

      // this.user = this.users.getLocalUser();
    });
    document.addEventListener(
      'backbutton',
      (event) => {
        event.preventDefault();
        event.stopPropagation();
        const url = this.router.url;
        console.log(url);
        this.createBackRoutingLogics(url);
      },
      false
    );
  }

  async createBackRoutingLogics(url) {
    if (
      url.includes('login') ||
      url.includes('signup') ||
      url.includes('home') ||
      url.includes('splash')
    ) {
      this.utility.hideLoader();

      const isModalOpen = await this.modals.modal.getTop();
      console.log(isModalOpen);
      if (isModalOpen) {
        this.modals.dismiss({ data: 'A' });
      } else {
        this.exitApp();
      }
    } else {
      // if(this.isModalOpen){
      // }
    }
  }

  exitApp() {
    navigator['app'].exitApp();
  }

  async ngOnInit() {
    this.user = await this.firebaseService.getCurrentUser();
    console.log(this.user);
    console.log('setting menu ctrl disable');
    this.menuCtrl.enable(false, 'drawer');
    this.firebaseInit();
    //this.routerOutlet.swipeGesture = false;
  }

  firebaseInit() {
    console.log('Initializing firebase');
    this.firebaseService.fireAuth.onAuthStateChanged((user) => {
      console.log('OnFireAuthChanged', user);
      if (user) {
        // this.firebase.grantPermission();
        console.log(user);
        this.zone.run(() => {
          console.log('Page change requested');
          // this.nav.push('pages/locationloader');
        });
      }
    });

    // Request permission to use push notifications
    // iOS will prompt user and return if they granted permission or not
    // Android will just grant without prompting
    PushNotifications.requestPermissions().then((result) => {
      if (result.receive === 'granted') {
        // Register with Apple / Google to receive push via APNS/FCM
        PushNotifications.register();
      } else {
        // Show some error
      }
    });

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration', (token: Token) => {
      console.log('FCM_TOKEN', token.value);
      localStorage.setItem('FCM_TOKEN', token.value);
      // alert('Push registration success, token: ' + token.value);
    });

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError', (error: any) => {
      alert('Error on registration: ' + JSON.stringify(error));
    });

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener(
      'pushNotificationReceived',
      (notification: PushNotificationSchema) => {
        this.showNotification(notification.title, notification.body);
        //  this.utility.alerts.showAlert(notification.body, notification.title);
        //alert('Push received: ' + JSON.stringify(notification));
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener(
      'pushNotificationActionPerformed',
      (notification: ActionPerformed) => {
        alert('Push action performed: ' + JSON.stringify(notification));
      }
    );
  }

  navigate(path) {
    this.nav.push(path);
  }

  handleClicked(title) {
    if (title === 'Logout') {
      console.log('onLogout');
      this.usersService.logoutUser();
    }
  }

  async showNotification(title, body) {
    const notifs = await LocalNotifications.schedule({
      notifications: [
        {
          title,
          body,
          id: 1,
          schedule: { at: new Date(Date.now() + 1000) },
          sound: 'default',
          attachments: null,
          actionTypeId: '',
          extra: null,
        },
      ],
    });
    console.log('scheduled notifications', notifs);
  }
}
